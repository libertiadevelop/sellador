package com.bmx.srv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

public class Servicios
{
	private File 	archivoServicios 	= null;
	private boolean estado;						// Si la operacion fue exitosa es true si no false.
	private String	descripcionError;			// Si la operacion es exitosa, vacio, si no error.
	
	private HashMap <String, String> pares	= new HashMap <String, String> ( );
	
	public static enum Servicio
	{
		COMPARA_BASES(1),
		RECARGA_BASE_TIMBRES(2),
		LANZA_VALIDA_TIMBRES(3),
		HORA_VALIDA_TIMBRES(4),
		ESTADO_VALIDACION(5),
		LIMPIA_TEMPORAL(6),
		HORA_DE_LIMPIEZA(7),
		HORA_ACTUALIZACION_PROVEEDORES(8),
		FORZA_ACTUALIZACION_PROVEEDORES(9),
		FORZA_ACTUALIZACION_COMPRADORES(10),
		FORZA_ACTUALIZA_MARCAS(11),
		HORA_ACTUALIZA_MARCAS(12),
		FORZA_ESTADISTICAS(13),
		HORA_CARGA_ESTADISTICAS(14),
		A�O_PARA_CARGA_ESTADISTICAS(15),
		FORZA_ACTUALIZACION_WEB(16)
		;
		
		private int iServicio;
		
		private Servicio( int s ){
			iServicio = s;
		}
		
		public String getServicio ()
		{
			String servicioDevuelto = "";
			switch ( iServicio ) {
				case 1:
					servicioDevuelto = "compara_bases";
					break;
				case 2:
					servicioDevuelto = "recarga_base_timbres";
					break;
				case 3:
					servicioDevuelto = "forza_valida_timbres";
					break;
				case 4:
					servicioDevuelto = "hora_valida_timbres";
					break;
				case 5:
					servicioDevuelto = "validacion_ejecutada";
					break;
				case 6:
					servicioDevuelto = "limpia_temporal";
					break;
				case 7:
					servicioDevuelto = "hora_de_limpieza";
					break;
				case 8:
					servicioDevuelto = "hora_actualiza_proveedores";
					break;
				case 9:
					servicioDevuelto = "forza_subida_de_proveedores";
					break;
				case 10:
					servicioDevuelto = "forza_subida_de_compradores";
					break;
				case 11:
					servicioDevuelto = "forza_actualiza_marcas";
					break;
				case 12:
					servicioDevuelto = "hora_actualiza_marcas";
					break;
				case 13:
					servicioDevuelto = "forza_estadisticas";
					break;
				case 14:
					servicioDevuelto = "hora_de_estadisticas";
					break;
				case 15:
					servicioDevuelto = "a�o_para_estadisticas";
					break;
				case 16:
					servicioDevuelto = "forza_actualizacion_web";
					break;
			}
			
			return servicioDevuelto;
		}
		
	}
	
	public boolean getEstado() {
		return estado;
	}
	
	public String getDescripcionError(){
		return descripcionError;
	}
	
	public Servicios ( String archivo )
	{
		archivoServicios = new File ( archivo );
		
		if ( archivoServicios.exists ( ) ){
			setEstado ( true, "" );
		} else {
			setEstado ( false, "El archivo " + archivo + " no existe." );
		}
		
		if (estado){
			getDatosFromFile ( );
		}
		
		
	}
	
	public boolean getBooleanServicio( Servicio servicio )
	{

		if ( !estado ){
			return false;
		}
		
		boolean resultado = false;
		
		// Va a leer del archivo de configuracion 1 = TRUE, 0 = FALSE
		
		resultado = getValorBooleanoDeCadena ( pares.get ( servicio.getServicio ( ) ) );
		
		return resultado;
	}
	
	public String getStringService( Servicio servicio )
	{
		if ( !estado ) {
			return "";
		}
		
		String resultado = "";
		
		resultado = pares.get ( servicio.getServicio ( ) );
		
		return resultado;
		
	}
	
	public void setBooleanServicio ( Servicio servicio, boolean valor )
	{
		eliminaArchivo ( );
		pares.put (  servicio.getServicio ( ), setValorBooleanoDeCadena ( valor ) );
		
		setDatosToFile ( );
	}
	
	private void eliminaArchivo()
	{
		if ( archivoServicios.exists ( ) ){
			try {
				archivoServicios.delete ( );
				setEstado ( true, "" );
			} catch ( Exception be ){
				setEstado ( false, "No se pudo borrar el archivo, " + archivoServicios.getName ( ) + be.toString ( ) );
			}
		}

	}
	
	private void setDatosToFile()
	{
		PrintWriter p = null;
		try {
			p = new PrintWriter ( archivoServicios );
			
			for ( String llave : pares.keySet ( ) ) {
				String valor = pares.get ( llave );
				p.println ( llave + "=" + valor );
			}
			
			p.close ( );
			
		} catch ( Exception we ){
			setEstado ( false, "No se pudo guardar informacion " + we.toString ( ) );
			return;
		} finally {
			if ( p != null ){
				p.close ( );
			}
		}
	}
	
	private void getDatosFromFile()
	{
		if ( !estado ) {
			return;
		}
	
		pares.clear ( );
			
			
		BufferedReader br = null;
		
		try {
			
			 br = new BufferedReader( new FileReader( archivoServicios ) );
			
			String s;
			
			while ( (s = br.readLine()) != null  ){
				if ( s.trim().length() > 0 ) {
					
					// Valida que no sea un comentario la linea a cargar, si es comentario, la linea empieza con //
					if ( !s.trim().substring ( 0,2 ).equals ( "//" ) ) {
					
						String llave = s.substring(0, s.indexOf("=") );
						String valor = s.substring(s.indexOf("=") + 1 );
						
						pares.put(llave, valor);
					}
		
				}
			}
			br.close();
			
			setEstado ( true, "" );
			
		} catch ( Exception lcee ){
			setEstado ( false, "No se pudo leer el archivo " + lcee.toString ( ) );
		} finally {
			if ( br != null ) {
				try {
					br.close();
				} catch (IOException e) { 
					descripcionError = e.toString ( );
				}
			}
		}
	}
	
	private boolean getValorBooleanoDeCadena ( String cadena )
	{
		boolean resultado = false;
		
		if ( cadena.equals (  "1" )  ){
			resultado = true;
		} else {
			resultado = false;
		}
		
		return resultado;
	}
	
	private String setValorBooleanoDeCadena ( boolean valorBooleano )
	{
		String valor = "";
		if ( valorBooleano ){
			valor = "1";
		} else {
			valor = "0";
		}
		return valor;
	}
	
	private void setEstado ( boolean resultadoOperacion, String Descripcion )
	{
		estado  			= resultadoOperacion;
		descripcionError 	= Descripcion;
	}
	

}

