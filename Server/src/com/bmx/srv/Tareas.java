package com.bmx.srv;

import static com.bmx.srv.Utilidades.MuestraInformacion;

import java.util.ArrayList;
import java.util.HashMap;

import com.bmx.srv.Utilidades.dondeSeMuestra;
import com.bmx.srv.Utilidades.nivel;
import com.bmx.srv.Utilidades.tipoDeDato;

public class Tareas
{
	static enum Tarea
	{
		ACTUALIZA_MARCAS(1)
		;
		int t;
		private Tarea( int ta ){
			t = ta;
		}
		public String getDescripcionTarea( String info )
		{
			String resultado = "";
			
			switch ( t ) {
				case 1:
					resultado = "Actualiza marcas";
					break;

				default:
					break;
			}
			
			return resultado; 
		}
	}

	public static void actualizaMarcas ( )
	{
		String Cliente = "Actualiza Descripcion de Marcas";
		
		String sentencia = "";
		
		HashMap <String, ArrayList<String>> Marcas 		= new HashMap <String, ArrayList<String>> ( );
		HashMap <String, ArrayList<String>> Productos 	= new HashMap <String, ArrayList<String>> ( );
		
		HashMap <String, ArrayList<String>> diferencias	= new HashMap <String, ArrayList<String>> ( );
		
		ArrayList<String> llaves = new ArrayList <String> ( );

		// Carga el catalogo de marcas
		MuestraInformacion ( "Iniciando actualizacion de marcas", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		// Carga el catalogo de productos vs marcas
		ConsultaODBC c = new ConsultaODBC( Conecciones.getERPCon ( ) );
		
		sentencia 	= "SELECT TB15_MARCA, TB15_DESC FROM MDGDIC15";
		
		Marcas 		= c.ConsultaVariasColumnasODBC ( sentencia );
		
		c 			= new ConsultaODBC( Conecciones.getERPCon ( ) );
		
		sentencia 	= "SELECT DISTINCT AR_LINE, AR_NOMMOD FROM MDGINV01";
		
		Productos 	= c.ConsultaVariasColumnasODBC ( sentencia );
		
		// Compara la informacion
		
		llaves.add ( "1" );
		
		diferencias = ConsultaODBC.validaRegistroEnCatalogo ( Productos, ConsultaODBC.resultadoACatalogo ( Marcas, llaves, 16 ) );
		
		MuestraInformacion ( "Se encontraron: " + diferencias.size ( ) + " marcas con difernecias.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		// Actualiza
		
		sentencia = "UPDATE MDGINV01 SET AR_NOMMOD=? WHERE AR_LINE=?";
		
		c = new ConsultaODBC( Conecciones.getERPCon ( ) );
		
		HashMap<Integer, tipoDeDato> datoDeColumna = new HashMap <Integer, tipoDeDato> ( );
		
		datoDeColumna.put ( 1, tipoDeDato.CADENA );
		datoDeColumna.put ( 2, tipoDeDato.ENTERO );
		
		c.updateRegistros ( sentencia, diferencias, datoDeColumna );
		
		MuestraInformacion ( "Finalizando actualizacion de marcas", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
	}
	
	
	
	
}


