package com.bmx.srv;
import java.io.File;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.*;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.bmx.uti.*;


public class Correo 
{
	//region VARIABLES INTERNAS
	private String 				fromAddress			= "";						// Almacena la direcci�n de correo origen
	private String 				HostSMTP			= "";						// Almacena el hostname o address del servidor de correo.
	private String				PortSMTP			= "";						// Almacena el puerto del servidor SMTP
	private ArrayList<String> 	toAddress			= new ArrayList<String>();	// Almacena las direcciones de correo destino
	private ArrayList<String>	CC					= new ArrayList<String>();	// Almacena el listado de direcciones a copias.
	private ArrayList<String>   BCC					= new ArrayList<String>();	// Almacena el listado de direcciones de copia oculta.
	private ArrayList<String>	ATT					= new ArrayList<String>();	// Almacena el listado de attachments a enviar.
	
	private Boolean				resultado			= true;
	
	private StringBuilder		log					= new StringBuilder();
	private Boolean				debug				= false;
	private String				error				= "";
	private String				version				= "0.0.0.0";
	//endregion 
	
	//region CONSTRUCTORES
	public Correo ( String from, String to )
	{
		//region VALIDA FROM
		// El from puede recivir n cantidad de direcciones, todas separadas por comas.
		if ( from.length() == 0 ){
			resultado 	= false;
			error		= "El campo De, esta vaci�.";
			return;
		}
		
		if ( !validaDireccion( from ) ){
			resultado 	= false;
			error		= "La direccion De: " + from + " es invalida";
			return;
		}
		
		fromAddress = from;
		//endregion
		//region VALIDA TO
		if ( to.length() == 0 ){
			resultado 	= false;
			error		= "El campo Para, esta vaci�,";
			return;
		}
		
		// Descompone to en sus elementos
		for ( String direccion : descomponeCadenaAListado( to ) ) {
			if ( validaDireccion( direccion )){
				toAddress.add( direccion );
			} else {
				error = "La direccion del campo to: " + direccion + " no es valida.";
			}
		}
		
		if ( toAddress.size() < 1 ){
			resultado 	= false;
			error		= "No hay direcciones para enviar";
		}
		
		
		//endregion
	
	}
	//endregion
	
	//region PROPIEDADES
	public String getVersion()
	{
		return version;
	}
	public Boolean getResultado ()
	{
		return resultado;
	}
	public ArrayList<String> getTo()
	{
		return toAddress;
	}
	//region SETTERS TO
	public void setTo( String toString )
	{
		toAddress = descomponeCadenaAListado( toString );
	}
	public void setTo ( ArrayList<String> toArreglo )
	{
		toAddress = toArreglo;
	}
	public void setTo ( String[] toString )
	{
		toAddress = descomponeCadenaAListado(toString);
	}
	//endregion
	public ArrayList<String> getCC()
	{
		return CC;
	}
	//region SETTERS CC
		public void setCC ( String toString )
		{
			CC = validaArrayMail( descomponeCadenaAListado( toString ) );
		}
		public void setCC ( ArrayList<String> toArreglo )
		{
			CC = validaArrayMail( toArreglo );
		}
		public void setCC ( String[] toString )
		{
			CC = validaArrayMail( descomponeCadenaAListado(toString) );
		}
	//endregion
	
	
	public ArrayList<String> getBCC()
	{
		return BCC;
	}
	
	//region SETTERS BCC
		public void setBCC ( String toString )
		{
			BCC = validaArrayMail( descomponeCadenaAListado( toString ) );
		}
		public void setBCC ( ArrayList<String> toArreglo )
		{
			BCC = validaArrayMail( toArreglo );
		}
		public void setBCC ( String[] toString )
		{
			BCC = validaArrayMail( descomponeCadenaAListado(toString) );
		}
		//endregion
	
	public ArrayList<String> getATTACH()
	{
		return ATT;
	}
	
	//region SETTERS ATTACH
		public void setAttach( String toString )
		{
			validaAttachments( descomponeCadenaAListado( toString ) );
		}
		public void setAttach ( ArrayList<String> toArreglo )
		{
			validaAttachments( toArreglo );
		}
		public void setAttach ( String[] toString )
		{
			validaAttachments( descomponeCadenaAListado(toString) );
		}
	//endregion
	
	public String getError()
	{
		return error;
	}
	
	public String getLog()
	{
		return log.toString();
	}
	
	public void setSMTPHost( String host )
	{
		if ( host.trim().length() == 0 ){
			resultado 	= false;
			error 		= "El host esta vaci�";
			return;
		}
		
		if ( host == "localhost" || host == "127.0.0.1" || host == "192.168.3.7" ){
			HostSMTP = host;
			resultado = true;
			return;
		}
		
		if ( !validaWebAddress( host ) ){
			resultado 	= false;
			error		= "El host tiene una direcci�n invalida";
		}
		
		if ( !validaIPWebAddress( host )){
			resultado 	= false;
			error		= "El host no resuelve";
			return;
		}
		
		HostSMTP	= host;
		
	}
	
	public void setSMPTPort( String puerto )
	{
		if ( puerto.length() == 0 ){
			resultado 	= false;
			error		= "El puerto esta vacio";
			return;
		}
		
		if ( !validaQuePuertoEsNumero(puerto) ){
			resultado	= false;
			error		= "El puerto debe ser numerico";
			return;
		}
		
		int NumPuertoSMTP = Integer.valueOf( puerto );
	
		if ( NumPuertoSMTP <= 0  ){
			resultado	= false;
			error		= "El puerto esta fuera de rango";
			return;
		}
		if ( NumPuertoSMTP >= 65536 ){
			resultado	= false;
			error		= "El puerto esta fuera de rango";
			return;
		}
		
		PortSMTP = puerto;
		
	}
	
	public void setDebug ( Boolean Debug )
	{
		debug = Debug;
	}
	
	//endregion
	
	//region MEDOTOS PUBLICOS
	
	public boolean enviaCorreo ( String Subject, String Cuerpo )
	{
		boolean resultadoDeEnvio = false;
		
		// Valida que exista un Host y Puerto antes de enviar
		
		if ( HostSMTP.length() == 0 ){
			error 				= "El host esta vacio, no se puede continuar.";
			resultado			= false;
			return false;
		}
		if ( PortSMTP.length() == 0 ){
			error 				= "El puerto esta vacio, no se puede continuar.";
			resultado			= false;
			return false;
		}
		
		resultado				= true;
		
		// Intenta enviar el mail.
		
		Properties	prop	= new Properties();
		prop.put( "mail.smtp.host", HostSMTP );
		prop.put( "mail.smtp.port", PortSMTP );
		
		try{
			resultadoDeEnvio = false;
			
			Session session		= Session.getInstance( prop, null );			
			session.setDebug( debug );
			
			MimeMessage	mensaje = new MimeMessage( session );
			
			mensaje.setFrom( new InternetAddress( fromAddress ));
			
			//mensaje.setRecipient( RecipientType.TO , new InternetAddress( "andres.garcia@barmex.com.mx" ) );
			
			for ( String direccion : toAddress ) {
				mensaje.setRecipient( RecipientType.TO , new InternetAddress( direccion ) );
			}
			
			if ( CC.size() > 0 ){
				for ( String address : CC ){
					mensaje.setRecipient( RecipientType.CC , new InternetAddress( address ) );
				}
			}
			
			if ( BCC.size() > 0 ){
				for ( String address : BCC ){
					mensaje.setRecipient( RecipientType.BCC , new InternetAddress( address ) );
				}
			}
			
			if ( Subject.length() == 0 ){
				mensaje.setSubject( "Correo de Barmex. " );
			} else {
				mensaje.setSubject( Subject );
			}
			
			Multipart 		multipart 				= new MimeMultipart();
			MimeBodyPart	cuerpoDelMensaje		= new MimeBodyPart();
			
			if ( Cuerpo.length() == 0 ) {
				cuerpoDelMensaje.setText( "Cuerpo del mensaje" );
			} else {
				cuerpoDelMensaje.setText( Cuerpo );
			}
			
			multipart.addBodyPart( cuerpoDelMensaje );
			
			// Agrega attachments si es que existen.
			
			if ( ATT.size() > 0 ){
				
				// Procede a cargar los attachments
				for (String archivo : ATT) {
					
					cuerpoDelMensaje = new MimeBodyPart();
					
					File a = new File( archivo );
					
					DataSource source = new FileDataSource( a );
					cuerpoDelMensaje.setDataHandler( new DataHandler( source ) );
					cuerpoDelMensaje.setFileName( a.getName( ).replace( "_S.xml", ".xml" ) );
					multipart.addBodyPart( cuerpoDelMensaje );
				}
			}
			
			mensaje.setContent( multipart );
			
			Transport.send( mensaje );
			
			resultadoDeEnvio 	= true;
			resultado			= resultadoDeEnvio;
			
		} catch ( Exception me ){
			resultadoDeEnvio 	= false;
			error				= me.toString();
		}

		return resultadoDeEnvio;
	}
	public boolean enviaCita ( String Subject, String Cuerpo, String inicio, String fin )
	{
		boolean resultadoDeEnvio = false;
		// Valida que exista un Host y Puerto antes de enviar
		
				if ( HostSMTP.length() == 0 ){
					error 				= "El host esta vacio, no se puede continuar.";
					resultado			= false;
					return false;
				}
				if ( PortSMTP.length() == 0 ){
					error 				= "El puerto esta vacio, no se puede continuar.";
					resultado			= false;
					return false;
				}
				
				resultado				= true;
				
				// Intenta enviar el mail.
				
				Properties	prop	= new Properties();
				prop.put( "mail.smtp.host", HostSMTP );
				prop.put( "mail.smtp.port", PortSMTP );
				
				try{
					resultadoDeEnvio = false;
					
					Session session		= Session.getInstance( prop, null );			
					session.setDebug( debug );
					
					MimeMessage	mensaje = new MimeMessage( session );
					
					mensaje.setFrom( new InternetAddress( fromAddress ));
					
					for ( String direccion : toAddress ) {
						mensaje.setRecipient( RecipientType.TO , new InternetAddress( direccion ) );
					}
					
					if ( CC.size() > 0 ){
						for ( String address : CC ){
							mensaje.setRecipient( RecipientType.CC , new InternetAddress( address ) );
						}
					}
					
					if ( BCC.size() > 0 ){
						for ( String address : BCC ){
							mensaje.setRecipient( RecipientType.BCC , new InternetAddress( address ) );
						}
					}
					
					if ( Subject.length() == 0 ){
						mensaje.setSubject( "Correo de Barmex. " );
					} else {
						mensaje.setSubject( Subject );
					}
					
					Multipart 		multipart 				= new MimeMultipart();
					MimeBodyPart	cuerpoDelMensaje		= new MimeBodyPart();
					
					if ( Cuerpo.length() == 0 ) {
						cuerpoDelMensaje.setText( "Cuerpo del mensaje" );
					} else {
						cuerpoDelMensaje.setText( Cuerpo );
					}
					
					multipart.addBodyPart( cuerpoDelMensaje );
					
					// Agrega attachments si es que existen.
					
					if ( ATT.size() > 0 ){
						cuerpoDelMensaje = new MimeBodyPart();
						// Procede a cargar los attachments
						for (String archivo : ATT) {
							DataSource source = new FileDataSource( archivo );
							cuerpoDelMensaje.setDataHandler( new DataHandler( source ) );
							cuerpoDelMensaje.setFileName( archivo );
							multipart.addBodyPart( cuerpoDelMensaje );
						}
					}
					
					IcalGenerador generador = new IcalGenerador( Subject , Cuerpo );
					DataSource source = new FileDataSource( generador.GeneraEvento( inicio, fin ) );
					cuerpoDelMensaje = new MimeBodyPart();
					
					cuerpoDelMensaje.setDataHandler( new DataHandler( source ) );
					cuerpoDelMensaje.setFileName( "evento.ics" );
					multipart.addBodyPart( cuerpoDelMensaje );
					
					mensaje.setContent( multipart );
					
					Transport.send( mensaje );
					
					resultadoDeEnvio 	= true;
					resultado			= resultadoDeEnvio;
					
				} catch ( Exception me ){
					resultadoDeEnvio 	= false;
					error				= me.toString();
				}
		
		return resultadoDeEnvio;
		
	}
	//endregion
	
	//region METODOS PRIVADOS
	private ArrayList<String> descomponeCadenaAListado ( String arreglo )
	{
		if ( arreglo.length() == 0 ){
			return null;
		}
		
		ArrayList<String> Resultado = new ArrayList<String>();
		
		String[] cadenas = arreglo.split( "," );
				
		for (String cadena : cadenas) {
			Resultado.add( cadena.trim() );
		}
		
		return Resultado;
	}
	private ArrayList<String> descomponeCadenaAListado( String[] arreglo )
	{
		if ( arreglo.length == 0 ){
			return null;
		}
		
		ArrayList<String> Resultado = new ArrayList<String>();
		
		for (String string : arreglo) {
			Resultado.add( string );
		}
		
		return Resultado;
	}
	private boolean validaDireccion ( String direccionDeCorreo )
	{
		boolean resultado	= false;
		Pattern pattern 	= Pattern.compile( "^[-a-zA-Z0-9][-._a-zA-Z0-9]*@[-.a-zA-Z0-9]+(\\.[-.a-zA-Z0-9]+)*\\.(com|edu|info|gov|int|mil|net|org|biz|name|museum|coop|aero|pro|tv|[a-zA-Z]{2})$" );
		Matcher match		= pattern.matcher(direccionDeCorreo);
		
		resultado = match.matches();
		
		return resultado;
	}
	private boolean validaWebAddress ( String direccion )
	{
		boolean resultado		= false;
		
		Pattern pattern		= Pattern.compile( "(@)?(href=')?(HREF=')?(HREF=\")?(href=\")?(http://)?[a-zA-Z_0-9\\-]+(\\.\\w[a-zA-Z_0-9\\-]+)+(/[#&\\n\\-=?\\+\\%/\\.\\w]+)?" );
		
		Matcher m = pattern.matcher( direccion );
		
		resultado = m.matches();
		
		return resultado;
	}
	private boolean validaIPWebAddress ( String direccion )
	{
		boolean resultado = true;
		try{
			@SuppressWarnings("unused")
			InetAddress addr = InetAddress.getByName(direccion);
		} catch ( Exception e ){
			error = e.toString();
			resultado = false;
			return false;
		}
		
		return resultado;
	}
	private boolean validaQuePuertoEsNumero ( String puerto )
	{
		boolean	resultado	= false;
		Pattern pattern 	= Pattern.compile("[0-9]+");
		
		Matcher	m			= pattern.matcher(puerto);
		
		resultado 			= m.matches();
		
		return resultado;
		
	}
	private ArrayList<String> validaArrayMail ( ArrayList<String> Mailes )
	{
		ArrayList<String> Resultado = new ArrayList<String>();
		
		for (String mail : Mailes) {
			if ( validaDireccion(mail) ){
				Resultado.add( mail );
			} else {
				Resultado.clear();
				return Resultado;
			}
		}
		
		return Resultado;
	}
	private void validaAttachments ( ArrayList<String> Files )
	{	
		for (String archivo : Files) {
			File validaArchivo = new File(archivo);
			if ( validaArchivo.exists() ){
				ATT.add( archivo );
			}
		}	
	}
	//endregion
}
