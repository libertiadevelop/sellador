package com.bmx.srv;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.sql.Connection;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.ResourceBundle;

import com.ibm.icu.math.BigDecimal;

//TODO: USAR EL LOGGER DE JAVA

public class Utilidades 
{
	public static enum sucursales {
		MEXICO			( "01", "MEX" ),
		MONTERREY 		( "02", "MTY" ),
		GUADALAJARA		( "03", "GDA" ),
		HERMOSILLO		( "04", "HMO" ),
		MEXICALI		( "05", "MXI" ),
		PEMEX			( "14", "PX" );
		
		
		private final String clave;
		private final String abreviacion;
		private sucursales ( String clave, String abreviacion )
		{
			this.clave 			= clave;
			this.abreviacion 	= abreviacion;
		}
		public String getClave() {
			return clave;
		}
		public String getAbreviacion() {
			return abreviacion;
		}
		
		public static sucursales getSucursal( String clave )
		{
			sucursales resultado = null;
			for( sucursales s : sucursales.values ( ) ){
				if ( s.clave.equals ( clave ) ){
					resultado = s;
				}
			}
			
			return resultado;
		}

	}
	public static enum nivel {
		ERROR,
		ALARMA,
		INFO,
		CONFIG,
		TABULADOR
	}
	public static enum dondeSeMuestra {
		CONSOLA,
		ARCHIVO,
		AMBOS
	}
	public static enum movimiento{
		EN_COLA,					//0
		INICIO_BLOB_REMOTO,			//1
		FIN_BLOB_REMOTO,			//2
		INICIO_BLOB_LOCAL,			//3
		FIN_BLOB_LOCAL,				//4
		INICIO_MAIL,				//5
		FIN_MAIL,					//6
		INICIO_MARCA,				//7
		FIN_MARCA,					//8
		INICIO_DUMMY,				//9
		FIN_DUMMY,					//10
		INICIO_FACTURACION,			//11
		FIN_FACTURACION,			//12
		INICIO_CANCELACION,			//13
		FIN_CANCELACION				//14
	}
	public static enum estado{
		NO_APLICA,					//0
		INICIO,
		CORRECTO,
		INCORRECTO
	}
	public static enum documento{
		NO_APLICA,					//0
		ARCHIVO_PDF,
		ARCHIVO_XML
	}
	public static enum tipoDeDato{
		CADENA, ENTERO, DOBLE, BLOB;
	}
	public static enum tipoDeDocumento	{
		
		FACTURA				( 1, "FAC" ),
		NOTA_DE_CARG0 		( 2, "CAR" ),
		NOTA_DE_CREDITO_B	( 3, "CRE" ),
		DEVOLUCION			( 4, "DEV" ),
		NOTA_DE_CREDITO_PP	( 5, "CRE" );
		
		private final int clave;
		private final String abreviacion;
		private tipoDeDocumento ( int clave, String abreviacion )
		{
			this.clave 			= clave;
			this.abreviacion 	= abreviacion;
		}
		public int getClave() {
			return clave;
		}
		public String getAbreviacion() {
			return abreviacion;
		}
		public String getDescripcion() {
			String descripcion = "";
			switch ( clave ) {
				case 1:
					descripcion = "FACTURA";
					break;
				case 2:
					descripcion = "NOTA DE CARGO";
					break;
				case 3:
					descripcion = "NOTA DE CREDITO POR BONIFICACION";
					break;
				case 4:
					descripcion = "DEVOLUCION";
					break;
				case 5:
					descripcion = "NOTA DE CREDITO POR PRONTO PAGO";
					break;
			}
			return descripcion;
		}
	
		public static tipoDeDocumento getDocumento( int clave )
		{
			tipoDeDocumento resultado = null;
			for( tipoDeDocumento d : tipoDeDocumento.values ( ) ){
				if ( d.clave == clave ){
					resultado = d;
				}
			}

			return resultado;
		}
	}
	public static enum componentesDocumentoFiscal{
		CLIENTE,FOLIO,SERIE;
	}
	public static enum basesDeAuditor{
		TIMBRES(1);	
		private final int t;
		private basesDeAuditor( int v )
		{
			t = v;
		}
		
		public String getNameBaseAuditor()
		{
			String resultado = "";
			switch ( t ) {
				case 1:
					resultado = "BARAUD01";
					break;
			}
			return resultado;
		}
		public String getNameAuditor()
		{
			String resultado = "";
			switch ( t ) {
				case 1:
					resultado = "TIMBRES";
					break;
			}
			return resultado;
		}
	}
	public static boolean CopiaArchivos( String Destino, String archivo )
	{
		boolean resultadoCopiaArchivos = false;
		
		InputStream 	iOrigen = null;
		OutputStream	iDestino = null;
		
		try{
			
			File fOrigen 	= new File( archivo );
			File fDestino 	= new File( Destino ); 
			
			if ( fDestino.exists() ) fDestino.delete();
			
			iOrigen 	= new FileInputStream( fOrigen );
			iDestino	= new FileOutputStream( fDestino );
			
			byte[] buffer = new byte[1024];
			
			int length;
			
			while ( ( length = iOrigen.read( buffer ) )  > 0 ){
				iDestino.write( buffer, 0, length );
			}
			
			iOrigen.close();
			iDestino.close();
						
			resultadoCopiaArchivos = true;
		} catch ( Exception e ){
			resultadoCopiaArchivos = false;
		} finally {
			try {
				if ( iOrigen != null ) iOrigen.close();
				if ( iDestino != null ) iDestino.close();
			} catch ( Exception e ){
				resultadoCopiaArchivos = false;
			}
		}
		
		return resultadoCopiaArchivos;	
	}
	public static String RecuperaRecurso ( String nombreDeRecurso )
	{
		String valor 	= "";
		
		ResourceBundle recurso = ResourceBundle.getBundle( "recursos" );
		valor  = recurso.getString( nombreDeRecurso );
	
		return valor;
	}
	
	public static String RecuperaRecursoDinamico ( String nombreDeRecurso, String propiedad )
	{
		//TODO POR IMPLEMENTAR
		
		String		valor 	= "";
		Properties 	p 		= new Properties( );
		
		File 	jarPath 		= new File( Utilidades.class.getProtectionDomain( ).getCodeSource( ).getLocation( ).getPath( ) );
		String	propertiesPath	= jarPath.getParentFile( ).getAbsolutePath( ) ;
		try {
			p.load( new FileInputStream( propertiesPath + File.separator + nombreDeRecurso + ".properties" ) );
			valor = p.getProperty( propiedad );
		} catch ( FileNotFoundException e ) {
			e.printStackTrace();
		} catch ( IOException e ) {
			e.printStackTrace();
		}
		
		return valor;
	}
	
	public static void MuestraInformacion( ArrayList<String> Mensaje, String Cliente, nivel Nivel, dondeSeMuestra mostrar )
	{
		boolean	primerMensaje	= true;
		
		for (String mensaje : Mensaje) {
			if ( primerMensaje ){
				MuestraInformacion( mensaje, Cliente, Nivel, mostrar );
				primerMensaje = false;
			} else {
				MuestraInformacion(mensaje, Cliente, nivel.TABULADOR, mostrar);
			}
		}
	}
	public static void MuestraInformacion( String Mensaje, String Cliente, nivel Nivel, dondeSeMuestra mostrar)
	{	
		DateFormat	formato				= new SimpleDateFormat( "dd/MM/yy HH:mm:ss" );
		Date		fecha				= new Date();
		String		marcaDeFecha		= formato.format( fecha );
		String		cadenaFormateada	= "";
		String 		separador			= " - ";
		String		tabulador			= 	"                 ";
		
		if ( Cliente.trim().length() == 0 ) {
			Cliente	= "Server";
		}
		
		switch ( Nivel ) {
		case ALARMA:
			cadenaFormateada = marcaDeFecha + separador + Cliente + separador + "alm: ";
			break;
		case ERROR:
			cadenaFormateada = marcaDeFecha + separador + Cliente + separador + "err: ";
			break;
		case INFO:
			cadenaFormateada = marcaDeFecha + separador + Cliente + separador + "inf: ";
			break;
		case CONFIG:
			cadenaFormateada = marcaDeFecha + separador + Cliente + separador + "cnf: ";
			break;
		case TABULADOR:
			cadenaFormateada = tabulador + separador + Cliente + separador + "     ";
			break;
		default:
			cadenaFormateada = tabulador + separador + Cliente + separador + "     ";
			break;
		}
		
		cadenaFormateada = cadenaFormateada + Mensaje;
		
		switch ( mostrar ) {
		case CONSOLA:
			System.out.println( cadenaFormateada );
			break;
		case ARCHIVO:
			EscribeAArchivo( cadenaFormateada );
			break;
		case AMBOS:
			System.out.println( cadenaFormateada );
			EscribeAArchivo( cadenaFormateada );
			break;
		default:
			System.out.println( cadenaFormateada );
			EscribeAArchivo( cadenaFormateada );
			break;
		}
			
	}
	public static void GuardaEstado ( String factura, movimiento m, estado e, documento d, String Nota, String NombreDeDocumento )
	{
	
		int Movimiento 	= 0;
		int Estado		= 0;
		int Documento	= 0;
		
		String nota					= Nota;
		String nombreDeDocumento 	= NombreDeDocumento;
		String Cliente				= "Guarda MySQL";
	
		
		switch ( m ) {
			case EN_COLA:
				Movimiento = 0;
				break;
			case INICIO_BLOB_REMOTO:
				Movimiento = 1;
				break;
			case FIN_BLOB_REMOTO:
				Movimiento = 2;
				break;
			case INICIO_BLOB_LOCAL:
				Movimiento = 3;
				break;
			case FIN_BLOB_LOCAL:
				Movimiento = 4;
				break;
			case INICIO_MAIL:
				Movimiento = 5;
				break;
			case FIN_MAIL:
				Movimiento = 6;
				break;
			case INICIO_MARCA:
				Movimiento = 7;
				break;
			case FIN_MARCA:
				Movimiento = 8;
				break;
			case INICIO_DUMMY:
				Movimiento = 9;
				break;
			case FIN_DUMMY:
				Movimiento = 10;
				break;
			case INICIO_FACTURACION:
				Movimiento = 11;
				break;
			case FIN_FACTURACION:
				Movimiento = 12;
				break;
			case INICIO_CANCELACION:
				Movimiento = 13;
				break;
			case FIN_CANCELACION:
				Movimiento = 14;
				break;
		}
		
		switch ( e ) {
			case NO_APLICA:
				Estado = 0;
				break;
			case INICIO:
				Estado = 1;
				break;
			case CORRECTO:
				Estado = 2;
				break;
			case INCORRECTO:
				Estado = 3;
				break;
		}
		
		switch ( d ) {
			case NO_APLICA:
				Documento = 0;
				break;
			case ARCHIVO_PDF:
				Documento = 1;
				break;
			case ARCHIVO_XML:
				Documento = 2;
				break;
		}
		
		DateFormat	formato				= new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
		Date		fecha				= new Date();
		String		marcaDeFecha		= formato.format( fecha );
		
		String ConsultaSql =	"INSERT " +
								"INTO 			control ( Factura, Movimiento, Estado, " +
														"tipoDeDocumento, Fecha, Nota, " +
														"nombreDeDocumento ) " +
								"VALUES (";
		
		String datos = 								"'" + factura + "'," + Movimiento + "," + Estado + "," +
													    	Documento + ",'" + marcaDeFecha + "','" + nota + "'," +
													    	"'" + nombreDeDocumento + "')";
		ConsultaSql = ConsultaSql + datos;
		Connection 	con = null;
		Statement 	st 	= null;
		
		
		try{
			con = Conecciones.getMySQLLocalFacturas( );
			st = con.prepareStatement( ConsultaSql );
			
			int resultado =  st.executeUpdate( ConsultaSql );
			
			if ( resultado > 0 ){
				MuestraInformacion( "Se guardo el estado del documento: " + factura, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			} else {
				MuestraInformacion( "No se guardo el estado del documento: " + factura, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			}
			
		} catch ( Exception ec ){
			ArrayList<String> error = new ArrayList<String>( );
			error.add( "No se pudo guardar el estado del documento" );
			error.add( ec.toString( ) );
			MuestraInformacion( error, Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
			
		}
		
		
		
		
		
		
	}
	public static HashMap<String, String> RecuperaProcesos()
	{
		String Cliente = "Recupera Cola";
		HashMap<String, ArrayList<String>> Resultado 	= new HashMap<String, ArrayList<String>>( );
		HashMap<String, String> ProcesosPendientes		= new HashMap<String, String>( );
		
		String ConsultaSql =	"SELECT			* " +
								"FROM			documentosencola";
		
		ConsultaODBC consulta = new ConsultaODBC( Conecciones.getMySQLLocalFacturas( ) );
		
		Resultado = consulta.ConsultaVariasColumnasMySQL( ConsultaSql );
		
		for ( String llave : Resultado.keySet( ) ) {
			ArrayList<String> datos = Resultado.get( llave );
			
			ProcesosPendientes.put( datos.get( 0 ), datos.get( 1 ) );
			
		}
		
		MuestraInformacion( "Se recuperaron: " + ProcesosPendientes.size( ) + " pendientes por procesar.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS  );
		
		return ProcesosPendientes;
	}
	
	public static boolean EliminaDocumentoDeCola( String Documento )
	{
		boolean resultado = false;
		
		String Sentencia = 	"UPDATE 			control " +
							"SET				nombreDeDocumento = '" + Documento + "' " +
							"WHERE				Factura = '" + Documento + "' " +
									"AND		Movimiento = 0 " +
									"AND		Estado = 0 " +
									"AND		tipoDeDocumento = 0 ";
		
		ConsultaODBC consulta = new ConsultaODBC( Conecciones.getMySQLLocalFacturas( ) );
		
		resultado = consulta.ActualizaRegistroMySQL( Sentencia );
		
		return resultado;
	}
	
	private static void EscribeAArchivo( String Mensaje )
	{
		
		DateFormat	formato				= new SimpleDateFormat( "dd-MM-yy" );
		Date		fecha				= new Date();
		String		nombreDeArchivo		= formato.format( fecha );
		
		nombreDeArchivo					= nombreDeArchivo + "-log.txt";
		
		Writer	escritor	= null;

		File log = new File( nombreDeArchivo );
		
		try {
		
			if ( log.exists() ){
				escritor = new BufferedWriter( new FileWriter( nombreDeArchivo, true ) );
			} else {
				escritor = new BufferedWriter( new FileWriter( nombreDeArchivo ) );
			}

			escritor.append( Mensaje + "\r\n" );
			escritor.flush();
			escritor.close();
			
		} catch ( Exception e ){
			ArrayList<String> l = new ArrayList<String>();
			l.add( "No se pudo guardar la informaci�n en el archivo." );
			l.add( e.toString() );
			
			MuestraInformacion(l, "", nivel.ERROR, dondeSeMuestra.CONSOLA );
			
		} finally {
			if ( escritor != null ) {
				try {
					escritor.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}	
		}
	}
	
	public static String getFechaERP( Date f )
	{
		String a�o = "";
		String mes = "";
		String dia = "";
		
		Calendar fecha = Calendar.getInstance ( );
		fecha.setTime ( f );
		
		a�o = String.valueOf( fecha.get( Calendar.YEAR ) );
		mes = String.valueOf( fecha.get( Calendar.MONTH ) + 1 );
		dia = String.valueOf( fecha.get(  Calendar.DAY_OF_MONTH ) );
		
		if ( mes.length( ) == 1 ) mes = "0" + mes;
		if ( dia.length( ) == 1 ) dia = "0" + dia;
		
		return a�o + mes + dia;
	}
	
	public static String getFechaMySQL( Date f )
	{
		String a�o = "";
		String mes = "";
		String dia = "";
		
		Calendar fecha = Calendar.getInstance ( );
		fecha.setTime ( f );
		
		a�o = String.valueOf( fecha.get( Calendar.YEAR ) );
		mes = String.valueOf( fecha.get( Calendar.MONTH ) + 1 );
		dia = String.valueOf( fecha.get(  Calendar.DAY_OF_MONTH ) );
		
		if ( mes.length( ) == 1 ) mes = "0" + mes;
		if ( dia.length( ) == 1 ) dia = "0" + dia;
		
		return "'"  + a�o + "-" + mes + "-" + dia + "'";
	}
	
	public static ArrayList <Date> DiasDelMes ()
	{
		ArrayList <Date> Resultado = new ArrayList <Date> ( );
		Calendar cal = Calendar.getInstance ( );
		int diaDelmes 	= cal.get ( Calendar.DAY_OF_MONTH );
		int mes			= cal.get ( Calendar.MONTH  ) ;
		int a�o			= cal.get ( Calendar.YEAR );
		
		// Procede a llenar el arreglo de dias.
		
		for ( int dia = 01; dia <= diaDelmes; dia++) {
			Calendar d = Calendar.getInstance ( );
			d.set ( Calendar.DAY_OF_MONTH, dia );
			d.set ( Calendar.MONTH, mes );
			d.set ( Calendar.YEAR, a�o );
			Resultado.add ( d.getTime ( ) );
		}
		return Resultado;
	}

	public static String getFechaFormatText( String fecha )
	{
		// Se espera yyyyMMdd
		
		String resultado = "";
		
		resultado= fecha.substring ( 2, 4 ) + "/" + fecha.substring( 4, 6 ) +"/"  + fecha.substring ( 6, 8 );
		
		return resultado;
		
	}
	
	public static HashMap<String, String> descomponeDocumento ( String documento )
	{
		HashMap <String, String> resultado = new HashMap <String, String> ( );
		
		resultado.put ( "suc", documento.substring ( 0, 2 ) );
		resultado.put ( "fol", documento.substring ( 2, 8 ) );
		resultado.put ( "fin", documento.substring ( 8, 10 ) );
	
		return resultado;
	}
	
	public static HashMap<componentesDocumentoFiscal, String> descomponeDocumentoFiscal( String documento )
	{
		HashMap <componentesDocumentoFiscal, String> resultado = new HashMap <Utilidades.componentesDocumentoFiscal, String> ( );
		try {
			resultado.put ( componentesDocumentoFiscal.CLIENTE, documento.substring ( 0, 6 ) );
			resultado.put ( componentesDocumentoFiscal.FOLIO, documento.substring ( 6, 12 ) );
			if ( documento.length ( ) < 18 ) {
				resultado.put ( componentesDocumentoFiscal.SERIE, documento.substring ( 12, 17 ) );
			} else {
				resultado.put ( componentesDocumentoFiscal.SERIE, documento.substring ( 12, 18 ) );
			}
		} catch ( Exception ddfe ) {
			Utilidades.MuestraInformacion ( "Error al recuperar informacion", "Descompone documentos fiscal", nivel.INFO, dondeSeMuestra.AMBOS );
		}
	
		return resultado;
	}

	public static String redondea ( String cantidad, int decimales )
	{
		String resultado = "";
		
		BigDecimal valor = new BigDecimal ( cantidad );
		
		resultado  = String.valueOf ( valor.setScale ( decimales, BigDecimal.ROUND_HALF_UP ));
		
		return resultado;
	}

	
}
