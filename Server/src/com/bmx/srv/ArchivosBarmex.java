package com.bmx.srv;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.bmx.srv.Utilidades;

public class ArchivosBarmex 
{
	//region VARIABLES PRIVADAS
	private String 		documentoRaiz;
	private String 		docPdf;
	private String		docXml;
	private String		rutaPDF;
	private String		rutaXML;
	
	private String		rutaFinalXML;
	private String		rutaFinalPDF;
	
	private boolean		existeXML;
	private boolean		existePDF;
	
	private File		pdf = null;
	private File		xml = null;
	
	private boolean		resultado;
	
	//endregion
	
	//region PROPIEDADES
	
	public boolean getResultado()
	{
		return resultado;
	}
	
	public boolean getExistePDF()
	{
		return existePDF;
	}
	public boolean getExisteXML()
	{
		return existeXML;
	}
	
	public String getRutaFinalXML()
	{
		return rutaFinalXML;
	}
	
	public String getRutaFinalPDF()
	{
		return rutaFinalPDF;
	}
	
	//endregion
	
	//region CONSTRUCTOR
	public ArchivosBarmex( String documento )
	{
		documentoRaiz 	= documento;
		resultado		= false;
		// Valida que el formato pasado sea el correcto.
		String 		regExPression 	= "\\d{12}[A-Z]{4,6}$";
		Pattern 	patron			= Pattern.compile( regExPression );
		
		Matcher		match			= patron.matcher( documentoRaiz );
		
		resultado = match.find();

		if ( resultado ){
			docPdf 	= documento + ".pdf";
			docXml	= documento + "_S.xml";
		}
	}

	//endregion 
	
	//region METODOS PUBLICOS
	public void validaExistencia( String ruta )
	{
		
		if ( !resultado ) {
			resultado = false;
			return ;
		} else {
		
			rutaPDF = ruta + docPdf;
			rutaXML = ruta + docXml;
			
			pdf 	= new File( rutaPDF );
			xml		= new File( rutaXML ); 
			
			existePDF = pdf.exists();
			existeXML = xml.exists();
		}
	}
	
	public boolean CopiaXML ( String Destino )
	{
		if ( !resultado ) {
			resultado = false;
			return  resultado;
		}
		
		boolean resultadoCopiaXML = false;
		
		if ( existeXML ){
			
			if ( Destino.length() > 0 ){
				if ( !( Destino.substring( Destino.length() - 1 ) == "\\" ) ){
					Destino = Destino + "\\";
				}
			}
			Destino = Destino + docXml;
			
			resultadoCopiaXML = Utilidades.CopiaArchivos( Destino, rutaXML );
			
			rutaFinalXML = Destino;
		}
		
		
		return resultadoCopiaXML;
		
	}
	public boolean CopiaPDF ( String Destino )
	{
		if ( !resultado ) {
			resultado = false;
			return resultado;
		}
		
		boolean resultadoCopiaPDF = false;
		
		if ( existePDF ){
			if ( Destino.length() > 0 ){
				if ( !( Destino.substring( Destino.length() - 1 ) == "\\" ) ){
					Destino = Destino + "\\";
				}
			}
			Destino = Destino + docPdf;
			
			resultadoCopiaPDF = Utilidades.CopiaArchivos( Destino, rutaPDF );
	
			rutaFinalPDF = Destino;
		}
		
		return resultadoCopiaPDF;

	}
	//endregion
	
	//region METODOS PRIVADOS
	
	
	
	//endregion
	
}
