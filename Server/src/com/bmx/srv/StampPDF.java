package com.bmx.srv;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Map;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfTemplate;



// Se encarga de marcar los pdf

public class StampPDF 
{
	//region VARIABLES PRIVADAS
	private String		nombreDeArchivo;
	private String		pathAlArchivo;
	private String		rutaCompleta;
	private String		log;
	private boolean		resultado;
	private boolean		continua;
	private String		version;
	private String		marca 	= "CANCELADA";
	private float		grados 	= 45f;
	
	private File	pdf 	= null;
	private File	mPdf	= null;
	
	//endregion
	
	//region PROPIEDADES
	public boolean getResultado()
	{
		return resultado;
	}
	public String getVersion()
	{
		return version;
	}
	public String getMarca()
	{
		return marca;
	}
	public void setMarca( String Marca )
	{
		marca = Marca;
	}
	public float getGrados()
	{
		return grados;
	}
	public void setGrados( float Grados )
	{
		grados = Grados;
	}
	public String getLog()
	{
		return log;
	}
	//endregion
	
	//region CONSTRUCTOR
	public StampPDF( String NombreDeArchivo, String PathAlArchivo )
	{
		nombreDeArchivo 		= NombreDeArchivo;
		pathAlArchivo			= PathAlArchivo;
		
		// Valida que la información pasada sea correcta.
		
		if ( nombreDeArchivo.length() == 0 || pathAlArchivo.length() == 0 ){
			resultado 	= false;
			continua 	= resultado;
			return;
		}
		
		// Valida que el archivo exista.
		
		if ( !( pathAlArchivo.substring( pathAlArchivo.length() - 1 ) == "\\" ) ) {
			pathAlArchivo = pathAlArchivo + "\\";
		}
		
		rutaCompleta = pathAlArchivo + nombreDeArchivo;
		
		pdf 	= new File( rutaCompleta );
		mPdf	= new File( rutaCompleta.replace( ".pdf", "P.pdf" ) );
		try{
			if ( mPdf.exists() ) mPdf.delete();
		} catch ( Exception e ){
			resultado  	= false;
			continua 	= resultado;
			return;
		}
		if ( !pdf.exists() ){
			resultado = false;
		} else {
			resultado = true;
		}
		
		continua = resultado;
		
	}
	//endregion
	
	//region METODOS PUBLICOS
	public boolean MarcaArchivo()
	{
		if ( !continua ) {
			return false;
		}
		
		boolean resultadoMarcaArchivo 	= false;
		
		InputStream	iArchivoPDF		= null;
		PdfReader	lectorPDF		= null;
		PdfStamper	marcaPDF		= null;
		
		
		
		try{
			iArchivoPDF = new FileInputStream( pdf );
			lectorPDF	= new PdfReader( iArchivoPDF );
			
			// Valida que el documento ya se haya marcado
			Map< String, String > i = lectorPDF.getInfo();
			if ( i.containsKey( "Keywords" ) ) {
				if ( i.get( "Keywords" ).equals( marca ) ) {
					log = "DOCUMENTO YA MARCADO";
					return true;
				}
			} 
			
			// Una vez validado que el documento no se haya marcado, se procede a marcar.
			
			int numeroDeHojas 	= lectorPDF.getNumberOfPages();
			int hoja			= 0;
			
			marcaPDF = new PdfStamper( lectorPDF, new FileOutputStream( mPdf ) );
			
			while ( hoja < numeroDeHojas ){
				
				hoja++;
				
				PdfContentByte 	underContent 	= marcaPDF.getUnderContent( hoja );
				PdfTemplate		watermark		= underContent.createTemplate( 700, 700 );
				BaseFont		bf				= BaseFont.createFont( BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.EMBEDDED );
				
				// Crea la marca
				watermark.beginText();
				watermark.setFontAndSize( bf, 115 );
				watermark.setTextMatrix( 0, 0 );
				watermark.setColorFill( BaseColor.RED );
				watermark.showText( marca );
				watermark.endText();
				
				Image img = Image.getInstance( watermark );
				img.setAbsolutePosition( -450,  120 );
				img.setRotationDegrees( grados );
				
				PdfGState g = new PdfGState();
				
				g.setFillOpacity( 0.6f );
				
				underContent.setGState( g );
				underContent.addImage( img );
				
			}
			
			Map<String, String> info = lectorPDF.getInfo();
			info.put( "Keywords", marca );
			
			marcaPDF.setMoreInfo( info );
			
			marcaPDF.close();
			lectorPDF.close();
			iArchivoPDF.close();
			
			// PROCEDE A COPIAR EL ARCHIVO VIEJO SOBRE EL NUEVO.
			
			if ( mPdf.exists() ){
				if ( pdf.exists() ) pdf.delete();
				
				if ( !Utilidades.CopiaArchivos( rutaCompleta, rutaCompleta.replace( ".pdf", "P.pdf" ) ) ) {
					resultadoMarcaArchivo = false;
					return resultadoMarcaArchivo;
				}
				mPdf.delete();
			}
			
			resultadoMarcaArchivo = true;
			
		} catch ( Exception e ){
			resultadoMarcaArchivo = false;
			log = e.toString();
		}
		
		
		return resultadoMarcaArchivo;
	}
	//endregion
	
	
	
}
