package com.bmx.srv;

import static com.bmx.srv.Utilidades.*;

import java.util.ArrayList;
import java.util.HashMap;

import com.bmx.srv.Usuarios.procesos_usuarios;



public class Proveedores
{
	
	/*
	 * LOS METODOS IMPLEMENTADOS EN PRIMERA INSTANCIA, SON ESTATICOS, YA QUE SERVIRAN PARA EL PROCESO DE "AUDITOR"
	 * LOS METODOS EN LA INSTANCIACION, REQUIEREN UNA CLAVE DE PROVEEDOR.
	 */
	
	//region METODOS ESTATICOS
	
	public static void ActualizaProveedores() {
		// SOLAMENTE SE SUBE INFORMACION, NO SE SINCRONIZA
		String Cliente = "Static Sync Proveedores";
		
		ArrayList<String> mensajes = new ArrayList <String> ( );
		HashMap<String, ArrayList<String>> origen 	= new HashMap <String, ArrayList<String>> ( );
		HashMap<String, ArrayList<String>> replica 	= new HashMap <String, ArrayList<String>> ( );
		
		HashMap<String, ArrayList<String>> rInsert	= new HashMap <String, ArrayList<String>> ( );
		
		ArrayList<String> llaves = new ArrayList <String> ( );
		
		
		MuestraInformacion ( "Iniciando sincronización de proveedores", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		ConsultaODBC cBarPro 	= new ConsultaODBC ( Conecciones.getMySQLLocalBarpro ( ) );
		ConsultaODBC cEBarmex	= new ConsultaODBC ( Conecciones.getMySQLRemoto ( ) );
		
		// Primero sube los nuevos proveedores
		
		String sentencia = "	SELECT 		GP_CVEPROV, GP_RAZON, GP_RFC, " +
				"							GP_TIPO, GP_PAR1, GP_PAR2, " +
				"							GP_PAR3, GP_PAR4, GP_PAR5 " +
				"				FROM 		BARPROV01";
		
		
		origen = cBarPro.ConsultaVariasColumnasODBC ( sentencia );
		replica = cEBarmex.ConsultaVariasColumnasMySQL ( sentencia );
		
		sentencia = "INSERT 	" +
					"INTO 		BARPROV01 ( GP_CVEPROV, GP_RAZON, GP_RFC, " +
					"						GP_TIPO, GP_PAR1, GP_PAR2, " +
					"						GP_PAR3, GP_PAR4, GP_PAR5 ) " +
								"values	  ( ?, ?, ?, " +
								"			?, ?, ?, " +
								"			?, ?, ?) ";
		
		
		// Despues valida, cuales cambiaron, y los sube de la tabla local. NUNCA SE BORRAN
		
		llaves.add ( "1" );
		
		MuestraInformacion ( "Iniciando subida de nuevos proveedores.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		rInsert = ConsultaODBC.comparaOrigenVsCopia ( origen, replica, llaves );
		
		HashMap <Integer, tipoDeDato> datoDeColumna = new HashMap <Integer, Utilidades.tipoDeDato> ( );
		
		datoDeColumna.put ( 1, tipoDeDato.CADENA );
		datoDeColumna.put ( 2, tipoDeDato.CADENA );
		datoDeColumna.put ( 3, tipoDeDato.CADENA );
		datoDeColumna.put ( 4, tipoDeDato.CADENA );
		datoDeColumna.put ( 5, tipoDeDato.ENTERO );
		datoDeColumna.put ( 6, tipoDeDato.ENTERO );
		datoDeColumna.put ( 7, tipoDeDato.ENTERO );
		datoDeColumna.put ( 8, tipoDeDato.ENTERO );
		datoDeColumna.put ( 9, tipoDeDato.ENTERO );
		
		int registrosGuardados = 0;
		
		if ( rInsert.keySet ( ).size ( ) > 0 ) {
		
			registrosGuardados = cEBarmex.InsertaRegistroMysql ( sentencia, rInsert, datoDeColumna, 50 );
		
			mensajes.clear ( );
			mensajes.add ( "Terminando de subir proveedores nuevos." );
			mensajes.add ( "Se agregaron: " + String.valueOf ( registrosGuardados ) + " nuevos registros" );
			MuestraInformacion ( mensajes, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			mensajes.clear ( );
			
		} else {
			MuestraInformacion ( "Terminando No hay proveedores nuevos.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		}
		
		MuestraInformacion ( "Iniciando actualizacion de proveedores.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		// Tiene el mismo numero de registros la replica que el origen, ahora valida que los valores no hayan cambiado.
		replica.clear ( );
		origen.clear ( );
		
		sentencia = "			SELECT 		GP_RAZON, GP_RFC, GP_TIPO, " +
				"							GP_PAR1, GP_PAR2, GP_PAR3, " +
				"							GP_PAR4, GP_PAR5, GP_CVEPROV " +
				"				FROM 		BARPROV01";
		
		
		origen 	= cBarPro.ConsultaVariasColumnasODBC ( sentencia );
		replica = cEBarmex.ConsultaVariasColumnasMySQL ( sentencia );
		
		llaves.clear ( );
		
		llaves.add ( "1" );
		llaves.add ( "2" );
		llaves.add ( "3" );
		llaves.add ( "4" );
		llaves.add ( "5" );
		llaves.add ( "6" );
		llaves.add ( "7" );
		llaves.add ( "8" );
		llaves.add ( "9" );
		
		rInsert = ConsultaODBC.comparaOrigenVsCopia ( origen, replica, llaves );
		
		registrosGuardados = 0;
		
		if ( rInsert.keySet ( ).size ( ) > 0 ) {
			
			datoDeColumna.put ( 1, tipoDeDato.CADENA );
			datoDeColumna.put ( 2, tipoDeDato.CADENA );
			datoDeColumna.put ( 3, tipoDeDato.CADENA );
			datoDeColumna.put ( 4, tipoDeDato.ENTERO );
			datoDeColumna.put ( 5, tipoDeDato.ENTERO );
			datoDeColumna.put ( 6, tipoDeDato.ENTERO );
			datoDeColumna.put ( 7, tipoDeDato.ENTERO );
			datoDeColumna.put ( 8, tipoDeDato.ENTERO );
			datoDeColumna.put ( 9, tipoDeDato.CADENA );
			
			sentencia = "UPDATE 	" +
						"BARPROV01 	SET GP_RAZON = ?, GP_RFC = ?, GP_TIPO = ?, " +
						"				GP_PAR1 = ?, GP_PAR2 = ?, GP_PAR3 = ?, " +
						"				GP_PAR4 = ?, GP_PAR5 = ? " +
						"WHERE			GP_CVEPROV = ?";
			
			registrosGuardados = cEBarmex.updateRegistrosMySQL ( sentencia, rInsert, datoDeColumna, 50 );
		
			mensajes.clear ( );
			mensajes.add ( "Terminando de actualizar proveedores." );
			mensajes.add ( "Se actualizaron: " + String.valueOf ( registrosGuardados ) + " registros" );
			MuestraInformacion ( mensajes, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			mensajes.clear ( );

		} else {
			MuestraInformacion ( "Terminando No hay proveedores para actualizar.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		}
		
		
		
		MuestraInformacion ( "Terminando sincronización de proveedores", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );

	}
		
	public static boolean SincronizaOrdenes() {
		boolean resultado = false;
		
		return resultado;
	}
	
	public static boolean DescargaDocumentos( ) {
		boolean resultado = false;
		
		return resultado;
	}
	
	public static void SincronizaTodo() {
		Usuarios.Proceso_usuarios ( procesos_usuarios.SINCRONIZA_WEB_TO_LOC );
		SincronizaOrdenes ( );
		DescargaDocumentos ( );
	}
	
	//endregion
	
	//region VARIABLES INTERNAS
	
	String version = "1.0.0.0";
	
	String _clave_proveedor;
	String Cliente;
	
	boolean estado;
	String  desError;
	String 	desMesError;
	//endregion
	
	//region ENUMERACIONES
	enum errores {
		SIN_ERROR(1);
		final int te;
		private errores( int tipoError )
		{
			te = tipoError;
		}
		public String getDesError(){
			String resultado ="";
			switch ( te ) {
				case 1:
					resultado = "Sin error";
					break;

			}
			
			return resultado;
		}
	}
	//endregion
	
	//region PROPIEDADES
	public boolean getEstado() {
		return estado;
	}
	public String getError() {
		return desError;
	}
	public String getDescripcionDeError() {
		return desMesError;
	}
	public String getVersion() {
		return version;
	}
	//endregion
	
	//region CONSTRUCTORES
	
	public Proveedores ( String clave_proveedor )
	{
		_clave_proveedor = clave_proveedor;
	}
	
	//endregion
	
	//region METODOS PUBLICOS
	
	public void ActualizaProveedor() {
		// SOLAMENTE SE SUBE INFORMACION, NO SE SINCRONIZA
		String Cliente = "Actualiza proveedor";
		
		MuestraInformacion ( "Iniciando actualizacion del proveedor " + _clave_proveedor, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		ArrayList<String> mensajes = new ArrayList <String> ( );
		HashMap<String, ArrayList<String>> origen 	= new HashMap <String, ArrayList<String>> ( );
		HashMap<String, ArrayList<String>> replica 	= new HashMap <String, ArrayList<String>> ( );
		
		HashMap<String, ArrayList<String>> rInsert	= new HashMap <String, ArrayList<String>> ( );
		
		ArrayList<String> llaves = new ArrayList <String> ( );
		
		String sentencia = "";
		
		int registrosGuardados = 0;
		
		HashMap <Integer, tipoDeDato> datoDeColumna = new HashMap <Integer, Utilidades.tipoDeDato> ( );
		
		ConsultaODBC cBarPro 	= new ConsultaODBC ( Conecciones.getMySQLLocalBarpro ( ) );
		ConsultaODBC cEBarmex	= new ConsultaODBC ( Conecciones.getMySQLRemoto ( ) );
		
		MuestraInformacion ( "Iniciando actualizacion del proveedor " + _clave_proveedor, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		// Tiene el mismo numero de registros la replica que el origen, ahora valida que los valores no hayan cambiado.
		replica.clear ( );
		origen.clear ( );
		
		sentencia = "			SELECT 		GP_RAZON, GP_RFC, GP_TIPO, " +
				"							GP_PAR1, GP_PAR2, GP_PAR3, " +
				"							GP_PAR4, GP_PAR5, GP_CVEPROV " +
				"				FROM 		BARPROV01 " +
				"				WHERE		GP_CVEPROV = '" + _clave_proveedor + "'";
		
		
		origen 	= cBarPro.ConsultaVariasColumnasODBC ( sentencia );
		replica = cEBarmex.ConsultaVariasColumnasMySQL ( sentencia );
		
		llaves.clear ( );
		
		llaves.add ( "1" );
		llaves.add ( "2" );
		llaves.add ( "3" );
		llaves.add ( "4" );
		llaves.add ( "5" );
		llaves.add ( "6" );
		llaves.add ( "7" );
		llaves.add ( "8" );
		llaves.add ( "9" );
		
		rInsert = ConsultaODBC.comparaOrigenVsCopia ( origen, replica, llaves );
		
		registrosGuardados = 0;
		
		if ( rInsert.keySet ( ).size ( ) > 0 ) {
			
			datoDeColumna.put ( 1, tipoDeDato.CADENA );
			datoDeColumna.put ( 2, tipoDeDato.CADENA );
			datoDeColumna.put ( 3, tipoDeDato.CADENA );
			datoDeColumna.put ( 4, tipoDeDato.ENTERO );
			datoDeColumna.put ( 5, tipoDeDato.ENTERO );
			datoDeColumna.put ( 6, tipoDeDato.ENTERO );
			datoDeColumna.put ( 7, tipoDeDato.ENTERO );
			datoDeColumna.put ( 8, tipoDeDato.ENTERO );
			datoDeColumna.put ( 9, tipoDeDato.CADENA );
			
			sentencia = "UPDATE 	" +
						"BARPROV01 	SET GP_RAZON = ?, GP_RFC = ?, GP_TIPO = ?, " +
						"				GP_PAR1 = ?, GP_PAR2 = ?, GP_PAR3 = ?, " +
						"				GP_PAR4 = ?, GP_PAR5 = ? " +
						"WHERE			GP_CVEPROV = ?";
			
			registrosGuardados = cEBarmex.updateRegistrosMySQL ( sentencia, rInsert, datoDeColumna, 50 );
		
			mensajes.clear ( );
			mensajes.add ( "Terminando de actualizar al proveedor " + _clave_proveedor );
			mensajes.add ( "Se actualizaron: " + String.valueOf ( registrosGuardados ) + " registros" );
			MuestraInformacion ( mensajes, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			mensajes.clear ( );

		} else {
			MuestraInformacion ( "Terminando no hay datos para actualizar del proveedor  " + _clave_proveedor, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		}
		
		MuestraInformacion ( "Teminando actualizacion del proveedor " + _clave_proveedor, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
	}
	
	public boolean SincronizaUsuario() {
		boolean resultado = false;
		
		
		return resultado;
	}
	
	public boolean SincronizaOrden(){
		boolean resultado = false;
		
		// Se encarga de cargar todas las ordenes de compra.
		
		
		
		
		return resultado;
	}
	
	public boolean DescargaDocumento() {
		boolean resultado = false;
		
		
		return resultado;
	}

	public static void sincronizaCompradores(){
		
		String Cliente = "Sincroniza Compradores";
		
		HashMap<String, ArrayList<String>> origen 	= new HashMap <String, ArrayList<String>> ( );
		HashMap<String, ArrayList<String>> enLocal 	= new HashMap <String, ArrayList<String>> ( );
		HashMap<String, ArrayList<String>> enRemoto = new HashMap <String, ArrayList<String>> ( );
		
		ArrayList<String> llaves = new ArrayList <String> ( );
		
		HashMap<String, ArrayList<String>> registros 	= new HashMap <String, ArrayList<String>> ( );
		
		
		// Recupera el listado de compradores del erp
		
		ConsultaODBC c = new ConsultaODBC( Conecciones.getERPCon ( ) );
		
		String sentencia = 	"SELECT * FROM MDGDIC01 WHERE TB_LLAV LIKE '17.%'";
		
		origen = c.ConsultaVariasColumnasODBC ( sentencia );
		
		// Corrige el origen, elimina el "17."
		
		for ( String llave : origen.keySet ( ) ){
			ArrayList<String> datos = origen.get ( llave );
			ArrayList<String> prov 	= new ArrayList <String> ( );
			
			prov.add ( datos.get ( 0 ).replaceAll ( "17.", "" ) );
			String nombre = datos.get ( 1 ).trim ( );
			
			if ( nombre.indexOf ( "@" ) > 1 ) {
				nombre = nombre.substring ( 0, 30 ).trim ( );
			}
			
			
			prov.add ( nombre );			
			
			origen.put ( llave, prov );
		}
		
		
		sentencia = "SELECT * FROM BARPROV07";
		c = new ConsultaODBC ( Conecciones.getMySQLLocalBarpro ( ) );
		enLocal = c.ConsultaVariasColumnasMySQL ( sentencia );

		c = new ConsultaODBC ( Conecciones.getMySQLRemoto ( ) );
		enRemoto = c.ConsultaVariasColumnasMySQL ( sentencia );
		
		// Compara la base del erp contra la local
		
		llaves.add ( "1" );
		
		registros = ConsultaODBC.comparaOrigenVsCopia ( origen, enLocal, llaves );
		
		sentencia = "INSERT INTO BARPROV07 (cve_comprador, nombre) values (?,?)";
		
		HashMap<Integer, tipoDeDato> datoDeColumna = new HashMap <Integer, Utilidades.tipoDeDato> ( );
		datoDeColumna.put ( 1, tipoDeDato.CADENA );
		datoDeColumna.put ( 2, tipoDeDato.CADENA );
		
		int Batch = 50;
		
		c = new ConsultaODBC ( Conecciones.getMySQLLocalBarpro ( ) );
		
		int insertados = c.InsertaRegistroMysql ( sentencia, registros, datoDeColumna, Batch );
		
		MuestraInformacion ( "Se subieron  " + String.valueOf ( insertados ) + " compradores nuevos (local).", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		registros = ConsultaODBC.comparaOrigenVsCopia ( origen, enRemoto, llaves );
		
		c = new ConsultaODBC ( Conecciones.getMySQLRemoto ( ) );
		
		insertados = c.InsertaRegistroMysql ( sentencia, registros, datoDeColumna, Batch );
		
		MuestraInformacion ( "Se subieron  " + String.valueOf ( insertados ) + " compradores nuevos (remoto).", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
	
	}
		
	public void Todo() {
		SincronizaUsuario ( );
		SincronizaOrden ( );
		DescargaDocumento ( );
	}
	
	//endregion
	
	//region METODOS PRIVADOS
	
	
	

	//endregion

}
