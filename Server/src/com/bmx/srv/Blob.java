package com.bmx.srv;
import java.net.InetAddress;
import java.sql.*;
// jdbc:mysql://localhost/test
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.io.*;

import com.bmx.srv.Utilidades.dondeSeMuestra;
import com.bmx.srv.Utilidades.nivel;


public class Blob 
{
	//region VARIABLES INTERNAS
	private String 				server 		= "";
	private String 				puerto		= "";
	private String 				base		= "";
	private String 				tabla		= "";
	private String				usuario		= "";
	private String				password	= "";
	
	private Boolean				resultado	= true;
	private StringBuilder		log			= new StringBuilder();
	
	private String				sentencia	= "";
	private ArrayList<String> 	archivos	= new ArrayList<String>();
	private int					numArchivos	= 0;
	private int					numColumnas	= 0;
	private boolean				continuar	= false;
	
	private Connection			coneccion	= null;
	private boolean				usarExterna	= false;
	
	private Connection			clocal 		= null;
	private Connection			cremoto 	= null;
	
	//endregion
	
	//region PROPIEDADES
	public Boolean getResultado()
	{
		return resultado;
	}
	public String getLog()
	{
		return log.toString();
	}
	public String getSentencia()
	{
		return sentencia;
	}
	public void setSentencia( String Sentencia )
	{
		sentencia = Sentencia;
		
		if ( sentencia.length() == 0 ) {
			continuar = false;
		} else {
			continuar = true;
		}
		
		// Descompone la sentencia y recupera el numero de columnas en donde se guardara la informacion.
		if ( continuar ){
			if ( Sentencia.indexOf( "?" ) > 0 ){
				// Recorre toda la sentencia y va guardando si encuentra un "?"
				String sentenciaProvisional = sentencia;
							
				while ( sentenciaProvisional.indexOf( "?" ) > 0 ){
					int posicionEncontrada = sentenciaProvisional.indexOf( "?" );
					sentenciaProvisional = sentenciaProvisional.substring( posicionEncontrada );
					numColumnas++;
				}
			} 
		}
	
	}
	public ArrayList<String> getArchivos()
	{
		return archivos;
	}
	public void setArchivos( String args )
	{
		for (String arg : args.split( "," )) {
			// Valida que el archivo exista antes de cargar la informacion al arreglo que se validara.
			File b = new File( arg );
			if ( b.exists() ){
				archivos.add( arg );
			}
		}
		numArchivos = archivos.size();
	}
	public String getUsuario()
	{
		return usuario;
	}
	public void setUsuario( String Usuario )
	{
		usuario = Usuario;
	}
	public String getPassword()
	{
		return password;
	}
	public void setPasswod( String Password )
	{
		password = Password;
	}
	public void setConeccion( Connection c )
	{
		if ( c != null ){
			coneccion 	= c;
			continuar	= true;
			usarExterna = true;
		} else {
			continuar = false;			
		}
	}
	
	//endregion
	//region CONSTRUCTOR
	public Blob ( String Server, String Puerto, String Base, String Tabla )
	{
		// Se asignan todos los valores a sus respectivos valores internos, y se eliminan los espacios.
		server 	= Server.trim();
		puerto 	= Puerto.trim();
		base	= Base.trim();
		tabla	= Tabla.trim();
		
		if ( server.length() == 0 ){
			resultado = false;
			log.append( "Se paso una cadena vacia." );
		} else {
			// Valida que la direccion pasada sea valida.
			resultado = validaIPWebAddress( server );
			if ( !resultado ) {
				log.append( "La dirección proporcionada es invalida." );
			}
			
		}
		
		if ( puerto.length() == 0 ){
			resultado = false;
			log.append( "Se paso un puerto vacio." );
		} else {
			// Valida el puerto, para primero que sea numerico y dentro del rango esperado.
			resultado = validaQuePuertoEsNumero( puerto );
			if ( !resultado ){
				log.append( "El valor pasado no es numerico " );
				resultado = false;
			}
		}
		if ( base.length() == 0 ){
			resultado = false;
			log.append( "Se paso un nombre de base vacio." );
		}
		if ( tabla.length() == 0 ){
			resultado = false;
			log.append( "Se paso un nombre de tabla vacio." );
		}
		
		if ( !resultado ){
			continuar 	= false;
			usarExterna = false;
		}
		
		
	}
	
	public Blob ( Connection c )
	{
		if ( c != null ){
			coneccion 	= c;
			usarExterna = true;
		} else {
			continuar = false;
		}
	}
	
	public Blob ( Connection local, Connection remoto )
	{
		clocal 	= local;
		cremoto	= remoto;
	}
	//endregion
	//region METODOS PUBLICOS
	

	// Carga Blob
	// Una vez que tiene toda la información procede a cargar la información en el blob.
	public boolean cargaBlob()
	{
		boolean resultadoCargaBlob = true;
		
		// Valida que los requerimientos basicos para la carga de blob se pueda realizar.
		if ( !continuar ){
			resultadoCargaBlob 	= false;
			resultado			= resultadoCargaBlob;
			return resultadoCargaBlob;
		}
		// Si no hubo ningun problema con la carga de informacion valida que la informacion sea congruente.
		// Esto es que al momento de pasar los archivos, estos correspondan con los nombres de columnas ( numeros ).
		
		if ( numColumnas != numArchivos ){
			resultadoCargaBlob 	= false;
			resultado			= resultadoCargaBlob;
			log.append( "El numero de archivos no es igual al de columnas" );
			return	resultadoCargaBlob;
		}
		
		if ( !usarExterna ) {
		
			if ( usuario.length() == 0 || password.length() == 0 ){
				resultadoCargaBlob 	= false;
				resultado			= false;
				log.append( "El usuario y/o el password estan vacions" );
				return resultadoCargaBlob;
			}
		}
		// Si todas las validaciones son correctas, procede a cargar la información a la base de datos.
		
		try {
		
			Connection con = null;
			
			if ( usarExterna ){
				
				con = coneccion;
				
			} else {
				Class.forName("com.mysql.jdbc.Driver");
				// Crea la URL para el driver JDBC.
				String URL = "jdbc:mysql://" + server + ":" + puerto +"/" + base;
				
				con = DriverManager.getConnection( URL, usuario, password );
			}

			con.setAutoCommit( true );
			
			FileInputStream 	fis = null;
			PreparedStatement	ps 	= null;
			int	contadorColumnas	= 1;
			
			for ( String archivo : archivos ) {
				File a = new File( archivo );
				fis = new FileInputStream( a );
				ps	= con.prepareStatement( sentencia );
				ps.setBinaryStream( contadorColumnas,  fis, (int)a.length() );	
				contadorColumnas++;
			}
			
			ps.executeUpdate();
			
			resultadoCargaBlob 	= true;
			resultado			= resultadoCargaBlob;
		} catch ( Exception e ){
			resultadoCargaBlob 	= false;
			resultado			= resultadoCargaBlob;
			log.append( e.toString() );
		}
	
		return resultadoCargaBlob;
	}
	
	public boolean cargaBlob( InputStream[] in )
	{
		boolean resultadoCargaBlob = true;
		
		// Valida que los requerimientos basicos para la carga de blob se pueda realizar.
		if ( !continuar ){
			resultadoCargaBlob 	= false;
			resultado			= resultadoCargaBlob;
			return resultadoCargaBlob;
		}
		// Si no hubo ningun problema con la carga de informacion valida que la informacion sea congruente.
		// Esto es que al momento de pasar los archivos, estos correspondan con los nombres de columnas ( numeros ).
		
		if ( in == null ){
			resultadoCargaBlob 	= false;
			resultado			= resultadoCargaBlob;
			log.append( "El input stream esta vacio" );
			return	resultadoCargaBlob;
		}
		
		if ( !usarExterna ) {
		
			if ( usuario.length() == 0 || password.length() == 0 ){
				resultadoCargaBlob 	= false;
				resultado			= false;
				log.append( "El usuario y/o el password estan vacions" );
				return resultadoCargaBlob;
			}
		}
		// Si todas las validaciones son correctas, procede a cargar la información a la base de datos.
		
		try {
		
			Connection con = null;
			
			if ( usarExterna ){
				
				con = coneccion;
				
			} else {
				Class.forName("com.mysql.jdbc.Driver");
				// Crea la URL para el driver JDBC.
				String URL = "jdbc:mysql://" + server + ":" + puerto +"/" + base;
				
				con = DriverManager.getConnection( URL, usuario, password );
			}
			con.setAutoCommit( true );
			
			PreparedStatement	ps 	= null;
			
			int contadorColumnas = 1;
			
			for ( InputStream i : in ) {
				ps	= con.prepareStatement( sentencia );	
				ps.setBinaryStream ( contadorColumnas, i );
				contadorColumnas++;
			}
			
			ps.executeUpdate();
			
			resultadoCargaBlob 	= true;
			resultado			= resultadoCargaBlob;
		} catch ( Exception e ){
			resultadoCargaBlob 	= false;
			resultado			= resultadoCargaBlob;
			log.append( e.toString() );
		}
	
		return resultadoCargaBlob;
	}
	
	public void cargaLista( ArrayList <String> documentos, int sizeBatch )
	{
		
		String Cliente = "Carga Lista";
		
		PreparedStatement psLoc = null;
		PreparedStatement psRem	= null;
		ResultSet		  rLoc	= null;
		
		String consulta = "	SELECT 		* " +
				"	FROM 		facturas " +
				"	WHERE 		nombre = ?";

		String insert	= "INSERT " +
				"	INTO 		facturas (	nombre, sl_numcli, sl_numdoc, " +
										"	serie, sl_tipmov, sl_refe, " +
										"	fecha, contenido, tipo, " +
										"	enviado) " +
				"	VALUES				 ( ?, ?, ?,  " +
										"  ?, ?, ?, " +
										"  ?, ?, ?, " +
										"  ?)";
		
		
		int RegistrosGuardados = 0;
		
		try {
			
			int contador = 0;
			
			for ( String documento : documentos ) {
				
				
				clocal.setAutoCommit 	( true );
				cremoto.setAutoCommit 	( true );
				
				if ( !validaSentencia ( consulta, cremoto ) ) {
				
					psLoc = clocal.prepareStatement ( consulta );
					psLoc.setString ( 1, documento );
					
					rLoc = psLoc.executeQuery ( );
					
					while ( rLoc.next ( ) ){
						psRem = cremoto.prepareStatement ( insert );
						psRem.setString ( 1, rLoc.getString ( 1 ) );
						psRem.setString ( 2, rLoc.getString ( 2 ) );
						psRem.setString ( 3, rLoc.getString ( 3 ) );
						psRem.setString ( 4, rLoc.getString ( 4 ) );
						psRem.setString ( 5, rLoc.getString ( 5 ) );
						psRem.setString ( 6, rLoc.getString ( 6 ) );
						psRem.setString ( 7, rLoc.getString ( 7 ) );
						psRem.setBinaryStream ( 8, rLoc.getBinaryStream ( 8 ) );		// blob
						psRem.setString ( 9, rLoc.getString ( 9 ) );
						psRem.setBinaryStream ( 10, rLoc.getBinaryStream ( 10 ) );	// blob
					}
					
					psRem.addBatch ( );
					contador++;
					
					if ( contador == sizeBatch ){
						int[] resultado = psRem.executeBatch ( );
						RegistrosGuardados = RegistrosGuardados + resultado.length;
					}
				}
			}
			
			Utilidades.MuestraInformacion ( "Se cargaron: " + String.valueOf ( RegistrosGuardados ), Cliente, nivel.INFO, dondeSeMuestra.AMBOS);
			
		}catch ( Exception ce ){
			log.append ( ce.toString ( ) );
		}
	}
	
	// Descarga Blob
	
	public boolean DescargaArchivo( String archivos )
	{
		boolean resultadoDescarga = false;
		
		if ( !usarExterna ) {
			if ( !continuar ){
				resultadoDescarga = false;
				resultado = resultadoDescarga;
				return resultado;
			}
			
			if ( usuario.length() == 0 || password.length() == 0 ){
				resultadoDescarga 	= false;
				resultado			= false;
				log.append( "El usuario y/o el password estan vacions" );
				return resultadoDescarga;
		}
		}
		try{
			
			Connection con = null;
			
			if ( usarExterna ){
				
				con = coneccion;
				
			} else {
				Class.forName("com.mysql.jdbc.Driver");
				// Crea la URL para el driver JDBC.
				String URL = "jdbc:mysql://" + server + ":" + puerto +"/" + base;
				
				con = DriverManager.getConnection( URL, usuario, password );
			}
			
			con.setAutoCommit( true );
			
			Statement st = con.createStatement();
			
			ResultSet		rs 		= st.executeQuery( sentencia );
			
			if ( rs.next() ){
				// Si se pudo ejecutar 
				java.sql.Blob blob = rs.getBlob( 1 );
				byte[] bBlob = blob.getBytes( 1, ( int ) blob.length() );
				
				// Graba el byte a archivo.
				
				// TODO: DESCARGAR VARIOS ARCHIVOS.
				
				FileOutputStream fos = new FileOutputStream( archivos );
				fos.write( bBlob );
				fos.flush();
				fos.close();
				
				resultadoDescarga = true;  
			} else {
				// No se pudo ejecutar
				
				resultadoDescarga = false;
			}
			
		} catch ( Exception e ){
			resultadoDescarga 	= false;
			resultado			= resultadoDescarga;
			log.append( e.toString() );
		}
		
		resultado = resultadoDescarga;
		return resultadoDescarga;
	}
	
	/** VALIDA QUE UNA SENTENCIA ( SOBRE LLAVE, EXISTA ), EN CASO DE NO EXISTIR ES FALSO
	 * 
	 * @param sentencia
	 * @return verdadero si se regreso al menos un registro.
	 */
	public boolean validaSentencia( )
	{

		boolean resultadoValidaSentencia = false;
		
		try{
			Connection con = null;
			
			if ( usarExterna ){
				
				con = coneccion;
				
			} else {
				
				Class.forName("com.mysql.jdbc.Driver");
				// Crea la URL para el driver JDBC.
				String URL = "jdbc:mysql://" + server + ":" + puerto +"/" + base;
				
				con = DriverManager.getConnection( URL, usuario, password );
				
			}
			
			
			con.setAutoCommit( true );
			
			Statement st = con.createStatement();
			
			ResultSet		rs 		= st.executeQuery( sentencia );
			
			if ( !rs.isBeforeFirst() ){
				resultadoValidaSentencia = false;
			} else {
				resultadoValidaSentencia = true;
			}
			
			
		} catch ( Exception e ){
			resultadoValidaSentencia 	= false;
			resultado			= resultadoValidaSentencia;
			log.append( e.toString() );
		}
		
		resultado = resultadoValidaSentencia;
		
		return resultado;
	}
	
	public boolean validaSentencia( String sen, Connection c )
	{

		boolean resultadoValidaSentencia = false;
		
		try{
			Connection con = c;
			
			con.setAutoCommit( true );
			
			Statement st = con.createStatement();
			
			ResultSet		rs 		= st.executeQuery( sen );
			
			if ( !rs.isBeforeFirst() ){
				resultadoValidaSentencia = false;
			} else {
				resultadoValidaSentencia = true;
			}
			
			
		} catch ( Exception e ){
			resultadoValidaSentencia 	= false;
			resultado			= resultadoValidaSentencia;
			log.append( e.toString() );
		}
		
		resultado = resultadoValidaSentencia;
		
		return resultado;
	}
	
	//endregion
	//region METODOS PRIVADOS
	private boolean validaIPWebAddress ( String direccion )
	{
		boolean resultado = true;
		try{
			@SuppressWarnings("unused")
			InetAddress addr = InetAddress.getByName(direccion);
		} catch ( Exception e ){
			resultado = false;
			return false;
		}
		
		return resultado;
	}
	
	private boolean validaQuePuertoEsNumero ( String puerto )
	{
		boolean	resultado	= false;
		Pattern pattern 	= Pattern.compile("[0-9]+");
		
		Matcher	m			= pattern.matcher(puerto);
		
		resultado 			= m.matches();
		
		return resultado;
		
	}
	
	//endregion
	
	//TODO: Crear una clase para encapsular este comportamiento.
	
}
