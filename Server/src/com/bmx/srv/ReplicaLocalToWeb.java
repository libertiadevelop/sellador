package com.bmx.srv;

import java.util.ArrayList;
import java.util.HashMap;
import com.bmx.srv.Utilidades;
import com.bmx.srv.Utilidades.dondeSeMuestra;
import com.bmx.srv.Utilidades.nivel;
import com.bmx.srv.Utilidades.tipoDeDato;

public class ReplicaLocalToWeb
{
	enum proceso {
		EXISTENCIAS,
		ARTICULOS,
		CLIENTES,
		SALDOS
		;
	}
	
	enum tablas {
		LOCAL_EXISTENCIAS(1),
		LOCAL_ARTICULOS(2),
		LOCAL_CLIENTES(3),
		LOCAL_SALDOS(4),
		
		WEB_EXISTENCIAS(11),
		WEB_ARTICULOS(12),
		WEB_CLIENTES(13),
		WEB_SALDOS(14)
		;
		int loc = 0;
		
		String tablaArticulosLoc 	= "MDGINV01";
		String tablaArticulosWeb 	= "martic";
		String tablaExistenciasLoc 	= "MDGINV03";
		String tablaExistenciasWeb	= "dexist";
		
		String tabla 	= "";
		String pro 		= "";
		
		private tablas ( int l ){
			loc = l;
			setTabla ( );
		}
		
		private void setTabla () {
			switch ( loc ) {
				case 1:
					tabla = tablaExistenciasLoc;
					pro = "Existencias local";
					break;
				case 11:
					tabla = tablaExistenciasWeb;
					pro = "Existencias web";
					break;
			}
		}
		
		public String getTabla() {
			return tabla;
		}
	
		public String getSelect(  ) {
			String resultado = "";
			switch ( loc ) {
				case 1:
					resultado = "SELECT * FROM " + tabla;
					break;
				case 11:
					resultado = "SELECT * FROM " + tabla;
					break;
			}
			return resultado;
		}
		
		public String getInsert( ) {
			String resultado = "";
			switch ( loc ) {
				case 1:
					resultado ="";
					break;
				case 11:
					resultado = "INSERT INTO " + tabla + " (ex_llav, ex_codart, ex_numalm, " +
													"ex_loca, ex_entr, ex_sali, " + 
													"ex_exis, ex_unimed, ex_canapa, " + 
													"ex_inve) " +
											"VALUES (?,?,?, " +
													"?,?,?, " +
													"?,?,?, " +
													"?)";
					break;
			}
			return resultado;
		}
		
		public String getUpdate( ) {
			String resultado = "";
			switch ( loc ) {
				case 1:
					resultado ="";
					break;
				case 11:
					resultado = "UPDATE " + tabla + " SET ex_entr = ?, ex_sali = ?, ex_exis = ?, ex_canapa = ? " +
								"WHERE	ex_llav = ?";
					break;
			}
			return resultado;
		}
		
		public String getProceso( ) {
			return pro;
		}
		
		public HashMap<Integer, tipoDeDato> getDatoColumnaIns( ){
			HashMap<Integer, tipoDeDato> resultado = new HashMap <Integer, Utilidades.tipoDeDato> ( );
			int key = 0;
			switch ( loc ) {
				case 1:
					resultado = null;
					break;
				case 11:
					resultado.put ( key++, tipoDeDato.CADENA );	//ex_llav
					resultado.put ( key++, tipoDeDato.CADENA );	//ex_codart
					resultado.put ( key++, tipoDeDato.ENTERO );	//ex_numalm
					resultado.put ( key++, tipoDeDato.CADENA );	//ex_loca
					resultado.put ( key++, tipoDeDato.DOBLE );	//ex_entr
					resultado.put ( key++, tipoDeDato.DOBLE );	//ex_sali
					resultado.put ( key++, tipoDeDato.DOBLE );	//ex_exis
					resultado.put ( key++, tipoDeDato.CADENA );	//ex_unimed
					resultado.put ( key++, tipoDeDato.DOBLE );	//ex_canapa
					resultado.put ( key++, tipoDeDato.CADENA );	//ex_inve
					resultado.put ( key++, tipoDeDato.CADENA );	//ex_work
					break;
			}
			return resultado;
		}
		
		public HashMap<Integer, tipoDeDato> getDatoColumnaUpd( ){
			HashMap<Integer, tipoDeDato> resultado = new HashMap <Integer, Utilidades.tipoDeDato> ( );
			int key = 0;
			switch ( loc ) {
				case 1:
					resultado = null;
					break;
				case 11:
					resultado.put ( key++, tipoDeDato.DOBLE );	//
					resultado.put ( key++, tipoDeDato.DOBLE );	//
					resultado.put ( key++, tipoDeDato.DOBLE );	//
					resultado.put ( key++, tipoDeDato.DOBLE );	//
					resultado.put ( key++, tipoDeDato.CADENA );	//
					break;
			}
			return resultado;
		}
	}

	enum error {
		SIN_ERROR(0),
		NO_SE_CARGO_LOCAL(1),
		NO_SE_CARGO_WEB(2)
		;
		int err = 0;
		private error ( int e ){
			err = e;
		}
		private int getCodError (){
			int resultado = 0;
			switch ( err ) {
				case 0:
					resultado = 0;
					break;
				case 1:
					resultado = 1;
					break;
				case 2:
					resultado = 2;
					break;
			}
			return resultado;
		}
		private String getDescripcionDelError(){
			String resultado = "";
			switch ( err ) {
				case 0:
					resultado = "No hay error";
					break;
				case 1:
					resultado = "No se pudo cargar la base local";
					break;
				case 2:
					resultado = "No se pudo cargar la base web";
					break;

			}
			return resultado;
		}
	}
	
	enum flujo{
		INICIO(1),
		CARGA_LOCAL(2),
		CONVIERTE_LOCAL(3),
		CARGA_WEB(4),
		COMPARA_NVOS_WEB_VS_LOC(5),
		SUBE_REGISTROS_NUEVOS(6),
		COMPARA_BORRADOS_LOCAL_VS_WEB(7),
		BORRA_REGISTROS_EN_WEB(8),
		COMPARA_CAMBIOS_LOC_VS_WEB(9),
		ACTUALIZA_CAMBIOS_EN_WEB(10),
		FINALIZA(11)
		;
		
		int pro = 0; 
		private flujo ( int p ){
			pro = p;
		}
		
		public String getDescripcionProceso(){
			String resultado ="";
			switch ( pro ) {
				case 1:
					resultado = "Inicio";
					break;
				case 2:
					resultado = "Carga tabla local";
					break;
				case 3:
					resultado = "Convierte tabla local";
					break;
				case 4:
					resultado = "Carga tabla web";
					break;
				case 5:
					resultado = "Busca registros nuevos en local";
					break;
				case 6:
					resultado = "Sube registros nuevos";
					break;
				case 7:
					resultado = "Buscar registros borrados en local";
					break;
				case 8:
					resultado = "Borra registros en web";
					break;
				case 9:
					resultado = "Compara cambios de local en web";
					break;
				case 10:
					resultado = "Actualiza tabla web";
					break;
				case 11:
					resultado = "Fin";
					break;
				
				
			}
			return resultado;
		}
		public int getNumeroDeProceso(){
			return pro;
		}
		
		
	}
	
	enum est{
		ERROR(1),
		OK(2)
		;
		int i = 2;
		private est( int n ){
			i = n;
		}
		public boolean getBolEstado(){
			boolean resultado = false;
			switch ( i ) {
				case 1:
					resultado = false;
					break;
				case 2:
					resultado = true;
					break;
			}
			return resultado;
		}
	}
	
	enum base {
		LOCAL,
		REMOTA
		;
	}
	
	HashMap <Integer, String> flujoDeInformacion = new HashMap <Integer, String> ( );
	int numeroDeProcesos = flujo.values ( ).length;
	
	String proceso = "";
	String sentenciaSelectLocal = "";
	String sentenciaSelectWeb 	= "";
	String sentenciaInsertWeb 	= "";
	String sentenciaUpdateWeb 	= "";
	
	HashMap <Integer,tipoDeDato> 	datoDeColumnaIns = new HashMap <Integer, Utilidades.tipoDeDato> ( );
	HashMap <Integer, tipoDeDato>	datoDeColumnaUpd = new HashMap <Integer, Utilidades.tipoDeDato> ( );

	HashMap <String, ArrayList <String>> tablaLoc		= null;
	HashMap <String, ArrayList <String>> tablaWeb		= null;
	HashMap <String, ArrayList <String>> tablaRes		= null;
	
	boolean estado 			= false;
	int		codError 		= 0;
	String  desError		= "";
	String  mensaje			= "";
	
	ArrayList <String> log 	= new ArrayList <String> ( );
	
	public boolean getEstado(){
		return estado;
	}
	public int getCodigoDeError(){
		return codError;
	}
	public String getDescripcionDeError() {
		return desError;
	}
	public String getMensaje(){
		return mensaje;
	}
	public ArrayList<String> getLog(){
		return log;
	}
	
	public ReplicaLocalToWeb( proceso p ){
		EjecutaProceso ( p );
	}
		
	void EjecutaProceso ( proceso d ) {
		
		tablas local = getTabla( d, base.LOCAL );
		tablas web	 = getTabla( d, base.REMOTA );
 		
		ConsultaODBC cLoc = new ConsultaODBC(Conecciones.getERPCon ( ));
		ConsultaODBC cWeb = new ConsultaODBC( Conecciones.getMySQLRemoto ( ));
		
		setEstado ( est.OK.getBolEstado ( ), error.SIN_ERROR, flujo.INICIO, "Inicio Carga local" );
		
		// Carga registros
		tablaLoc = cLoc.ConsultaVariasColumnasMySQL ( local.getSelect ( ) );
		
		if ( cLoc.getResultado ( ) ) {
			setEstado ( est.OK.getBolEstado ( ), error.SIN_ERROR, flujo.CARGA_LOCAL, "Fin Carga local" );
		} else {
			setEstado ( est.ERROR.getBolEstado ( ), error.NO_SE_CARGO_LOCAL, flujo.CARGA_LOCAL, "Fin Carga - " + cLoc.getError ( ) );
			return;
		}
		
		tablaWeb = cWeb.ConsultaVariasColumnasMySQL ( web.getSelect ( ) );
		
		if ( cWeb.getResultado ( ) ) {
			setEstado ( est.OK.getBolEstado ( ), error.SIN_ERROR, flujo.CARGA_WEB, "" );
		} else {
			setEstado ( est.ERROR.getBolEstado ( ), error.NO_SE_CARGO_WEB, flujo.CARGA_WEB, cLoc.getError ( ) );
			return;
		}
		
		// Transforma la base de datos
		
		// Busca nuevos productos
		
		// Elimina registros
		
		// Actualiza registros
		
		
		
		setEstado ( est.OK.getBolEstado ( ), error.SIN_ERROR, flujo.FINALIZA, "" );
	}
	
	void setEstado( boolean es, error er, flujo fl, String me ){

		estado 		= es;
		codError 	= er.getCodError ( );
		desError 	= er.getDescripcionDelError ( );
		mensaje		= me;
		
		log.add ( fl.getDescripcionProceso ( ) );
		
		Utilidades.MuestraInformacion ( er.getDescripcionDelError ( ), "Replica ", nivel.INFO, dondeSeMuestra.AMBOS );
		
	}
	
	tablas getTabla( proceso p, base b ){
		tablas resultado = tablas.LOCAL_ARTICULOS;
		if ( b == base.LOCAL ){
			// Regresa las bases locales
			switch ( p ) {
				case EXISTENCIAS:
					resultado = tablas.LOCAL_EXISTENCIAS;
					break;
				case ARTICULOS:
					resultado = tablas.LOCAL_ARTICULOS;
					break;
				case CLIENTES:
					resultado = tablas.LOCAL_CLIENTES;
					break;
				case SALDOS:
					resultado = tablas.LOCAL_SALDOS;
					break;
			}
		} else {
			// Regresa las bases del web
			switch ( p ) {
				case EXISTENCIAS:
					resultado = tablas.WEB_EXISTENCIAS;
					break;
				case ARTICULOS:
					resultado = tablas.WEB_ARTICULOS;
					break;
				case CLIENTES:
					resultado = tablas.WEB_CLIENTES;
					break;
				case SALDOS:
					resultado = tablas.WEB_SALDOS;
					break;
			}
		}
		return resultado;
	}
	
}
