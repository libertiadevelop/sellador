package com.bmx.srv;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import com.bmx.srv.Utilidades.sucursales;

public class FileSystem
{
	private ArrayList <String> archivos = new ArrayList <String> ( );
	
	public ArrayList <String> getFiles()
	{
		limpiaArchivos ( );
		
		for (sucursales suc : sucursales.values ( ) ){
			String fldClave = "fld_" + suc.getAbreviacion ( ).toLowerCase ( );
			String folder	= Utilidades.RecuperaRecurso ( fldClave );
			
			File fldEnFileSystem = new File(folder);
				
	
			for ( File doc : fldEnFileSystem.listFiles (  ) ){
				
				boolean esArchivo 				= !doc.isDirectory ( );
				
				if ( esArchivo ){
					if ( ElArchivoSeDebeSubir(doc.getPath ( ) )) {
						String nombre 	= doc.getName ( );
						
						archivos.add ( nombre );
						
					}
				}
				
			}
			
		}
		
		return archivos;
	}
	
	private boolean ElArchivoSeDebeSubir( String path ){
		
		boolean resultado = false;
		
		String [] 	extencionesValidas 	= {"xml", "pdf"};
		String 		extencion			= "";
		
		int indexExtencion = path.lastIndexOf ( '.' );
		
		if ( indexExtencion > 0 ) {
			extencion = path.substring ( indexExtencion + 1 ).toLowerCase ( );
			
			resultado = Arrays.asList ( extencionesValidas ).contains ( extencion );
			
		}
		
		return resultado;
	}
	
	private void limpiaArchivos()
	{
		archivos.clear ( );
	}
}
