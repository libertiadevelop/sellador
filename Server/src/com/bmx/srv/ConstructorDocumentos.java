package com.bmx.srv;

import com.bmx.srv.Utilidades.tipoDeDocumento;

public class ConstructorDocumentos
{
	//region VARIABLES PRIVADAS
	private String 	cliente;
	private String 	folio;
	private String 	serie;
	private String 	sucursal;
	private int 	NumSucursal;
	private String 	cveSucursal;
	
	private String NombreDeDocumento;
	//endregion
	
	//region PROPIEDADES
	
	public String getNombreDeDocumento ( ) {
		return NombreDeDocumento;
	}
	
	//endregion
	
	


	//region CONSTRUCTORES
	public ConstructorDocumentos( String Cliente, String Documento, tipoDeDocumento t )
	{
		cliente 	= Cliente;
		cveSucursal	= Documento.substring ( 0, 2 ) ;
		folio		= Documento.substring ( 2, 8 );
		
		NumSucursal = Integer.valueOf ( cveSucursal );
		
		switch ( t ) {
			case FACTURA:
				serie = "FAC";
				break;
			case DEVOLUCION:
				serie = "DEV";
				break;
			case NOTA_DE_CARG0:
				serie = "CAR";
				break;
			case NOTA_DE_CREDITO_B:
				serie = "CRE";
			case NOTA_DE_CREDITO_PP:
				serie = "CRE";
				break;
		}
		
		switch ( NumSucursal ) {
			case 1:
				sucursal = "MEX";
				break;
			case 2:
				sucursal = "MTY";
				break;
			case 3:
				sucursal = "GDA";
				break;
			case 4:
				sucursal = "HMO";
				break;
			case 5:
				sucursal = "MXI";
				break;
		}

		NombreDeDocumento = ( cliente + folio + serie + sucursal );
	}
	//endregio
	
}
