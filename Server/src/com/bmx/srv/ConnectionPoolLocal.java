package com.bmx.srv;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Vector;
import static com.bmx.srv.Utilidades.*;


public class ConnectionPoolLocal 
{
	private Vector<Connection> 	conLibres;
	private Vector<Connection> 	conUsadas;
	
	private String url;
	private String driver;
	private String usuario;
	private String password;
	private String Base;
	private String urlMysql;
	private String ip;
	private String puerto;
	
	private int minSize;
	private int maxSize;
	private int step;
	
	ArrayList<String> 			descripcion = new ArrayList<String>();
	private static ConnectionPoolLocal pool = null;
	
	private boolean continuar = true;
	
	private ConnectionPoolLocal()
	{
		String Cliente = "Pool local iniciando";
		
		ResourceBundle rb = ResourceBundle.getBundle( "recursos" );
		ip 			= rb.getString( "bdl_host" );
		puerto		= rb.getString( "bdl_puerto" );
		url 		= rb.getString( "bdl_url" );
		Base 		= rb.getString( "bdl_base" );
		driver 		= rb.getString( "bdl_driver" );
		usuario		= rb.getString( "bdl_user" );
		password	= rb.getString( "bdl_password" );
		
		minSize		= Integer.parseInt( rb.getString( "bdl_min" ) );
		maxSize		= Integer.parseInt( rb.getString( "bdl_maz" ) );
		step		= Integer.parseInt( rb.getString( "bdl_ste" ) );
		
		conLibres = new Vector<Connection>();
		conUsadas = new Vector<Connection>();
		
		try{
			
			MuestraInformacion( "Iniciando connection pool local.", Cliente, nivel.INFO , dondeSeMuestra.AMBOS );
			Class.forName( driver );
			MuestraInformacion( "Clase cargada correctamente.", Cliente, nivel.INFO , dondeSeMuestra.AMBOS );
			urlMysql = url + ip + ":" + puerto + "/" + Base;
			MuestraInformacion( "Generando primeras conexiones.", Cliente, nivel.INFO , dondeSeMuestra.AMBOS );
			continuar = true;
			_instanciar( minSize );
			
		} catch ( Exception e ){
			descripcion.clear();
			descripcion.add( "No se pudo inciar el pool local." );
			descripcion.add( e.toString() );
			continuar = false;
			MuestraInformacion( descripcion, Cliente, nivel.ERROR , dondeSeMuestra.AMBOS );
		}			
	}
	
	private void _instanciar ( int n )
	{
		if ( !continuar ) return;
		String Cliente = "Pool local Instanciando";
		try{
			Connection  con;
			for ( int i = 0; i < n; i++ ){
				MuestraInformacion( "Instanciando conexion " + i + " de " + n, Cliente, nivel.INFO , dondeSeMuestra.AMBOS );
				con	= DriverManager.getConnection( urlMysql, usuario, password );
				con.setAutoCommit( false );
				conLibres.add( con );
			}	
			
			MuestraInformacion( "Se crearon " + n + " nuevas conexiones.", Cliente, nivel.INFO , dondeSeMuestra.AMBOS );
			
		} catch ( Exception  e ){
			descripcion.clear();
			descripcion.add( "No se pudieron crear las conexiones" );
			descripcion.add( e.toString() );
			MuestraInformacion( descripcion, Cliente, nivel.ERROR , dondeSeMuestra.AMBOS );
			continuar = false;
		}
	}
	
	public synchronized static ConnectionPoolLocal getPool()
	{
		if ( pool == null ){
			pool = new ConnectionPoolLocal();
		}
		return pool;
	}

	public synchronized Connection getConnection()
	{
		String Cliente = "Pool local llamando conexiones";
		if ( !continuar ) return null;
		//_ValidaConecciones();
		if ( conLibres.size() == 0 ){
			if ( !_crearMasConexiones() ){
				MuestraInformacion( "No se pudieron crear mas conexiones.", Cliente, nivel.ERROR , dondeSeMuestra.AMBOS );
				continuar = false;
			}
		}
		
		Connection con = conLibres.remove( 0 );
		conUsadas.add( con );
		return con;
	}
	
	private boolean _crearMasConexiones()
	{
		String Cliente = "Pool local creando";
		int actuales = conLibres.size() + conUsadas.size();
		int n = Math.min( maxSize - actuales, step );
		
		if ( n > 0 ) {
			MuestraInformacion( "Creando mas conexiones.", Cliente, nivel.INFO , dondeSeMuestra.AMBOS );
			_instanciar( n );
		}
		
		return n > 0;
	}
	
	public synchronized void releaseConnection ( Connection con )
	{
		String Cliente = "Pool local liberando";
		boolean ok = conUsadas.remove( con  );
		if ( ok ){
			conLibres.add( con );
		} else {
			MuestraInformacion( "Esta devolviendo una conexion que no es del server.", Cliente, nivel.ERROR , dondeSeMuestra.AMBOS );
		}
		
	}
	
	public synchronized void close()
	{
		String Cliente = "Cerrando conexiones";
		try{
			for ( Connection con : conLibres ){
				con.close();
			}
			
			for ( Connection con : conUsadas ){
				con.close();
			}
		} catch ( Exception e ){
			descripcion.clear();
			descripcion.add( "No se pudieron cerrar todas las conexiones");
			descripcion.add( e.toString() );
			MuestraInformacion( descripcion, Cliente, nivel.ERROR , dondeSeMuestra.AMBOS );
		}
	}
	
	
	
}
