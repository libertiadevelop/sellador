package com.bmx.srv;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import sun.jdbc.odbc.OdbcDef;

import com.bmx.srv.Utilidades.dondeSeMuestra;
import com.bmx.srv.Utilidades.nivel;
import com.bmx.srv.SshManagment;

public class ReplicaSSH
{
	String manejador 	= "Replica";
	String modulo;
	
	boolean estado;
	int		codError;
	String	desError;
	String	mensaje;
	
	ArrayList <String> log = new ArrayList <String> ( );
	
	public boolean getEstado(){
		return estado;
	}
	public int getCodError(){
		return codError;
	}
	public String getDescripcionDelError(){
		return desError;
	}
	public String getMensaje(){
		return mensaje;
	}
	public ArrayList <String> getLog(){
		return log;
	}
	
	private enum error{
		SIN_ERROR(0),
		PATH_VACIO(1)
		;
		int er;
		private error( int e ){
			er = e;
		}
		public int getCodError(){
			return er;
		}
		public String getDescripcionDelError() {
			String resultado = "";
			switch ( er ) {
				case 0:
					resultado = "Sin error";
					break;
				case 1:
					resultado = "Path vacio";
					break;
			}
			return resultado;
		}
		
	}

	private enum flujo{
		INICIANDO(0),
		RECUPERA_ARCHIVO_DE_LOCAL(1),
		RECUPERA_ARCHIVO_DE_REMOTO(2),
		COMPARA(3),
		ELIMINA_ARCHIVO_DE_LOCAL(4),
		GUARDA_ARCHIVO_A_LOCAL(5)
		;
		int f;
		private flujo(int fl){
			f = fl;
		}
		public String getDescripcionDelFlujo(){
			String resultado ="";
			switch ( f ) {
				case 0:
					resultado = "Iniciando";
					break;
				case 1:
					resultado = "Recuperando informacion del archivo local";
					break;
				case 2:
					resultado = "Recuperando informacion del server";
					break;
				case 3:
					resultado = "Compara registros";
					break;
				case 4:
					resultado = "Elimina archivo de local";
					break;
				case 5:
					resultado = "Guardando archivo de remoto a local.";
					break;
			}
			
			return resultado;
		}
		
		
	}

	private enum est{
		ERROR(1),
		OK(2)
		;
		int i = 2;
		private est( int n ){
			i = n;
		}
		public boolean getEstado(){
			boolean resultado = false;
			switch ( i ) {
				case 1:
					resultado = false;
					break;
				case 2:
					resultado = true;
					break;
			}
			return resultado;
		}
	}

	File 	enLocal 	= null;
	boolean existeLocal = false;
	
	HashMap <String, ArrayList <String>> datosEnLocal 	= new HashMap <String, ArrayList<String>> ( );
	HashMap <String, ArrayList <String>> datosEnServer	= new HashMap <String, ArrayList<String>> ( );
	HashMap <String, ArrayList <String>> datosResultado	= new HashMap <String, ArrayList<String>> ( );
	
	public ReplicaSSH( String pathServer, String pathLocal ){
		String modulo = "Constructor";
		if ( pathServer.isEmpty ( ) ){
			setEstado ( est.ERROR.getEstado ( ), error.PATH_VACIO, flujo.RECUPERA_ARCHIVO_DE_REMOTO, "", modulo );
		}
		if ( estado ){
			enLocal = validaFile ( pathLocal, flujo.RECUPERA_ARCHIVO_DE_LOCAL, modulo );	
			existeLocal = enLocal.exists ( ) ? true : false;
		}
	}
	
	public void RecuperaInformacion(){
		if ( !estado ){
			return;
		}
	
		// Recupera de local
		
		if ( existeLocal ) {
			XDBC x = new XDBC ( "exi.txt" );
			datosEnLocal = x.getEstado ( ) ? x.getArreglo ( ) : null;
		} 
		
		SshManagment s = new SshManagment ( "192.168.10.252", "22" );
		s.EjecutandoProceso ( "export LD_LIBRARY_PATH=\"/XDBC_8/bin\";export GENESIS_HOME=\"/XDBC_8\"; /XDBC_8/bin/asql.sh -r /srv/sentenciaExistencias.txt > /srv/exi.txt");
		if ( s.estado ) {
			s.RecuperaArchivo ( "/srv/exi.txt", "exi.txt" );
		}
		
		if ( s.estado ){
			XDBC x = new XDBC ( "exi.txt" );
			datosEnServer = x.getEstado ( ) ? x.getArreglo ( ) : null;
		}

		ArrayList <String> llaves = new ArrayList <String> ( );
		llaves.add ( "1" );
		llaves.add ( "2" );
		
		if ( existeLocal ) {
			
			datosResultado = ConsultaODBC.comparaOrigenVsCopia ( datosEnLocal, datosEnServer, llaves);
			
			if ( datosResultado.size ( ) > 0 ){
				ConsultaODBC c = new ConsultaODBC( Conecciones.getMySQLRemoto ( ) );
				//c.updateRegistrosMySQL ( sentencia, documentos, datoDeColumna, Batch );
			}

		}
		
	}
	
	public void ComparaInformacion() {
		if ( !estado ){
			return;
		}
		
		XDBC x = new XDBC ( "exi.txt" );
		
	}
	
	void setEstado( boolean es, error er, flujo fl, String me, String modulo ){

		estado 		= es;
		codError 	= er.getCodError ( );
		desError 	= er.getDescripcionDelError ( );
		mensaje		= me;
		
		log.add ( fl.getDescripcionDelFlujo ( ) );
		
		String mensajeFinal = er.getDescripcionDelError ( ) + " - " + me;
		String cliente 		= manejador + "-" + modulo;
		
		Utilidades.MuestraInformacion ( mensajeFinal, cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
	}

	File validaFile( String path, flujo f, String modulo ){
		File resultado = null;
		
		if ( path.isEmpty ( ) ){
			setEstado ( est.ERROR.getEstado ( ), error.PATH_VACIO,f, "El path fue: " + path, modulo );
		}
		if ( estado ){
			resultado = new File ( path );
		}
		
		return resultado;
	}
}