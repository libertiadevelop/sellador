package com.bmx.srv;

import java.util.HashMap;
import java.util.ResourceBundle;

public class Documento 
{
	//region VARIABLES PRIVADAS
	ResourceBundle nombres = ResourceBundle.getBundle( "recursos" );
	
	private HashMap<String, String> datos 	= new HashMap<String, String>();
	private String					version = "1.0.0.0";
	
	//endregion
	
	//region PROPIEDADES	
	public HashMap<String , String>  getDatos()
	{
		return datos;
	}
	
	public String getVersion()
	{
		return version;
	}
	//endregion

	//region METODOS PUBLICOS
	
	public void RecuperaCampos ( String base )
	{
		_RecuperaCamposBase( base );
	}
	public void RecuperaCampos ( String base, String Calculados)
	{
		_RecuperaCamposBase( base );
		_RecuperaCamposCalculados( Calculados );
	}
	//endregion

	
	//region METODOS PRIVADOS
	public HashMap<String, Integer> RecuperaRango( String informacion, boolean tomarSegundaOpcion )
	{
		HashMap<String, Integer> resultadoDeLaOperacion = new HashMap<String, Integer>();
		
		resultadoDeLaOperacion.put( "ini", 0 );
		resultadoDeLaOperacion.put( "fin", 0 );
		
		String cadenaADescomponer = informacion;
		
		if ( informacion.indexOf( "*" ) > 0 ){
			if ( tomarSegundaOpcion ){
				cadenaADescomponer = cadenaADescomponer.substring( cadenaADescomponer.indexOf( "*" ) + 1 );
			} else {
				cadenaADescomponer = cadenaADescomponer.substring( 0, cadenaADescomponer.indexOf( "*" ) );
			}
		}
		
		
		resultadoDeLaOperacion.put("ini", Integer.parseInt( cadenaADescomponer.substring( 0, cadenaADescomponer.indexOf("-") ) ) );
		resultadoDeLaOperacion.put("fin", Integer.parseInt( cadenaADescomponer.substring( cadenaADescomponer.indexOf("-") + 1 ) ) );
		
		return resultadoDeLaOperacion;

	}
	
	public void _RecuperaCamposBase(String cadenaRecivida) 
	{
		String 	cadenaDeLlaves 				= nombres.getString( "datos" );
		String 	cadenaDelimitadores 		= nombres.getString( "datos_delimitador" );
		String 	condicionalDelimitadores 	= "";
		boolean	tomarSegundaOpcion			= false;
		
		String[] llaves 		= cadenaDeLlaves.split( "," ); 
		
		
		if ( cadenaDelimitadores.indexOf( ":" ) > 0 ){
			
			condicionalDelimitadores	= cadenaDelimitadores.substring( cadenaDelimitadores.indexOf(":") + 1 );
			cadenaDelimitadores 		= cadenaDelimitadores.substring( 0, cadenaDelimitadores.indexOf( ":" ) - 1 );
			
			if ( cadenaRecivida.indexOf( condicionalDelimitadores ) > 0 ){
				tomarSegundaOpcion = true;
			} else {
				tomarSegundaOpcion = false;
			}
			
		}
		
		String[] delimitadores 	= cadenaDelimitadores.split( "," );
		
		// En una primera instancia es uno a uno.
		int lugarEnCadena = 0;
		for ( String llave : llaves) {
			
			int ini = RecuperaRango( delimitadores[lugarEnCadena], tomarSegundaOpcion ).get("ini");
			int fin = RecuperaRango( delimitadores[lugarEnCadena], tomarSegundaOpcion ).get("fin");
			
			datos.put( llave , cadenaRecivida.substring( ini, fin ) );
			lugarEnCadena++;
		}
	}
	
	public void _RecuperaCamposCalculados(String CadenaDeConstruccion) 
	{
		// Calcula campos en base a la información proporcionada.
		String[] CamposABuscar = CadenaDeConstruccion.split( "," );
		
		for( String campo : CamposABuscar ){
			// Se espera nombre:etiqueta_comodin
			
			String Etiqueta 	= campo.substring( 0, campo.indexOf( ":" ) );
			String SinEtiqueta 	= campo.substring( campo.indexOf( ":" ) + 1 );
			String comodin		= SinEtiqueta.substring( 0, SinEtiqueta.indexOf( "_" ) + 1 );
			String nombre		= SinEtiqueta.substring(  SinEtiqueta.indexOf( "_" ) + 1 );
			
			// Consulta al resource, siempre en minusculas
			
			String Valor = nombres.getString( comodin + datos.get(nombre).toLowerCase() );
			
			datos.put( Etiqueta, Valor );
		}
	}
	//endregion


}
