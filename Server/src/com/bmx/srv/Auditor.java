package com.bmx.srv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.bmx.srv.Utilidades.basesDeAuditor;
import com.bmx.srv.Utilidades.componentesDocumentoFiscal;
import com.bmx.srv.Utilidades.dondeSeMuestra;
import com.bmx.srv.Utilidades.nivel;
import com.bmx.srv.Utilidades.sucursales;
import com.bmx.srv.Utilidades.tipoDeDato;
import com.bmx.srv.Utilidades.tipoDeDocumento;

public class Auditor
{
	private ArrayList <String> documentosERP 			= new ArrayList <String> ( );
	private ArrayList <String> documentosEnBaseDeDatos 	= new ArrayList <String> ( );
	private ArrayList <String> documentosEnBaseRemota	= new ArrayList <String> ( );
	private ArrayList <String> documentosFaltantesEnBD	= new ArrayList <String> ( );

	private ArrayList <String> documentosEnFileSystem	= new ArrayList <String> ( );

	private String cliente								= "Auditor ";
	
	private void _recuperaDocumentosERP( tipoDeDocumento td, String sucursal, String Fecha )
	{ 
		String sentenciaSQL = "SELECT LV_NUMCLI, LV_NUMDOC FROM MDGCLI06 WHERE LV_TIPREG = " + String.valueOf( td.getClave( ) ) + " AND LV_FECH = " + Fecha ;
		
		if ( sucursal.length ( ) > 0 ) {
			sentenciaSQL = sentenciaSQL + " AND LV_NUMDOC LIKE '" + sucursal + "%'";
		}
		
		ConsultaODBC consulta = new ConsultaODBC( Conecciones.getERPCon( ) );
		
		HashMap <String, ArrayList <String>> resultado = consulta.ConsultaVariasColumnasODBC ( sentenciaSQL );
		
		//TODO Si el resultado es null, salir.
		
		for ( String llave : resultado.keySet ( ) ) {
			ArrayList <String> valoresEnRenglon = resultado.get ( llave );
			
			String cliente 		= valoresEnRenglon.get ( 0 );
			String documento	= valoresEnRenglon.get ( 1 );
			
			ConstructorDocumentos c = new ConstructorDocumentos ( cliente, documento, td );
			
			documentosERP.add ( c.getNombreDeDocumento ( ) );
			
		}
		
	}
	
	private void _recuperaDocumentosFacturacionLocal( tipoDeDocumento td, String sucursal, String Fecha )
	{
		String sentenciaSQL 	= "SELECT DISTINCT REPLACE(REPLACE(nombre, '.pdf', ''),'_S.xml','' ) FROM facturas " ;
		
		String sentenciaWHERE	= "WHERE " + RecuperaWhereFechaMysql ( Fecha ) + " AND sl_numdoc LIKE '" + sucursal + "%' ";
		
		
		
		switch ( td ) {
			case FACTURA:
				sentenciaWHERE = sentenciaWHERE + "AND sl_tipmov = 1 ";
				break;
			default:
				sentenciaWHERE = sentenciaWHERE + "AND sl_tipmov = 1 ";
				break;
		}
		
		sentenciaSQL = sentenciaSQL + sentenciaWHERE;
		
		ConsultaODBC consulta = new ConsultaODBC ( Conecciones.getMySQLLocalFacturas ( ) );
		
		HashMap <String, ArrayList <String>> resultado = consulta.ConsultaVariasColumnasODBC ( sentenciaSQL );
		
		for ( String llave : resultado.keySet ( ) ) {
			ArrayList <String> valoresEnRenglon = resultado.get ( llave );
			
			String doc 		= valoresEnRenglon.get ( 0 );
			
			documentosEnBaseDeDatos.add ( doc );
			
		}
		
	}
	
	private void _recuperaDocumentosFacturacionRemota( tipoDeDocumento td, String sucursal, String Fecha )
	{
		String sentenciaSQL 	= "SELECT DISTINCT REPLACE(REPLACE(nombre, '.pdf', ''),'_S.xml','' ) FROM facturas " ;
		String sentenciaWHERE	= "WHERE " + RecuperaWhereFechaMysql ( Fecha ) + " AND sl_numdoc LIKE '" + sucursal + "%' ";
		switch ( td ) {
			case FACTURA:
				sentenciaWHERE = sentenciaWHERE + "AND sl_tipmov = 1 ";
				break;
			default:
				sentenciaWHERE = sentenciaWHERE + "AND sl_tipmov = 1 ";
				break;
		}
		
		sentenciaSQL = sentenciaSQL + sentenciaWHERE;
		
		ConsultaODBC consulta = new ConsultaODBC ( Conecciones.getMySQLRemoto ( ) );
		
		HashMap <String, ArrayList <String>> resultado = consulta.ConsultaVariasColumnasODBC ( sentenciaSQL );
		
		for ( String llave : resultado.keySet ( ) ) {
			ArrayList <String> valoresEnRenglon = resultado.get ( llave );
			
			String doc 		= valoresEnRenglon.get ( 0 );
			
			documentosEnBaseRemota.add ( doc );
			
		}
		
	}

	
	private void _recuperaDocumentosLocal()
	{
		documentosEnBaseDeDatos.clear ( );
		
		String sentenciaSQL 	= "SELECT nombre FROM facturas " ;
		
		String sentenciaWHERE	= "";
		
		sentenciaSQL = sentenciaSQL + sentenciaWHERE;
		
		ConsultaODBC consulta = new ConsultaODBC ( Conecciones.getMySQLLocalFacturas ( ) );
		
		HashMap <String, ArrayList <String>> resultado = consulta.ConsultaVariasColumnasMySQL ( sentenciaSQL );
		
		for ( String llave : resultado.keySet ( ) ) {
			
			ArrayList <String> valoresEnRenglon = resultado.get ( llave );
			
			documentosEnBaseDeDatos.addAll ( valoresEnRenglon );
		}
		
	}
	
	private void _recuperaDocumentosRemoto()
	{
		documentosEnBaseRemota.clear ( );
		
		String sentenciaSQL 	= "SELECT nombre FROM facturas " ;
		
		String sentenciaWHERE	= "";
		
		sentenciaSQL = sentenciaSQL + sentenciaWHERE;
		
		ConsultaODBC consulta = new ConsultaODBC ( Conecciones.getMySQLRemoto ( ) );
		
		HashMap <String, ArrayList <String>> resultado = consulta.ConsultaVariasColumnasMySQL ( sentenciaSQL );
		
		for ( String llave : resultado.keySet ( ) ) {
			
			ArrayList <String> valoresEnRenglon = resultado.get ( llave );
			
			documentosEnBaseRemota.addAll ( valoresEnRenglon );
		}
	}
	
	private void _comparaBaseLocalVsBaseRemota()
	{
		documentosFaltantesEnBD.clear ( );
		documentosFaltantesEnBD.addAll ( documentosEnBaseDeDatos );
	}
	
	private String RecuperaWhereFechaMysql ( String fecha )
	{
		String WhereFecha = "";
		switch ( fecha.length ( ) ) {
			case 10:
				WhereFecha = " fecha = " + fecha + " ";
				break;
			case 7:
				WhereFecha = " MONTH(fecha) = " + fecha.substring ( 5,7 ) + " AND YEAR(fecha) = " + fecha.substring ( 0,4 ) + " ";
				break;
			case 4:
				WhereFecha = " YEAR(fecha) = " + fecha.substring ( 0,4 ) + " ";
				break;
			default:
				WhereFecha = " fecha = " + fecha + " ";
				break;
		}
		
		return WhereFecha;
	}
	
	//TODO DEVOLUCIONES, NOTAS DE CARGO Y CREDITO
	
	public HashMap<String, String> FacturasErpVsFactura( Date d , String Sucursal )
	{
		String Cliente = cliente + "ERPvsDB"; 
		HashMap <String, String> resultado = new HashMap <String, String> ( );
		
		Utilidades.MuestraInformacion ( "Cargando documentos de erp", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		_recuperaDocumentosERP ( tipoDeDocumento.FACTURA, Sucursal, Utilidades.getFechaERP ( d ) );
		Utilidades.MuestraInformacion ( "Cargando documentos de BD", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		_recuperaDocumentosFacturacionLocal ( tipoDeDocumento.FACTURA, Sucursal, Utilidades.getFechaMySQL ( d ) );
		
		
		Utilidades.MuestraInformacion ( "Comparando", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		documentosFaltantesEnBD.addAll ( documentosERP );
		documentosFaltantesEnBD.removeAll ( documentosEnBaseDeDatos );
		
		Utilidades.MuestraInformacion ( "Reportando diferencias", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		for ( String doc : documentosFaltantesEnBD ) {
			
			// Valida que la llave no se este duplicando.
			
			if ( !resultado.containsKey ( doc ) ){
				resultado.put ( doc, "11" );
			}
			
		}
		
		return resultado;
	}
	
	public HashMap <String, String> FacturasLocVsFacturasRem ( Date d, String Sucursal )
	{
		String Cliente = cliente + "LocVsRem"; 
		HashMap <String, String> resultado = new HashMap <String, String> ( );
		
		Utilidades.MuestraInformacion ( "Cargando documentos de Local", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		_recuperaDocumentosFacturacionLocal ( tipoDeDocumento.FACTURA, Sucursal, Utilidades.getFechaMySQL ( d ) );
		Utilidades.MuestraInformacion ( "Cargando documentos de BD", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		_recuperaDocumentosFacturacionRemota ( tipoDeDocumento.FACTURA, Sucursal, Utilidades.getFechaMySQL ( d ) );
		
		
		Utilidades.MuestraInformacion ( "Comparando - " + Utilidades.getFechaMySQL ( d ), Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		documentosFaltantesEnBD.addAll ( documentosEnBaseDeDatos );
		documentosFaltantesEnBD.removeAll ( documentosEnBaseRemota );
		
		Utilidades.MuestraInformacion ( "Reportando diferencias - " + Utilidades.getFechaMySQL ( d ), Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		for ( String doc : documentosFaltantesEnBD ) {
			
			// Valida que la llave no se este duplicando.
			
			if ( !resultado.containsKey ( doc ) ){
				resultado.put ( doc, "1" );
			}
			
		}
		
		return resultado;
	}
	
	public HashMap <String, String> FacturasLocVsFacturasRem ( String Fecha, String Sucursal )
	{
		String Cliente = cliente + "LocVsRem"; 
		HashMap <String, String> resultado = new HashMap <String, String> ( );
		
		Utilidades.MuestraInformacion ( "Cargando documentos de Local", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		_recuperaDocumentosFacturacionLocal ( tipoDeDocumento.FACTURA, Sucursal, Fecha );
		Utilidades.MuestraInformacion ( "Cargando documentos de BD", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		_recuperaDocumentosFacturacionRemota ( tipoDeDocumento.FACTURA, Sucursal, Fecha );
		
		
		Utilidades.MuestraInformacion ( "Comparando", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		documentosFaltantesEnBD.addAll ( documentosEnBaseDeDatos );
		documentosFaltantesEnBD.removeAll ( documentosEnBaseRemota );
		
		Utilidades.MuestraInformacion ( "Reportando diferencias", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		for ( String doc : documentosFaltantesEnBD ) {
			
			// Valida que la llave no se este duplicando.
			
			if ( !resultado.containsKey ( doc ) ){
				resultado.put ( doc, "11" );
			}
			
		}
		
		return resultado;
	}
	
	public HashMap <String, String> ValidaDocumentosErpVsBDLocal()
	{
		FileSystem fs = new FileSystem ( );
		
		ArrayList <String> DEFS = fs.getFiles ( );
		
		documentosEnFileSystem.addAll( DEFS );	
		
		documentosFaltantesEnBD.addAll ( documentosEnFileSystem );
		
		_recuperaDocumentosLocal ( );
		
		documentosFaltantesEnBD.removeAll ( documentosEnBaseDeDatos );
	
		return null;
	}
	
	public ArrayList <String> comparaBaseLocalVsBaseRemota()
	{
		_recuperaDocumentosLocal ( );
		_recuperaDocumentosRemoto ( );
		_comparaBaseLocalVsBaseRemota ( );
		
		return documentosFaltantesEnBD;
	}
	
	// Validacion de timbre fiscal
	
	public void CargaDatosValidacion ( tipoDeDocumento td, Date FechaInicial, Date FechaFinal)
	{
		String Cliente = "Carga Datos SAT";
		String fechaInicial = Utilidades.getFechaERP ( FechaInicial );
		String fechaFinal	= Utilidades.getFechaERP ( FechaFinal );
		
		String select			= "";
		String fechaConsulta 	= "";
		String documento 		= "";
		String filtro			= "";
		
		String espacio			= " ";
		String and				= espacio + "AND" + espacio;
		
		if ( fechaInicial.equals ( fechaFinal ) ) {
			// Solo es una fecha
			fechaConsulta = "FECH = " + fechaInicial;
		} else {
			// Es un rango de fechas
			fechaConsulta = "FECH BETWEEN " + fechaInicial + and + fechaFinal;
		} 
		
		
		switch ( td ) {
			case FACTURA:
				documento 		= "LV_TIPREG = " + String.valueOf ( td.getClave ( ) );
				fechaConsulta	= "LV_" + fechaConsulta;
				select			= "SELECT LV_TIPREG, LV_NUMDOC, LV_FECH, LV_IMPTOT*LV_TIPCAM, LV_CANC, LV_NUMCLI, LV_IVA*LV_TIPCAM FROM MDGCLI06 WHERE";
				break;
			case DEVOLUCION:
				documento 		= "LV_TIPREG = " + String.valueOf ( td.getClave ( ) );
				fechaConsulta	= "LV_" + fechaConsulta;
				select			= "SELECT LV_TIPREG, CONCAT(CONCAT(SUBSTR(LV_NUMDOC,1,2),LV_REFE),'10') AS LV_NUMDOC, LV_FECH, LV_IMPTOT*LV_TIPCAM, LV_CANC, LV_NUMCLI, LV_IVA*LV_TIPCAM FROM MDGCLI06 WHERE";
				break;
			case NOTA_DE_CARG0:
				documento 		= "LV_TIPREG = " + String.valueOf ( td.getClave ( ) );
				fechaConsulta	= "LV_" + fechaConsulta;
				select			= "SELECT LV_TIPREG, CONCAT(CONCAT(SUBSTR(LV_NUMDOC,1,2),LV_REFE),'10') AS LV_NUMDOC, LV_FECH, LV_IMPTOT*LV_TIPCAM, LV_CANC, LV_NUMCLI, LV_IVA*LV_TIPCAM FROM MDGCLI06 WHERE";
				break;
			case NOTA_DE_CREDITO_B:
				documento = 	"SL_TIPMOV = 20" + String.valueOf ( td.getClave ( ) );
				fechaConsulta	= "SL_" + fechaConsulta;
				select 			= "SELECT '3', SL_REFPAG, SL_FECH, SL_IMPO, '', SL_NUMCLI, SL_NUMDOC FROM MDGCLI05 WHERE";
				filtro			= espacio + and + espacio + "SL_REFPAG LIKE '%*C%'";
				fechaConsulta.replace ( "LV_", "SL_" );
				break;
			case NOTA_DE_CREDITO_PP:
				documento = 	"SL_TIPMOV = 208";//  + String.valueOf ( td.getClave ( ) );
				fechaConsulta	= "SL_" + fechaConsulta;
				select 			= "SELECT '3', SL_REFPAG, SL_FECH, SL_IMPO, '', SL_NUMCLI, SL_NUMDOC FROM MDGCLI05 WHERE";
				filtro			= espacio + and + espacio + "SL_REFPAG LIKE '%*C%'";
				fechaConsulta.replace ( "LV_", "SL_" );
				break;
			default:
				break;

		}
		
		String sentencia = select + espacio + documento + and + fechaConsulta + filtro;
	
		Utilidades.MuestraInformacion ( "Iniciando carga de " + td.getDescripcion ( ) , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		HashMap<String, ArrayList <String>> consulta = getdocumentos ( sentencia );
		
		switch ( td ) {
			case FACTURA:
				guardaDocumentosValidaSat ( td, consulta );
				break;
			case DEVOLUCION:
				guardaDocumentosValidaSat ( td, consulta );
				break;
			case NOTA_DE_CARG0:
				guardaDocumentosValidaSat ( td, consulta );
				break;
			case NOTA_DE_CREDITO_B:
				consulta = recuperaOtrosDatos ( td, consulta );
				guardaDocumentosValidaSat( td, consulta );
				break;
			case NOTA_DE_CREDITO_PP:
				consulta = recuperaOtrosDatos ( td, consulta );
				guardaDocumentosValidaSat( td, consulta );
				break;
			default:
				break;
		}
		
		Utilidades.MuestraInformacion ( "Terminando carga de " + td.getDescripcion ( ) , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
	}
	
	private HashMap <String, ArrayList <String>> recuperaOtrosDatos ( tipoDeDocumento td, HashMap <String, ArrayList <String>> consulta ) 
	{
		String Cliente = "Recupera datos";
		
		HashMap<String, ArrayList<String>> resultado = new HashMap <String, ArrayList<String>> ( );
		
		switch ( td ){
			case NOTA_DE_CREDITO_B:		
				Cliente += " Bonificacion";
			break;
		
			case NOTA_DE_CREDITO_PP:
				Cliente += " Pronto Pago";
			break;
			case NOTA_DE_CARG0:
				Cliente += " Cargo";
			break;
			case DEVOLUCION:
				break;
			case FACTURA:
				break;

		}
		
		Utilidades.MuestraInformacion ( "Recuperando informacion.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		ArrayList<String> foliosGuardados = new ArrayList<String>();
		
		// Se encarga de recuperar la informacion de numero de documento para notas de credito y cargo.
		for( String registro : consulta.keySet ( ) ){
			ArrayList<String> datos = consulta.get ( registro );
			
			// Valida si el registro tiene folio
			
			String folio = datos.get ( 1 );
			folio = folio.substring ( folio.indexOf ( "*C" ) + 2 );		
			
			if ( isNumeric(folio) ){
				// Es un numero procede a salvar el registro a resultado
				ArrayList<String> datosAGuardar = new ArrayList <String> ( );
				//    0            1          2              3              4       5           6
				//LV_TIPREG, 	LV_NUMDOC, LV_FECH, LV_IMPTOT*LV_TIPCAM, LV_CANC, LV_NUMCLI
				//3,  			SL_REFPAG, SL_FECH, SL_IMPO, 			 '', 	  SL_NUMCLI, SL_NUMDOC
				
				HashMap<String, String> d = Utilidades.descomponeDocumento ( datos.get(6) );
				
				folio = d.get ( "suc" ) + folio + "10";
				
				if ( ! foliosGuardados.contains ( folio ) ) { 
				
					foliosGuardados.add ( folio );
					
					datosAGuardar.add ( "3" );
					datosAGuardar.add ( folio );
					datosAGuardar.add ( datos.get ( 2 ) );
					datosAGuardar.add ( datos.get ( 3 ) );
					datosAGuardar.add ( " " );
					datosAGuardar.add ( datos.get ( 5 ) );
					datosAGuardar.add ( "0" );
					
					resultado.put ( registro, datosAGuardar );
				}
				
			}
			

		}
		
		Utilidades.MuestraInformacion ( "Finalizando recuperacion de informacion.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		return resultado;
	}

	private boolean isNumeric ( String folio ) {
		
	    for (char c : folio.toCharArray())
	    {
	        if (!Character.isDigit(c)) return false;
	    }
	    return true;
	}

	public void validaSat()
	{
		String Cliente = "ValidaSat";
		Utilidades.MuestraInformacion ( "Inicio - Acompleta informacion", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		acompletaInformacion ( );
		Utilidades.MuestraInformacion ( "Fin - Acompleta informacion", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		Utilidades.MuestraInformacion ( "Inicio - Recupera UUID", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		recuperaUUID ( );
		Utilidades.MuestraInformacion ( "Fin - Recupera UUID", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		Utilidades.MuestraInformacion ( "Inicio - Valida WEB", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		validaWEB ( );
		Utilidades.MuestraInformacion ( "Fin - Valida WEB", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
	}
	
	public boolean limpiaBasesDeAuditores( basesDeAuditor b )
	{
		String Cliente = "Limpia Bases Auditores";
		String sentencia = "";
		
		boolean r = false;
		
		Utilidades.MuestraInformacion ( "Iniciando la limpieza de bases de auditores", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		switch ( b ) {
			case TIMBRES:
				sentencia = "DELETE FROM BARAUD01";
				break;
		}
		
		Utilidades.MuestraInformacion ( "Limpiando base de " + b.getNameAuditor ( ) + " base " + b.getNameBaseAuditor ( ) , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		ConsultaODBC co = new ConsultaODBC( Conecciones.getMySQLLocalBarpro ( ) );
		
		if ( co.DeleteRegistrosMySQL ( sentencia ) ){
			Utilidades.MuestraInformacion ( "Se limpio la base", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			r = true;
			r = true;
		} else {
			Utilidades.MuestraInformacion ( "No se limpio la base", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
			r = false;
		}
		
		
		Utilidades.MuestraInformacion ( "Terminando la limpieza de bases de auditores", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		return r;
	}
	
	private HashMap<String, ArrayList <String>> getdocumentos( String consulta )
	{
		HashMap <String, ArrayList <String>> resultado = new HashMap <String, ArrayList<String>> ( );
		
		ConsultaODBC co = new ConsultaODBC ( Conecciones.getERPCon ( ) );
		
		resultado = co.ConsultaVariasColumnasODBC ( consulta );
			
		return resultado;
	}
		
	private void guardaDocumentosValidaSat ( tipoDeDocumento td, HashMap<String, ArrayList <String>> documentos )
	{
		String sentencia = "";
		switch ( td ) {
			case FACTURA:
				sentencia = "INSERT INTO BARAUD01 (sucursal, tipo_doc,cve_cliente,serie,folio,fecha,monto,estatus, IVA) VALUES (?,?,?,?,?,?,?,?,?)";
				guardaDocumentosSimple ( sentencia, documentos );
				break;
			case DEVOLUCION:
				sentencia = "INSERT INTO BARAUD01 (sucursal, tipo_doc,cve_cliente,serie,folio,fecha,monto,estatus, IVA) VALUES (?,?,?,?,?,?,?,?,?)";
				guardaDocumentosSimple ( sentencia, documentos );
				break;
			case NOTA_DE_CREDITO_B:
				sentencia = "INSERT INTO BARAUD01 (sucursal, tipo_doc,cve_cliente,serie,folio,fecha,monto,estatus, IVA) VALUES (?,?,?,?,?,?,?,?,?)";
				guardaDocumentosSimple ( sentencia, documentos );
				break;
			case NOTA_DE_CREDITO_PP:
				sentencia = "INSERT INTO BARAUD01 (sucursal, tipo_doc,cve_cliente,serie,folio,fecha,monto,estatus, IVA) VALUES (?,?,?,?,?,?,?,?,?)";
				guardaDocumentosSimple ( sentencia, documentos );
				break;
			case NOTA_DE_CARG0:
				sentencia = "INSERT INTO BARAUD01 (sucursal, tipo_doc,cve_cliente,serie,folio,fecha,monto,estatus, IVA) VALUES (?,?,?,?,?,?,?,?,?)";
				guardaDocumentosSimple ( sentencia, documentos );
				break;

		}
		
	}

	private void guardaDocumentosSimple( String sentencia, HashMap<String, ArrayList <String>> documentos )
	{
		String Cliente = "Guarda Documentos";
		HashMap<String, ArrayList <String>> paraGuardar = new HashMap <String, ArrayList<String>> ( );
		
		for ( String renglon : documentos.keySet ( ) ) {
			
			HashMap <Integer, String> deConsulta = new HashMap <Integer, String> ( );
			
			ArrayList<String> recuperados = documentos.get ( renglon );
			//LV_TIPREG (1), LV_NUMDOC(2), LV_FECH(3), LV_IMPTOT*LV_TIPCAM(4), LV_CANC(5), LV_NUMCLI(6)
			int cont = 0;
			for ( String valor : recuperados ) {
				cont++;
				deConsulta.put ( cont, valor );
			}
			
			ArrayList <String> campos = new ArrayList <String> ( );
			
			HashMap<String, String> d = Utilidades.descomponeDocumento ( deConsulta.get ( 2 ) );
			
			if ( d.get ( "fin" ).equals ( "14" ) ){
				d.put ( "suc", "14" );
			}
			
			String serie = tipoDeDocumento.getDocumento ( Integer.parseInt ( deConsulta.get ( 1 ) ) ).getAbreviacion ( ) + sucursales.getSucursal ( d.get ( "suc" ) ).getAbreviacion ( ) ; 
			
			
			campos.add ( d.get ( "suc" )  ); 														// sucursal 		01
			campos.add ( deConsulta.get ( 1 ) ); 													// tipo_doc 		1
			campos.add ( deConsulta.get ( 6 ) ); 													// cve_cliente	000000
			campos.add ( serie ); 																	// serie			FACMEX
			campos.add ( d.get ( "fol" ) ); 														// folio			000000
			campos.add ( Utilidades.getFechaFormatText ( deConsulta.get ( 3 ) ) ); 					// fecha			01/01/2015
			campos.add ( Utilidades.redondea ( deConsulta.get ( 4 ), 2 ) ); 						// monto			000000.00
			campos.add ( deConsulta.get ( 5 ) ); 													// estatus		C
			campos.add ( deConsulta.get ( 7 ) );													//IVA
		
			paraGuardar.put ( renglon, campos );
			
		}
	
		ConsultaODBC co = new ConsultaODBC ( Conecciones.getMySQLLocalBarpro ( ) );
		
		HashMap<Integer, tipoDeDato> datosDecolumna = new HashMap <Integer, Utilidades.tipoDeDato> ( );
		
		datosDecolumna.put ( 1, tipoDeDato.CADENA );
		datosDecolumna.put ( 2, tipoDeDato.ENTERO );
		datosDecolumna.put ( 3, tipoDeDato.CADENA );
		datosDecolumna.put ( 4, tipoDeDato.CADENA );
		datosDecolumna.put ( 5, tipoDeDato.CADENA );
		datosDecolumna.put ( 6, tipoDeDato.CADENA );
		datosDecolumna.put ( 7, tipoDeDato.DOBLE );
		datosDecolumna.put ( 8, tipoDeDato.CADENA );
		datosDecolumna.put ( 9, tipoDeDato.DOBLE);
		
		int registrosGuardados = co.InsertaRegistroMysql ( sentencia, paraGuardar, datosDecolumna, 1000 );
	
		Utilidades.MuestraInformacion ( "Se cargaron: " + registrosGuardados + " registros", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
	}
	
	private void acompletaInformacion()
	{
		String Cliente = "Acompleta informacion";
		// Recupera el catalogo de clientes
		String sentencia = "SELECT CL_NUMCLI, CONCAT(CONCAT(CL_NOMCLI,'--'), CL_RFC) FROM MDGCLI02";
		
		ConsultaODBC co = new ConsultaODBC ( Conecciones.getERPCon ( ) );
		
		HashMap<String, ArrayList<String>> r = co.ConsultaVariasColumnasODBC ( sentencia );
		
		HashMap<String, String> clientes = new HashMap <String, String> ( );
		
		for ( String registro : r.keySet ( ) ){
			ArrayList <String> campos = r.get ( registro );
			HashMap<Integer, String> reg = new HashMap <Integer, String> ( );
			int ca = 0;
			for( String c : campos ){
				ca++;
				reg.put ( ca, c );
			}
			
			clientes.put ( reg.get ( 1 ), reg.get ( 2 ) );
	
		}
		
		// Recupera el listado de clientes que no tienen nombre
		
		sentencia = "SELECT DISTINCT cve_cliente FROM BARAUD01 WHERE nom_cliente IS NULL";
		
		co = new ConsultaODBC ( Conecciones.getMySQLLocalBarpro ( ) );
		
		r.clear ( );
		r = co.ConsultaVariasColumnasMySQL ( sentencia );
		
		// Genera el arreglo de clientes, sin nombre.
		ArrayList<String> clientesSinNombre = new ArrayList <String> ( );
		for ( String registro : r.keySet ( ) ){
			ArrayList<String> campos = r.get ( registro );
			for( String cte : campos ){
				clientesSinNombre.add ( cte );
			}
		}

		HashMap<String, ArrayList<String>> clavesParaActualizar = new HashMap <String, ArrayList<String>> ( );
		
		int registrosAGuardar = 0;
		
		for( String clave : clientesSinNombre ){
			String dato = clientes.get ( clave );
			
			List<String> d = Arrays.asList ( dato.split ( "--" ) );
			
			d.removeAll ( Arrays.asList ( "", null ) );
			
			String [] datos = new String[ d.size ( ) ];
			
			datos = d.toArray ( datos );
			
			String nombre 	= datos[0];
			String rfc		= datos[1];	
			
			
			if ( nombre.trim ( ).length ( ) > 0 ){
				registrosAGuardar++;
				
				ArrayList<String> valores = new ArrayList <String> ( );
				
				valores.add ( nombre );
				valores.add ( rfc );
				valores.add ( clave );
				
				clavesParaActualizar.put ( String.valueOf ( registrosAGuardar ), valores );
			}
		}
		
		co = new ConsultaODBC ( Conecciones.getMySQLLocalBarpro ( ) );
		
		String sentenciaInsertClientes = "UPDATE BARAUD01 SET nom_cliente = ?, RFC = ? WHERE cve_cliente = ?";
		
		HashMap<Integer, tipoDeDato> datoDeColumna = new HashMap <Integer, Utilidades.tipoDeDato> ( );
		datoDeColumna.put ( 1, tipoDeDato.CADENA );
		datoDeColumna.put ( 2, tipoDeDato.CADENA );
		datoDeColumna.put ( 3, tipoDeDato.CADENA );
		
		int registrosActualizados = co.updateRegistrosMySQL ( sentenciaInsertClientes, clavesParaActualizar, datoDeColumna, 1000 );
		
		Utilidades.MuestraInformacion ( "Se actualizaron: " + registrosActualizados + " registros", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
	}

	private void recuperaUUID()
	{
		String Cliente = "Recupera UUID";
		// recupera toda la tabla de UUID
		String sentencia = "SELECT TF_TIPREG, TF_NUMCLI, TF_NUMDOC, IFNULL(TF_UUID,'0'), IFNULL(TF_CANC,'0'), TF_REFE FROM PTBCFD05";
		ConsultaODBC con = new ConsultaODBC ( Conecciones.getERPCon ( ) );
		
		HashMap<String, ArrayList <String>> timbres = con.ConsultaVariasColumnasODBC ( sentencia );
		
		HashMap <String, String> consultaTimbre = new HashMap <String, String> ( );
		
		
		for( String renglon : timbres.keySet ( ) ){
			ArrayList<String> campos = timbres.get ( renglon );
			HashMap <Integer, String> vXc = new HashMap <Integer, String> ( );
			
			int columna = 0;
			
			for ( String valor : campos ){
				columna++;
				vXc.put ( columna, valor );
			}
		
			String llave = "";
			String valor = "";
			
			// La pasa a un HashMap, en donde la llave es: TIPODEREGISTRO-NUMCLI-DOCU Y EL VALOR ES UUID,UUIDCAN
			
			if ( vXc.get(1).equals ( "1" ) ) {
				llave = vXc.get ( 1 ) + "-" + vXc.get ( 2 ) + "-" + vXc.get ( 3 );
				valor = vXc.get ( 4 ) +"," + vXc.get ( 5 );
			} 
			
			if ( vXc.get(1).equals ( "4" ) ) {
				
				// Construye numdoc
				
				String documento 	= vXc.get(3);
				String refe			= vXc.get(6);
				String docRefe		= "";
				
				docRefe = documento.substring ( 0, 2 ) + refe + "10";
				
				llave = vXc.get(1) + "-" + vXc.get ( 2 ) + "-" + docRefe;
				valor = vXc.get ( 4 ) +"," + vXc.get ( 5 );
			
			}
			
				if ( vXc.get(1).equals ( "3" ) ) {
				
				// Construye numdoc
				
				String documento 	= vXc.get(3);
				String refe			= vXc.get(6);
				String docRefe		= "";
				
				docRefe = documento.substring ( 0, 2 ) + refe + "10";
				
				llave = vXc.get(1) + "-" + vXc.get ( 2 ) + "-" + docRefe;
				valor = vXc.get ( 4 ) +"," + vXc.get ( 5 );
			
			}
			
			consultaTimbre.put ( llave, valor );
			
		}
		
		sentencia = "SELECT sucursal, tipo_doc, cve_cliente, folio FROM BARAUD01 ";
		
		con = new ConsultaODBC ( Conecciones.getMySQLLocalBarpro ( ) );
		
		HashMap<String, ArrayList <String>> registros = con.ConsultaVariasColumnasMySQL ( sentencia );
		ArrayList <String> registrosEnAuditor =  new ArrayList <String> ( );
		
		for ( String renglon : registros.keySet ( ) ){
			
			ArrayList <String> campos = registros.get ( renglon );
			
			int columna = 0;
			
			HashMap<Integer, String> vxc = new HashMap <Integer, String> ( );
			
			for ( String valor : campos ){
				columna++;
				vxc.put ( columna, valor );	
			}
			
			// Realiza las conversiones
			
			String sucursal 	= vxc.get ( 1 );
			String tipoDocument	= vxc.get ( 2 );
			String claveClinte	= vxc.get ( 3 );
			String folio		= vxc.get ( 4 );
			
			String documento	= "";
			
			if ( sucursal.equals ( "14" ) ){
				documento = "01" + folio + "14";
			} else {
				documento = sucursal + folio + "10";
			}
			
			registrosEnAuditor.add ( tipoDocument + "-" + claveClinte + "-" + documento );
			
		}
		
		// realiza la busqueda y lo que se recupere lo almacena en otro hash map, que es copia del paso 2
		
		HashMap<String, ArrayList<String>> valoresAGuardar = new HashMap <String, ArrayList<String>> ( );
		
		int registrosAGuardar = 0;
		
		for ( String llave : registrosEnAuditor ) {
			
			if (llave.substring ( 0, 1 ).equals ( "4" ) ){
				@SuppressWarnings ( "unused" )
				String ol = "";
			}
			
			String valor = consultaTimbre.get ( llave );
			
			if ( valor != null ){
				ArrayList<String> columnas = new ArrayList <String> ( );
				registrosAGuardar++;
				String [] valores 	= valor.split ( "," );	// uuid - can uuid
				
				if ( valores[0].equals ( "0" ) ){
					columnas.add ( " " );
				} else {
					columnas.add ( valores[0] );
				}
			
				
				if ( valores[1].equals ( "0" ) ){
					columnas.add ( " " );
				} else {
					columnas.add ( valores[1] );
				}
				
				String [] llaves	= llave.split ( "-" ); // tipo de documento - clave cliente - documento
	
				HashMap<String, String> datosDeDocumento = Utilidades.descomponeDocumento ( llaves[2] );
				
				if ( datosDeDocumento.get ( "fin" ).equals ( "14" ) ) {
					datosDeDocumento.put ( "suc", "14" );
				}
				
				String suc = sucursales.getSucursal ( datosDeDocumento.get ( "suc" ) ).getAbreviacion ( );
				String tdc = tipoDeDocumento.getDocumento ( Integer.parseInt ( llaves[0] ) ).getAbreviacion ( );
				String Serie = tdc+suc;
				
				columnas.add ( Serie );
				columnas.add ( datosDeDocumento.get ( "fol" ) );
				
				valoresAGuardar.put ( String.valueOf ( registrosAGuardar ), columnas );
				
			}
			
		}
		
		// guarda la informacion
		
		HashMap<Integer, tipoDeDato> datosDeColumna = new HashMap <Integer, Utilidades.tipoDeDato> ( );
		
		datosDeColumna.put ( 1, tipoDeDato.CADENA );
		datosDeColumna.put ( 2, tipoDeDato.CADENA );
		datosDeColumna.put ( 3, tipoDeDato.CADENA );
		datosDeColumna.put ( 4, tipoDeDato.CADENA );
		
		sentencia = "UPDATE BARAUD01 SET uuid = ?, can_uuid = ? WHERE serie = ? AND folio = ? ";
		
		con = new ConsultaODBC ( Conecciones.getMySQLLocalBarpro ( ) );
		
		int registrosActualizados = con.updateRegistrosMySQL ( sentencia, valoresAGuardar, datosDeColumna, 1000 );
		
		Utilidades.MuestraInformacion ( "Se actualizaron: " + registrosActualizados + " registros", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
	}
	
	private void validaWEB()
	{
		String Cliente = "Valida Web";
		// recupera el listado de todos los documentos en la web ( pdf y xml ), los guarda en un arrayList
		String sentencia = "SELECT DISTINCT nombre FROM facturas";
		
		ConsultaODBC co = new ConsultaODBC ( Conecciones.getMySQLRemoto ( ) );
		
		HashMap<String, ArrayList<String>> documentos = co.ConsultaVariasColumnasMySQL ( sentencia );

		ArrayList<String> documentosEnWeb = new ArrayList <String> ( );
		
		for ( String renglon : documentos.keySet ( ) ){
			ArrayList<String> campos = documentos.get ( renglon );
			
			for ( String doc : campos ){
				documentosEnWeb.add ( doc );
			}	
		}
		
		// recupera todos los documentos por tipo, de la tabla BARAUD01
		sentencia = "SELECT  cve_cliente, folio, serie FROM BARAUD01";
		
		co = new ConsultaODBC ( Conecciones.getMySQLLocalBarpro ( ) );
		
		HashMap<String, ArrayList<String>> documentosEnLocal = co.ConsultaVariasColumnasMySQL ( sentencia );
		
		ArrayList <String> documentosAValidar = new ArrayList <String> ( );
		
		for ( String renglon : documentosEnLocal.keySet ( ) ){
			ArrayList <String> campos = documentosEnLocal.get ( renglon );
			
			int contador = 0;
			
			HashMap<Integer, String> vxc = new HashMap <Integer, String> ( );
			
			for ( String valorEnCampo : campos ){
				contador++;
				vxc.put ( contador, valorEnCampo );
			}
			
			// Arma el raiz, del documento, CLIENTE-FOLIO-SERIE
			
			documentosAValidar.add ( vxc.get ( 1 ) + vxc.get ( 2 ) + vxc.get ( 3 ) );
				
		}
		
		// busca los documentos en el array del paso 1, si los encuentra los guarda en un hashmap, con la llave 
		// y si se encuentran en web xml y pdf
		
		HashMap<String, ArrayList<String>> documentosAActualizar = new HashMap <String, ArrayList<String>> ( );
		
		HashMap<String, String> formato = new HashMap <String, String> ( );
		
		formato.put ( "pdf", ".pdf" );
		formato.put ( "xml", "_S.xml" );
		
		sentencia = "UPDATE BARAUD01 SET pdf = ?, xml= ? WHERE serie = ? AND folio = ?";
		
		int DocumentosAGuardar = 0;
		
		for ( String documentoRaiz : documentosAValidar ) {
			// Recupera el folio y la serie
			HashMap <componentesDocumentoFiscal, String> vRaiz = Utilidades.descomponeDocumentoFiscal ( documentoRaiz );
			
			ArrayList<String> datosParaActualizar = new ArrayList <String> ( );
		
			for ( String f : formato.keySet ( ) ) {
				String documentoAValidar = documentoRaiz + formato.get ( f );
				
				if ( documentosEnWeb.contains ( documentoAValidar ) ){
					datosParaActualizar.add ( "S" );
				} else {
					datosParaActualizar.add ( "N" );
				}	
			}
			
			datosParaActualizar.add ( vRaiz.get ( componentesDocumentoFiscal.SERIE ) );
			datosParaActualizar.add ( vRaiz.get ( componentesDocumentoFiscal.FOLIO ) );
			
			DocumentosAGuardar++;
			
			documentosAActualizar.put ( String.valueOf ( DocumentosAGuardar ), datosParaActualizar );
			
		}
		
		HashMap<Integer, tipoDeDato> datosDeColumna = new HashMap <Integer, Utilidades.tipoDeDato> ( );
		
		datosDeColumna.put ( 1, tipoDeDato.CADENA );
		datosDeColumna.put ( 2, tipoDeDato.CADENA );
		datosDeColumna.put ( 3, tipoDeDato.CADENA );
		datosDeColumna.put ( 4, tipoDeDato.CADENA );
		
		co = new ConsultaODBC ( Conecciones.getMySQLLocalBarpro ( ) );
		
		int registrosActualizados = co.updateRegistrosMySQL ( sentencia, documentosAActualizar, datosDeColumna, 1000 );
		
		Utilidades.MuestraInformacion ( "Se actualizaron: " + registrosActualizados + " registros", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
	}
}
