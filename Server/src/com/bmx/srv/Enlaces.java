package com.bmx.srv;

import java.util.ArrayList;
import java.util.HashMap;

import com.bmx.srv.Utilidades.dondeSeMuestra;
import com.bmx.srv.Utilidades.nivel;
import com.bmx.srv.Utilidades.tipoDeDato;

public class Enlaces
{

	private static ArrayList <String> 						llaves 				= new ArrayList <String> ( );
	private static HashMap<Integer, tipoDeDato> 			datoDeColumna 		= new HashMap <Integer, Utilidades.tipoDeDato> ( );
	private static ArrayList <String> 						nombreDeCampo 		= new ArrayList <String> ( );
	private static String									sentencia			= "";
	private static String									sentenciaInsert		= "";
	private static int 										Batch;
	
	private static HashMap<String, ArrayList <String>> 		rWeb 				= null;
	private static HashMap<String, ArrayList <String>> 		rLoc 				= null;
	private static HashMap<String, ArrayList <String>> 		registros 			= null;
	
	private static HashMap<String, ArrayList <Object>> 		rWeb_wb 			= null;
	private static HashMap<String, ArrayList <Object>> 		rLoc_wb 			= null;
	private static HashMap<String, ArrayList <Object>> 		registros_wb		= null;

	private static int										res					= 0;
	
	private static ConsultaODBC								sLoc				= null;
	private static ConsultaODBC								sWeb				= null;
	
	private static void loadValores()
	{
		sLoc = new ConsultaODBC( Conecciones.getMySQLLocalBarpro ( ) );
		sWeb = new ConsultaODBC( Conecciones.getMySQLRemoto ( ) );
	}
	
	private static void cleanArreglos() 
	{
		llaves.clear ( );
		datoDeColumna.clear ( );
		nombreDeCampo.clear ( );
		sentencia = "";
	}
	
	public static boolean DescargaEnlaces () {
		boolean resultado = false;
		
		// Inicia la descarga de informacion de los documentos en la web.
		
		loadValores ( );
		
		//BARPROV07
		cleanArreglos ( );
		sentencia = "SELECT * FROM BARPROV07";
		sentenciaInsert = "INSERT INTO BARPROV07 (cve_comprador, nombre) VALUES (?,?)";
		llaves.add ( "1" );
		datoDeColumna.put ( 1, tipoDeDato.CADENA );
		datoDeColumna.put ( 2, tipoDeDato.CADENA );
		nombreDeCampo.add ( "cve_comprador" );
		nombreDeCampo.add ( "nombre" );
		
		rWeb = sWeb.ConsultaVariasColumnasMySQL ( sentencia );
		rLoc = sLoc.ConsultaVariasColumnasMySQL ( sentencia );
		
		registros = ConsultaODBC.comparaOrigenVsCopia ( rWeb, rLoc, llaves );
		
		res = sLoc.InsertaRegistroMysql ( sentenciaInsert, registros, datoDeColumna, Batch );
		
		//BARPROV08
		cleanArreglos ( );
		sentencia = "SELECT * FROM BARPROV08";
		sentenciaInsert = "INSERT INTO BARPROV08 (cve_comprador, OR_NUMORD, nombre ) VALUES (?,?,?)";
		llaves.add ( "1" );
		llaves.add ( "2" );
		datoDeColumna.put ( 1, tipoDeDato.ENTERO );
		datoDeColumna.put ( 2, tipoDeDato.CADENA );
		datoDeColumna.put ( 3, tipoDeDato.CADENA );
		nombreDeCampo.add ( "cve_comprador" );
		nombreDeCampo.add ( "OR_NUMORD" );
		nombreDeCampo.add ( "nombre" );
		
		rWeb = sWeb.ConsultaVariasColumnasMySQL ( sentencia );
		rLoc = sLoc.ConsultaVariasColumnasMySQL ( sentencia );
		
		registros = ConsultaODBC.comparaOrigenVsCopia ( rWeb, rLoc, llaves );
		
		res = sLoc.InsertaRegistroMysql ( sentenciaInsert, registros, datoDeColumna, Batch );
		
		//BAPROV12
		cleanArreglos ( );
		sentencia = "SELECT * FROM BARPROV12";
		sentenciaInsert = "INSERT INTO BARPROV12 (clave_proveedor, cve_comprador, cve_moneda, OR_NUMORD, OR_FECORD, OR_IMPORTE) VALUES (?,?,?,?,?,?)";
		llaves.add ( "4" );
		datoDeColumna.put ( 1, tipoDeDato.CADENA );
		datoDeColumna.put ( 2, tipoDeDato.ENTERO );
		datoDeColumna.put ( 3, tipoDeDato.CADENA );
		datoDeColumna.put ( 4, tipoDeDato.CADENA );
		datoDeColumna.put ( 5, tipoDeDato.CADENA );
		datoDeColumna.put ( 6, tipoDeDato.DOBLE );
		nombreDeCampo.add ( "clave_proveedor" );
		nombreDeCampo.add ( "cve_comprador" );
		nombreDeCampo.add ( "cve_moneda" );
		nombreDeCampo.add ( "OR_NUMORD" );
		nombreDeCampo.add ( "OR_FECORD" );
		nombreDeCampo.add ( "OR_IMPORTE" );
		
		rWeb = sWeb.ConsultaVariasColumnasMySQL ( sentencia );
		rLoc = sLoc.ConsultaVariasColumnasMySQL ( sentencia );
		
		registros = ConsultaODBC.comparaOrigenVsCopia ( rWeb, rLoc, llaves );
		
		res = sLoc.InsertaRegistroMysql ( sentenciaInsert, registros, datoDeColumna, Batch );
		
		//BARPROV14
		cleanArreglos ( );
		sentencia = "SELECT * FROM BARPROV14";
		sentenciaInsert = "INSERT INTO BARPROV14 (folio, fecha_enlace, monto, OR_NUMORD, clave_usuario) VALUES (?,?,?,?,?)";
		llaves.add ( "1" );
		datoDeColumna.put ( 1, tipoDeDato.ENTERO );
		datoDeColumna.put ( 2, tipoDeDato.CADENA );
		datoDeColumna.put ( 3, tipoDeDato.DOBLE );
		datoDeColumna.put ( 4, tipoDeDato.CADENA );
		datoDeColumna.put ( 5, tipoDeDato.ENTERO );
		
		nombreDeCampo.add ( "folio" );
		nombreDeCampo.add ( "fecha_enlace" );
		nombreDeCampo.add ( "monto" );
		nombreDeCampo.add ( "OR_NUMORD" );
		nombreDeCampo.add ( "clave_usuario" );
		
		rWeb = sWeb.ConsultaVariasColumnasMySQL ( sentencia );
		rLoc = sLoc.ConsultaVariasColumnasMySQL ( sentencia );
		
		registros = ConsultaODBC.comparaOrigenVsCopia ( rWeb, rLoc, llaves );
		
		res = sLoc.InsertaRegistroMysql ( sentenciaInsert, registros, datoDeColumna, Batch );
		
		//BARPROV10
		cleanArreglos ( );
		sentencia = "SELECT * FROM BARPROV10";
		sentenciaInsert = "INSERT INTO BARPROV10 (doc_nombre, doc_content, folio, doc_cve_tipo, doc_cve_formato, consecutivo) VALUES (?,?,?,?,?,?)";
		llaves.add ( "6" );
		datoDeColumna.put ( 1, tipoDeDato.CADENA );
		datoDeColumna.put ( 2, tipoDeDato.BLOB );
		datoDeColumna.put ( 3, tipoDeDato.ENTERO);
		datoDeColumna.put ( 4, tipoDeDato.ENTERO );
		datoDeColumna.put ( 5, tipoDeDato.ENTERO );
		datoDeColumna.put ( 6, tipoDeDato.ENTERO );
		nombreDeCampo.add ( "doc_nombre" );
		nombreDeCampo.add ( "doc_content" );
		nombreDeCampo.add ( "folio" );
		nombreDeCampo.add ( "doc_cve_tipo" );
		nombreDeCampo.add ( "doc_cve_formato" );
		nombreDeCampo.add ( "consecutivo" );
		
		rWeb_wb = sWeb.ConsultaVariasColumnasMySQL_withBlob ( sentencia, datoDeColumna );
		rLoc_wb = sLoc.ConsultaVariasColumnasMySQL_withBlob ( sentencia, datoDeColumna );
		
		registros_wb = ConsultaODBC.comparaOrigenVsCopia ( rWeb_wb, rLoc_wb, llaves, datoDeColumna );
		
		res = sLoc.InsertaRegistroMysql_wb ( sentenciaInsert, registros_wb, datoDeColumna, Batch );
		
		Utilidades.MuestraInformacion ( "Se recuperaron: " + res + " documentos.", "Documentos", nivel.INFO, dondeSeMuestra.AMBOS );
		
		return resultado;
	}
}
