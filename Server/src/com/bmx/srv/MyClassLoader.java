package com.bmx.srv;

import java.io.*;
import java.net.*;

/* ES UN CARGADOS DINAMICO, PARA QUE SE CARGEN ARCHIVOS JAR
 * 
 */

public class MyClassLoader extends ClassLoader
{
	String version 		= "1.0.0.0";
	String claseACargar = "";
	String ruta			= "";
	
	public String getClaseACargar()
	{
		return claseACargar;
	}
	
	public void setClaseACargar( String NombreDeLaClase )
	{
		claseACargar = NombreDeLaClase;
	}
	
	public void setPathClase ( String pathClase )
	{
		ruta = pathClase.replace( '\\', '/' );
	}
	
	
	public String getVersion()
	{
		return version;	
	}
	
	public MyClassLoader( ClassLoader parent )
	{
		super ( parent );
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Class loadClass ( String name  ) throws ClassNotFoundException
	{
		if ( !claseACargar.equals( name )){
			return super.loadClass( name );
		} 
		
		try {
			String 			url 		= "file:" + ruta;
			URL 			myUrl 		= new URL( url );
			URLConnection 	connection 	= myUrl.openConnection();
			
			InputStream				input		= connection.getInputStream();
			ByteArrayOutputStream	buffer		= new ByteArrayOutputStream();
			int						data 		= input.read();
			
			while ( data != -1 ){
				buffer.write( data );
				data = input.read();
			}
			
			input.close();
			
			byte[] classData	= buffer.toByteArray();
			
			return defineClass ( name, classData,0 , classData.length );
			
			
		} catch ( MalformedURLException e ){
			e.printStackTrace();
			
		} catch ( IOException e ){
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	

}
