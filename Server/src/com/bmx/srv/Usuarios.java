package com.bmx.srv;

import static com.bmx.srv.Utilidades.MuestraInformacion;

import java.util.ArrayList;
import java.util.HashMap;

import com.bmx.srv.Utilidades.dondeSeMuestra;
import com.bmx.srv.Utilidades.nivel;
import com.bmx.srv.Utilidades.tipoDeDato;

public class Usuarios
{
	
	//region ENUMERACIONES ESTATICAS
	public static enum procesos_usuarios{
		SUBE_USUARIOS(1),
		DESCARGA_USUARIOS(2),
		SUBE_CAMBIOS(3),
		DESCARGA_CAMBIOS(4),
		SUBE_PERMISOS(5),
		DESCARGA_PERMISOS(6),
		SINCRONIZA_WEB_TO_LOC(21),
		SINCRONIZA_LOC_TO_WEB(22),
		;
		
		int proceso = 0;
		
		private procesos_usuarios( int p ){
			proceso = p;
		}
		
		public String getDescripcion(){
			String descripcion = "";
			switch ( proceso ) {
				case 1:
					descripcion = "Sube usuarios";
					break;
				case 2:
					descripcion = "Descarga usuarios";
					break;
				case 3:
					descripcion = "Sube cambios";
					break;
				case 4:
					descripcion = "Descarga cambios";
					break;
				case 5:
					descripcion = "Descarga permisos";
					break;
				case 6:
					descripcion = "Sube permisos";
					break;
				case 21:
					descripcion = "Sincroniza Web a Local";
					break;
				case 22:
					descripcion = "Sincroniza Local a Web";
					break;
			}
			return descripcion;
		}
		
	}
	
	public static enum sentido_proceso{
		DOWNLOAD,
		UPLOAD;
	}
	//endregion
	
	//region VARIABLES ESTATICAS
	private static ArrayList <String> 			llaves 				= new ArrayList <String> ( );
	private static HashMap<Integer, tipoDeDato> datoDeColumna 		= new HashMap <Integer, Utilidades.tipoDeDato> ( );
	private static ArrayList <String> 			nombreDeCampo 		= new ArrayList <String> ( );
	private static int 							Batch;
	
	private static ArrayList <String> 			llavesPerm 			= new ArrayList <String> ( );
	private static HashMap<Integer, tipoDeDato> datoDeColumnaPerm 	= new HashMap <Integer, Utilidades.tipoDeDato> ( );
	private static ArrayList <String> 			nombreDeCampoPerm 	= new ArrayList <String> ( );
	
	private static ConsultaODBC					sLoc				= null;
	private static ConsultaODBC					sWeb				= null;
	
	//endregion
	
	//region METODOS ESTATICOS
	
	@SuppressWarnings ( "incomplete-switch" )
	public static void Proceso_usuarios ( procesos_usuarios p )
	{
		String Cliente = "Procesos Usuarios";
		int 	res 		= 0;
		
		String 	sentencia 	= "SELECT * FROM BARPROV15";
		String	sentencia2	= "SELECT * FROM BARPROV16";
		
		sLoc = new ConsultaODBC( Conecciones.getMySQLLocalBarpro ( ) );
		sWeb = new ConsultaODBC( Conecciones.getMySQLRemoto ( ) );
		
		loadValores ( );
		
		switch ( p ) {
			case SUBE_USUARIOS:
				Cliente = Cliente + " - " + procesos_usuarios.SUBE_USUARIOS.getDescripcion ( ) ;
				MuestraInformacion ( "Iniciando subida de usuarios", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				res = SubeUsuarios ( sentencia, llaves, datoDeColumna, Batch );
				MuestraInformacion ( "Terminando subida de usuarios, se subieron: "+ String.valueOf ( res ), Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				break;
			case DESCARGA_USUARIOS:
				Cliente = Cliente + " - " + procesos_usuarios.DESCARGA_USUARIOS.getDescripcion ( ) ;
				MuestraInformacion ( "Iniciando descarga de usuarios", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				res = DescargaUsuarios ( sentencia, llaves, datoDeColumna, Batch );
				MuestraInformacion ( "Terminando descarga de usuarios, se descargaron: "+ String.valueOf ( res ), Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				break;
			case SUBE_CAMBIOS:
				Cliente = Cliente + " - " + procesos_usuarios.SUBE_CAMBIOS.getDescripcion ( ) ;
				MuestraInformacion ( "Iniciando descarga de cambios usuarios", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				res = SubeCambios ( sentencia, llaves, datoDeColumna, nombreDeCampo, Batch );
				MuestraInformacion ( "Terminando descarga de cambios usuarios, se actualizaron: "+ String.valueOf ( res ), Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				break;
			case DESCARGA_CAMBIOS:
				Cliente = Cliente + " - " + procesos_usuarios.DESCARGA_CAMBIOS.getDescripcion ( ) ;
				MuestraInformacion ( "Iniciando descarga de cambios usuarios", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				res = DescargaCambios ( sentencia, llaves, datoDeColumna, nombreDeCampo, Batch );
				MuestraInformacion ( "Terminando descarga de cambios usuarios, se actualizaron: "+ String.valueOf ( res ), Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				break;
			case SINCRONIZA_WEB_TO_LOC:
				Cliente = Cliente + " - " + procesos_usuarios.SINCRONIZA_WEB_TO_LOC.getDescripcion ( );
				MuestraInformacion ( "Iniciando sincronizacion usuarios", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				res = DescargaUsuarios ( sentencia, llaves, datoDeColumna, Batch );
				res = DescargaCambios ( sentencia, llaves, datoDeColumna, nombreDeCampo, Batch );
				res = DescargaPermisos ( sentencia2, llavesPerm, datoDeColumnaPerm, nombreDeCampoPerm, Batch);
				MuestraInformacion ( "Termiando sincronizacion de usuarios", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				break;
			case SINCRONIZA_LOC_TO_WEB:
				Cliente = Cliente + " - " + procesos_usuarios.SINCRONIZA_LOC_TO_WEB.getDescripcion ( );
				MuestraInformacion ( "Iniciando sincronizacion usuarios", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				res = SubeUsuarios ( sentencia, llaves, datoDeColumna, Batch );
				res = SubeCambios ( sentencia, llaves, datoDeColumna, nombreDeCampo, Batch );
				res = SubePermisos ( sentencia2, llavesPerm, datoDeColumnaPerm, nombreDeCampoPerm, Batch );
				MuestraInformacion ( "Termiando sincronizacion de usuarios", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		}
	}
	
	private static void loadValores ( )
	{
		llaves.clear ( );
		
		llaves.add ( "1" );
		llaves.add ( "10" );
		
		llavesPerm.clear ( );
		
		llavesPerm.add ( "1" );
		llavesPerm.add ( "2" );
		llavesPerm.add ( "3" );
		llavesPerm.add ( "4" );
		
		datoDeColumna.clear ( );
		
		datoDeColumna.put ( 1, tipoDeDato.ENTERO );
		datoDeColumna.put ( 2, tipoDeDato.CADENA );
		datoDeColumna.put ( 3, tipoDeDato.CADENA );
		datoDeColumna.put ( 4, tipoDeDato.CADENA );
		datoDeColumna.put ( 5, tipoDeDato.CADENA );
		datoDeColumna.put ( 6, tipoDeDato.CADENA );
		datoDeColumna.put ( 7, tipoDeDato.CADENA );
		datoDeColumna.put ( 8, tipoDeDato.CADENA );
		datoDeColumna.put ( 9, tipoDeDato.ENTERO );
		datoDeColumna.put ( 10, tipoDeDato.CADENA );
	
		datoDeColumnaPerm.clear ( );
		
		datoDeColumnaPerm.put ( 1, tipoDeDato.ENTERO );
		datoDeColumnaPerm.put ( 2, tipoDeDato.ENTERO );
		datoDeColumnaPerm.put ( 3, tipoDeDato.ENTERO );
		datoDeColumnaPerm.put ( 4, tipoDeDato.ENTERO );
		datoDeColumnaPerm.put ( 5, tipoDeDato.CADENA );
		datoDeColumnaPerm.put ( 6, tipoDeDato.ENTERO );
		
		nombreDeCampo.clear ( );
		
		nombreDeCampo.add ( "clave_usuario" );
		nombreDeCampo.add ( "nombre" );
		nombreDeCampo.add ( "paterno" );
		nombreDeCampo.add ( "materno" );
		nombreDeCampo.add ( "correo" );
		nombreDeCampo.add ( "usuario" );
		nombreDeCampo.add ( "clave" );
		nombreDeCampo.add ( "puesto" );
		nombreDeCampo.add ( "activo" );
		nombreDeCampo.add ( "clave_proveedor" );
		
		nombreDeCampoPerm.clear ( );
		
		nombreDeCampoPerm.add ( "clave_usuario" );
		nombreDeCampoPerm.add ( "clave_pagina" );
		nombreDeCampoPerm.add ( "clave_elemento" );
		nombreDeCampoPerm.add ( "clave_perfil" );
		nombreDeCampoPerm.add ( "comentarios" );
		nombreDeCampoPerm.add ( "permiso" );
		
		Batch = 50;
	}
	
	private static int SubeUsuarios( String sentencia, ArrayList <String> llaves, HashMap<Integer, tipoDeDato> datoDeColumna, int Batch)
	{
		
		int resultado = 0;
		
		ConsultaODBC cWeb 	= sWeb;
		ConsultaODBC cLoc	= sLoc;
		
		HashMap<String, ArrayList <String>> rWeb 		= null;
		HashMap<String, ArrayList <String>> rLoc 		= null;
		
		HashMap<String, ArrayList <String>> registros 	= null;
		
		rWeb = cWeb.ConsultaVariasColumnasMySQL ( sentencia );
		rLoc = cLoc.ConsultaVariasColumnasMySQL ( sentencia );
		
		registros = ConsultaODBC.comparaOrigenVsCopia ( rLoc, rWeb, llaves );
		
		String sentenciaInsert = 	"INSERT " +
									"INTO 		BARPROV15 (	clave_usuario, nombre, paterno, " +
														"	materno, correo, usuario," +
														"	clave, puesto, activo," +
														"	clave_proveedor) " +
												"values   ( ?,?,?," +
														"	?,?,?," +
														"	?,?,?," +
														"	?) ";
		
		resultado = cWeb.InsertaRegistroMysql ( sentenciaInsert, registros, datoDeColumna, Batch );
		return resultado;
	}
	
	private static int DescargaUsuarios( String sentencia, ArrayList <String> llaves, HashMap<Integer, tipoDeDato> datoDeColumna, int Batch )
	{
		
		int resultado = 0;
		
		ConsultaODBC cWeb 	= sWeb;
		ConsultaODBC cLoc	= sLoc;
		
		HashMap<String, ArrayList <String>> rWeb 		= null;
		HashMap<String, ArrayList <String>> rLoc 		= null;
		
		HashMap<String, ArrayList <String>> registros 	= null;
		
		rWeb = cWeb.ConsultaVariasColumnasMySQL ( sentencia );
		rLoc = cLoc.ConsultaVariasColumnasMySQL ( sentencia );
		
		registros = ConsultaODBC.comparaOrigenVsCopia ( rWeb, rLoc, llaves );
		
		String sentenciaInsert = 	"INSERT " +
									"INTO 		BARPROV15 (	clave_usuario, nombre, paterno, " +
														"	materno, correo, usuario," +
														"	clave, puesto, activo," +
														"	clave_proveedor) " +
												"values   ( ?,?,?," +
														"	?,?,?," +
														"	?,?,?," +
														"	?) ";
		
		resultado = cLoc.InsertaRegistroMysql ( sentenciaInsert, registros, datoDeColumna, Batch );
			
		return resultado;
	}
	
	private static int DescargaCambios( String sentencia, ArrayList <String> llaves, HashMap<Integer, tipoDeDato> datoDeColumna, ArrayList<String> nombreDeCampo, int Batch )
	{
		
		String nombreDeTabla = "BARPROV15";
		int resultado = 0;
		
		ConsultaODBC cWeb 	= sWeb;
		ConsultaODBC cLoc	= sLoc;
		
		HashMap<String, ArrayList <String>> rWeb 		= null;
		HashMap<String, ArrayList <String>> rLoc 		= null;
		
		HashMap<String, ArrayList <String>> registros 	= null;
		
		rWeb = cWeb.ConsultaVariasColumnasMySQL ( sentencia );
		rLoc = cLoc.ConsultaVariasColumnasMySQL ( sentencia );
		
		registros = ConsultaODBC.buscaModificacionesOrigenVsCopia ( rWeb, rLoc, llaves );
		
		resultado = cLoc.updateRegistrosMySQL ( registros, nombreDeCampo, datoDeColumna, Batch, llaves, nombreDeTabla );
			
		return resultado;
	}

	private static int SubeCambios( String sentencia, ArrayList <String> llaves, HashMap<Integer, tipoDeDato> datoDeColumna, ArrayList<String> nombreDeCampo, int Batch )
	{
		
		String nombreDeTabla = "BARPROV15";
		int resultado = 0;
		
		ConsultaODBC cWeb 	= sWeb;
		ConsultaODBC cLoc	= sLoc;
		
		HashMap<String, ArrayList <String>> rWeb 		= null;
		HashMap<String, ArrayList <String>> rLoc 		= null;
		
		HashMap<String, ArrayList <String>> registros 	= null;
		
		rWeb = cWeb.ConsultaVariasColumnasMySQL ( sentencia );
		rLoc = cLoc.ConsultaVariasColumnasMySQL ( sentencia );
		
		registros = ConsultaODBC.buscaModificacionesOrigenVsCopia ( rLoc, rWeb, llaves );
		
		resultado = cWeb.updateRegistrosMySQL ( registros, nombreDeCampo, datoDeColumna, Batch, llaves, nombreDeTabla );
			
		return resultado;
	}
	
	private static int DescargaPermisos( String sentencia, ArrayList <String> llaves, HashMap<Integer, tipoDeDato> datoDeColumna, ArrayList<String> nombreDeCampo, int Batch )
	{
		
		String nombreDeTabla = "BARPROV16";
		int resultado = 0;
		
		ConsultaODBC cWeb = sWeb;
		ConsultaODBC cLoc = sLoc;
		
		HashMap<String, ArrayList <String>> rWeb 		= null;
		HashMap<String, ArrayList <String>> rLoc 		= null;
		
		HashMap<String, ArrayList <String>> registros 	= null;
		
		rWeb = cWeb.ConsultaVariasColumnasMySQL ( sentencia );
		rLoc = cLoc.ConsultaVariasColumnasMySQL ( sentencia );
		
		registros = ConsultaODBC.buscaModificacionesOrigenVsCopia ( rWeb, rLoc, llaves );
		
		resultado = cLoc.updateRegistrosMySQL ( registros, nombreDeCampo, datoDeColumna, Batch, llaves, nombreDeTabla );
			
		return resultado;

	}

	private static int SubePermisos( String sentencia, ArrayList <String> llaves, HashMap<Integer, tipoDeDato> datoDeColumna, ArrayList<String> nombreDeCampo, int Batch )
	{
		String nombreDeTabla = "BARPROV16";
		int resultado = 0;
		
		ConsultaODBC cWeb 	= sWeb;
		ConsultaODBC cLoc	= sLoc;
		
		HashMap<String, ArrayList <String>> rWeb 		= null;
		HashMap<String, ArrayList <String>> rLoc 		= null;
		
		HashMap<String, ArrayList <String>> registros 	= null;
		
		rWeb = cWeb.ConsultaVariasColumnasMySQL ( sentencia);
		rLoc = cLoc.ConsultaVariasColumnasMySQL ( sentencia );
		
		registros = ConsultaODBC.comparaOrigenVsCopia ( rLoc, rWeb, llavesPerm );
		
		String si = "INSERT INTO BARPROV16 (clave_usuario, clave_pagina, clave_elemento, clave_perfil, comentarios, permiso) VALUES (?,?,?,?,?,? )";
		
		resultado = cWeb.InsertaRegistroMysql ( si, registros, datoDeColumna, Batch );
		
		registros = ConsultaODBC.buscaModificacionesOrigenVsCopia ( rLoc, rWeb, llavesPerm );
		
		resultado = cWeb.updateRegistrosMySQL ( registros, nombreDeCampo, datoDeColumna, Batch, llavesPerm, nombreDeTabla );
			
		return resultado;
	}

	//endregion
	
	//region VARIABLES DE CLASE
	String clave_usuario 	= "";
	String clave_proveedor	= "";
	
	ConsultaODBC 	cLoc	= null;
	ConsultaODBC 	cWeb	= null;
	
	//endregion
	
	//region CONSTRUCTOR
	public Usuarios ( String cve_usuario_cve_proveedor ){
		Usuarios.loadValores ( );
		
		String[] datos = cve_usuario_cve_proveedor.split ( "-" );
		
		clave_usuario 	= datos[2];
		clave_proveedor	= datos[1];
		cLoc 			= new ConsultaODBC( Conecciones.getMySQLLocalBarpro ( ) );
		cWeb 			= new ConsultaODBC( Conecciones.getMySQLRemoto ( ) );
	}
	//endregion
	
	//region METODOS PUBLICOS
	
	public boolean SubeUsuario( ) {
		boolean resultado = false;
		
		String Cliente = "Sube usuario";
		ArrayList<String> mensajes = new ArrayList <String> ( );
		
		MuestraInformacion ( "Iniciando - subida del usuario " + clave_usuario, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		// Sube un nuevo usuario.
		// Recupera la informacion del usuario a subir.
		String sentencia = "SELECT * FROM BARPROV15 WHERE clave_usuario = " + clave_usuario ;
		
		HashMap<String, ArrayList <String>> reg = new HashMap <String, ArrayList<String>> ( );
		
		reg = cLoc.ConsultaVariasColumnasMySQL ( sentencia );
		
		int c = 0;
		
		if ( reg.size ( ) == 1 ){
			c = cWeb.InsertaRegistroMysql ( sentencia, reg, datoDeColumna, 50 );
		}
		
		mensajes.clear ( );
		mensajes.add ( "Termiando subida del usuario " + clave_usuario );
		mensajes.add ( "El resultado fue: " + String.valueOf ( resultado ) );
		mensajes.add ( "Los registros subidos son:" + String.valueOf ( c ) );
		MuestraInformacion ( mensajes, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		mensajes.clear ( );
		
		return resultado;
	}
	
	public boolean EliminaUsuario(  ) {
		boolean resultado = false;
		
		String Cliente = "Elimina usuario";
		ArrayList<String> mensajes = new ArrayList <String> ( );
		
		MuestraInformacion ( "Iniciando - Borrado del usuario " + clave_usuario, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		// Borra un usuario.
		
		String sentencia = "DELETE FROM BARPROV15 WHERE clave_usuario = " + clave_usuario + " AND clave_proveedor = " + clave_proveedor ;
		
		// Borrando de local
		
		boolean lBorrado = false;
		boolean rBorrado = false;
		
		lBorrado = cLoc.DeleteRegistrosMySQL ( sentencia );
		
		if ( lBorrado ) {
			MuestraInformacion ( "Se borro el usuario " + clave_usuario + " de local", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		} else {
			MuestraInformacion ( "No se borro el usuario " + clave_usuario + " de local", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		}
		
		// Borrando de remoto
		
		rBorrado = cWeb.DeleteRegistrosMySQL ( sentencia );
		
		if ( rBorrado ) {
			MuestraInformacion ( "Se borro el usuario " + clave_usuario + " de remoto", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		} else {
			MuestraInformacion ( "No se borro el usuario " + clave_usuario + " de remoto", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		}
		
		mensajes.clear ( );
		mensajes.add ( "Terminando borrado del usuario " + clave_usuario );
		mensajes.add ( "El resultado fue: " + String.valueOf ( lBorrado ) + " de local." );
		mensajes.add ( "El resultado fue: " + String.valueOf ( lBorrado ) + " de remoto." );
		MuestraInformacion ( mensajes, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		mensajes.clear ( );
		
		if ( lBorrado && rBorrado ){
			resultado = true;
		} else {
			resultado = false;
		}
		
		return resultado;
	}
	
	public boolean ActualizaUsuario( ) {
		boolean resultado = false;
		
		String Cliente = "Actualizacion usuario";
		ArrayList<String> mensajes = new ArrayList <String> ( );
		
		MuestraInformacion ( "Iniciando - actualizacion del usuario " + clave_usuario, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		// Actualiza usuario.
		
		
		mensajes.clear ( );
		mensajes.add ( "Termiando actualizacion del usuario " + clave_usuario );
		mensajes.add ( "El resultado fue: " + String.valueOf ( resultado ) );
		MuestraInformacion ( mensajes, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		mensajes.clear ( );
		
		return resultado;
	}
	
	//endregion

}
