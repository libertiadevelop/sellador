package com.bmx.srv;

import java.util.ArrayList;
import java.util.HashMap;

import com.bmx.srv.Conecciones;
import com.bmx.srv.ConsultaODBC;
import com.bmx.srv.Utilidades.dondeSeMuestra;
import com.bmx.srv.Utilidades.nivel;
import com.bmx.srv.Utilidades.tipoDeDato;
import com.bmx.srv.Utilidades;


public class ReplicaBases
{
	enum proceso{
		ESTADISTICA_07,
		ESTADISTICA_14
		;
	}
	
	private enum tablas{
		ESTADISTICA_11,
		ESTADISTICA_14,
		CLIENTES,
		ARTICULOS
		;
	}
	
	private static String 	a�o 		= "";
	private static boolean 	setWhereA�o	= false;
	
	private static void setA�o ( String A�o ){
		a�o = A�o;
		
		if ( a�o.trim ( ).length ( ) > 0 ){
			setWhereA�o = true;
		} else {
			setWhereA�o = false;
		}
	}
	
	public static boolean replicaOdbcTableToMySQL( proceso p, int Batch, String A�o ) {
		boolean resultado = false;
	
		setA�o ( A�o );
		
		resultado = executeProceso ( p, Batch );
		
		return resultado;
	}
	
	private static boolean executeProceso( proceso p, int Batch ){
		
		boolean resultado = true;
		
		ConsultaODBC c 	= null;
		ConsultaODBC g	= null;
		
		HashMap<String, ArrayList<String>> res = null;
		
		int numRegistros = 0;
		
		String Cliente = "";
		
		switch ( p ) {
			case ESTADISTICA_07:
				Cliente = "Estadistica 07";
				Utilidades.MuestraInformacion ( "Iniciando,  sincronizaciond de Tablas", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				c 		= new ConsultaODBC ( Conecciones.getERPCon ( ) );
				g 		= new ConsultaODBC ( Conecciones.getMySQLLocalBarpro ( ) );
				
				res		= c.ConsultaVariasColumnasMySQL ( getSentenciaSelectAll ( tablas.ESTADISTICA_11 ) );
				
				if ( res.size ( ) == 0 ){
					Utilidades.MuestraInformacion ( "No se encontraron registros en ODBC", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
					resultado = false;
				} else {
					Utilidades.MuestraInformacion ( "Inicio carga de EST11", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
					g.DeleteRegistrosMySQL ( getSentenciaDel ( tablas.ESTADISTICA_11 ) );
					
					numRegistros = g.InsertaRegistroMysql ( getSentenciaInsert ( tablas.ESTADISTICA_11 ), res, getDatoDeColumna ( tablas.ESTADISTICA_11 ), Batch );
					
					Utilidades.MuestraInformacion ( "Fin carga de EST11", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
					if ( numRegistros > 0 ) {
						Utilidades.MuestraInformacion ( "Inicio carga de CLI02", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
						// carga Articulos
						res 	= c.ConsultaVariasColumnasMySQL ( getSentenciaSelectAll ( tablas.CLIENTES ) );
						
						if ( res.size ( ) == 0 ){
							resultado = false;
							Utilidades.MuestraInformacion ( "No se recuperaron clientes", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
						} else {
							g.DeleteRegistrosMySQL ( getSentenciaDel ( tablas.CLIENTES ) );
							
							numRegistros = g.InsertaRegistroMysql ( getSentenciaInsert ( tablas.CLIENTES ), res, getDatoDeColumna ( tablas.CLIENTES ), Batch );
							Utilidades.MuestraInformacion ( "Fin carga de CLI02", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
							if ( numRegistros == 0 ){
								resultado = false;
								Utilidades.MuestraInformacion ( "No se pudieron insertar registros", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
							}
						}
						
					} else {
						Utilidades.MuestraInformacion ( "No se pudieron insertar registros en EST11", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
					}
				}
				
				Utilidades.MuestraInformacion ( "Finalizando sincronizacion de tablas", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				break;
				
			case ESTADISTICA_14:
				Cliente = "Estadistica 14";
				Utilidades.MuestraInformacion ( "Iniciando, sincronizacion de Tablas", Cliente, nivel.INFO, dondeSeMuestra.AMBOS);
				c 		= new ConsultaODBC ( Conecciones.getERPCon ( ) );
				g 		= new ConsultaODBC ( Conecciones.getMySQLLocalBarpro ( ) );
				
				res 	= c.ConsultaVariasColumnasMySQL ( getSentenciaSelectAll ( tablas.ESTADISTICA_14 ) );
				
				if ( res.size ( ) == 0 ){
					Utilidades.MuestraInformacion ( "No se encontraron registros en ODBC", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
					resultado = false;
				} else {
					
					Utilidades.MuestraInformacion ( "Inicio carga de EST14", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
					g.DeleteRegistrosMySQL ( getSentenciaDel ( tablas.ESTADISTICA_14 ) );
					
					numRegistros = g.InsertaRegistroMysql ( getSentenciaInsert ( tablas.ESTADISTICA_14 ), res, getDatoDeColumna ( tablas.ESTADISTICA_14 ), Batch );
					
					Utilidades.MuestraInformacion ( "Fin carga de EST14", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
					if ( numRegistros > 0 ) {
						Utilidades.MuestraInformacion ( "Inicio carga de INV01", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
						// carga Articulos
						res 	= c.ConsultaVariasColumnasMySQL ( getSentenciaSelectAll ( tablas.ARTICULOS ) );
						
						if ( res.size ( ) == 0 ){
							resultado = false;
							Utilidades.MuestraInformacion ( "No se recuperaron articulos", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
						} else {
							g.DeleteRegistrosMySQL ( getSentenciaDel ( tablas.ARTICULOS ) );
							
							numRegistros = g.InsertaRegistroMysql ( getSentenciaInsert ( tablas.ARTICULOS ), res, getDatoDeColumna ( tablas.ARTICULOS ), Batch );
							Utilidades.MuestraInformacion ( "Fin carga de INV01", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
							if ( numRegistros == 0 ){
								resultado = false;
								Utilidades.MuestraInformacion ( "No se pudieron insertar registros", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
							}
						}
						
					} else {
						Utilidades.MuestraInformacion ( "No se pudieron insertar registros en EST14", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
					}
				
				}
				break;
		}
		
		Utilidades.MuestraInformacion ( "Finalizando sincronizacion de tablas", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		return resultado;
	}
	
	private static HashMap<Integer, tipoDeDato> getDatoDeColumna( tablas t ){
		
		HashMap<Integer, tipoDeDato> datoDeColumna = new HashMap <Integer,tipoDeDato> ( );
		int h = 0;
		switch ( t ) {
			case ESTADISTICA_11:
				h++;
				datoDeColumna.put ( h++, tipoDeDato.CADENA );			
				datoDeColumna.put ( h++, tipoDeDato.CADENA );
				datoDeColumna.put ( h++, tipoDeDato.ENTERO );
				datoDeColumna.put ( h++, tipoDeDato.ENTERO );
				
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				break;
				
			case ESTADISTICA_14:
				h++;
				datoDeColumna.put ( h++, tipoDeDato.CADENA );
				datoDeColumna.put ( h++, tipoDeDato.CADENA );
				datoDeColumna.put ( h++, tipoDeDato.CADENA );
				datoDeColumna.put ( h++, tipoDeDato.CADENA );
				datoDeColumna.put ( h++, tipoDeDato.ENTERO);
				
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				datoDeColumna.put ( h++, tipoDeDato.DOBLE );
				break;
			case ARTICULOS:
				
				h++;
				datoDeColumna.put ( h++, tipoDeDato.CADENA );
				datoDeColumna.put ( h++, tipoDeDato.ENTERO );
				datoDeColumna.put ( h++, tipoDeDato.ENTERO );
				break;
			case CLIENTES:
				h++;
				datoDeColumna.put ( h++, tipoDeDato.CADENA );
				datoDeColumna.put ( h++, tipoDeDato.ENTERO );
				break;

		}
		
		return datoDeColumna;
	}
	
	private static String getSentenciaSelectAll( tablas t ){
		String sentencia = "";

		
		switch ( t ) {
			case ESTADISTICA_11:
				sentencia = "SELECT			E1_CODART, E1_NUMCLI, E1_SUCU, E1_EJER, " +
											"SUM(IFNULL(E1_IMPVEN_1,0)), SUM(IFNULL(E1_IMPVEN_2,0)), SUM(IFNULL(E1_IMPVEN_3,0)), SUM(IFNULL(E1_IMPVEN_4,0)), " +
											"SUM(IFNULL(E1_IMPVEN_5,0)), SUM(IFNULL(E1_IMPVEN_6,0)), SUM(IFNULL(E1_IMPVEN_7,0)), SUM(IFNULL(E1_IMPVEN_8,0)), " +
											"SUM(IFNULL(E1_IMPVEN_9,0)), SUM(IFNULL(E1_IMPVEN_10,0)), SUM(IFNULL(E1_IMPVEN_11,0)), SUM(IFNULL(E1_IMPVEN_12,0)) " +
							"FROM			PTBEST11 "	+
		  ( (setWhereA�o) ? "WHERE			E1_EJER = " + a�o + " " : "" )+				
							"GROUP BY		E1_CODART, E1_NUMCLI, E1_SUCU, E1_EJER";	
				break;
			case ESTADISTICA_14:
				sentencia = "SELECT 		E4_CODART, E4_SUCU, '0', '0', E4_EJER, " +
											"SUM(IFNULL(E4_CANVEN_1,0)), SUM(IFNULL(E4_CANVEN_2,0)), SUM(IFNULL(E4_CANVEN_3,0)), SUM(IFNULL(E4_CANVEN_4,0)), SUM(IFNULL(E4_CANVEN_5,0)), SUM(IFNULL(E4_CANVEN_6,0)), SUM(IFNULL(E4_CANVEN_7,0)), SUM(IFNULL(E4_CANVEN_8,0)), SUM(IFNULL(E4_CANVEN_9,0)), SUM(IFNULL(E4_CANVEN_10,0)), SUM(IFNULL(E4_CANVEN_11,0)), SUM(IFNULL(E4_CANVEN_12,0)), " +
											"SUM(IFNULL(E4_IMPVEN_1,0)), SUM(IFNULL(E4_IMPVEN_2,0)), SUM(IFNULL(E4_IMPVEN_3,0)), SUM(IFNULL(E4_IMPVEN_4,0)), SUM(IFNULL(E4_IMPVEN_5,0)), SUM(IFNULL(E4_IMPVEN_6,0)), SUM(IFNULL(E4_IMPVEN_7,0)), SUM(IFNULL(E4_IMPVEN_8,0)), SUM(IFNULL(E4_IMPVEN_9,0)), SUM(IFNULL(E4_IMPVEN_10,0)), SUM(IFNULL(E4_IMPVEN_11,0)), SUM(IFNULL(E4_IMPVEN_12,0)), " +
											"SUM(IFNULL(E4_COSVEN_1,0)), SUM(IFNULL(E4_COSVEN_2,0)), SUM(IFNULL(E4_COSVEN_3,0)), SUM(IFNULL(E4_COSVEN_4,0)), SUM(IFNULL(E4_COSVEN_5,0)), SUM(IFNULL(E4_COSVEN_6,0)), SUM(IFNULL(E4_COSVEN_7,0)), SUM(IFNULL(E4_COSVEN_8,0)), SUM(IFNULL(E4_COSVEN_9,0)), SUM(IFNULL(E4_COSVEN_10,0)), SUM(IFNULL(E4_COSVEN_11,0)), SUM(IFNULL(E4_COSVEN_12,0)), " +
											"SUM(IFNULL(E4_IMPPRE_1,0)), SUM(IFNULL(E4_IMPPRE_2,0)), SUM(IFNULL(E4_IMPPRE_3,0)), SUM(IFNULL(E4_IMPPRE_4,0)), SUM(IFNULL(E4_IMPPRE_5,0)), SUM(IFNULL(E4_IMPPRE_6,0)), SUM(IFNULL(E4_IMPPRE_7,0)), SUM(IFNULL(E4_IMPPRE_8,0)), SUM(IFNULL(E4_IMPPRE_9,0)), SUM(IFNULL(E4_IMPPRE_10,0)), SUM(IFNULL(E4_IMPPRE_11,0)), SUM(IFNULL(E4_IMPPRE_12,0)) " +
							"FROM			PTBEST14 " +				
		 ( (setWhereA�o) ?  "WHERE			E4_EJER = " + a�o + " " : "" ) +
							"GROUP BY		E4_CODART, E4_SUCU, E4_EJER";
				break;
			case ARTICULOS:
				sentencia = "SELECT 		AR_CODART, AR_LINE, " +
										   "AR_SUBLIN " +
							"FROM 			MDGINV01 " +
							"WHERE 			AR_LINE > 0 " +
								"AND 		AR_SUBLIN > 0";
				break;
				
			case CLIENTES:
				sentencia = "SELECT			CL_NUMCLI, CL_ZONVEN " +
							"FROM			MDGCLI02 ";	
				break;
		}
		
		return sentencia;
	}

	private static String getSentenciaInsert ( tablas t ){
		String sentencia = "";
		
		switch ( t ) {
			case ESTADISTICA_11:
				sentencia = 	"INSERT INTO PTBEST11 (E1_CODART, E1_NUMCLI, E1_SUCU, E1_EJER, "			 +
													  "E1_IMPVEN_1,E1_IMPVEN_2,E1_IMPVEN_3,E1_IMPVEN_4," 	 +
													  "E1_IMPVEN_5,E1_IMPVEN_6,E1_IMPVEN_7,E1_IMPVEN_8," 	 +
													  "E1_IMPVEN_9,E1_IMPVEN_10,E1_IMPVEN_11,E1_IMPVEN_12) " +
								"VALUES 			  (?,?,?,?, " +
													  "?,?,?,?, " +
													  "?,?,?,?, " +
													  "?,?,?,?)";
				break;
			case ESTADISTICA_14:
				sentencia = 	"INSERT INTO PTBEST14 (E4_CODART, E4_SUCU, E4_LINE, E4_SUBLIN, E4_EJER, " +
						 							  "E4_CANVEN_1, E4_CANVEN_2, E4_CANVEN_3, E4_CANVEN_4, E4_CANVEN_5, E4_CANVEN_6, E4_CANVEN_7, E4_CANVEN_8, E4_CANVEN_9, E4_CANVEN_10, E4_CANVEN_11, E4_CANVEN_12, " +
						 							  "E4_IMPVEN_1, E4_IMPVEN_2, E4_IMPVEN_3, E4_IMPVEN_4, E4_IMPVEN_5, E4_IMPVEN_6, E4_IMPVEN_7, E4_IMPVEN_8, E4_IMPVEN_9, E4_IMPVEN_10, E4_IMPVEN_11, E4_IMPVEN_12, " +
						 							  "E4_COSVEN_1, E4_COSVEN_2, E4_COSVEN_3, E4_COSVEN_4, E4_COSVEN_5, E4_COSVEN_6, E4_COSVEN_7, E4_COSVEN_8, E4_COSVEN_9, E4_COSVEN_10, E4_COSVEN_11, E4_COSVEN_12, " +
						 							  "E4_IMPPRE_1, E4_IMPPRE_2, E4_IMPPRE_3, E4_IMPPRE_4, E4_IMPPRE_5, E4_IMPPRE_6, E4_IMPPRE_7, E4_IMPPRE_8, E4_IMPPRE_9, E4_IMPPRE_10, E4_IMPPRE_11, E4_IMPPRE_12 " +
						 							  ") " +
						 		"VALUES				 (?,?,?,?,?, " +
						 							 "?,?,?,?,?,?,?,?,?,?,?,?, " +
						 							 "?,?,?,?,?,?,?,?,?,?,?,?, " +
						 							 "?,?,?,?,?,?,?,?,?,?,?,?, " +
						 							 "?,?,?,?,?,?,?,?,?,?,?,?) " ;
				break;
			case CLIENTES:
				sentencia = 	"INSERT INTO MDGCLI02P (CL_NUMCLI, CL_ZONVEN) " +
								"VALUES 			   (?,?)";
				break;
			case ARTICULOS:
				sentencia = 	"INSERT INTO MDGINV01P (AR_CODART, AR_LINE, AR_SUBLIN) " +
								"VALUES				   (?,?,?)";
				break;
		}
		
		
		
		return sentencia;
	}

	private static String getSentenciaDel ( tablas t ){
		String sentencia = "";
		
		switch ( t ) {
			case ESTADISTICA_11:
				sentencia = "DELETE FROM PTBEST11 " + (( setWhereA�o ) ? " WHERE E1_EJER = " + a�o + " ": "" ) ;
				break;
			case ESTADISTICA_14:
				sentencia = "DELETE FROM PTBEST14 " + (( setWhereA�o ) ? " WHERE E4_EJER = " + a�o + " ": "" );
				break;
			case ARTICULOS:
				sentencia = "DELETE FROM MDGINV01P";
				break;
			case CLIENTES:
				sentencia = "DELETE FROM MDGCLI02P";
				break;
		}
		
		return sentencia;
	}
}
