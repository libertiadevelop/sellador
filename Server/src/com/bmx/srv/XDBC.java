package com.bmx.srv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import com.bmx.srv.Utilidades.dondeSeMuestra;
import com.bmx.srv.Utilidades.nivel;

public class XDBC
{
	String manejador 	= "XDBC";
	String parteFlujo;
	
	boolean estado;
	int		codError;
	String	desError;
	String	mensaje;
	
	ArrayList <String> log = new ArrayList <String> ( );
	
	ArrayList<String> 						columnas 	= new ArrayList <String> ( );
	ArrayList<Integer>						lenColumnas = new ArrayList <Integer> ( );
	HashMap <String, ArrayList <String>> 	datos 		= new HashMap <String, ArrayList<String>> ( );
	
	int registros = 0;
	
	File 	archivo = null;
	String  modulo 	= "";
	ArrayList <String> datosDeArchivo 	= new ArrayList <String> ( );
	ArrayList <String> datosOriginales 	= new ArrayList <String> ( );
	
	public boolean getEstado(){
		return estado;
	}
	public int getCodError(){
		return codError;
	}
	public String getDescripcionDelError(){
		return desError;
	}
	public String getMensaje(){
		return mensaje;
	}
	public ArrayList <String> getLog(){
		return log;
	}
	
	public ArrayList <String> getColumnas(){
		return columnas;
	}
	
	public HashMap <String, ArrayList <String>> getArreglo(){
		return datos;
	}

	private enum error {
		SIN_ERROR(0),
		NO_EXISTE_EL_ARCHIVO(1),
		EL_ARCHIVO_ESTA_VACIO(2),
		NO_SE_PUDO_RECUPERAR_EL_ENCABEZADO(3),
		ERROR_AL_LEER_EL_ARCHIVO(4),
		NO_SE_PASO_UN_PATH(5),
		AL_CERRAR_EL_ARCHIVO(6),
		NO_SE_PUDO_GUARDAR_EL_ARCHIVO(7)
		;
		int er;
		private error( int e ){
			er = e;
		}
		public int getCodError(){
			return er;
		}
		public String getDescripcionDelError() {
			String resultado = "";
			switch ( er ) {
				case 0:
					resultado = "Sin error";
					break;
				case 1:
					resultado = "No se paso un archivo";
					break;
				case 2:
					resultado = "El archivo no tiene informacion";
					break;
				case 3:
					resultado = "Error al leer el encabezado";
					break;
				case 4: 
					resultado = "Error al leer el archivo";
					break;
				case 5:
					resultado = "No se paso el path al archivo";
					break;
				case 6:
					resultado = "Problemas al cerrar el archivo";
					break;
				case 7:
					resultado = "No se pudo guardar el archivo";
					break;
			}
			return resultado;
		}
		
		
	}
	
	private enum flujo {
		ABRIENDO_ARCHIVO(0),
		LIMPIANDO_ENCABEZADO(1),
		RECUPERANDO_ENCABEZADO(2),
		GENERANDO_ARREGLO_COLUMNAS(3),
		GENERANDO_ARREGLO_PARTIDAS(4),
		GUARDANDO_ARCHIVO(6),
		BORRANDO_ARCHIVO_PREVIO(7)
		;
		int f;
		private flujo(int fl){
			f = fl;
		}
		public String getDescripcionDelFlujo(){
			String resultado ="";
			switch ( f ) {
				case 0:
					resultado = "Abriendo el archivo";
					break;
				case 1:
					resultado = "Limpiando encabezado de archivo";
					break;
				case 2:
					resultado = "Recuperando encabezado ( longitud / nombre )";
					break;
				case 3:
					resultado = "Recuperando nombre de columnas y longitud";
					break;
				case 4:
					resultado = "Recuperando partidas";
					break;
				case 5: 
					resultado = "Guardando archivo de texto";
					break;
				case 6:
					resultado = "Borrando archivo previo";
					break;

			}
			
			return resultado;
		}
	}
	
	private enum est{
		ERROR(1),
		OK(2)
		;
		int i = 2;
		private est( int n ){
			i = n;
		}
		public boolean getEstado(){
			boolean resultado = false;
			switch ( i ) {
				case 1:
					resultado = false;
					break;
				case 2:
					resultado = true;
					break;
			}
			return resultado;
		}
	}
	
	private enum tipoDeLinea {
		ENCABEZADO,
		COLUMNAS,
		DATO
		;
	}
	
	public XDBC( String pathToFile ) {
		
		modulo = "Constructor";
		
		setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.ABRIENDO_ARCHIVO, "", modulo );
		
		if ( pathToFile.isEmpty ( ) ){
			setEstado ( est.ERROR.getEstado ( ) , error.NO_SE_PASO_UN_PATH, flujo.ABRIENDO_ARCHIVO, "", modulo );
			return;
		}
		
		archivo = new File ( pathToFile );
		
		if ( !archivo.exists ( ) ){
			setEstado ( est.ERROR.getEstado ( ), error.NO_EXISTE_EL_ARCHIVO, flujo.ABRIENDO_ARCHIVO, "El archivo: " + pathToFile + " no existe", modulo );
		}
		if ( estado ){
			recuperaInformacion ( );
		}
	}
	
	void recuperaInformacion(){
		LeeArchivo ( );
		if ( estado ){
			// Procede a leer el arreglo
			int cont=0;
			
			for ( String linea : datosDeArchivo ){
				switch ( cont++ ) {
					case 0:
						AnalizaLinea ( linea, tipoDeLinea.ENCABEZADO );
						break;
					case 1:
						AnalizaLinea ( linea, tipoDeLinea.COLUMNAS );
						break;
					default:
						AnalizaLinea ( linea, tipoDeLinea.DATO );
						break;
				}
				
				if ( !estado ){
					break;
				}
				
			}
			
		}
	}
	
	void LeeArchivo(){
		modulo ="Lector";
		setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.ABRIENDO_ARCHIVO, "Leyendo", modulo );
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(archivo));
		    String line = br.readLine();
		    int lineas = 1;
		    while (line != null) {
		    	
		    	if ( lineas++ > 7 ){
		    		datosDeArchivo.add ( line );
		    	}
		    	datosOriginales.add ( line );
		        line = br.readLine();
		    }
		    setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.ABRIENDO_ARCHIVO, "", modulo );
		} catch ( FileNotFoundException e ) {
			setEstado ( est.ERROR.getEstado ( ), error.NO_EXISTE_EL_ARCHIVO, flujo.ABRIENDO_ARCHIVO, e.toString ( ), modulo );
		} catch ( IOException e ) {
			setEstado ( est.ERROR.getEstado ( ), error.ERROR_AL_LEER_EL_ARCHIVO, flujo.ABRIENDO_ARCHIVO, e.toString ( ), modulo );
		} finally {
		    try {
				br.close();
			} catch ( IOException e ) {
				setEstado ( est.ERROR.getEstado ( ), error.ERROR_AL_LEER_EL_ARCHIVO, flujo.ABRIENDO_ARCHIVO, e.toString ( ), modulo );
			}
		}
		
	}

	void GuardaArchivo( String pathFile, boolean Elimina ){
		modulo = "Escritor";
		
		if ( pathFile.isEmpty ( ) ){
			setEstado ( est.ERROR.getEstado ( ), error.EL_ARCHIVO_ESTA_VACIO, flujo.GUARDANDO_ARCHIVO, "No hay cadena de path", modulo );
		}
		
		File archivoAGuardar = new File ( pathFile );
		
		if ( archivoAGuardar.exists ( ) && Elimina ) {
			archivoAGuardar.delete ( );
		}
		
		if ( !estado ) {
			return;
		}
		
		PrintWriter writer = null;
		
		try {
			writer = new PrintWriter ( archivoAGuardar );
			
			for ( String linea : datosOriginales ){
				writer.println ( linea );
			}
			writer.flush ( );
			writer.close ( );
		} catch ( Exception e ){
			setEstado ( est.ERROR.getEstado ( ), error.NO_SE_PUDO_GUARDAR_EL_ARCHIVO, flujo.GUARDANDO_ARCHIVO, e.toString ( ), modulo );
		} finally {
			if ( writer != null ){
				try {
					writer.flush ( );
					writer.close ( );
				} catch ( Exception e ){}
			}
		}
		
	}
	
	void AnalizaLinea( String linea, tipoDeLinea t ){
		switch ( t ) {
			case ENCABEZADO:
				linea = linea.trim ( );
				String intermedio = linea.replaceAll("^ +| +$|( )+", " ");
				String[] x = intermedio.split ( " " );
				columnas = new ArrayList <String> ( Arrays.asList ( x ) );
				break;
			case COLUMNAS:
				linea = linea.trim ( );
				String[] col = linea.split ( " " );
				
				for ( String c : col ){
					int len = c.length ( );
					lenColumnas.add ( len );
				}
				
				break;
			case DATO:
				ArrayList <String> dat = new ArrayList <String> ( );
				String resultante = linea;
				for ( int l : lenColumnas) {
					resultante = resultante.substring ( 1 );
					String dato = resultante.substring ( 0, l ).trim ( );
					resultante = resultante.substring ( l );
					dat.add ( dato );
				}
				
				datos.put ( String.valueOf ( registros++ ), dat );
				
				break;
		}
	}
	
	void setEstado( boolean es, error er, flujo fl, String me, String modulo ){

		estado 		= es;
		codError 	= er.getCodError ( );
		desError 	= er.getDescripcionDelError ( );
		mensaje		= me;
		
		log.add ( fl.getDescripcionDelFlujo ( ) );
		
		String mensajeFinal = er.getDescripcionDelError ( ) + " - " + me;
		String cliente 		= manejador + "-" + modulo;
		
		Utilidades.MuestraInformacion ( mensajeFinal, cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
	}
}
