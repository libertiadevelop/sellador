package com.bmx.srv;

import java.io.FileInputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import com.bmx.srv.Utilidades.dondeSeMuestra;
import com.bmx.srv.Utilidades.nivel;
import com.bmx.srv.Utilidades.tipoDeDato;
import com.mysql.jdbc.MySQLConnection;


public class ConsultaODBC 
{
	
	//region ENUMERACIONES
	enum DatoDeColumna {
		NOMBRE,
		VALOR,
		TIPO_DE_DATO;
	}
	//endregion
	
	//region VARIABLES PRIVADAS
	boolean 		resultado;
	boolean			continuar;
	
	Connection		con;
	MySQLConnection mCom;
	
	ArrayList<String> 	log		=  new ArrayList<String>();
	String				error;
	//endregion
	
	//region PROPIEDADES
	public boolean getResultado() 
	{
		return resultado;
	}
	
	public String getError()
	{
		return error;
	}
	
	public void setMySQLConn( MySQLConnection c )
	{
		if ( c == null ){
			continuar = false;
		} else {
			mCom = c;
			continuar = true;
		}
		
		resultado = continuar;
	}
	//endregion
	
	//region CONSTRUCTOR
	public ConsultaODBC( Connection c )
	{
		if ( c == null ){
			continuar = false;
		} else {
			con = c;
			continuar = true;
		}
		
		resultado = continuar;
	}
	public ConsultaODBC( MySQLConnection c )
	{
		if ( c == null ){
			continuar = false;
		} else {
			mCom = c;
			continuar = true;
		}
		
		resultado = continuar;
	}
	//endregion
	
	//region METODOS PUBLICOS
	
	public HashMap<String, ArrayList<String>> ConsultaVariasColumnasODBC( String sentencia ){
		
		if ( !continuar ) {
			error 		= "La conexion esta vacia";
			resultado	= false;
			return null;
		}
		
		HashMap<String, ArrayList<String>> resultadoConsultaVariasColumnas = new HashMap<String, ArrayList<String>>();
		
		ResultSet 			rs = null;
		PreparedStatement	ps = null;
		
		try {
			
			//region REALIZA LA CONSULTA
			ps = con.prepareStatement( sentencia );
			rs = ps.executeQuery();
			//endregion
			
			//region VALIDA QUE HAYA REGISTROS
			//if ( !rs.isBeforeFirst ( ) ){
			//	resultado 	= false;
			//	error		= "No se recuperaron registros";
			//	return 		null;
			//}
			//endregion
			
			//region RECUPERA LA INFORMACION
			ResultSetMetaData  metadata =  rs.getMetaData();
			
			int numColumnas = metadata.getColumnCount();
			int numRenglon	= 0;
			
			while ( ( rs.next() )  ) {
				numRenglon ++;
				ArrayList<String> columnas = new ArrayList<String>();
				
				for( int i = 1; i <= numColumnas; i++ ){
					columnas.add( rs.getString( i ));
				}
				
				resultadoConsultaVariasColumnas.put( String.valueOf( numRenglon ), columnas );
			}
			 
			rs.close();
			
			resultado = true;
			
			return resultadoConsultaVariasColumnas;
			
			//endregion
			
		} catch (SQLException e) {
			error = e.toString();
			resultado = false;
		} catch (Exception e) {
			error = e.toString();
			resultado = false;
		} finally {
			
			if ( con != null ){
			
				try {
					con.close();
				} catch ( Exception e ) {
					error = e.toString();
				}
			}
		}
		
		if ( !resultado ) {
			resultadoConsultaVariasColumnas = null;
		}
		
		return resultadoConsultaVariasColumnas;
			
	}
	
	public HashMap<String, ArrayList<String>> ConsultaVariasColumnasMySQL( String sentencia ){
		
		if ( !continuar ) {
			error 		= "La conexion esta vacia";
			resultado	= false;
			return null;
		}
		
		HashMap<String, ArrayList<String>> resultadoConsultaVariasColumnas = new HashMap<String, ArrayList<String>>();
		
		ResultSet 			rs = null;
		PreparedStatement	ps = null;
		
		try {
			
			//region REALIZA LA CONSULTA
			ps = con.prepareStatement( sentencia );
			rs = ps.executeQuery();
			//endregion
			
			//region VALIDA QUE HAYA REGISTROS
			
			
			
			//endregion
			
			//region RECUPERA LA INFORMACION
			ResultSetMetaData  metadata =  rs.getMetaData();

			int numColumnas = metadata.getColumnCount();
			int numRenglon	= 0;
			
			while ( ( rs.next() )  ) {
				numRenglon ++;
				ArrayList<String> columnas = new ArrayList<String>();
				
				for( int i = 1; i <= numColumnas; i++ ){
					columnas.add( rs.getString( i ));
				}
				resultadoConsultaVariasColumnas.put( String.valueOf( numRenglon ), columnas );
			}
			 
			rs.close();
			
			resultado = true;
			
			return resultadoConsultaVariasColumnas;
			
			//endregion
			
		} catch (SQLException e) {
			error = e.toString();
			resultado = false;
		} catch (Exception e) {
			error = e.toString();
			resultado = false;
		} finally {
			if ( mCom != null ) {
				try {
					mCom.close();
				} catch ( Exception e ){
					error = e.toString();
				}
			}
		}
		
		if ( !resultado ) {
			resultadoConsultaVariasColumnas = null;
		}
		
		return resultadoConsultaVariasColumnas;
			
	}
	
	public HashMap<String, ArrayList<Object>> ConsultaVariasColumnasMySQL_withBlob( String sentencia, HashMap<Integer, tipoDeDato> datoDeColumna ){
		
		if ( !continuar ) {
			error 		= "La conexion esta vacia";
			resultado	= false;
			return null;
		}
		
		HashMap<String, ArrayList<Object>> resultadoConsultaVariasColumnas = new HashMap <String, ArrayList<Object>> ( );
		
		ResultSet 			rs = null;
		PreparedStatement	ps = null;
		
		try {
			
			//region REALIZA LA CONSULTA
			ps = con.prepareStatement( sentencia );
			rs = ps.executeQuery();
			//endregion
			
			//region VALIDA QUE HAYA REGISTROS
			
			//endregion
			
			//region RECUPERA LA INFORMACION
			ResultSetMetaData  metadata =  rs.getMetaData();

			int numColumnas = metadata.getColumnCount();
			int numRenglon	= 0;
			
			while ( ( rs.next() )  ) {
				numRenglon ++;
				ArrayList<Object> columnas = new ArrayList <Object> ( );
				
				for( int i = 1; i <= numColumnas; i++ ){
					
					tipoDeDato t = datoDeColumna.get ( i );
					
					switch ( t ) {
						case CADENA:
							columnas.add ( rs.getString ( i ) );
							break;
						case ENTERO:
							columnas.add ( rs.getInt ( i ) );
							break;
						case DOBLE:
							columnas.add ( rs.getDouble ( i ) );
							break;
						case BLOB:
							columnas.add ( rs.getBlob ( i ) );
							break;
					}
				}

				resultadoConsultaVariasColumnas.put( String.valueOf( numRenglon ), columnas );
			}
			 
			rs.close();
			
			resultado = true;
			
			return resultadoConsultaVariasColumnas;
			
			//endregion
			
		} catch (SQLException e) {
			error = e.toString();
			resultado = false;
		} catch (Exception e) {
			error = e.toString();
			resultado = false;
		} finally {
			if ( mCom != null ) {
				try {
					mCom.close();
				} catch ( Exception e ){
					error = e.toString();
				}
			}
		}
		
		if ( !resultado ) {
			resultadoConsultaVariasColumnas = null;
		}
		
		return resultadoConsultaVariasColumnas;

	}
	
	public boolean ActualizaRegistroMySQL ( String sentencia ){
		boolean ResultadoActualizaRegistro = false;
		
		if ( !continuar ) {
			error 		= "La conexion esta vacia";
			resultado	= false;
			return false;
		}
		
		PreparedStatement	ps = null;
		
		
		
		try {
			
			//region REALIZA LA actualizacion
			ps = con.prepareStatement( sentencia );
			int Registros = ps.executeUpdate( );
			//endregion
				
			if ( Registros > 0 ) {
				resultado = true;
			} else {
				resultado = false;
			}
	
			ResultadoActualizaRegistro = resultado;
			
			return ResultadoActualizaRegistro;
			
		} catch (SQLException e) {
			error = e.toString();
			resultado = false;
		} catch (Exception e) {
			error = e.toString();
			resultado = false;
		} finally {
			if ( mCom != null ) {
				try {
					mCom.close();
				} catch ( Exception e ){
					error = e.toString();
				}
			}
		}
		
		if ( !resultado ) {
			ResultadoActualizaRegistro = false;
		}
		
		return ResultadoActualizaRegistro;
	}
	
	public int updateRegistrosMySQL (HashMap<String, ArrayList<String>> datos, 
			ArrayList<String> nombreDeCampo, 
			HashMap<Integer, tipoDeDato> datoDeColumna, 
			int Batch, 
			ArrayList<String> llave, 
			String nombreDeTabla ){
		
		
		HashMap<String, String> 						sentencias 		= new HashMap <String, String> ( );
		HashMap<String, ArrayList<String>> 				documentos 		= new HashMap <String, ArrayList<String>> ( );
		HashMap<String, HashMap<Integer, tipoDeDato>>	datosDeColumna	= new HashMap <String, HashMap<Integer,tipoDeDato>> ( );
		
		HashMap<Integer, tipoDeDato>					ddc				= new HashMap <Integer, Utilidades.tipoDeDato> ( );
		
		int									registro	= 0;
		
		int resultado = 0;
		// Se encarga de armar la sentencia.
		for ( String key : datos.keySet ( ) ){
			
			int numDatoColumna = 1;
			
			ArrayList<String> valores = datos.get ( key );
			
			HashMap<Integer, HashMap <DatoDeColumna, String>> p = getPares ( nombreDeCampo, valores, true );
			
			// Genera la sentencia where 
			
			String separador 	= " ";
			String and			= "AND";
			String where		= "WHERE";
			String set			= "SET";
			String separadorU	= ",";
			
			String datosSentenciaWhere = "";
			
			String sentenciaWhere = "";
			
			String sentencia = "";
			
			ArrayList<String> datosParaWhere = new ArrayList <String> ( );
			
			boolean inicio = true;
			
			for ( String colLlave : llave ){
				int numColLlave = Integer.valueOf ( colLlave );
				
				if ( inicio ) {
					inicio = false;
				} else {
					datosSentenciaWhere = datosSentenciaWhere + separador + and + separador;
				}
				
				datosSentenciaWhere = datosSentenciaWhere + p.get ( numColLlave ).get ( DatoDeColumna.NOMBRE ) + "=?" + separador;
				datosParaWhere.add ( p.get ( numColLlave ).get ( DatoDeColumna.VALOR ) );
				ddc.put ( numDatoColumna++, datoDeColumna.get ( numColLlave ) );
				// Elimina la llave de los pares, para que no se vuelva a considerar
				p.remove ( numColLlave );
			}
			
			sentenciaWhere = where + separador + datosSentenciaWhere;
			
			// Generar los campos a actualizar
			
			inicio = true;
			
			String datosSentenciaUdate = "";
			
			ArrayList<String> datosParaSet = new ArrayList <String> ( );
			
			for ( int col : p.keySet ( ) ){
									
				if ( inicio ){
					inicio = false;
				} else {
					datosSentenciaUdate = datosSentenciaUdate + separadorU + separador;
				}
				
				datosSentenciaUdate = datosSentenciaUdate + p.get ( col ).get ( DatoDeColumna.NOMBRE ) + "=?" + separador;
				datosParaSet.add ( p.get ( col ).get ( DatoDeColumna.VALOR ) );
				ddc.put ( numDatoColumna++,  datoDeColumna.get(col) );
			}
			
			// Genera la sentencia para guardar
			
			sentencia = 	"UPDATE" + separador + nombreDeTabla + separador + 
										set + separador + datosSentenciaUdate + separador + 
										sentenciaWhere;
			
			// Genera el array de datos a guardar.
			ArrayList<String> valoresToUpdate = new ArrayList <String> ( );
			
			int size = datosParaSet.size ( );
			
			for ( int col = 0; col < size ; col ++ ){
				valoresToUpdate.add ( datosParaSet.get ( col ) );
			}
			
			size = datosParaWhere.size ( );
			
			for ( int col = 0; col < size ; col++ ){
				valoresToUpdate.add ( datosParaWhere.get ( col ) );
			}
			
			// Invierte los datos de datos de columna.
			
			size = ddc.size ( );
			
			HashMap<Integer, tipoDeDato> invertido = new HashMap <Integer, Utilidades.tipoDeDato> ( );
			
			int colInvertido = 1;
			
			for ( int col = size; col > 0; col-- ){
				invertido.put ( colInvertido++, ddc.get ( col ) );
			}
			
			
			
			// manda la informacion
		
			sentencias.put ( String.valueOf ( registro ), sentencia );
			documentos.put ( String.valueOf ( registro ), valoresToUpdate );
			datosDeColumna.put ( String.valueOf ( registro ), invertido );
			registro++;
		}
		
		resultado = updateRegistrosMySQL (sentencias, documentos, datosDeColumna, 100);
		
		
		return resultado;
	}
	
	public boolean InsertaRegistroMySQL ( String sentencia )
	{
		boolean resultadoInsertaRegistrosMySQL = true;
		
		if ( !continuar ){
			error 		= "La conexion esta vacia";
			resultado	= false;
			return false;
		}
		
		PreparedStatement	ps = null;
		
		try {
			
			//region REALIZA LA actualizacion
			ps = con.prepareStatement( sentencia );
			int Registros = ps.executeUpdate( );
			//endregion
				
			if ( Registros > 0 ) {
				resultado = true;
			} else {
				resultado = false;
			}
	
			resultadoInsertaRegistrosMySQL = resultado;
			
			return resultadoInsertaRegistrosMySQL;
			
		} catch (SQLException e) {
			error = e.toString();
			resultado = false;
		} catch (Exception e) {
			error = e.toString();
			resultado = false;
		} finally {
			if ( mCom != null ) {
				try {
					mCom.close();
				} catch ( Exception e ){
					error = e.toString();
				}
			}
		}
		
		return resultado;
	}
	
	public int InsertaRegistroMysql( String sentencia, HashMap<String, ArrayList <String>> registros, HashMap<Integer, tipoDeDato> datoDeColumna, int Batch )
	{
		int registrosGuardados = 0;
		
		if ( !continuar ){
			error 		= "La conexion esta vacia";
			resultado	= false;
			return registrosGuardados;
		}
		
		PreparedStatement	ps = null;
		
		try {
			
			ps = con.prepareStatement ( sentencia );
			
			int sen = 0;
			
			for ( String renglon : registros.keySet ( ) ){
			
				HashMap <Integer, String> valoresRecuperados = new HashMap <Integer, String> ( );
				int campo = 0;
				for ( String valor : registros.get ( renglon ) ) {
					campo++;
					
					if ( valor == null  ){
						valor = "0";
					}
					
					valoresRecuperados.put ( campo, valor );
				}
				
				for ( int col : valoresRecuperados.keySet ( ) ){
					tipoDeDato ddc = datoDeColumna.get ( col );
					
					switch ( ddc ) {
						case CADENA:
							ps.setString ( col, valoresRecuperados.get ( col ) );
							break;
						case ENTERO:
							ps.setInt ( col, Integer.parseInt ( valoresRecuperados.get ( col ) ) );
							break;
						case DOBLE:
							ps.setDouble ( col, Double.parseDouble ( valoresRecuperados.get( col ) ) );
							break;
						case BLOB:
							ps.setBlob ( col, new FileInputStream ( valoresRecuperados.get( col ) ) );
					}
					
				}
				
				ps.addBatch ( );
				
				sen++;
				
				if ( sen == Batch ){
					sen = 0;
					registrosGuardados = registrosGuardados + ps.executeBatch ( ).length;
				}
				
				
			}
			
			if ( sen > 0 ) {
				registrosGuardados = registrosGuardados + ps.executeBatch ( ).length;
			}
			
		} catch (SQLException e) {
			error = e.toString();
			resultado = false;
		} catch (Exception e) {
			error = e.toString();
			resultado = false;
		} finally {
			if ( mCom != null ) {
				try {
					mCom.close();
				} catch ( Exception e ){
					error = e.toString();
				}
			}
		}
		
		return registrosGuardados;
	}
	
	public int InsertaRegistroMysql_wb( String sentencia, HashMap<String, ArrayList <Object>> registros, HashMap<Integer, tipoDeDato> datoDeColumna, int Batch )
	{
		int registrosGuardados = 0;
		
		if ( !continuar ){
			error 		= "La conexion esta vacia";
			resultado	= false;
			return registrosGuardados;
		}
		
		PreparedStatement	ps = null;
		
		try {
			
			ps = con.prepareStatement ( sentencia );
			
			int sen = 0;
			
			for ( String renglon : registros.keySet ( ) ){
			
				HashMap <Integer, Object> valoresRecuperados = new HashMap <Integer, Object> ( );
				int campo = 0;
				for ( Object valor : registros.get ( renglon ) ) {
					campo++;
					
					if ( valor == null  ){
						valor = "0";
					}
					
					valoresRecuperados.put ( campo, valor );
				}
				
				for ( int col : valoresRecuperados.keySet ( ) ){
					tipoDeDato ddc = datoDeColumna.get ( col );
					
					switch ( ddc ) {
						case CADENA:
							ps.setString ( col, (String)valoresRecuperados.get ( col ) );
							break;
						case ENTERO:
							ps.setInt ( col, (Integer) valoresRecuperados.get ( col ) );
							break;
						case DOBLE:
							ps.setDouble ( col,  (Double) valoresRecuperados.get( col )  );
							break;
						case BLOB:
							ps.setBlob ( col, (Blob) valoresRecuperados.get( col ) );
					}
					
				}
				
				ps.addBatch ( );
				
				sen++;
				
				if ( sen == Batch ){
					sen = 0;
					registrosGuardados = registrosGuardados + ps.executeBatch ( ).length;
				}
				
				
			}
			
			if ( sen > 0 ) {
				registrosGuardados = registrosGuardados + ps.executeBatch ( ).length;
			}
			
		} catch (SQLException e) {
			error = e.toString();
			resultado = false;
		} catch (Exception e) {
			error = e.toString();
			resultado = false;
		} finally {
			if ( mCom != null ) {
				try {
					mCom.close();
				} catch ( Exception e ){
					error = e.toString();
				}
			}
		}
		
		return registrosGuardados;
	}
	
	@SuppressWarnings ( "incomplete-switch" )
	public int updateRegistros( String sentencia, HashMap<String, ArrayList<String>> documentos, HashMap<Integer, tipoDeDato> datoDeColumna )
	{
		
		int registrosGuardados = 0;
		
		if ( !continuar ){
			error 		= "La conexion esta vacia";
			resultado	= false;
			return registrosGuardados;
		}
		
		PreparedStatement	ps = null;
		
		try {
			
			for ( String registro : documentos.keySet ( ) ){
				
				String sent = new String ( sentencia );
				
				ArrayList <String> valoresDeRegistro = documentos.get ( registro );
				
				int contador = 0;
				HashMap<Integer, String> valorAGuardar = new HashMap <Integer, String> ( );
				for ( String valor : valoresDeRegistro ){
					contador++;
					valorAGuardar.put ( contador, valor );
				}
				
				HashMap <Integer, tipoDeDato> ddcs = datoDeColumna;
				
				for ( int columna : valorAGuardar.keySet ( ) ){
					tipoDeDato ddc = ddcs.get ( columna );
					
					String datoAGuardar = "";
					
					switch ( ddc ) {
						case CADENA:				
							datoAGuardar = "'" + valorAGuardar.get ( columna  ) + "'" ;
							break;
						case ENTERO:
							datoAGuardar =  valorAGuardar.get ( columna );
							break;
						case DOBLE:
							datoAGuardar =  valorAGuardar.get ( columna ) ;
							break;
					}
					
					// Busca el ? y lo substituye 
					
					int dondeInicia = sent.indexOf ( "?" );
					
					// Lo remplaza, solo el primero que encuentre
					
					sent = sent.substring ( 0, dondeInicia ) + datoAGuardar + sent.substring ( dondeInicia  + 1 );
					
				}
				
				ps = con.prepareStatement ( sent );
				registrosGuardados += ps.executeUpdate ( );
				ps.close ( );
			}
			
		} catch (SQLException e) {
			error = e.toString();
			resultado = false;
		} catch (Exception e) {
			error = e.toString();
			resultado = false;
		} finally {
			if ( mCom != null ) {
				try {
					mCom.close();
				} catch ( Exception e ){
					error = e.toString();
				}
			}
			
			if ( ps != null ){
				try {
					ps.close ( );
				} catch ( Exception e ){
					error = e.toString ( );
				}
			}
		}
		
		
		return registrosGuardados;
	}
	
	public int updateRegistrosMySQL( String sentencia, HashMap<String, ArrayList<String>> documentos, HashMap<Integer, tipoDeDato> datoDeColumna, int Batch )
	{
		
		int registrosGuardados = 0;
		
		if ( !continuar ){
			error 		= "La conexion esta vacia";
			resultado	= false;
			return registrosGuardados;
		}
		
		PreparedStatement	ps = null;
		
		try {
			
			ps = con.prepareStatement ( sentencia );
			
			int sen = 0;
			
			for ( String registro : documentos.keySet ( ) ){
				
				ArrayList <String> valoresDeRegistro = documentos.get ( registro );
				
				int contador = 0;
				HashMap<Integer, String> valorAGuardar = new HashMap <Integer, String> ( );
				for ( String valor : valoresDeRegistro ){
					contador++;
					valorAGuardar.put ( contador, valor );
				}
				
				
				for ( int columna : valorAGuardar.keySet ( ) ){
					tipoDeDato ddc = datoDeColumna.get ( columna );
					
					switch ( ddc ) {
						case CADENA:
							ps.setString ( columna,valorAGuardar.get ( columna ) );
							break;
						case ENTERO:
							ps.setInt ( columna, Integer.parseInt ( valorAGuardar.get ( columna ) ) );
							break;
						case DOBLE:
							ps.setDouble ( columna, Double.parseDouble ( valorAGuardar.get ( columna ) ) );
							break;
						case BLOB:
							ps.setBlob ( columna, new FileInputStream ( valorAGuardar.get( columna ) ) );
							break;
					}
					
				}
				
				ps.addBatch ( );
				
				sen++;
				
				if ( sen == Batch ){
					sen = 0;
					registrosGuardados = registrosGuardados + ps.executeBatch ( ).length;
				}
				
				
			}
			
			if ( sen > 0 ) {
				registrosGuardados = registrosGuardados + ps.executeBatch ( ).length;
			}
			
		} catch (SQLException e) {
			error = e.toString();
			resultado = false;
		} catch (Exception e) {
			error = e.toString();
			resultado = false;
		} finally {
			if ( mCom != null ) {
				try {
					mCom.close();
				} catch ( Exception e ){
					error = e.toString();
				}
			}
		}
		
		
		return registrosGuardados;
	}
	
	@SuppressWarnings ( "incomplete-switch" )
	public int updateRegistrosMySQL( HashMap<String, String> sentencias, HashMap<String, ArrayList<String>> documentos, HashMap<String,HashMap<Integer, tipoDeDato>> datosDeColumna, int Batch )
	{
		
		int registrosGuardados = 0;
		
		if ( !continuar ){
			error 		= "La conexion esta vacia";
			resultado	= false;
			return registrosGuardados;
		}
		
		Statement	ps = null;
		
		try {
			
			int sen = 0;
			
			ps = con.createStatement ( );
			
			for ( String registro : documentos.keySet ( ) ){
				
				String sentencia = sentencias.get ( registro ) ;
				
				ArrayList <String> valoresDeRegistro = documentos.get ( registro );
				
				int contador = 0;
				HashMap<Integer, String> valorAGuardar = new HashMap <Integer, String> ( );
				for ( String valor : valoresDeRegistro ){
					contador++;
					valorAGuardar.put ( contador, valor );
				}
				
				HashMap <Integer, tipoDeDato> ddcs = datosDeColumna.get ( registro );
				
				
				for ( int columna : valorAGuardar.keySet ( ) ){
					tipoDeDato ddc = ddcs.get ( columna );
					
					String datoAGuardar = "";
					
					switch ( ddc ) {
						case CADENA:				
							datoAGuardar = "'" + valorAGuardar.get ( columna ) + "'" ;
							break;
						case ENTERO:
							datoAGuardar =  valorAGuardar.get ( columna );
							break;
						case DOBLE:
							datoAGuardar =  valorAGuardar.get ( columna ) ;
							break;
					}
					
					// Busca el ? y lo substituye 
					
					int dondeInicia = sentencia.indexOf ( "?" );
					
					// Lo remplaza, solo el primero que encuentre
					
					sentencia = sentencia.substring ( 0, dondeInicia ) + datoAGuardar + sentencia.substring ( dondeInicia  + 1 );
					
				}
				
				
				
				ps.addBatch ( sentencia );
				
				sen++;
				
				if ( sen == Batch ){
					sen = 0;
					registrosGuardados = registrosGuardados + ps.executeBatch ( ).length;
				}
				
				
			}
			
			if ( sen > 0 ) {
				registrosGuardados = registrosGuardados + ps.executeBatch ( ).length;
			}
			
		} catch (SQLException e) {
			error = e.toString();
			resultado = false;
		} catch (Exception e) {
			error = e.toString();
			resultado = false;
		} finally {
			if ( mCom != null ) {
				try {
					mCom.close();
				} catch ( Exception e ){
					error = e.toString();
				}
			}
		}
		
		
		return registrosGuardados;
	}
	
	public int actualizaBlobs( Connection cReplica, String tablaOrigen, String tablaReplica, String insert, HashMap<Integer, tipoDeDato> datoDeColumna, int Batch, ArrayList<String> llaves )
	{
		int resultado = 0;
		
		if ( !continuar ) {
			error 		= "La conexion esta vacia";
			resultado	= 0;
			return resultado;
		}
		
		
		
		ResultSet rs 			= null;
		PreparedStatement ps 	= null;
		
		PreparedStatement psr	= null;
		
		ArrayList <String> llavesEnReplica = null;
		
		try {
			
			// Consulta la replica,
			
			ps = cReplica.prepareStatement ( getSentenciaSelect ( tablaReplica ) );
			rs = ps.executeQuery ( );
			
			// Genera las llaves de la replica
			
			llavesEnReplica = getArrayLlaves ( rs, llaves );
			rs.close ( );
			ps.close ( );
			
			// Consulta el origen.
			
			ps = con.prepareStatement (  getSentenciaSelect ( tablaOrigen ) );
			rs = ps.executeQuery ( );
			
			// Comienza a buscar si se encuentra el 
			psr = cReplica.prepareStatement ( insert );
			
			ResultSetMetaData rsm = rs.getMetaData ( );
			
			int columnas = rsm.getColumnCount ( );
			
			int sen = 0;
			
			while ( rs.next ( ) ){
				
				HashMap<Integer, String> dato = new HashMap <Integer, String> ( );
				
				for ( int i = 1; i <= columnas; i++ ){
					dato.put ( i, rs.getString ( i ) );
				}
				
				String llave = getStringLlave ( dato, llaves );
				
				if ( !llavesEnReplica.contains ( llave ) ) {
					// Carga la informacion en el batch de carga.
					for ( int o = 1; o <= columnas; o++ ){
						
						tipoDeDato dcc = datoDeColumna.get ( o );
						
						switch (dcc){
							case CADENA:
								psr.setString ( o, rs.getString ( o ) );
								break;
							case ENTERO:
								psr.setInt ( o,  rs.getInt ( o ) );
								break;
							case DOBLE:
								psr.setDouble ( o, rs.getDouble ( o ) );
								break;
							case BLOB:
								psr.setBlob ( o, rs.getBlob ( o ) );
								break;
						}
					}
					
					psr.addBatch ( );
					sen++;
				}
				
				if ( sen == Batch ){
					resultado = resultado + psr.executeBatch ( ).length;
					sen = 0;
				}
				
			}
			
			if ( sen > 0 ){
				resultado = resultado + psr.executeBatch ( ).length;
			}
			
			
		} catch (SQLException e) {
			error = e.toString();
			resultado = 0;
		} catch (Exception e) {
			error = e.toString();
			resultado = 0;
		} finally {
			if ( mCom != null ) {
				try {
					mCom.close();
				} catch ( Exception e ){
					error = e.toString();
				}
			}
		}
		
		
		
		return resultado;
	}
	
	public boolean  DeleteRegistrosMySQL( String sentencia )
	{

		
		if ( !continuar ){
			error 		= "La conexion esta vacia";
			resultado	= false;
		}
		
		PreparedStatement	ps = null;
		
		try {
			
			ps = con.prepareStatement ( sentencia );
			
			ps.execute ( );
			
			resultado = true;
			
		} catch (SQLException e) {
			error = e.toString();
			resultado = false;
		} catch (Exception e) {
			error = e.toString();
			resultado = false;
		} finally {
			if ( mCom != null ) {
				try {
					mCom.close();
				} catch ( Exception e ){
					error = e.toString();
				}
			}
		}
		
		
		return resultado;
	}
	
	//endregion

	//region METODOS ESTATICOS
	
	/*
	 * Si la comparación se hace usando la llave, se buscan registros nuevos.
	 * Si se hace con todos los campos, busca modificaciones.
	 */
	
	public static HashMap<String, ArrayList<String>> comparaOrigenVsCopia( HashMap<String, ArrayList<String>> origen, HashMap<String, ArrayList<String>> replica, ArrayList<String> llaves) {
		HashMap<String, ArrayList<String>> resultado = new HashMap <String, ArrayList<String>> ( );
		String Cliente = "Compara Origen Vs replica";
		
		// Genera arreglos de llaves.
		
		HashMap<String, String> llavesEnOrigen 	= new HashMap <String, String> ( );
		HashMap<String, String> llavesEnReplica = new HashMap <String, String> ( );
		
		Utilidades.MuestraInformacion ( "Generando llaves en origen", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		for ( String renglon : origen.keySet ( ) ) {
			// Recupera los campos
			ArrayList<String> campos = origen.get ( renglon );
			
			String llaveAGuardar = "";
			
			int columna = 1;
			for ( String campo : campos ){
				
				if (llaves.contains ( String.valueOf ( columna  ) ) ) {
					llaveAGuardar = llaveAGuardar + "-" + campo;
				}
				columna++;
			}
			
			llavesEnOrigen.put ( llaveAGuardar, renglon );
			
		}
		
		Utilidades.MuestraInformacion ( "Generando llaves en copia", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		for ( String renglon : replica.keySet ( ) ){
			// Recupera los campos
			ArrayList<String> campos = replica.get ( renglon );
			
			String llaveAGuardar = "";
			
			int columna = 1;
			for ( String campo : campos ){
				
				if (llaves.contains ( String.valueOf ( columna  ) ) ) {
					llaveAGuardar = llaveAGuardar + "-" + campo;
				}
				columna++;
			}
			
			llavesEnReplica.put ( llaveAGuardar, renglon );			
		}
		
		Utilidades.MuestraInformacion ( "Comparando origen vs copia", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		// Compara ambos registros.
	
		int registrosNoEnReplica = 0;
		
		for ( String key : llavesEnOrigen.keySet ( ) ) {
			
			if ( !llavesEnReplica.containsKey ( key ) ) {
				
				String renglon = llavesEnOrigen.get ( key );
				
				resultado.put ( String.valueOf ( registrosNoEnReplica ), origen.get ( renglon )  );
				
				registrosNoEnReplica++;
			}
			
		}
		
		Utilidades.MuestraInformacion ( "Se encontraron: " + resultado.keySet ( ).size ( ) + " registros que estan en origen, pero no en copia." , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		return resultado;
	}
	
	public static HashMap<String, ArrayList<Object>> comparaOrigenVsCopia(HashMap<String, ArrayList<Object>> origen, HashMap<String, ArrayList<Object>> replica, ArrayList<String> llaves, HashMap<Integer, tipoDeDato> datoDeColumna) {
		HashMap <String, ArrayList<Object>> resultado = new HashMap <String, ArrayList<Object>> ( );
		
		String Cliente = "Compara Origen Vs replica";
		
		// Genera arreglos de llaves.
		
		HashMap<String, String> llavesEnOrigen 	= new HashMap <String, String> ( );
		HashMap<String, String> llavesEnReplica = new HashMap <String, String> ( );
		
		Utilidades.MuestraInformacion ( "Generando llaves en origen", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		for ( String renglon : origen.keySet ( ) ) {
			// Recupera los campos
			ArrayList<Object> campos = origen.get ( renglon );
			
			String llaveAGuardar = "";
			
			int columna = 1;
			for ( Object campo : campos ){
				
				if (llaves.contains ( String.valueOf ( columna  ) ) ) {
					llaveAGuardar = llaveAGuardar + "-" + campo;
				}
				columna++;
			}
			
			llavesEnOrigen.put ( llaveAGuardar, renglon );
			
		}
		
		Utilidades.MuestraInformacion ( "Generando llaves en copia", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		for ( String renglon : replica.keySet ( ) ){
			// Recupera los campos
			ArrayList<Object> campos = replica.get ( renglon );
			
			String llaveAGuardar = "";
			
			int columna = 1;
			for ( Object campo : campos ){
				
				if (llaves.contains ( String.valueOf ( columna  ) ) ) {
					llaveAGuardar = llaveAGuardar + "-" + campo;
				}
				columna++;
			}
			
			llavesEnReplica.put ( llaveAGuardar, renglon );			
		}
		
		Utilidades.MuestraInformacion ( "Comparando origen vs copia", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		// Compara ambos registros.
	
		int registrosNoEnReplica = 0;
		
		for ( String key : llavesEnOrigen.keySet ( ) ) {
			
			if ( !llavesEnReplica.containsKey ( key ) ) {
				
				String renglon = llavesEnOrigen.get ( key );
				
				resultado.put ( String.valueOf ( registrosNoEnReplica ), origen.get ( renglon )  );
				
				registrosNoEnReplica++;
			}
			
		}
		
		Utilidades.MuestraInformacion ( "Se encontraron: " + resultado.keySet ( ).size ( ) + " registros que estan en origen, pero no en copia." , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		
		return resultado;
	}
	
	
	// USO DE MODIFICACIONES ORIGEN VS COPIA
	
	//El siguiente metodo, se usa, tomando en cuenta que los campos son iguales, que ya existen los mismos registros
	// tanto en la base origen como en la replica.
	
	public static HashMap<String, ArrayList<String>> buscaModificacionesOrigenVsCopia( HashMap<String, ArrayList<String>> origen, HashMap<String, ArrayList<String>> replica, ArrayList<String> llaves) 
	{
		HashMap<String, ArrayList<String>> resultado = new HashMap <String, ArrayList<String>> ( );
		String Cliente = "Busca modificaciones Origen Vs replica";
		
		// Genera arreglos de llaves.
		
		HashMap<String, HashMap<String, String>> llavesEnOrigen 	= new HashMap <String, HashMap<String,String>> ( );
		HashMap<String, HashMap<String, String>> llavesEnReplica 	= new HashMap <String, HashMap<String,String>> ( );
		
		Utilidades.MuestraInformacion ( "Separando informacion en origen", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );

		boolean recuperarTamañoInicial	= false;
		int 	tamañoOriginal			= 0;  // Se cuenta el 1 -> numCampo
		
		for ( String renglon : origen.keySet ( ) ){
			// Recupera los campos
			ArrayList<String> campos = origen.get ( renglon );
			
			String llaveAGuardar = "";
			HashMap<String, String> datosAGuardar = new HashMap <String, String> ( );
			
			int columna = 1;
			for ( String campo : campos ){
				
				if (llaves.contains ( String.valueOf ( columna  ) ) ) {
					llaveAGuardar = llaveAGuardar + "--" + campo;
				} else {
					datosAGuardar.put ( String.valueOf ( columna ), campo );
				}
				columna++;
			}
			
			llaveAGuardar = llaveAGuardar.substring ( 2 );
			
			if ( !recuperarTamañoInicial ){
				tamañoOriginal = columna - 1;
			}
			
			llavesEnOrigen.put ( llaveAGuardar, datosAGuardar );
			
		}
		
		Utilidades.MuestraInformacion ( "Separando informacion en copia", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		for ( String renglon : replica.keySet ( ) ){
			// Recupera los campos
			ArrayList<String> campos = replica.get ( renglon );
			
			String llaveAGuardar = "";
			HashMap<String, String> datosAGuardar = new HashMap <String, String> ( );
			
			int columna = 1;
			for ( String campo : campos ){
				
				if (llaves.contains ( String.valueOf ( columna  ) ) ) {
					llaveAGuardar = llaveAGuardar + "--" + campo;
				} else {
					datosAGuardar.put ( String.valueOf ( columna ), campo );
				}
				columna++;
			}
			
			llaveAGuardar = llaveAGuardar.substring ( 2 );
			
			llavesEnReplica.put ( llaveAGuardar, datosAGuardar );			
		}
		
		Utilidades.MuestraInformacion ( "Comparando origen vs copia", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		// Compara ambos registros.
	
		int registrosNoEnReplica = 0;
		
		
		for ( String key : llavesEnOrigen.keySet ( ) ) {
			
			if ( llavesEnReplica.containsKey ( key ) ) {
				
				HashMap<String, String> enOrigen 		= llavesEnOrigen.get ( key );
				HashMap<String, String> enReplica		= llavesEnReplica.get ( key );
				
				HashMap<String, String> resultadoProv 	= new HashMap <String, String> ( );
				
				// Compara el valor de origen contra replica ( Sobreescribe el valor de replica 
				
				for ( String numeroCampo : enOrigen.keySet ( ) ) {
					
					String valorEnCampoOrigen 	= enOrigen.get ( numeroCampo );
					String valorEnCampoReplica	= enReplica.get ( numeroCampo );
					
					if ( !valorEnCampoOrigen.equals ( valorEnCampoReplica ) ) {
						resultadoProv.put ( numeroCampo, valorEnCampoOrigen );
					} 
				}
				
				
				if ( resultadoProv.size ( ) > 0 ) {
					
					// Procede a guardar la cadena en donde se coloca la informacion
					HashMap<Integer,String> p = new HashMap <Integer, String> ( );
					
					// Recupera primero las llaves.
					
					String[] llav = key.split ( "--" );
					
					int c = 0;
					
					for ( String ll : llaves ){
						p.put ( Integer.valueOf ( ll ), llav[c] );
						c++;
					}
					
					// Coloca los valores, que cambiaron
					
					for ( String lla : resultadoProv.keySet ( ) ){
						p.put ( Integer.valueOf ( lla ), resultadoProv.get ( lla ) );
					}
					
					// Genera el arreglo que se cargara en el resultado.
					
					ArrayList<String> camposConCambios = new ArrayList <String> ( );
					
					for ( int f = 1; f <= tamañoOriginal ; f ++ ){
						
						if ( p.containsKey ( f ) ){
							camposConCambios.add ( p.get ( f ) );
						} else {
							camposConCambios.add ( "" );
						}
						
					}
					
					// Guarda la informacion en el resultado;
					
					resultado.put ( String.valueOf ( registrosNoEnReplica ), camposConCambios );
					
					registrosNoEnReplica++;
					
				}
				
			}
			
		}
		
		Utilidades.MuestraInformacion ( "Se encontraron: " + resultado.keySet ( ).size ( ) + " registros que estan en origen, pero no en copia." , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		return resultado;
	}
	
	public static HashMap<String, ArrayList<String>> validaRegistroEnCatalogo ( HashMap<String,ArrayList<String>> resultado, HashMap<String,String> catalogo)
	{
		HashMap<String,ArrayList<String>> r = new HashMap <String, ArrayList<String>> ( );
		
		int contador = 0;
		
		try {
			for ( String key : resultado.keySet ( ) ){
				ArrayList <String> valores = resultado.get ( key );
				
				ArrayList<String> datosAGuardar = new ArrayList <String> ( );
				
				if ( valores == null ) {
					continue;
				}
				
				if ( valores.get ( 0 ) == null ){
					continue;
				}
				
				if ( valores.get ( 1 ) == null ){
					continue;
				}
				
				String cve_marca	= valores.get ( 0 );
				String marca		= valores.get ( 1 );
				
				String nombreEnCatalogo = catalogo.get ( cve_marca );
				
				if ( ! marca.equals ( nombreEnCatalogo ) ) {
					datosAGuardar.add ( nombreEnCatalogo );
					datosAGuardar.add ( cve_marca );
					r.put ( String.valueOf ( contador++ ), datosAGuardar );
				}
	
			}
		} catch ( Exception e ){
			Utilidades.MuestraInformacion ( "Error en comparacion", "Valida registro", nivel.ERROR, dondeSeMuestra.AMBOS );
		}
		
		return r;
	}
	
	public static HashMap<String,String> resultadoACatalogo( HashMap<String, ArrayList <String>> resultado, ArrayList<String>llaves, int len) 
	{
		HashMap<String,String> r = new HashMap <String, String> ( );
		
		for ( String key : resultado.keySet ( ) ){
			String llave 	= "";
			String valores 	= "";
			String valor	= "";
			
			ArrayList<String> d = resultado.get ( key );
			
			int 	contador = 1;
			String 	espacioLlave 	= "";
			String	espacioValor	= "";
			
			for ( String v : d ){
				
				if ( llaves.contains ( String.valueOf ( contador ) ) ){
					// El registro es llave
					if ( llave.length ( ) == 0 ){
						// Primer registro.
						espacioLlave = "";
					} else {
						// Registros subsecuentes
						espacioLlave = "-";
					}
					
					llave = llave + espacioLlave + v;
				} else {
					// El registro es valor
					if ( valores.length ( ) == 0 ){
						// Primer registro
						espacioValor = "";
					} else {
						// Subsecuentes
						espacioValor = "-";
					}
					
					valor = "";
					
					if ( len != 0 ){
						if ( v.length ( ) < len ){
							valor = v;
						} else {
							valor = v.substring ( 0,len );
						}
					} else {
						valor = v;
					}
					
					valores = valores + espacioValor + valor;
					
				}
			
				contador++;
			}
			
			r.put ( llave, valores );	
			
		}
		
		
		return r;
	}
	
	public static HashMap<String, ArrayList <Blob>> comparaCamposBlob( Connection origen, Connection replica, String tablaOrigen, String tablaReplica, HashMap<Integer, tipoDeDato> datoDeColumna, ArrayList<String> llaves )
	{
		HashMap<String, ArrayList<Blob>> resultado 	= new HashMap <String, ArrayList<Blob>> ( );
		
		HashMap<String, ArrayList<Blob>> origenMap 	= new HashMap <String, ArrayList<Blob>> ( );
		HashMap<String, ArrayList<Blob>> replicaMap	= new HashMap <String, ArrayList<Blob>> ( );
		
		
		PreparedStatement 	pso = null;
		ResultSet			rso = null;
		PreparedStatement 	psr = null;
		ResultSet			rsr = null;
		
		try {
			
			pso = origen.prepareStatement 	( "SELECT * FROM " + tablaOrigen );
			rso = pso.executeQuery ( );
			psr = replica.prepareStatement 	( "SELECT * FROM " + tablaReplica );
			rsr = psr.executeQuery ( );
			
			// Genera la matriz para el origen.
			
			ResultSetMetaData rm = rso.getMetaData ( );
			
			int numCol = rm.getColumnCount ( );
			
			while ( rso.next ( ) ){
				HashMap<Integer, String> d =  new HashMap <Integer, String> ( );
				ArrayList <Blob>		 b = new ArrayList <Blob> ( );
				
				for ( int c = 1; c <= numCol; c ++ ){

					// Get tipo de campo
					tipoDeDato dcc = datoDeColumna.get ( c );
					
					switch ( dcc ) {
						case BLOB:
							b.add ( rso.getBlob ( c ) );
							break;

						default:
							d.put ( c, rso.getString ( c ) );
							break;
					}
					
				}
				
				origenMap.put ( getStaticStringLlave ( d, llaves ), b );	
				
			}
			
			// Genera la matriz para la replica.
			
			while ( rsr.next ( ) ){
				HashMap<Integer, String> d =  new HashMap <Integer, String> ( );
				ArrayList <Blob>		 b = new ArrayList <Blob> ( );
				
				for ( int c = 1; c <= numCol; c ++ ){

					// Get tipo de campo
					tipoDeDato dcc = datoDeColumna.get ( c );
					
					switch ( dcc ) {
						case BLOB:
							b.add ( rsr.getBlob ( c ) );
							break;

						default:
							d.put ( c, rsr.getString ( c ) );
							break;
					}
					
				}
				
				replicaMap.put ( getStaticStringLlave ( d, llaves ), b );
				
			}
			
			// Compara los blobs.
			
			for ( String key : origenMap.keySet ( ) ){
				ArrayList <Blob> o = origenMap.get ( key );
				//ArrayList <Blob> r = replicaMap.get ( key );
				
				ArrayList <Blob> noEncontrado = new ArrayList <Blob> ( );
				for ( Blob rr : o ){
					if ( !o.contains ( rr ) ){
						noEncontrado.add ( rr );
					}
				}
				
				if ( noEncontrado.size ( ) > 0 ) {
					resultado.put ( key, noEncontrado );
				}
				
			}
			
		} catch (SQLException e) {
			System.out.println ( e.toString() );
			resultado = null;
		} catch (Exception e) {
			System.out.println ( e.toString() );
			resultado = null;
		} finally {
			if ( origen != null ) {
				try {
					origen.close();
				} catch ( Exception e ){
					System.out.println ( e.toString() );
				}
			}
			if ( replica != null ) {
				try {
					replica.close();
				} catch ( Exception e ){
					System.out.println ( e.toString() );
				}
			}
		}
		
		
		
		return resultado;
	}
	
	private static String getStaticStringLlave (HashMap <Integer, String>dato, ArrayList <String> llaves)
	{
		String resultado = "";
		
		boolean inicio = true;
		
		for ( int col : dato.keySet ( ) ){
			if ( llaves.contains ( String.valueOf ( col ) ) ){
				if ( inicio ) {
					inicio = false;
				} else {
					resultado = resultado + "-";
				}
				
				resultado = resultado + dato.get ( col );
			}
		}
		
		return resultado;
	}
	
	//endregion

	//region METODOS PRIVADOS
	
	private HashMap<Integer, HashMap<DatoDeColumna,String>> getPares ( ArrayList<String> nombreDeCampo, ArrayList<String> valores, boolean conDatos ){
		
		HashMap<Integer, HashMap<DatoDeColumna,String>> resultado = new HashMap <Integer, HashMap<DatoDeColumna,String>> ( );
		
		int numeroDeCampos = nombreDeCampo.size ( );
		
		for ( int c = 1; c < numeroDeCampos ; c++) {
		
			HashMap<DatoDeColumna,String> 	campo 			= new HashMap <ConsultaODBC.DatoDeColumna, String> ( );
			String							nombre			= nombreDeCampo.get ( c - 1 );
			String							valorDeCampo	= valores.get ( c - 1 );
			boolean							tieneDatos		= false;
			
			if ( valorDeCampo.trim ( ).length ( ) > 0 ){
				tieneDatos = true;
			}
			
			campo.put ( DatoDeColumna.NOMBRE, nombre );
			campo.put ( DatoDeColumna.VALOR, valorDeCampo );
			
			if ( conDatos ){
				if ( tieneDatos ){
					resultado.put ( c, campo );
				}
			} else {
				resultado.put ( c, campo );
			}
		}
	
		return resultado;
	}

	private ArrayList <String> getArrayLlaves ( ResultSet datos, ArrayList <String> llaves ) throws SQLException 
	{
		ArrayList <String> resultado = new ArrayList <String> ( );
		
		ResultSetMetaData md = datos.getMetaData ( );
		
		int numColumnas = md.getColumnCount ( );
		
		while ( datos.next ( ) ){
			HashMap<Integer, String> valores = new HashMap <Integer, String> ( );
			
			for ( int i = 1; i <= numColumnas; i++ ){
				valores.put ( i, datos.getString ( i ) );
			}
			
			resultado.add ( getStringLlave ( valores, llaves ) );
		}
		
		return resultado;
	}
	
	private String getStringLlave( HashMap<Integer, String> dato, ArrayList <String> llaves )
	{
		String resultado = "";
		
		boolean inicio = true;
		
		for ( int col : dato.keySet ( ) ){
			if ( llaves.contains ( String.valueOf ( col ) ) ){
				if ( inicio ) {
					inicio = false;
				} else {
					resultado = resultado + "-";
				}
				
				resultado = resultado + dato.get ( col );
			}
		}
		
		return resultado;
	}
	
	private String getSentenciaSelect ( String tabla ){
		
		String resultado = "";
		
		resultado = "SELECT * FROM " + tabla;
		
		return resultado;
		
	}
	
	//endregion

}
