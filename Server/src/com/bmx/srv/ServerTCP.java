package com.bmx.srv;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;

import com.bmx.srv.Servicios.Servicio;
import com.ibm.icu.text.DateFormat;
import com.ibm.icu.util.Calendar;

import static com.bmx.srv.Utilidades.*;

public class ServerTCP extends Thread 
{

	//region INFORMACION DEL SERVIDOR
	
	static String version			= "1.3.0.0";
	static String descricpion		= "Procesos para informaci�n de proveedores";
	static String nombre			= "Proveedores";
	
	//endregion
	
	//region VARIABLES PRIVADAS
	private 		Socket				socket 			= null;
	private			DataInputStream		dis				= null;
	private			DataOutputStream	dos				= null;
	
	private	static	ArrayList<String> 	error			= new ArrayList<String>();
	public static	String				folderTemp		= "";
	
	public static 	Procesos 		contadorDeProcesos	= new Procesos( 10 );
	public static	int				maxProcesosEnCola;
	
	public static	boolean			bloquearAuditores	= false;
	
	//region PERIODOS DE TIEMPO
	private static long Un_minuto 			= 1000 * 60;
	private static long Cinco_Minutos		= Un_minuto * 5;
	private static long Quince_Minutos		= Un_minuto * 15;
	private static long Treinta_Minutos		= Un_minuto * 30;
	private static long Una_hora			= Un_minuto * 60;
	private static long Veinticuatro_Horas 	= Un_minuto * 60 * 24;
	//endregion
	
	//endregion

	//region CONSTRUCTOR
	public ServerTCP( Socket s) 
	{
		this.socket = s;
	}
	//endregion
	
	/**
	 * @param args
	 */
	
	//region METODOS ESTATICOS
	
	public static void main(String[] args) 
	{
		Timer timer					= new Timer ( );
		Timer ValidaFac				= new Timer ( );
		Timer ValidaDocsMonth		= new Timer ( );
		//Timer ValidaBDs				= new Timer ( );
		Timer ValidaSat				= new Timer ( );
		Timer LimpiaArchivos		= new Timer ( );
		Timer Proveedores			= new Timer ( );
		Timer Tareas				= new Timer ( );
		Timer Estadisticas			= new Timer ( );
		Timer ActualizaWeb			= new Timer ( );
		
		String	Cliente = "Server";
		
		MuestraInformacion( "Iniciando servidor.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS);
		MuestraInformacion( "La version del server es: " + version + " - El nombre es: " + nombre , Cliente, nivel.INFO, dondeSeMuestra.AMBOS);
		MuestraInformacion( "Descripcion del server: " + descricpion , Cliente, 	nivel.INFO, dondeSeMuestra.AMBOS);
		MuestraInformacion( "Version del archivo de recursos: " + Utilidades.RecuperaRecurso( "version" ), Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		MuestraInformacion( "Se aceptan hasta "  + Utilidades.RecuperaRecurso( "server_maxconnections" ) + " conecciones.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		MuestraInformacion( "Arrancando el controlador de procesos", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		contadorDeProcesos.setMaxProceos( Integer.valueOf( Utilidades.RecuperaRecurso( "server_maxconnections" )  ) );
		ServerTCP.maxProcesosEnCola = Integer.valueOf( Utilidades.RecuperaRecurso( "server_maxprocesoscola" ) );
		
		MuestraInformacion( "Numero de procesos en cola maximos para lanzar: " + ServerTCP.maxProcesosEnCola , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		ServerSocket 	ss 	= null;
		Socket			s	= null;
		
		//region DA DE ALTA PARAMETROS
		
		folderTemp = Utilidades.RecuperaRecurso( "server_fldIntercambio" );
		
		File folder = new File( folderTemp );
		
		if ( !folder.exists() ){
			folder.mkdir();
		}
		
		//endregion
		
		try {
			
			nivel Nivel 			= nivel.INFO;
			dondeSeMuestra mostrar 	= dondeSeMuestra.AMBOS;
			
			MuestraInformacion( "Recuperando el numero de puerto del archivo de recursos.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			String puertoDelServer = Utilidades.RecuperaRecurso( "server_socket" );
			MuestraInformacion("El puerto recuperado es: " + puertoDelServer, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			
			MuestraInformacion("Creando el socket,  para escuchar", Cliente, Nivel, mostrar);
			ss = new ServerSocket( Integer.parseInt( puertoDelServer ) );
			MuestraInformacion("Comenzando a escuchar", Cliente, Nivel, mostrar);
			
		} catch ( Exception e ){
			error.clear();
			error.add( "No se pudo recuperar el numero de puerto se para el servicio." );
			error.add( e.toString() );
			MuestraInformacion( error, Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
			return;
		}
		
		//region TIMER TASKS		
		TimerTask DocumentosEnCola 			= new TimerTask( )
		{
			String Cliente = "Timer Documentos en Cola";
			public void run ( ) {
				
				if ( bloquearAuditores ) {
					MuestraInformacion( "Se esta corriendo el auditor mensual, se bloquea este auditor.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );	
				} else {
				
					MuestraInformacion( "Validando Documentos en cola iniciando", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					HashMap<String, String> procesosPendientes = Utilidades.RecuperaProcesos( );
					int contadorDeProcesosEnCola = 0;
					for ( String Llave : procesosPendientes.keySet( ) ) {
						int TipoProceso = Integer.parseInt( procesosPendientes.get( Llave ) );
						// Lanza el proceso si es que existe un ticket libre, en caso de no existir, no hace nada.
						if ( contadorDeProcesos.getProceso( ) ){				
							MuestraInformacion( "Mandando proceso en cola: " + Llave , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
							contadorDeProcesosEnCola++;
							try {
								contadorDeProcesos.EjecutaProceso( TipoProceso, Llave );
								Utilidades.EliminaDocumentoDeCola( Llave );
								MuestraInformacion( "Terminando proceso en cola con exito: " + Llave , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
							} catch ( Exception e ){
								MuestraInformacion( "Terminando proceso en cola con error: " + Llave , Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
								MuestraInformacion ( e.toString ( ), Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
							}
							contadorDeProcesos.releaseProceso ( );
							if ( contadorDeProcesosEnCola == ServerTCP.maxProcesosEnCola ){
								MuestraInformacion( "Se enviaron el m�ximo de procesos en cola." , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
								return;
							} else {
								MuestraInformacion ( "Hay " + contadorDeProcesosEnCola + " procesos corriendo." , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
							}
						} else {
							MuestraInformacion( "Se alcanzo el limite de procesos, terminando. " + Llave , Cliente, nivel.ALARMA, dondeSeMuestra.AMBOS );
							break;
						}
					}
					MuestraInformacion( "Validando Documentos en cola terminado.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				}
			}
		};
		TimerTask ValidaDocumentos 			= new TimerTask( )
		{
			String Cliente = "Timer Valida documentos";
			public void run ( ) {
				
				if ( bloquearAuditores ){
					MuestraInformacion( "Se esta corriendo el auditor mensual, se bloquea este auditor.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );	
				} else {
					Auditor a = new Auditor ( );
					//region ERP CONTRA LOCAL
					MuestraInformacion ( "Validando ERP contra Dominio. INICIO", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					//TODO Meter todos los documentos ciclo for.
					MuestraInformacion ( "Validando documento: Facturas INICIO" , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					for ( sucursales s : sucursales.values ( ) ){
						MuestraInformacion ( "Validando, " + s.getAbreviacion ( ) + " ERP contra local. INICIO", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
						String sucursal = s.getClave ( );
						
						Calendar c = Calendar.getInstance ( );
						c.setTimeInMillis ( 0 );
						c.set ( 2015, 3, 25, 1, 0, 0 );
						
						Date prov = c.getTime ( );
						//HashMap<String, String> procesosPendientes = a.FacturasErpVsFactura ( new Date() , sucursal );
						HashMap<String, String> procesosPendientes = a.FacturasErpVsFactura ( prov , sucursal );
						MuestraInformacion ( "Se encontraron: " + procesosPendientes.size ( ) + " procesos." , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );	
						int contadorDeProcesosEnCola = 0;
						for ( String Llave : procesosPendientes.keySet( ) ) {	
							int TipoProceso = Integer.parseInt( procesosPendientes.get( Llave ) );
							// Lanza el proceso si es que existe un ticket libre, en caso de no existir, no hace nada.
							if ( contadorDeProcesos.getProceso( ) ){
								MuestraInformacion( "Mandando proceso : " + Llave , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
								contadorDeProcesosEnCola++;
								try {
									ServerTCP.contadorDeProcesos.EjecutaProceso( TipoProceso, Llave );
									Utilidades.EliminaDocumentoDeCola( Llave );
									contadorDeProcesos.releaseProceso ( );
									MuestraInformacion( "Terminando proceso : " + Llave + " con exito", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
								} catch ( Exception e ){
									//TODO Dependiendo del error, se elimina. Si no existen los archivos se borra de cola y se manda un correo.
									MuestraInformacion( "Terminando proceso : " + Llave + " con error", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
									MuestraInformacion ( e.toString ( ), Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
								}
								if ( contadorDeProcesosEnCola == ServerTCP.maxProcesosEnCola ){
									MuestraInformacion( "Se enviaron el m�ximo de procesos ." , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
									return;
								}
							} else {
								MuestraInformacion( "Se alcanzo el maximo de procesos : " , Cliente, nivel.ALARMA, dondeSeMuestra.AMBOS );
								break;
							}
						}
						MuestraInformacion ( "Validando, " + s.getAbreviacion ( ) + " ERP contra local. FIN", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					}
					MuestraInformacion ( "Validando documento: Facturas FIN" , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					MuestraInformacion ( "Validando ERP contra Dominio. FIN", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );			
					//endregion
					//region LOCAL CONTRA REMOTO
				MuestraInformacion ( "Validando Dominio contra remoto. INICIO", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				//TODO Meter todos los documentos ciclo for.
				MuestraInformacion ( "Validando documento: Facturas INICIO" , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				for ( sucursales s : sucursales.values ( ) ){
					MuestraInformacion ( "Validando, " + s.getAbreviacion ( ) + " Dominio contra remoto. INICIO", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					String sucursal = s.getClave ( );
					HashMap<String, String> procesosPendientes = a.FacturasLocVsFacturasRem ( new Date(), sucursal );
					int contadorDeProcesosEnCola = 0;
					for ( String Llave : procesosPendientes.keySet( ) ) {				
						int TipoProceso = Integer.parseInt( procesosPendientes.get( Llave ) );
						// Lanza el proceso si es que existe un ticket libre, en caso de no existir, no hace nada.
						if ( contadorDeProcesos.getProceso( ) ){
							MuestraInformacion( "Mandando proceso : " + Llave , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );			
							contadorDeProcesosEnCola++;
							try {
								ServerTCP.contadorDeProcesos.EjecutaProceso( TipoProceso, Llave );
								Utilidades.EliminaDocumentoDeCola( Llave );
								contadorDeProcesos.releaseProceso ( );
								MuestraInformacion( "Terminando proceso : " + Llave + " con exito", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
							} catch ( Exception e ){
								//TODO Dependiendo del error, se elimina. Si no existen los archivos se borra de cola y se manda un correo.
								MuestraInformacion( "Terminando proceso : " + Llave + " con error", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
								MuestraInformacion ( e.toString ( ), Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
							}
							if ( contadorDeProcesosEnCola == ServerTCP.maxProcesosEnCola ){
								MuestraInformacion( "Se enviaron el m�ximo de procesos en cola." , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
								return;
							}			
						} else {
							MuestraInformacion( "Se alcanzo el maximo de procesos en cola: " , Cliente, nivel.ALARMA, dondeSeMuestra.AMBOS );
							break;
						}
					}			
					MuestraInformacion ( "Validando, " + s.getAbreviacion ( ) + " Dominio contra remoto. FIN", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				}
				MuestraInformacion ( "Validando documento: Facturas FIN" , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				MuestraInformacion ( "Validando Dominio contra remoto. INICIO", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				//endregion
				}
			}
		};
		TimerTask ValidaDocumentosMensual 	= new TimerTask( )
		{
			/* DESCRIPCION DEL FUNCIONAMIENTO
			 * Este se encarga exclusivamente de validar que todos los archivos se encuentren en local y en remoto
			 * no manda correo.
			 */
			String Cliente = "Timer Valida documentos mensual";
			public void run ( ) {
				
				/* 
				 * Este auditor solo se puede correr, en la madrugada 
				 *   entre las 4 y 6 am, 
				 * O entre las 8 y 10 pm
				 */
				// TODO, es para produccion
				
				
				// 
				
				bloquearAuditores = true;
				
				Auditor a = new Auditor ( );
				MuestraInformacion( "Se lanzo el validador de documentos contra ERP - MENSUAL. INICIO", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				MuestraInformacion( "Valida facturas.INICIO", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				ArrayList <Date> DiasDelMes 				= Utilidades.DiasDelMes ( );
				HashMap<String, String> procesosPendientes 	= new HashMap <String, String> ( );
				//region VALIDA ERP VS LOCAL
				for ( sucursales s : sucursales.values ( ) ){
					MuestraInformacion ( "Validando, " + s.getAbreviacion ( ) + " ERP contra local. INICIO", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					String sucursal = s.getClave ( );
					for ( Date d : DiasDelMes ) {
						procesosPendientes.putAll ( a.FacturasErpVsFactura(d,sucursal) );
					}
					
					MuestraInformacion ( "Validando, " + s.getAbreviacion ( ) + " ERP contra local. FIN", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				}
				//endregion
				//region VALIDA LOCAL VS REMOTO
				
				int NumeroDeDocumentosPendientes = 0;
				
				for ( sucursales s : sucursales.values ( ) ){
					MuestraInformacion ( "Validando, " + s.getAbreviacion ( ) + " local contra remoto. INICIO", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					String sucursal = s.getClave ( );
					procesosPendientes.clear ( );
					for ( Date d : DiasDelMes ) {
						HashMap <String, String> documentosPendientes = a.FacturasLocVsFacturasRem ( d, sucursal );
						
						NumeroDeDocumentosPendientes = NumeroDeDocumentosPendientes + documentosPendientes.size ( );
						
						procesosPendientes.putAll ( documentosPendientes );
						
					}
						
					MuestraInformacion ( "Validando, " + s.getAbreviacion ( ) + " local contra remoto. FIN", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				}
				//endregion
				
				MuestraInformacion ( "Se encontraron: " + NumeroDeDocumentosPendientes + " y se procesaran " + procesosPendientes.size ( ), Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				MuestraInformacion( "Valida facturas. FIN", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				
				MuestraInformacion ( "Inicio - mandando documentos,  sin contador", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				
				MandaProcesosSinContador( procesosPendientes );
				
				MuestraInformacion ( "Fin - mandando documentos,  sin contador", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				
				MuestraInformacion ( "Termina el validador de documentos MENSUAL. FIN", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				
				bloquearAuditores = false;
			}
		};
		TimerTask ValidaBasesDeDatos		= new TimerTask( )
		{
			public void run ( ) {
				
				String Cliente = "Compara Bases";
				// Valida que exista el archivo de configuracion
				
				MuestraInformacion ( "Lanzando validacion de bases de datos", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				
				Servicios s = new Servicios("servicios.cfg");
				
				if ( !s.getEstado ( ) ){
					MuestraInformacion ( "No se puede continuar el archivo no existe", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				}
				
				// Extrae el valor del para validar si se tiene que lanzar el correo.
				
				if ( s.getBooleanServicio ( Servicio.COMPARA_BASES ) ){
					MuestraInformacion ( "Se procede a comparar las bases.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
					Auditor a = new Auditor ( );
					
					ArrayList <String> DocumentosFaltantes = a.comparaBaseLocalVsBaseRemota ( );
					
					MuestraInformacion ( "Se encontraron " + DocumentosFaltantes.size ( ) + " documentos faltantes en la base remota.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
					MuestraInformacion ( "Actualizando informacion", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
					bloquearAuditores = true;
					MandaDocumentosFaltantesEnRemoto ( DocumentosFaltantes );
					bloquearAuditores = false;
					Servicio se = Servicio.COMPARA_BASES;
					s.setBooleanServicio ( se, false );
				} else {
					return;
				}
			
			}
		};
		TimerTask ValidaTimbres				= new TimerTask( )
		{
			public void run ( ) {
				String Cliente = "Audita SAT";

				Servicios s = new Servicios("servicios.cfg");
				
				boolean recargaDesdeElInicio 	= false;
				boolean forzaValidacion			= false;
				String	horaParaValidar			= "";
				boolean validacionEjecutada		= false;
				boolean	ejecutaValidacion		= false;
				
				if ( !s.getEstado ( ) ){
					MuestraInformacion ( "El archivo no existe, se recarga desde el inicio", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					recargaDesdeElInicio 	= true;
					ejecutaValidacion		= true;
				} else {
					recargaDesdeElInicio 	= s.getBooleanServicio ( Servicio.RECARGA_BASE_TIMBRES );
					forzaValidacion			= s.getBooleanServicio ( Servicio.LANZA_VALIDA_TIMBRES );
					horaParaValidar			= s.getStringService ( Servicio.HORA_VALIDA_TIMBRES );
					validacionEjecutada		= s.getBooleanServicio ( Servicio.ESTADO_VALIDACION );
		
				}
				
				Calendar ff = Calendar.getInstance ( );
				
				if ( forzaValidacion ) {
					MuestraInformacion ( "Forzando validacion de bases de documentos en del SAT", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					ejecutaValidacion = forzaValidacion;
				} else {
					
					int HoraLocal = ff.get ( Calendar.HOUR_OF_DAY );
					
					if ( HoraLocal == Integer.parseInt ( horaParaValidar ) ){
						if ( !validacionEjecutada ){
							ejecutaValidacion = true;
							MuestraInformacion ( "Lanzando validacion de bases de documentos en del SAT, por programa.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
						} 
					} else {
						MuestraInformacion ( "Aun no es la hora para lanzar la validacion.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					}
					
					// Coloca en cero, despues de la hora que se ejecuto
					
					if ( (Integer.parseInt ( horaParaValidar ) + 1 == HoraLocal ) && (validacionEjecutada) ) {
						s.setBooleanServicio ( Servicio.ESTADO_VALIDACION, false );
					}
					
				}
				
				if ( ejecutaValidacion ) {
				
					bloquearAuditores = true;
					Auditor a = new Auditor ( );
										
					Calendar fi = Calendar.getInstance ( );

					if ( recargaDesdeElInicio ){
						fi.set ( 2015, 0, 1 );
						
						if (! a.limpiaBasesDeAuditores ( basesDeAuditor.TIMBRES ) ){
							Utilidades.MuestraInformacion ( "No se pudo limpiar la base de datos", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
							MuestraInformacion ( "Validacion terminada", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
							return;
						} else {
							Utilidades.MuestraInformacion ( "Se limpio la base de datos " + basesDeAuditor.TIMBRES.getNameBaseAuditor ( ), Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
						}
						
					}
					
					DateFormat df = DateFormat.getDateInstance ( );
					
					MuestraInformacion ( "La validacion se lanza desde: " + df.format ( fi.getTime ( ) ) + " hasta " + df.format ( ff.getTime ( ) )  , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
					a.CargaDatosValidacion ( tipoDeDocumento.FACTURA, fi.getTime ( ), ff.getTime ( ) );
					a.CargaDatosValidacion ( tipoDeDocumento.DEVOLUCION, fi.getTime ( ), ff.getTime ( ) );
					a.CargaDatosValidacion ( tipoDeDocumento.NOTA_DE_CREDITO_B, fi.getTime ( ), ff.getTime ( ) );
					a.CargaDatosValidacion ( tipoDeDocumento.NOTA_DE_CREDITO_PP, fi.getTime ( ), ff.getTime ( ) );
					a.CargaDatosValidacion ( tipoDeDocumento.NOTA_DE_CARG0, fi.getTime ( ), ff.getTime ( ) );
					a.validaSat ( );
					
					MuestraInformacion ( "Validacion terminada", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					// Una vez cargados todos los registros, procede a colocar, en falso la bandera, para que no se vuelvan a recargar todos los timbres.
					if ( !s.getEstado ( ) && recargaDesdeElInicio ){
						s.setBooleanServicio ( Servicio.RECARGA_BASE_TIMBRES, false );
					}
					// Indica que ya se lanzo la validacion para que no se vuelva a lanzar el proceso. Si es que no se forzo
					if ( !forzaValidacion ) {
						s.setBooleanServicio ( Servicio.ESTADO_VALIDACION, true );
					} else {
						s.setBooleanServicio ( Servicio.LANZA_VALIDA_TIMBRES, false );
					}
					
					if ( recargaDesdeElInicio ) {
						s.setBooleanServicio ( Servicio.RECARGA_BASE_TIMBRES, false );
					}
					
					bloquearAuditores = false;	
				}
			}
		};
		TimerTask LimpiezaDeArchivos		= new TimerTask( )
		{
			public void run() {
				String Cliente = "Limpieza de archivos";
				
				Servicios s = new Servicios("servicios.cfg");
				
				boolean limpiaTimbres 			= false;
				String 	horaDeLimpieza			= "";
				boolean	archivoEncontrado		= false;
				
				
				if ( !s.getEstado ( ) ){
					MuestraInformacion ( "El archivo no existe, se recarga desde el inicio", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					limpiaTimbres = false;
				} else {
					limpiaTimbres 	= s.getBooleanServicio ( Servicio.LIMPIA_TEMPORAL );
					horaDeLimpieza	= s.getStringService ( Servicio.HORA_DE_LIMPIEZA );
					archivoEncontrado = true;
				}
				
				if (archivoEncontrado) {
					
					if ( limpiaTimbres ){
						MuestraInformacion ( "Se forza la limpieza de archivos.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
						
					} else {
						Calendar hr = Calendar.getInstance ( );
						
						int horaDelDiay = hr.get ( Calendar.HOUR_OF_DAY );
						
						if ( horaDelDiay == Integer.parseInt ( horaDeLimpieza ) ){
							limpiaTimbres = true;
						} else {
							MuestraInformacion ( "Aun no es la hora para lanzar la limpieza.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
							limpiaTimbres = false;
						}
					}
				}
				
				if ( limpiaTimbres ){
					MuestraInformacion ( "Lanzando la limpieza.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					String folder = Utilidades.RecuperaRecurso ( "server_fldIntercambio" );
				
					
					File dirFld = new File(folder);
				
					int contador = 0;
					
					if ( dirFld.isDirectory ( ) ){
						for( File a : dirFld.listFiles ( ) ){
							if ( a.exists ( ) ){
								contador++;
								a.delete ( );
							}
						}
					}
					
					MuestraInformacion ( "Se borraron " + contador + " archivos.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					MuestraInformacion ( "Terminando la limpieza.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
					s.setBooleanServicio ( Servicio.LIMPIA_TEMPORAL, false );
					
				}
			}
		};
		TimerTask CatalogosDeProveedores	= new TimerTask( )
		{
			public void run ( ) {
				String Cliente = "Proveedores";
				
				Servicios s = new Servicios("servicios.cfg");
				
				boolean actualizaProveedores	= false;
				boolean actualizaCompradores	= false;
				String 	horaDeActualizacion		= "";
				boolean	archivoEncontrado		= false;
				
				
				if ( !s.getEstado ( ) ){
					MuestraInformacion ( "El archivo no existe, se recarga desde el inicio", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					actualizaProveedores = false;
				} else {
					actualizaProveedores 	= s.getBooleanServicio ( Servicio.FORZA_ACTUALIZACION_PROVEEDORES );
					actualizaCompradores	= s.getBooleanServicio ( Servicio.FORZA_ACTUALIZACION_COMPRADORES );
					horaDeActualizacion		= s.getStringService ( Servicio.HORA_DE_LIMPIEZA );
					archivoEncontrado = true;
				}
				
				if ( archivoEncontrado ) {
					
					if ( actualizaProveedores ){
						MuestraInformacion ( "Se forza la actualizacion de proveedors", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					} else {
						Calendar hr = Calendar.getInstance ( );
						
						int horaDelDiay = hr.get ( Calendar.HOUR_OF_DAY );
						
						if ( horaDelDiay == Integer.parseInt ( horaDeActualizacion ) ){
							actualizaProveedores = true;
						} else {
							MuestraInformacion ( "Aun no es la hora para actualizar proveedores", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
							actualizaProveedores = false;
						}
					}
					
					if ( actualizaCompradores ){
						MuestraInformacion ( "Se forza la actualizacion de compradores", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					}
				}
				
				if ( actualizaProveedores ){
					MuestraInformacion ( "Lanzando la actualizacion de proveedores", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
					String Llave = "-";
					
					try {

						contadorDeProcesos.EjecutaProceso( 104, Llave );

					} catch ( Exception e ){
						MuestraInformacion( "Fallo la actualizacion de proveedores" , Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
						MuestraInformacion ( e.toString ( ), Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
					}
					contadorDeProcesos.releaseProceso ( );
					
					MuestraInformacion ( "Terminando la actualizacion de proveedores.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
					s.setBooleanServicio ( Servicio.FORZA_ACTUALIZACION_PROVEEDORES, false );
				}
				
				if ( actualizaCompradores ){
					MuestraInformacion ( "Lanzando la actualizacion de compradores", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
					String Llave = "-";
					
					try {

						contadorDeProcesos.EjecutaProceso( 105, Llave );

					} catch ( Exception e ){
						MuestraInformacion( "Fallo la actualizacion de compradores" , Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
						MuestraInformacion ( e.toString ( ), Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
					}
					contadorDeProcesos.releaseProceso ( );
					
					MuestraInformacion ( "Terminando la actualizacion de compradores.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
					s.setBooleanServicio ( Servicio.FORZA_ACTUALIZACION_COMPRADORES, false );
				}
				
			}
		};
		TimerTask RealizaTareas				= new TimerTask( )
		{
			public void run ( ) {
				String Cliente = "Tareas";
				
				Servicios s = new Servicios("servicios.cfg");
				
				boolean actualizaMarcas			= false;
				String 	horaDeActualizacion		= "";
				boolean	archivoEncontrado		= false;
				
				
				if ( !s.getEstado ( ) ){
					MuestraInformacion ( "El archivo no existe, se recarga desde el inicio", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					actualizaMarcas = false;
				} else {
					actualizaMarcas 	= s.getBooleanServicio ( Servicio.FORZA_ACTUALIZA_MARCAS );
					horaDeActualizacion		= s.getStringService ( Servicio.HORA_ACTUALIZA_MARCAS );
					archivoEncontrado = true;
				}
				
				if ( archivoEncontrado ) {
					
					if ( actualizaMarcas ){
						MuestraInformacion ( "Se forza la actualizacion de marcas", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					} else {
						Calendar hr = Calendar.getInstance ( );
						
						int horaDelDiay = hr.get ( Calendar.HOUR_OF_DAY );
						
						if ( horaDelDiay == Integer.parseInt ( horaDeActualizacion ) ){
							actualizaMarcas = true;
						} else {
							MuestraInformacion ( "Aun no es la hora para actualizar marcas", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
							actualizaMarcas = false;
						}
					}
					
				}
				
				if ( actualizaMarcas ){
					MuestraInformacion ( "Lanzando la actualizacion de marcas", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
					String Llave = "-";
					
					try {

						contadorDeProcesos.EjecutaProceso( 104, Llave );

					} catch ( Exception e ){
						MuestraInformacion( "Fallo la actualizacion de marcas" , Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
						MuestraInformacion ( e.toString ( ), Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
					}
					contadorDeProcesos.releaseProceso ( );
					
					MuestraInformacion ( "Terminando tareas.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
					s.setBooleanServicio ( Servicio.FORZA_ACTUALIZA_MARCAS, false );
				}
				
			}
			
		};
		TimerTask cargaEstadisticas			= new TimerTask( )
		{
			public void run ( ) {
			
				String Cliente = "Estadisticas";
				
				Servicios s = new Servicios("servicios.cfg");
				
				boolean actualizaEstadisticas	= false;
				String 	horaDeActualizacion		= "";
				boolean	archivoEncontrado		= false;
				String 	a�oDeActualizacion		= "";
				
				if ( !s.getEstado ( ) ){
					MuestraInformacion ( "El archivo no existe, carga todo", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					actualizaEstadisticas = true;
				} else {
					actualizaEstadisticas 	= s.getBooleanServicio ( Servicio.FORZA_ESTADISTICAS );
					horaDeActualizacion		= s.getStringService ( Servicio.HORA_CARGA_ESTADISTICAS );
					a�oDeActualizacion		= s.getStringService ( Servicio.A�O_PARA_CARGA_ESTADISTICAS );
					archivoEncontrado = true;
				}
				
				if ( archivoEncontrado ) {
					
					if ( actualizaEstadisticas ){
						MuestraInformacion ( "Se forza la carga de estadisticas", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					} else {
						Calendar hr = Calendar.getInstance ( );
						
						int horaDelDiay = hr.get ( Calendar.HOUR_OF_DAY );
						
						if ( horaDelDiay == Integer.parseInt ( horaDeActualizacion ) ){
							actualizaEstadisticas = true;
						} else {
							MuestraInformacion ( "Aun no es la hora para actualizar las estadisticas", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
							actualizaEstadisticas = false;
						}
					}
					
				}
				
				if ( actualizaEstadisticas ){
					MuestraInformacion ( "Lanzando la actualizacion de Estadisticas", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
					String Llave = (( a�oDeActualizacion.trim ( ).length ( ) > 0 ) ? a�oDeActualizacion : "-");
					
					try {

						
						contadorDeProcesos.EjecutaProceso( 80, Llave );

					} catch ( Exception e ){
						MuestraInformacion( "Fallo la actualizacion de marcas" , Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
						MuestraInformacion ( e.toString ( ), Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
					}
					contadorDeProcesos.releaseProceso ( );
					
					MuestraInformacion ( "Terminando actualizacion de estadisticas.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
					s.setBooleanServicio ( Servicio.FORZA_ESTADISTICAS, false );
				}
				
			}
			
		};

		TimerTask actualizaWeb				= new TimerTask( )
		{
			
			public void run ( ) {
				String Cliente = "Actualiza Web";
				
				Servicios s = new Servicios("servicios.cfg");
				
				boolean seEstaActualizandoLaWeb	= false;
				
				if ( !s.getEstado ( ) ){
					MuestraInformacion ( "El archivo no existe, actualiza web", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					seEstaActualizandoLaWeb 	= false;
				} else {
					seEstaActualizandoLaWeb 	= s.getBooleanServicio ( Servicio.FORZA_ACTUALIZACION_WEB );
					MuestraInformacion ( "Se manda la actualizacion de archivos en web", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				}
				
				if ( !seEstaActualizandoLaWeb ){
					MuestraInformacion ( "Lanzando la actualizacion de archivos en web.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					s.setBooleanServicio ( Servicio.FORZA_ACTUALIZACION_WEB, true );
					MuestraInformacion ( "Se marco, que se esta mandando una actualizacion.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					String Llave = "-";					
					try {
						contadorDeProcesos.EjecutaProceso( 85, Llave );

					} catch ( Exception e ){
						MuestraInformacion( "Fallo la actualizacion de web" , Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
						MuestraInformacion ( e.toString ( ), Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
					}
					contadorDeProcesos.releaseProceso ( );
					
					MuestraInformacion ( "Terminando actualizacion web.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
					
				} else {
					MuestraInformacion ( "Se esta actualizando, no se puede volver a lanzar, hasta que termine", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				}
				
			}
			
		};
		
		//endregion
		
		//region TIMERS
		// Valida la cola de facturacion.
//		timer.scheduleAtFixedRate ( DocumentosEnCola, Un_minuto, Cinco_Minutos );
		// Valida los datos del erp vs base local vs base remota.
//		ValidaFac.scheduleAtFixedRate ( ValidaDocumentos, Treinta_Minutos, Quince_Minutos );
		// Valida los datos que se han generadon en el mes.
//		ValidaDocsMonth.scheduleAtFixedRate ( ValidaDocumentosMensual, Una_hora, Veinticuatro_Horas  );
		// Valida que la informacion en la base de datos remota sea igual a la base de datos local.
	//TODO implementar el validador, de fileSystem vs Base de datos local, vs base de datos remota
		//ValidaBDs.scheduleAtFixedRate ( ValidaBasesDeDatos, Un_minuto, Quince_Minutos );
		ValidaSat.scheduleAtFixedRate ( ValidaTimbres, Un_minuto, Una_hora );
//		LimpiaArchivos.scheduleAtFixedRate( LimpiezaDeArchivos, Un_minuto, Treinta_Minutos);
//		Proveedores.scheduleAtFixedRate ( CatalogosDeProveedores, Un_minuto , Treinta_Minutos );
//		Tareas.scheduleAtFixedRate ( RealizaTareas, Un_minuto, Una_hora );
//		Estadisticas.scheduleAtFixedRate ( cargaEstadisticas, Un_minuto, Una_hora );
		//endregion		
		
		// Comenzando a escuchar en el puerto especificado.
		
		while ( true ){
			try {
				s = ss.accept();
				MuestraInformacion( "Se recivion una conexion de: " + s.getInetAddress().toString() , "Server", nivel.INFO, dondeSeMuestra.AMBOS );
				new ServerTCP(s).start();
			} catch ( Exception e ){
				MuestraInformacion( "Error al generar el hilo", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
			}
			
		}
	}
	
	//endregion
	
	//region METODOS PUBLICOS
	public void run()
	{
		String 		Cliente 	= "Recepcion";

		try{
			dis = new DataInputStream( socket.getInputStream() );
			dos = new DataOutputStream( socket.getOutputStream() );
			
			int servicioSolicitado = dis.readInt();
			
			MuestraInformacion( "Iniciando comuncaci�n con el cliente" , Cliente, nivel.INFO,dondeSeMuestra.AMBOS );
			
			//region COMUNICACION CON EL CLIENTE
			
			MuestraInformacion( "Se solicito el servicio: " + servicioSolicitado , Cliente, nivel.INFO,dondeSeMuestra.AMBOS );
			MuestraInformacion( "Enviando confirmacion de recepcion:" , Cliente, nivel.INFO,dondeSeMuestra.AMBOS );
			dos.writeUTF( String.valueOf( servicioSolicitado ) );
			MuestraInformacion( "Esperando documento" , Cliente, nivel.INFO,dondeSeMuestra.AMBOS );
			
			boolean esperaRecepcionDeDocumento = true;
			
			while ( esperaRecepcionDeDocumento ){
				if ( dis.available() > 0 ){
					MuestraInformacion( "dis.availalbe: " + dis.available() , Cliente, nivel.INFO,dondeSeMuestra.AMBOS );
					esperaRecepcionDeDocumento = false;
				}
			}
			
			MuestraInformacion( "Informacion recibida." , Cliente, nivel.INFO,dondeSeMuestra.AMBOS );
			
			byte[] datosRecividos = new byte[dis.available()];
			MuestraInformacion( "Datos recibidos: " + datosRecividos.length , Cliente, nivel.INFO,dondeSeMuestra.AMBOS );
			
			for ( int i = 0; i < datosRecividos.length; i++ ){
				datosRecividos[i] = dis.readByte();
			}
			
			String DocumentoRecivido = new String ( datosRecividos );
			
			DocumentoRecivido = DocumentoRecivido.replaceAll("[^\\p{L}\\p{Nd}]+", "");
			
			MuestraInformacion( "El documento recivido fue: " + DocumentoRecivido , Cliente, nivel.INFO,dondeSeMuestra.AMBOS );
			
			MuestraInformacion( "Enviando confirmaci�n al cliente", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			
			dos.writeUTF( String.valueOf( servicioSolicitado ) );
			
			// Termina por cerrar la informacion que no se ocupa
			
			if ( !(servicioSolicitado == 17  || servicioSolicitado == 19 || servicioSolicitado == 21 || servicioSolicitado == 25) ){
				dis.close();
				dos.close();
				socket.close();
			} else {
				contadorDeProcesos.setDis( dis );
				contadorDeProcesos.setDos( dos );
			}
			//endregion

			/** TODOS LOS DOCUMENTOS RECIVIDOS SON CON EL SIGUIENTE FORMATO:
			 * 
			 * 	XXXXXXYYYYYYWWWZZZ
			 * 	
			 * 	XXXXXX 	= CLIENTE 
			 *  YYYYYY 	= NUMERO DE DOCUMENTO
			 *  WWW		= SERIE DEL DOCUMENTO
			 *  ZZZ		= SUCURSAL
			 * 
			 *  TODOS A EXCEPCION DE PEMEX EL CUAL ES:
			 * 
			 * 	XXXXXXYYYYYYWWZZ
			 *  
			 *  XXXXXX 	= CLIENTE 
			 *  YYYYYY 	= NUMERO DE DOCUMENTO
			 *  WW		= SERIE DEL DOCUMENTO  ( FA )
			 *  ZZ		= SUCURSAL			   ( PX )
			 * 
			 * 
			 */

			/** COMO FUNCIONA
			 *  En este caso al recivir informacion solo puede tener dos posibles acciones.
			 *  O ejecuta el proceso solicitado
			 *  O guarda la informacion para que esta se ejecute posteriormente
			 */
			
			/** PROCESO DE TICKETS
			 *  VALIDA QUE HAYA TICKETS DISPONIBLES ( 20 ) TICKETS DE INICIO.
			 *  
			 *  SI HAY UN TICKET DISPONIBLE, TOMA Y COMIENZA A REALIZAR EL PROCESO QUE SE LE PIDIO.
			 * 
			 * 	SI NO HAY TICKET GUARDA LA INFORMACION EN BASE DE DATOS Y TERMINA EL PROCESO.
			 */
			
			// TODO REALIZAR EL CODIGO PARA PODER IMPLEMENTAR ESTA SOLUCION EL CODIGO QUE ELIGE EL SERVICIO SE SACA DE AQUI
			// 		PARA QUE SE PUEDA VALIDAR POR FUERA JUNTO CON UN CONTADOR PARA PODER MANDARLO.
			
			// Variable para indicar que no se toma en cuenta procesos.
			
			ArrayList<Integer> ProcesoExentos = new ArrayList <Integer> ( );
			
			ProcesoExentos.add ( 19 );
			ProcesoExentos.add ( 21 );
			ProcesoExentos.add ( 23 );
			ProcesoExentos.add ( 25 );
			
			boolean ProcesoExento = false;

			ProcesoExento = ProcesoExentos.contains ( servicioSolicitado ) ? true : false;
			
			if ( ProcesoExento ){
				contadorDeProcesos.EjecutaProceso ( servicioSolicitado, DocumentoRecivido );
			} else {	 
				if ( contadorDeProcesos.getProceso( ) ){
					// Puede empezar con el proceso
					MuestraInformacion( "Procede a mandar el proceso", "Server", nivel.INFO,dondeSeMuestra.AMBOS );
					// Continua con el siguiente paso.
					contadorDeProcesos.EjecutaProceso( servicioSolicitado, DocumentoRecivido );
					contadorDeProcesos.releaseProceso ( );
				} else {
					// No puede realizar el proceso y lo manda a la base de datos.
					GuardaEstado( DocumentoRecivido, movimiento.EN_COLA, estado.NO_APLICA, documento.NO_APLICA, movimiento.values( )[servicioSolicitado].toString( ), "" );
					return;
				}
			}
		} catch ( Exception e ){
			contadorDeProcesos.releaseProceso ( );
			error.clear();
			error.add( "Error al recibir conexion" );
			error.add( e.toString() );
			MuestraInformacion( error , "Server", nivel.ERROR, dondeSeMuestra.AMBOS );
		} finally {
			try {
				if ( dis != null ) dis.close();
				if ( dos != null ) dos.close();
				if ( socket != null ) socket.close();
			} catch ( Exception errorCerrado ) {
				MuestraInformacion( "Error al cerrar recursos.", "Server", nivel.ERROR , dondeSeMuestra.AMBOS );
			} finally {
				dis 	= null;
				dos 	= null;
				socket 	= null;
			}
		}
	}
	//endregion

	//region METODOS PRIVADOS
	static void MandaProcesosSinContador( HashMap <String, String> procesosPendientes )
	{
		String Cliente = "Auditor Mensual";
		int DocumentosPorProcesar 	= procesosPendientes.size ( ) ;
		int DocumentoProcesado 		= 0;
		
		for ( String Llave : procesosPendientes.keySet( ) ) {
			
			MuestraInformacion ( "Procesando documento " + DocumentoProcesado++ + " de  " + DocumentosPorProcesar, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			
			int TipoProceso = Integer.parseInt( procesosPendientes.get( Llave ) );
			// Lanza el proceso si es que existe un ticket libre, en caso de no existir, no hace nada.
			MuestraInformacion( "Mandando proceso en cola, sin limite: " + Llave , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			try {
				contadorDeProcesos.EjecutaProceso( TipoProceso, Llave );
				//Utilidades.EliminaDocumentoDeCola( Llave );
				MuestraInformacion( "Terminando proceso en cola con exito: " + Llave , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			} catch ( Exception e ){
				MuestraInformacion( "Terminando proceso en cola con error: " + Llave , Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
				MuestraInformacion ( e.toString ( ), Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
			}
			contadorDeProcesos.releaseProceso ( );
		}
		
	}
	
	static void MandaDocumentosFaltantesEnRemoto ( ArrayList <String> listado )
	{
		String Cliente = "Auditor BasesDeDatos";
		int DocumentosPorProcesar 	= listado.size ( ) ;
		int DocumentoProcesado 		= 0;
		
		for ( String Llave : listado ) {
			
			MuestraInformacion ( "Procesando documento " + DocumentoProcesado++ + " de  " + DocumentosPorProcesar, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			
			int TipoProceso = Integer.parseInt( "31" );
			// Lanza el proceso si es que existe un ticket libre, en caso de no existir, no hace nada.
			MuestraInformacion( "Mandando proceso en cola, sin limite: " + Llave , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			try {
				contadorDeProcesos.EjecutaProceso( TipoProceso, listado, 1 );
				MuestraInformacion( "Terminando subida de: " + Llave , Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			} catch ( Exception e ){
				MuestraInformacion( "Terminando subida con error: " + Llave , Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
				MuestraInformacion ( e.toString ( ), Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
			}
			contadorDeProcesos.releaseProceso ( );
		}
	}
	
	
	//endregion
	
}
	//endregion

//TODO PARA LAS CONEXIONES A BASE DE DATOS CREAR CONSTRUCTORES QUE TOMEN CONECCION EN VEZ DE QUE ELLAS HAGAN TODO

/** DESCRIPCION DE FUNCIONES Y FECHAS
 * 	14/04/14 - 	EL server realiza una carga dinamica de los archivos jar. Se tomo la configuraci�n de de interfaces, antes que de superclases
 * 				La primera clase que se usara es la correspondiente a la carga
 * 	16/07/14 -	Lee archivos xml, con UTF-8. 
 * 				VERSION: 1.0.2.0		
 * 
 * 
 * 
 */
//TODO PARA LAS CONEXIONES A BASE DE DATOS CREAR CONSTRUCTORES QUE TOMEN CONECCION EN VEZ DE QUE ELLAS HAGAN TODO

/** DESCRIPCION DE FUNCIONES Y FECHAS
 * 	14/04/14 - 	EL server realiza una carga dinamica de los archivos jar. Se tomo la configuraci�n de de interfaces, antes que de superclases
 * 				La primera clase que se usara es la correspondiente a la carga
 * 	16/07/14 -	Lee archivos xml, con UTF-8. 
 * 				VERSION: 1.0.2.0		
 * 
 * 
 * 
 */


