package com.bmx.srv;
import java.io.InputStream;
import java.util.ArrayList;
import com.bmx.srv.Utilidades.dondeSeMuestra;
import com.bmx.srv.Utilidades.nivel;
import com.jcraft.jsch.*;


public class SshManagment
{
	String manejador 	= "SSHManagment";
	String parteFlujo;
	
	String host 	= "";
	String puerto 	= "";
	
	boolean estado;
	int		codError;
	String	desError;
	String	mensaje;
	
	ArrayList <String> log = new ArrayList <String> ( );
	
	InputStream in;
	
	JSch 	jsch 	= null;
	Session session = null;
	Channel	channel	= null;
	
	public boolean getEstado(){
		return estado;
	}
	public int getCodError(){
		return codError;
	}
	public String getDescripcionDelError(){
		return desError;
	}
	public String getMensaje(){
		return mensaje;
	}
	public ArrayList <String> getLog(){
		return log;
	}
	public InputStream getInput(){
		return in;
	}
	
	private enum error{
		SIN_ERROR(0),
		HOST_VACIO(1),
		PUERTO_VACIO(2),
		NO_SE_CONECTO(3),
		NO_HAY_COMMANDO(4),
		NO_SE_PUDO_EJECUTAR(5),
		NO_SE_PUDO_DESCONECTAR(6),
		NO_SE_PUDO_TERMINAR_LA_SESSION(7),
		PATH_DEL_SERVER_VACIO(8),
		PATH_LOCAL_VACIO(9)
		;
		int er;
		private error( int e ){
			er = e;
		}
		public int getCodError(){
			return er;
		}
		public String getDescripcionDelError() {
			String resultado = "";
			switch ( er ) {
				case 0:
					resultado = "Sin error";
					break;
				case 1:
					resultado = "La cadena de host no existe";
					break;
				case 2:
					resultado = "No se tiene un puerto";
					break;
				case 3:
					resultado = "No se pudo conectar";
					break;
				case 4:
					resultado = "No se paso un comando";
					break;
				case 5:
					resultado = "No se pudo ejecutar el comando";
					break;
				case 6:
					resultado = "No se pudo desconectar";
					break;
				case 7:
					resultado = "No se pudo terminar la session";
					break;
				case 8:
					resultado = "La ruta del archivo en el server, esta vacia";
					break;
				case 9:
					resultado = "La ruta local esta vacia";
					break;
			}
			return resultado;
		}
		
	}

	private enum flujo{
		RECABANDO_INFORMACION(0),
		CONECTANDO(1),
		EJECUTANDO_PROCESO(2),
		DESCONECTANDO(3),
		TERMINANDO_SESSION(4),
		RECUPERANDO_ARCHIVO(5)
		;
		int f;
		private flujo(int fl){
			f = fl;
		}
		public String getDescripcionDelFlujo(){
			String resultado ="";
			switch ( f ) {
				case 0:
					resultado = "Recuperando informacion";
					break;
				case 1:
					resultado = "Conectando";
					break;
				case 2:
					resultado = "Ejecutando comando";
					break;
				case 3:
					resultado = "Desconectando";
					break;
				case 4:
					resultado = "Terminando session";
					break;
				case 5:
					resultado = "Copiando archivo de server a local";
					break;
			}
			
			return resultado;
		}
		
		
	}

	private enum est{
		ERROR(1),
		OK(2)
		;
		int i = 2;
		private est( int n ){
			i = n;
		}
		public boolean getEstado(){
			boolean resultado = false;
			switch ( i ) {
				case 1:
					resultado = false;
					break;
				case 2:
					resultado = true;
					break;
			}
			return resultado;
		}
	}
	
	public SshManagment( String Host, String Puerto )
	{
		setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.RECABANDO_INFORMACION, "" );
		
		Host 	= Host.trim ( );
		Puerto 	= Puerto.trim ( );
		
		if ( Host.isEmpty ( ) ) {
			setEstado ( est.ERROR.getEstado ( ), error.HOST_VACIO , flujo.RECABANDO_INFORMACION, "La cadena es: " + Host );
		} else {
			host = Host;
		}
		
		if ( Puerto.isEmpty ( ) ){
			setEstado ( est.ERROR.getEstado ( ), error.PUERTO_VACIO, flujo.RECABANDO_INFORMACION, "La cadena es: " + Puerto );
		} else {
			puerto = Puerto;
		}
		
		if ( estado ){
			jsch = new JSch ( );
		}
		
	}
	
	public void Connect(){
		conecta ( );
	}
	public void Desconecta(){
		desconecta ( );
	}
	public void TerminaSession(){
		
	}

	public void EjecutandoProceso( String proceso ){
		if ( proceso.isEmpty ( ) ){
			setEstado ( est.ERROR.getEstado ( ), error.NO_HAY_COMMANDO, flujo.EJECUTANDO_PROCESO, "La cadena pasada es: " + proceso );
		}
		// manda el proceso.
		try {
			
			conecta ( );
			
			channel = session.openChannel ( "exec" );
			
			setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.EJECUTANDO_PROCESO, "" );
			((ChannelExec)channel).setCommand ( proceso );
			channel.setInputStream ( null );
			((ChannelExec)channel).setErrStream ( System.err );
			InputStream in = channel.getInputStream ( );
			
			channel.connect ( );
			
			byte[] tmp = new byte[1024];
			while(true){
				while(in.available ( ) > 0 ){
					int i = in.read ( tmp, 0, 1024);
					if ( i < 0 ) break;
					System.out.println ( new String (tmp, 0 , i ) );
				}
				
				if ( channel.isClosed ( ) ){
					if ( in.available ( ) > 0 ) continue;
					
					setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.EJECUTANDO_PROCESO, "El codigo de salida fue: " + String.valueOf ( channel.getExitStatus ( ) ) );
					
					break;
				}
				try{ Thread.sleep ( 1000 ); } catch( Exception e){ System.out.println (e.toString ( ));};
				
			}
			
		} catch ( Exception e ){
			setEstado ( est.ERROR.getEstado ( ), error.NO_SE_PUDO_EJECUTAR, flujo.EJECUTANDO_PROCESO, e.toString ( ) );
		}
		
		setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.EJECUTANDO_PROCESO, "" );
		
		desconecta ( );
		
	}
	
	public void RecuperaArchivo ( String pathServer, String pathLocal ){
		
		pathLocal 	= pathLocal.trim ( );
		pathServer 	= pathServer.trim ( );
		
		if ( pathServer.isEmpty ( ) ){
			setEstado ( est.ERROR.getEstado ( ), error.PATH_DEL_SERVER_VACIO, flujo.RECUPERANDO_ARCHIVO, "" );
		} 
		
		if ( pathLocal.isEmpty ( ) ){
			setEstado ( est.ERROR.getEstado ( ), error.PATH_LOCAL_VACIO, flujo.RECUPERANDO_ARCHIVO, "" );
		}
		
		try {
			
			conecta ( );
			
			channel = session.openChannel ( "sftp" );
			
			channel.connect ( );
			
			ChannelSftp sftp = (ChannelSftp) channel;
			
			sftp.get ( pathServer, pathLocal );
			
			desconecta ( );
			
			conecta ( );
			
			channel = session.openChannel ( "exec" );
			
			setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.EJECUTANDO_PROCESO, "" );
			((ChannelExec)channel).setCommand ( "rm " + pathServer );
			channel.setInputStream ( null );
			((ChannelExec)channel).setErrStream ( System.err );
			InputStream in = channel.getInputStream ( );
			
			channel.connect ( );
			
			byte[] tmp = new byte[1024];
			while(true){
				while(in.available ( ) > 0 ){
					int i = in.read ( tmp, 0, 1024);
					if ( i < 0 ) break;
					System.out.println ( new String (tmp, 0 , i ) );
				}
				
				if ( channel.isClosed ( ) ){
					if ( in.available ( ) > 0 ) continue;
					
					setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.EJECUTANDO_PROCESO, "El codigo de salida fue: " + String.valueOf ( channel.getExitStatus ( ) ) );
					
					break;
				}
				try{ Thread.sleep ( 1000 ); } catch( Exception e){ System.out.println (e.toString ( ));};
				
			}
					
			
		} catch ( Exception e ){
			setEstado ( est.ERROR.getEstado ( ), error.NO_SE_PUDO_EJECUTAR, flujo.EJECUTANDO_PROCESO, e.toString ( ) );
		}
		
		setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.EJECUTANDO_PROCESO, "" );
		
		desconecta ( );
		
	}
	
	void conecta(){
		
		try{
			setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.CONECTANDO, "" );
			session = jsch.getSession ( "root", host, Integer.parseInt ( puerto ) );
			session.setPassword ( "gbr2007" );
			session.setConfig ( "StrictHostKeyChecking", "no");
			session.connect ( 60000 );
		} catch ( Exception e ){
			setEstado ( est.ERROR.getEstado ( ), error.NO_SE_CONECTO, flujo.CONECTANDO, e.toString ( ) );
		}
		setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.CONECTANDO, "Coneccion exitosa" );
	}
	void desconecta(){
		
		if ( channel == null ){
			setEstado ( est.OK.getEstado ( ), error.NO_SE_CONECTO, flujo.DESCONECTANDO, "No estaba conectado" );
			return;
		}
		
		try{
			setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.DESCONECTANDO, "" );
			if ( channel.isConnected ( ) ){
				channel.disconnect ( );
			} 
		} catch ( Exception e ){
			setEstado ( est.ERROR.getEstado ( ), error.NO_SE_CONECTO, flujo.DESCONECTANDO, e.toString ( ) );
		}
		setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.DESCONECTANDO, "Desconeccion exitosa." );
	}
	void terminaSession(){
		try{
			setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.TERMINANDO_SESSION, "" );
			
			if ( channel.isConnected ( ) ){
				setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.TERMINANDO_SESSION, "Terminando canal" );
				channel.disconnect ( );
			}
			
			if ( session.isConnected ( ) ){
				setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.TERMINANDO_SESSION, "Terminando session" );
				session.disconnect ( );
			}
			 
		} catch ( Exception e ){
			setEstado ( est.ERROR.getEstado ( ), error.NO_SE_CONECTO, flujo.TERMINANDO_SESSION, e.toString ( ) );
		}
		setEstado ( est.OK.getEstado ( ), error.SIN_ERROR, flujo.TERMINANDO_SESSION, "Finalizacion exitosa" );
	}
	
	
	void setEstado( boolean es, error er, flujo fl, String me ){

		estado 		= es;
		codError 	= er.getCodError ( );
		desError 	= er.getDescripcionDelError ( );
		mensaje		= me;
		
		log.add ( fl.getDescripcionDelFlujo ( ) );
		
		String mensajeFinal = er.getDescripcionDelError ( ) + " - " + me;
		
		Utilidades.MuestraInformacion ( mensajeFinal, "Replica ", nivel.INFO, dondeSeMuestra.AMBOS );
		
	}
}
