package com.bmx.srv;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;

public class LectorXML 
{
	//region VARIABLES PRIVADAS
	private	String		xmlDoc;
	private boolean		resultado;
	private boolean		continuar;
	
	private ArrayList <String> error = new ArrayList <String> ( );
	
	private boolean		guardaArchivo = false;
	
	File		xml		= null;
	//endregion
	
	//region PROPIEDADES
	
	public boolean getResultado()
	{
		return resultado;
	}
	
	public ArrayList <String> getError() 
	{
		return error;
	}
	
	public void setGuardaArchivo ( boolean guarda )
	{
		guardaArchivo = guarda;
	}
	
	//endregion
	
	//region CONSTRUCTOR
	
	public LectorXML( String XmlDoc )
	{
		
		xmlDoc = XmlDoc;
		
		// Valida que el documento exista, de no ser asi, manda error.
		
		xml = new File( xmlDoc );
		
		if ( !xml.exists() ){
			continuar = false;
		} else {
			continuar = true;
		}
		resultado = continuar;

	}
	
	//endregion
	
	//region METODOS PUBLICOS
	
	public String RecuperaAtributoDeRaiz ( String nombre )
	{
		String Resultado = "";
		
		DocumentBuilderFactory		factory 	= DocumentBuilderFactory.newInstance();
		InputSource					is			= null;
		
		try {
			DocumentBuilder				builder		= factory.newDocumentBuilder();
			
			is = validaArchivo ( );
			
			if ( is != null ){

				org.w3c.dom.Document		document	= builder.parse( is );
				org.w3c.dom.Element			root		= document.getDocumentElement();
				
				Resultado = root.getAttribute( nombre );
				resultado = true;
			} else {
				resultado = false;
			}
			
		} catch (SAXException e) {
			resultado = false;
			error.add ( e.toString ( ) );
		} catch (ParserConfigurationException e) {
			resultado = false;
			error.add ( e.toString ( ) );
		} catch ( IOException e ) {
			resultado = false;
			error.add ( e.toString ( ) );
		} 
		
		return Resultado;
	}
	
	//endregion
	
	//region METODOS PRIVADOS
	@SuppressWarnings ( "resource" )
	private InputSource validaArchivo()
	{
		InputSource resultado = null;
		// Convierte a BYTE[]
		
		byte[] xmlContent = new byte[(int) xml.length ( )];
		FileInputStream 		fis 	= null;
		UnicodeBOMInputStream 	ubis	= null;
		Reader 					r		= null;
		
		try {
			fis = new FileInputStream ( xml );
			
			fis.read ( xmlContent );
			
			CharsetDetector detector = new CharsetDetector ( );
			detector.setText ( xmlContent );
			
			CharsetMatch cm = detector.detect ( );
			
			String encoding = cm.getName ( );
			
			byte[] utf8 = null;
			
			if ( encoding != "UTF-8" ){
				// Convierte el documento a UTF-8 
				utf8 = new String( xmlContent, encoding).getBytes ( "UTF-8" );	
			} else {
				utf8 = xmlContent;
			}
			// Guarda el archivo y procede a trabajar con el
			
			if ( guardaArchivo ){
				guardaArchivoValidado ( utf8 );
			}

			ubis = new UnicodeBOMInputStream ( new ByteArrayInputStream ( utf8 ) );
			
			// Determina si tiene BOM, para eliminarlo.
			
			if ( ubis.getBOM ( ).toString ( ) == "UTF-8"  ){
				ubis.skipBOM ( );	
			}
			
			r = new InputStreamReader ( ubis, "UTF-8" );

			resultado = new InputSource ( r );
			
		} catch ( FileNotFoundException e ) {
			error.clear ( );
			error.add ( "No se pudo validar el archivo" );
			error.add (  e.toString ( ) );
		} catch ( IOException e ) {
			error.clear ( );
			error.add ( "Se tuvieron problemas con el archivo" );
			error.add ( e.toString ( ) );
		} 
		
		return resultado;
	}
	
	private void guardaArchivoValidado( byte[] arreglo ) 
	{
		FileOutputStream 	fos 		= null;
		BufferedReader		bur			= null;
		try {
			fos = new FileOutputStream ( xml.getName ( ).replace ( ".xml", "_validado.xml" ) );
			
			fos.write ( arreglo );
			fos.flush ( );
			fos.close ( );
			
		} catch ( Exception e ){
			error.add ( "No se pudo guardar el archivo validado" );
			error.add ( e.toString ( ) );
		} finally {
			try {
				if ( fos != null ) fos.close ( );
				if ( bur != null ) bur.close ( );
			} catch ( Exception e ) { 
				error.add ( "No se pudo cerrar adecuadamente " );
				error.add ( e.toString ( ) );
			}
			 
		}
	}
	
	//endregion
	
}
