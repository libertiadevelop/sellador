package com.bmx.srv;

import static com.bmx.srv.Utilidades.MuestraInformacion;
import static com.bmx.srv.Utilidades.documento;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import com.bmx.srv.ReplicaBases.proceso;
import com.bmx.srv.Usuarios.procesos_usuarios;
import com.bmx.srv.Utilidades.dondeSeMuestra;
import com.bmx.srv.Utilidades.estado;
import com.bmx.srv.Utilidades.movimiento;
import com.bmx.srv.Utilidades.nivel;
import com.bmx.zip.ZIPFILES;


public class Procesos
{
	//region VARIABLES PARA EL LLAMADO DE PROCEDIMIENTOS
	
	public static final int	BLOB_REMOTO					= 1;		// Solo sube la informacion a local
	public static final int	BLOB_LOCAL					= 3;		// Solo sube la información al remoto
	public static final int	CORREO						= 5;		// Manda un correo.
	public static final int MARCA						= 7;		// Marca un documento ya se como cancelado o como inactivo
	
	public static final int	DUMMY						= 9;		// Proceso completo de dummy
	public static final int FACTURACION					= 11;		// Proceso completo de facturacion
	public static final int	CANCELACION					= 13;		// Proceso completo de cancelacion
	public static final int VALIDACION_BLOB				= 15;		// Proceso completo de validación
	
	public static final int RECUPERA_FACTURA			= 17;		// Proceso para recuperar archivos de factura
	
	public static final int RECUPERA_LISTAS_DE_PRECIOS	= 19;		// Proceso para recuperar lista de precios.
	public static final int RECUPERA_LISTA				= 21;		// Proceso para recuperar una lista de precios.
	public static final int BORRA_LISTA					= 23;		// Proceso para borrar una lista de precios.
	public static final int CARGA_LISTA					= 25;		// Proceso para cargar una lista de precios.
	
	public static final int SUBE_UN_SOLO_ARCHIVO		= 31;		// Proceso para subir solo un archivo.
	
	public static final int CARGA_ESTADISTICAS			= 80;		// Actualiza las tablas de estadisticas.
	
	public static final int ACTUALIZA_ARCHIVOS_EN_WEB	= 85;		// Actualiza las tablas en web.
	
							// PROCESO DE PROVEEDORES
	
	public static final int PROV_TODOS_NVO_LOC_TO_WEB		= 100;		// TODO Sube todas las tablas
	public static final int PROV_TODOS_NVO_WEB_TO_LOC		= 101;		// TODO Descarga todas las tablas
	
	public static final int PROV_PROV_NVO_LOC_TO_WEB		= 110;		// TODO Sube los nuevos proveedores.
	
	public static final int PROV_USER_NVO_LOC_TO_WEB		= 120;		// TODO Sube nuevos usuarios.
	public static final int PROV_USER_NVO_WEB_TO_LOC		= 121;		// TODO Descarga los usuarios.
	public static final int PROV_USER_ACT_LOC_TO_WEB		= 122;		// TODO Sube cambios de los usuario.
	public static final int PROV_USER_ACT_WEB_TO_LOC		= 123;		// TODO Descarga cambios de los usario.
	public static final int PROV_USER_DEL_LOC_TO_WEB		= 124;		// TODO Borra el usuario de la base de datos remota.
	public static final int PROV_USER_DEL_WEB_TO_LOC		= 125;		// TODO Borra usuarios de la web a local.
	
	public static final int PROV_PERM_NVO_LOC_TO_WEB		= 126;		// TODO Sube los permisos.
	public static final int	PROV_PERM_NVO_WEB_TO_LOC		= 127;		// TODO Descarga los cambios en los permisos
	public static final int PROV_PERM_ACT_LOC_TO_WEB		= 128;		// TODO Sube los permisos.
	public static final int	PROV_PERM_ACT_WEB_TO_LOC		= 129;		// TODO Descarga los cambios en los permisos
	
	public static final int PROV_USER_SIN_WEB_TO_LOC		= 130;		// TODO Sincroniza las dos tablas, de web a local ( primero agrega usuarios, despues valida actualizaciones )
	public static final int PROV_USER_SIN_LOC_TO_WEB		= 131;		// TODO Sincroniza las dos tablas, de web a local ( primero agrega usuarios, despues valida actualizaciones )
	
	public static final	int	PROV_DOC_SIN_WEB_TO_LOC			= 200;		// TODO Sincroniza los documentos de la web a local.
	
	public static final int	PROV_ORDENES					= 202;		// TODO Proceso para la sincronizacion de ordenes, compradores, contactos, moneda
	public static final int PROV_DOCUMENTOS					= 203;		// TODO Proceso de actualizacion de documentos, enlaces ( Descarga )
	public static final int PROV_PROVEEDORES				= 204;		// TODO Proceso de actualizacion de documentos, enlaces ( Descarga )
	public static final int PROV_COMPRADORES		 		= 205;		// TODO Proceso de actualizacion de compradores.

	public static final int PROV_TODOS						= 250;		// TODO Realiza todos los procesos de proveedores
	public static final int	PROV_ORDEN						= 252;		// TODO Proceso para la sincronizacion de ordenes, compradores, contactos, moneda
	public static final int PROV_DOCUMENTO					= 253;		// TODO Proceso de actualizacion de documentos, enlaces ( Descarga )
	public static final int PROV_PROVEEDOR					= 254;		// TODO Proceso de actualizacion de documentos, enlaces ( Descarga )
	
	public static final int PRUEBA_DE_COMUNICACION			= 1000;
	
	//endregion
	
	public static enum estadoCarga {
		YA_EXISTE, NO_EXISTE, CARGADO, NO_CARGADO;
	}
	
	public estadoCarga resultadoCarga;
	
	static int MaxNumeroDeProcesos;
	static int ProcesosCorriendo;
	
	private DataInputStream 	dis = null;
	private DataOutputStream 	dos = null;
	
	
	
	public void setDis( DataInputStream d )
	{
		dis = d;
	}
	public void setDos ( DataOutputStream d )
	{
		dos = d;
	}
	
	public estadoCarga getResultadoCarga ()
	{
		return resultadoCarga;
	}
	
	ArrayList<String> error = new ArrayList<String>( );
	
	public Procesos ( int numeroDeProcesosPermitidos )
	{
		MaxNumeroDeProcesos = numeroDeProcesosPermitidos;
	}
	
	public void setMaxProceos ( int maxProcesos )
	{
		MaxNumeroDeProcesos = maxProcesos;
	}
	
	public synchronized Boolean getProceso()
	{
		Boolean		permitirproceso = true;
		if ( ProcesosCorriendo < MaxNumeroDeProcesos ){
			permitirproceso = true;
			ProcesosCorriendo ++;
		} else {
			permitirproceso = false;
		}
		
		return permitirproceso;
	}
	
	public synchronized void releaseProceso()
	{
		ProcesosCorriendo --;
	}
	
	public void EjecutaProceso ( int servicioSolicitado, String DocumentoRecivido ) 
	{
		switch ( servicioSolicitado ) {
			case BLOB_REMOTO:
				MuestraInformacion( "Guardando blob en remoto", "Facturacion", nivel.INFO,dondeSeMuestra.AMBOS );
				_FacturacionSubeInformacionARemoto( DocumentoRecivido );
				break;
			case BLOB_LOCAL:
				MuestraInformacion( "Guardando blob en local", "Facturacion", nivel.INFO,dondeSeMuestra.AMBOS );
				_FacturacionSubeInformacionALocal(DocumentoRecivido);
				break;
			case CORREO:
				
				Utilidades.GuardaEstado( DocumentoRecivido, movimiento.INICIO_MAIL, estado.INICIO, documento.NO_APLICA, "Inicio de proceso", DocumentoRecivido );
				
				MuestraInformacion( "Mandando correo", "Facturacion", nivel.INFO,dondeSeMuestra.AMBOS );
				
				// Prepara todo para mandar el correo electronico
				String Cliente = "ENVIA CORREO ELECTRONICO";
				
				ArrayList<Object> datosRecuperados = _PreparaInformacion( DocumentoRecivido, movimiento.INICIO_MAIL, Cliente );
				
				// El primer objeto es un documento, el siguiente es un archivo.
				
				Documento 		d = ( Documento )  		datosRecuperados.get( 0 );
				ArchivosBarmex	a = ( ArchivosBarmex )	datosRecuperados.get( 1 );
				
				// Manda el correo electronico
				
				boolean resultado = _EnviaCorreoElectronico( d.getDatos( ), a.getRutaFinalXML( ), a.getRutaFinalPDF( ) );
				
				if ( resultado ) {
					Utilidades.GuardaEstado( DocumentoRecivido, movimiento.FIN_MAIL, estado.CORRECTO, documento.NO_APLICA, "Fin de proceso correcto", DocumentoRecivido );
				} else {
					Utilidades.GuardaEstado( DocumentoRecivido, movimiento.FIN_MAIL, estado.INCORRECTO, documento.NO_APLICA, "Fin de proceso incorrecto", DocumentoRecivido );
				}
				
				break;
				
			case MARCA:
				MuestraInformacion( "Marcando documento", "Facturacion", nivel.INFO,dondeSeMuestra.AMBOS );
				break;
			case DUMMY:
				MuestraInformacion( "Generando factura dummy", "Facturacion", nivel.INFO,dondeSeMuestra.AMBOS );
				_FacturacionProcesoCompleto ( DocumentoRecivido );
				break;
			case FACTURACION:
				MuestraInformacion( "Proceso completo de facturacion", "Facturacion", nivel.INFO,dondeSeMuestra.AMBOS );
				_FacturacionProcesoCompleto ( DocumentoRecivido );
				break;
			case CANCELACION:
				MuestraInformacion( "Proceso completo de cancelacion", "Cancelacion", nivel.INFO,dondeSeMuestra.AMBOS );
				_CancelacionProcesoCompleto( DocumentoRecivido );
				break;
			case RECUPERA_FACTURA:
				MuestraInformacion( "Recuperacion de archivos de facturacion", "Cancelacion", nivel.INFO,dondeSeMuestra.AMBOS );
				_RecuperaArchivosDeFacturacion ( DocumentoRecivido );
				MuestraInformacion( "Finaliza recuperacion de archivos de facturacion", "Cancelacion", nivel.INFO,dondeSeMuestra.AMBOS );
				break;
			case RECUPERA_LISTAS_DE_PRECIOS:
				MuestraInformacion( "Proceso completo de cancelacion", "Cancelacion", nivel.INFO,dondeSeMuestra.AMBOS );
				//TODO RECUPERA LISTA DE PRECIOS
			case VALIDACION_BLOB:
				// VALIDA QUE SE ENCUENTREN LOS ARCHIVOS EN BLOB LOCAL Y REMOTO.
				MuestraInformacion( "Proceso completo de validacion", "Cancelacion", nivel.INFO,dondeSeMuestra.AMBOS );
				break;
			case CARGA_LISTA:
				// VALIDA QUE SE ENCUENTREN LOS ARCHIVOS EN BLOB LOCAL Y REMOTO.
				MuestraInformacion( "Carga lista de precios.", "Lista", nivel.INFO,dondeSeMuestra.AMBOS );
				_CargaListaDePrecios ( DocumentoRecivido );
				break;
			case PROV_PROV_NVO_LOC_TO_WEB:
				MuestraInformacion( "Inicio - Actualizacion/Subida de proveedores.", "Proveedores", nivel.INFO,dondeSeMuestra.AMBOS );
				Proveedores.ActualizaProveedores ( );
				MuestraInformacion( "Fin - Actualizacion/Subida de proveedores.", "Proveedores", nivel.INFO,dondeSeMuestra.AMBOS );
				break;
			case PROV_PROVEEDOR:
				String clave_proveedor = DocumentoRecivido;
				MuestraInformacion( "Inicio - Actualizacion/Subida de proveedor " + clave_proveedor, "Proveedores", nivel.INFO,dondeSeMuestra.AMBOS );
				
				Proveedores p = new Proveedores ( clave_proveedor );
				p.ActualizaProveedor ( );
				
				MuestraInformacion( "Fin - Actualizacion/Subida de proveedor " + clave_proveedor, "Proveedores", nivel.INFO,dondeSeMuestra.AMBOS );
				break;
			case PROV_USER_NVO_LOC_TO_WEB:
				String clave_usuario = DocumentoRecivido;
				MuestraInformacion ( "Inicio - Subida de usuario: " + clave_usuario, "Proveedores", nivel.INFO, dondeSeMuestra.AMBOS );
				Usuarios.Proceso_usuarios ( procesos_usuarios.SINCRONIZA_LOC_TO_WEB );
				MuestraInformacion ( "Fin - Subida de usuario: " + clave_usuario, "Proveedores", nivel.INFO, dondeSeMuestra.AMBOS );
				break;
			case PROV_USER_ACT_LOC_TO_WEB:
				
				break;
			case PROV_PERM_ACT_LOC_TO_WEB:
				
				break;
			case PROV_USER_DEL_LOC_TO_WEB:
				
				break;
			case PROV_USER_SIN_WEB_TO_LOC: 
				
				MuestraInformacion ( "Inicio - Sincronizacion de usuarios" , "Proveedores - usuarios", nivel.INFO, dondeSeMuestra.AMBOS );
				
				Usuarios.Proceso_usuarios ( procesos_usuarios.SINCRONIZA_WEB_TO_LOC );
				
				MuestraInformacion ( "Fin - Sincronizacion de usuarios" , "Proveedores - usuarios", nivel.INFO, dondeSeMuestra.AMBOS );
				break;
				
			case PROV_USER_SIN_LOC_TO_WEB:
				MuestraInformacion ( "Inicio - Sincronizacion de usuarios" , "Proveedores - usuarios", nivel.INFO, dondeSeMuestra.AMBOS );
				
				Usuarios.Proceso_usuarios ( procesos_usuarios.SINCRONIZA_LOC_TO_WEB );
				
				MuestraInformacion ( "Fin - Sincronizacion de usuarios" , "Proveedores - usuarios", nivel.INFO, dondeSeMuestra.AMBOS );
				break;
				
			case PROV_COMPRADORES:

				MuestraInformacion( "Inicio - Carga Compradores ", "Proveedores", nivel.INFO,dondeSeMuestra.AMBOS );
				
				Proveedores.sincronizaCompradores ( );
				
				MuestraInformacion( "Fin - Carga Compradores ", "Proveedores", nivel.INFO,dondeSeMuestra.AMBOS );
				
				break;
			case PRUEBA_DE_COMUNICACION:
				MuestraInformacion( "Inicio - Prueba de comunicacion ", "Test", nivel.INFO,dondeSeMuestra.AMBOS );
				MuestraInformacion( "Se recivio: " + DocumentoRecivido, "Test", nivel.INFO,dondeSeMuestra.AMBOS );
				MuestraInformacion( "Fin - Prueba de comunicacion ", "Test", nivel.INFO,dondeSeMuestra.AMBOS );
				break;
			case PROV_DOC_SIN_WEB_TO_LOC:
				MuestraInformacion ( "Inicio - Sincronizacion documentos", "Proveedores", nivel.INFO, dondeSeMuestra.AMBOS );
				Enlaces.DescargaEnlaces ( );
				MuestraInformacion ( "Fin - Sincronizacion documentos", "Proveedores", nivel.INFO, dondeSeMuestra.AMBOS );
				break;
			case CARGA_ESTADISTICAS:
				MuestraInformacion ( "Inicio - Actualizacion de estadisticas", "Estadisticas", nivel.INFO, dondeSeMuestra.AMBOS );
				
				MuestraInformacion ( "Inicio - Estadistica 7", "Estadisticas", nivel.INFO, dondeSeMuestra.AMBOS );
				ReplicaBases.replicaOdbcTableToMySQL ( proceso.ESTADISTICA_07, 100 , DocumentoRecivido );
				MuestraInformacion ( "Fin - Estadistica 7", "Estadisticas", nivel.INFO, dondeSeMuestra.AMBOS );
				
				MuestraInformacion ( "Inicio - Estadistica 14", "Estadisticas", nivel.INFO, dondeSeMuestra.AMBOS );
				ReplicaBases.replicaOdbcTableToMySQL ( proceso.ESTADISTICA_14, 100 , DocumentoRecivido );
				MuestraInformacion ( "Fin - Estadistica 14", "Estadisticas", nivel.INFO, dondeSeMuestra.AMBOS );
				
				MuestraInformacion ( "Fin - Actualizacion de estadisticas", "Estadisticas", nivel.INFO, dondeSeMuestra.AMBOS );
				break;
			case ACTUALIZA_ARCHIVOS_EN_WEB:
				MuestraInformacion ( "Inicio - Actualizacion de tablas web", "Actualiza - web", nivel.INFO, dondeSeMuestra.AMBOS );
				
				MuestraInformacion ( "Fin - Actualizacion de tablas web", "Actualiza - web", nivel.INFO, dondeSeMuestra.AMBOS );
				break;
			default:
				MuestraInformacion( "No se revicio solicitud.", "Server", nivel.ALARMA,dondeSeMuestra.AMBOS );
				break;
			}
	}
	
	public void EjecutaProceso ( int servicioSolicitado, ArrayList <String> documentos, int sizeBatch )
	{
		switch ( servicioSolicitado ) {
			case SUBE_UN_SOLO_ARCHIVO:
				// VALIDA QUE SE ENCUENTREN LOS ARCHIVOS EN BLOB LOCAL Y REMOTO.
				MuestraInformacion( "Cargando listado de documentos.", "Auditor", nivel.INFO,dondeSeMuestra.AMBOS );
				_CargaListadoDeDocumentos ( documentos, sizeBatch ) ;
				break;
		}
	}
	
	private void _CargaListadoDeDocumentos ( ArrayList <String> docs, int sizeBatch )
	{
		Blob b = new Blob ( Conecciones.getMySQLLocalFacturas ( ), Conecciones.getMySQLRemoto ( )  );
		b.cargaLista ( docs, sizeBatch );
	}
	
	private void _CargaListaDePrecios( String ListaACargar )
	{
		String Cliente = "Carga Lista";
		InputStream		in = null;
		// En este caso solo se espera que llegue el archivo.
		// Se asume que la lista es nueva, no se valida ya que la llave es númerica no la lista en si.
		
		MuestraInformacion ( "Recibiendo archivo de la lista: "  + ListaACargar, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		// Recive el documento
		try {
			byte[] buf = new byte[1024*10000];
			
			int len;
			int tis = 0;
			
			while ( (len = dis.read(buf) ) != -1 ){
				tis = tis + len;
			}
			
			in = new ByteArrayInputStream( buf );
			
			MuestraInformacion ( "Se recibieron: " + String.valueOf ( tis ) + " bytes."  + ListaACargar, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			MuestraInformacion ( "Guardando blob de la lista: "  + ListaACargar, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			
			// Lo guarda como blob
			
			// Primero guarda la lista para obtener el id de esta.
			
			ConsultaODBC c = new ConsultaODBC ( Conecciones.getMySQLLocalBarpro ( ) );
			
			String sentencia = "INSERT INTO listadeprecios (Nombre) VALUES ('" + ListaACargar + "')";
			
			c.InsertaRegistroMySQL ( sentencia );
			
			if ( c.resultado ) {
				MuestraInformacion ( "Se cargo el encabezado, recuperando llave", Cliente, nivel.INFO , dondeSeMuestra.AMBOS );
				
				sentencia = "SELECT idListaDePrecios FROM listadeprecios WHERE Nombre like '" + ListaACargar + "'";
				
				HashMap <String, ArrayList <String>> llave = c.ConsultaVariasColumnasMySQL ( sentencia );
				
				String id = "";
				
				if ( llave.isEmpty ( ) ){
					// Recupera la llave.
					for ( String lla : llave.keySet ( ) ) {
						ArrayList <String> ll = llave.get ( lla );
						
						id = ll.get ( 0 );
		
					}
					
					if ( id.trim ( ).length ( ) > 0 ){
						MuestraInformacion ( "La llave fue: " + id, Cliente, nivel.INFO , dondeSeMuestra.AMBOS );
						MuestraInformacion ( "Intentando subir blob. ", Cliente, nivel.INFO , dondeSeMuestra.AMBOS );
						
						Blob b = new Blob ( Conecciones.getMySQLLocalBarpro ( ) );
						
						InputStream[] i = { in }; 
						
						if ( b.cargaBlob ( i ) ){
							MuestraInformacion ( "El archivo se cargo correctamente", Cliente, nivel.INFO , dondeSeMuestra.AMBOS );
						} else {
							MuestraInformacion ( "El archivo no se cargo", Cliente, nivel.INFO , dondeSeMuestra.AMBOS );
						}
						
						
					} else {
						MuestraInformacion ( "No se pudo cargar el blob.", Cliente, nivel.INFO , dondeSeMuestra.AMBOS );
						return;
					}
					
					
				} else {
					MuestraInformacion ( "No se pudo cargar la llave primaria.", Cliente, nivel.INFO , dondeSeMuestra.AMBOS );
					return;
				}
				
			} else {
				MuestraInformacion ( "No se pudo guardar el encabezado de la lista: " + ListaACargar, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				MuestraInformacion ( c.getError ( ), Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
			}
			// Despues guarda el blob.
			
			
			MuestraInformacion ( "Terminando carga de la lista: "  + ListaACargar, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		} catch ( Exception e ){
			error.clear ( );
			error.add ( "No se pudo carga el archivo, para la lista: " + ListaACargar );
			error.add ( e.toString ( ) );
			MuestraInformacion ( error, Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
		}
		// Sale
		MuestraInformacion ( "Terminando carga de la lista: "  + ListaACargar, Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
	}
	
	private void _RecuperaArchivosDeFacturacion( String documentoRecivido )
	{
		String Cliente = "Recupera archivos facturacion";
		
		// Estos archivos los recupera de la base local.
		try {
			dos.writeInt( 17 );
			// Procede a recupera la informacion.
			@SuppressWarnings ( "unused" )
			int subProceso = dis.readInt( );
			
			Blob d = new Blob( Conecciones.getMySQLLocalFacturas( ) );
			
			String archivoXML = documentoRecivido + "_S.xml";
			String archivoPDF = documentoRecivido + ".pdf";
			String archivoZIP = "tmp.zip";
			
			// Descarga el xml
			
			String Sentencia = "SELECT contenido FROM facturas WHERE nombre = '" + archivoXML + "'";
			
			d.setSentencia( Sentencia );
			
			d.DescargaArchivo( archivoXML );
			
			// Descarga el PDF
			
			d = new Blob( Conecciones.getMySQLLocalFacturas( ) );
			
			Sentencia = "SELECT contenido FROM facturas WHERE nombre = '" + archivoPDF + "'";
			
			d.setSentencia( Sentencia );
			
			d.DescargaArchivo( archivoPDF );	
			
			// Comprime 
			ArrayList<String> archivos = new ArrayList<String>( );
			archivos.add( archivoPDF );
			archivos.add( archivoXML );
			
			ZIPFILES z = new ZIPFILES( archivoZIP );
			
			z.Comprime( archivos );
			
			// Elimina los archivos residuales
			
			File x 	= new File ( archivoXML );
			File p	= new File ( archivoPDF );
			
			if ( x.exists ( ) ) x.delete ( );
			if ( p.exists ( ) ) p.delete ( ) ;
			
			// Envia
		
			byte[] archivoAEnviar = new byte[1024 * 10000];
			int len;
			
			FileInputStream fis = new FileInputStream ( archivoZIP );
			
			while ( ( len = fis.read ( archivoAEnviar ) ) != -1 ) {
				dos.write ( archivoAEnviar, 0, len );
				dos.flush ( );
			}
			
			// Elimina los archivos residuales
			
			File zi 	= new File ( archivoZIP );
			if ( zi.exists ( ) ) zi.delete ( );
			
			fis.close ( );
			dos.close ( );
			dis.close ( );
			
					
		} catch ( Exception e ){
			error.clear ( );
			error.add ( "No se pudo terminar el proceso" );
			error.add ( e.toString ( ) );
			MuestraInformacion ( error, Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
		}
		
	}
	
	private void _FacturacionProcesoCompleto( String documentoRecivido )
	{
		
		Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_FACTURACION, estado.INICIO, documento.NO_APLICA, "Inicio de proceso", documentoRecivido );
		
		String Cliente = "Facturacion Completa";
		
		//region RECUPERA INFORMACION DEL DOCUMENTO RECIBIDO
		// El listado de informacion a recuperar es: 
		// cliente, documento, serie, sucursal, movimiento, path
		// Una vez recuperada esta información procede a armar el documento, mismo que se cargara en la base de datos.
		
		ArrayList<Object> datos = _PreparaInformacion( documentoRecivido, movimiento.INICIO_FACTURACION, Cliente );	
		
		if ( datos == null ){
			Utilidades.MuestraInformacion ( "No se pudieron recuperar los archivos de facturacion, " + documentoRecivido + " proceso detenido.", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
			return;
		}
		
		// El primer objeto es un documento, el segundo es un archivo y el tercero una cadena.
		
		Documento 		d 		= ( Documento )			datos.get( 0 );
		ArchivosBarmex	a 		= ( ArchivosBarmex ) 	datos.get( 1 );
		String 			fecha 	= ( String )			datos.get( 2 );
		
		//endregion
		
		//region CARGA ARCHIVOS A MYSQL LOCAL
		
		MuestraInformacion( "------ INICIANDO SUBIDA A LOCAL.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_BLOB_LOCAL, estado.INICIO, documento.ARCHIVO_XML, "Inicio de proceso", documentoRecivido );
		
		boolean resultadoCargaMySQL = false;
		
		resultadoCargaMySQL = _SubeInformacionALocal( documentoRecivido + "_S.xml", fecha, a.getRutaFinalXML() , d.getDatos(), documento.ARCHIVO_XML ); 
		
		if ( resultadoCargaMySQL ){
			MuestraInformacion( "Se cargo el archivo: " + documentoRecivido + "_S.xml", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_BLOB_LOCAL, estado.CORRECTO, documento.ARCHIVO_XML, "Se cargo el documento", documentoRecivido );
		} else {
			MuestraInformacion( "No se cargo el archivo: " + documentoRecivido + "_S.xml", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_BLOB_LOCAL, estado.INCORRECTO, documento.ARCHIVO_XML, "No se cargo", documentoRecivido );
		}
		
		// Despues continua con el pdf
		
		Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_BLOB_LOCAL, estado.INICIO, documento.ARCHIVO_PDF, "Inicio de proceso", documentoRecivido );
		
		resultadoCargaMySQL = _SubeInformacionALocal( documentoRecivido + ".pdf", fecha, a.getRutaFinalPDF() , d.getDatos(), documento.ARCHIVO_PDF ); 
		
		if ( resultadoCargaMySQL ){
			MuestraInformacion( "Se cargo el archivo: " + documentoRecivido + ".pdf", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_BLOB_LOCAL, estado.CORRECTO, documento.ARCHIVO_PDF, "Se cargo", documentoRecivido );
		} else {
			MuestraInformacion( "No se cargo el archivo: " + documentoRecivido + ".pdf", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_BLOB_LOCAL, estado.INCORRECTO, documento.ARCHIVO_PDF, "No se cargo", documentoRecivido );
		}
		
		MuestraInformacion( "------ TERMINANDO SUBIDA A LOCAL.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		//endregion
		
		//region CARGA ARCHIVOS A MYSQL REMOTO
		
		MuestraInformacion( "------ INICIANDO SUBIDA A REMOTO.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_BLOB_REMOTO, estado.CORRECTO, documento.ARCHIVO_XML, "Inicio de proceso", documentoRecivido );
		
		// 140115 VALIDA QUE EL DOCUMENTO NO ESTE GUARDADO
		//TODO VALIDAR QUE EL DOCUMENTO NO EXISTA EN LA BASE DE DATOS
		
		resultadoCargaMySQL = _SubeInformacionARemoto( documentoRecivido + "_S.xml", fecha, a.getRutaFinalXML() , d.getDatos(), documento.ARCHIVO_XML ); 
		
		if ( resultadoCargaMySQL ){
			MuestraInformacion( "Se cargo el archivo: " + documentoRecivido + "_S.xml", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_BLOB_REMOTO, estado.CORRECTO, documento.ARCHIVO_XML, "Se cargo", documentoRecivido );
		} else {
			MuestraInformacion( "No se cargo el archivo: " + documentoRecivido + "_S.xml", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_BLOB_REMOTO, estado.INCORRECTO, documento.ARCHIVO_XML, "No se cargo", documentoRecivido );
		}
		
		// Despues continua con el pdf
		
		Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_BLOB_REMOTO, estado.CORRECTO, documento.ARCHIVO_PDF, "Inicio de proceso", documentoRecivido );
		
		// 140115 VALIDA QUE EL DOCUMENTO NO ESTE GUARDADO
		//TODO VALIDAR QUE EL DOCUMENTO NO EXISTA EN LA BASE DE DATOS
		
		resultadoCargaMySQL = _SubeInformacionARemoto( documentoRecivido + ".pdf", fecha, a.getRutaFinalPDF() , d.getDatos(), documento.ARCHIVO_PDF ); 
		
		if ( resultadoCargaMySQL ){
			MuestraInformacion( "Se cargo el archivo: " + documentoRecivido + ".pdf", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_BLOB_REMOTO, estado.CORRECTO, documento.ARCHIVO_PDF, "Se cargo", documentoRecivido );
		} else {
			MuestraInformacion( "No se cargo el archivo: " + documentoRecivido + ".pdf", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_BLOB_REMOTO, estado.INCORRECTO, documento.ARCHIVO_PDF, "No se cargo", documentoRecivido );
		}
		
		MuestraInformacion( "------ TERMINANDO SUBIDA A REMOTO.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		//endregion
		
		//region MANDA CORREO
		
		MuestraInformacion( "------ INICIANDO ENVIO DE CORREO.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		// 140115 VALIDA QUE NO SE HAYA ENVIADO EL CORREO PARA NO CAER EN VARIOS ENVIOS DEL MISMO DOCUMENTO.
		//TODO VALIDAR ENVIO DE CORREOS
		
		resultadoCargaMySQL = _EnviaCorreoElectronico( d.getDatos(), a.getRutaFinalXML(), a.getRutaFinalPDF() );
		
		Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_MAIL, estado.CORRECTO, documento.NO_APLICA, "Inicio de proceso", documentoRecivido );
		
		if ( resultadoCargaMySQL ){
			MuestraInformacion( "Se envio el correo: ", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_MAIL, estado.CORRECTO, documento.NO_APLICA, "No se envio", documentoRecivido );
			// 140115 SI EL CORREO ES MANDADO EXITOSAMENTE, SE PROCEDE A MARCAR EN LA BASE PARA INDICAR QUE NO SE VUELVA A ENVIAR
			//TODO CONFIRMAR EL ENVIO DEL CORREO
			
		} else {
			MuestraInformacion( "No se envio el correo: ", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_MAIL, estado.INCORRECTO, documento.NO_APLICA, "Se envio", documentoRecivido );
		}
		
		MuestraInformacion( "------ TERMINANDO ENVIO DE CORREO.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		//endregion
	
		//region LIMPIA LA INFORMACION
		
		File b = null;
		
		b = new File( a.getRutaFinalPDF() );
		if ( b.exists() ) {
			b.delete();
		}
		b = new File( a.getRutaFinalXML() );
		if ( b.exists() ) {
			b.delete();
		}
		
		
		//endregion
		
		Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_FACTURACION, estado.CORRECTO, documento.NO_APLICA, "Fin de proceso", documentoRecivido );
	
	}
	
	private void _FacturacionSubeInformacionALocal( String documentoRecivido )
	{
		
		Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_FACTURACION, estado.INICIO, documento.NO_APLICA, "Inicio de proceso", documentoRecivido );
		
		String Cliente = "Facturacion Completa";
		
		//region RECUPERA INFORMACION DEL DOCUMENTO RECIBIDO
		// El listado de informacion a recuperar es: 
		// cliente, documento, serie, sucursal, movimiento, path
		// Una vez recuperada esta información procede a armar el documento, mismo que se cargara en la base de datos.
		
		ArrayList<Object> datos = _PreparaInformacion( documentoRecivido, movimiento.INICIO_FACTURACION, Cliente );	
		
		// El primer objeto es un documento, el segundo es un archivo y el tercero una cadena.
		
		Documento 		d 		= ( Documento )			datos.get( 0 );
		ArchivosBarmex	a 		= ( ArchivosBarmex ) 	datos.get( 1 );
		String 			fecha 	= ( String )			datos.get( 2 );
		
		//endregion
		
		//region CARGA ARCHIVOS A MYSQL LOCAL
		
		MuestraInformacion( "------ INICIANDO SUBIDA A LOCAL.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_BLOB_LOCAL, estado.INICIO, documento.ARCHIVO_XML, "Inicio de proceso", documentoRecivido );
		
		boolean resultadoCargaMySQL = false;
		
		resultadoCargaMySQL = _SubeInformacionALocal( documentoRecivido + "_S.xml", fecha, a.getRutaFinalXML() , d.getDatos(), documento.ARCHIVO_XML ); 
		
		if ( resultadoCargaMySQL ){
			MuestraInformacion( "Se cargo el archivo: " + documentoRecivido + "_S.xml", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_BLOB_LOCAL, estado.CORRECTO, documento.ARCHIVO_XML, "Se cargo el documento", documentoRecivido );
		} else {
			MuestraInformacion( "No se cargo el archivo: " + documentoRecivido + "_S.xml", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_BLOB_LOCAL, estado.INCORRECTO, documento.ARCHIVO_XML, "No se cargo", documentoRecivido );
		}
		
		// Despues continua con el pdf
		
		Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_BLOB_LOCAL, estado.INICIO, documento.ARCHIVO_PDF, "Inicio de proceso", documentoRecivido );
		
		resultadoCargaMySQL = _SubeInformacionALocal( documentoRecivido + ".pdf", fecha, a.getRutaFinalPDF() , d.getDatos(), documento.ARCHIVO_PDF ); 
		
		if ( resultadoCargaMySQL ){
			MuestraInformacion( "Se cargo el archivo: " + documentoRecivido + ".pdf", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_BLOB_LOCAL, estado.CORRECTO, documento.ARCHIVO_PDF, "Se cargo", documentoRecivido );
		} else {
			MuestraInformacion( "No se cargo el archivo: " + documentoRecivido + ".pdf", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_BLOB_LOCAL, estado.INCORRECTO, documento.ARCHIVO_PDF, "No se cargo", documentoRecivido );
		}
		
		MuestraInformacion( "------ TERMINANDO SUBIDA A LOCAL.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		//endregion
		
		Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_FACTURACION, estado.CORRECTO, documento.NO_APLICA, "Fin de proceso", documentoRecivido );
	
	}
	
	private void _FacturacionSubeInformacionARemoto( String documentoRecivido )
	{
		
		Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_FACTURACION, estado.INICIO, documento.NO_APLICA, "Inicio de proceso", documentoRecivido );
		
		String Cliente = "Facturacion Completa";
		
		//region RECUPERA INFORMACION DEL DOCUMENTO RECIBIDO
		// El listado de informacion a recuperar es: 
		// cliente, documento, serie, sucursal, movimiento, path
		// Una vez recuperada esta información procede a armar el documento, mismo que se cargara en la base de datos.
		
		ArrayList<Object> datos = _PreparaInformacion( documentoRecivido, movimiento.INICIO_FACTURACION, Cliente );	
		
		// El primer objeto es un documento, el segundo es un archivo y el tercero una cadena.
		
		Documento 		d 		= ( Documento )			datos.get( 0 );
		ArchivosBarmex	a 		= ( ArchivosBarmex ) 	datos.get( 1 );
		String 			fecha 	= ( String )			datos.get( 2 );
		
		//endregion
		
		//region CARGA ARCHIVOS A MYSQL REMOTO
		
		boolean resultadoCargaMySQL = false;
		
		MuestraInformacion( "------ INICIANDO SUBIDA A REMOTO.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_BLOB_REMOTO, estado.CORRECTO, documento.ARCHIVO_XML, "Inicio de proceso", documentoRecivido );
		
		resultadoCargaMySQL = _SubeInformacionARemoto( documentoRecivido + "_S.xml", fecha, a.getRutaFinalXML() , d.getDatos(), documento.ARCHIVO_XML ); 
		
		if ( resultadoCargaMySQL ){
			MuestraInformacion( "Se cargo el archivo: " + documentoRecivido + "_S.xml", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_BLOB_REMOTO, estado.CORRECTO, documento.ARCHIVO_XML, "Se cargo", documentoRecivido );
		} else {
			
			if ( resultadoCarga == estadoCarga.YA_EXISTE ) {
				MuestraInformacion( "No se cargo el archivo: " + documentoRecivido + "_S.xml, YA EXISTE", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			} else {
				MuestraInformacion( "No se cargo el archivo: " + documentoRecivido + "_S.xml", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_BLOB_REMOTO, estado.INCORRECTO, documento.ARCHIVO_XML, "No se cargo", documentoRecivido );
			}
		}
		
		// Despues continua con el pdf
		
		Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_BLOB_REMOTO, estado.CORRECTO, documento.ARCHIVO_PDF, "Inicio de proceso", documentoRecivido );
		
		resultadoCargaMySQL = _SubeInformacionARemoto( documentoRecivido + ".pdf", fecha, a.getRutaFinalPDF() , d.getDatos(), documento.ARCHIVO_PDF ); 
		
		if ( resultadoCargaMySQL ){
			MuestraInformacion( "Se cargo el archivo: " + documentoRecivido + ".pdf", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_BLOB_REMOTO, estado.CORRECTO, documento.ARCHIVO_PDF, "Se cargo", documentoRecivido );
		} else {
			
			if ( resultadoCarga == estadoCarga.YA_EXISTE ) {
				MuestraInformacion( "No se cargo el archivo: " + documentoRecivido + ".pdf, YA EXISTE", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
			} else {
				MuestraInformacion( "No se cargo el archivo: " + documentoRecivido + ".pdf", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
				Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_BLOB_REMOTO, estado.INCORRECTO, documento.ARCHIVO_XML, "No se cargo", documentoRecivido );
			}
			
		}
		
		MuestraInformacion( "------ TERMINANDO SUBIDA A REMOTO.", Cliente, nivel.INFO, dondeSeMuestra.AMBOS );
		
		//endregion

		//region LIMPIA LA INFORMACION
		
		File b = null;
		
		b = new File( a.getRutaFinalPDF() );
		if ( b.exists() ) {
			b.delete();
		}
		b = new File( a.getRutaFinalXML() );
		if ( b.exists() ) {
			b.delete();
		}
		
		
		//endregion
		
		Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_FACTURACION, estado.CORRECTO, documento.NO_APLICA, "Fin de proceso", documentoRecivido );
	
	}
	
	private void _CancelacionProcesoCompleto ( String documentoRecivido )
	{
		Utilidades.GuardaEstado( documentoRecivido, movimiento.INICIO_CANCELACION, estado.INICIO, documento.ARCHIVO_PDF, "Inicio de cancelacion", documentoRecivido );
		// Descarga el documento (PDF), a la carpeta local ( Descarga desde el sitio local )
		
		String documentoACancelar 	= documentoRecivido + ".pdf";
		String rutaParaMarcar		= ServerTCP.folderTemp ;
		String rutaConArchivo 		= rutaParaMarcar + "\\" + documentoACancelar;
		
		if ( !_DescargaDocumentoDeLocal( documentoRecivido, rutaConArchivo )){
			Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_CANCELACION, estado.INCORRECTO, documento.ARCHIVO_PDF, "No se encontro el archivo", documentoRecivido );
			return;
		}
		
		// Marca el documento
		
		StampPDF s = new StampPDF( documentoACancelar, rutaParaMarcar );
		
		s.setMarca( "CANCELADO" );
		
		if ( !s.MarcaArchivo( ) ){
			Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_CANCELACION, estado.INCORRECTO, documento.ARCHIVO_PDF, "No se pudo marcar el archivo", documentoRecivido );
			return;
		}
		
		// Actualiza en local

		if ( !_ActualizaInformacionEnLocal( documentoACancelar, rutaConArchivo )){
			Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_CANCELACION, estado.INCORRECTO, documento.ARCHIVO_PDF, "No se pudo subir a local", documentoRecivido );
		}
		
		// Actualiza en remoto
		
		if ( !_ActualizaInformacionEnRemoto( documentoACancelar, rutaConArchivo ) ) {
			Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_CANCELACION, estado.INCORRECTO, documento.ARCHIVO_PDF, "No se pudo subir a Remoto", documentoRecivido );
		}
		
		// Envia el correo electronico
		
		Documento d = new Documento();
		
		d.RecuperaCampos( documentoRecivido, "movimiento:mov_serie,path:fld_sucursal,suc:suc_sucursal" );
		
		
		if (!_EnviaCorreoElectronicoPDF( d.getDatos( ), rutaConArchivo )) {
			Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_CANCELACION, estado.INCORRECTO, documento.ARCHIVO_PDF, "No se pudo enviar el correo", documentoRecivido );
		}
	
		// Limpia la informacion
		File c = new File( rutaConArchivo );
		
		if ( c.exists( ) ) c.delete( );
		
		Utilidades.GuardaEstado( documentoRecivido, movimiento.FIN_CANCELACION, estado.CORRECTO, documento.ARCHIVO_PDF, "Fin de cancelacion", documentoRecivido );
	}
	
	private boolean _EnviaCorreoElectronicoPDF (HashMap<String, String> datosDocumento, String RutaPDF )
	{
		boolean 		resultadoEnviaCorreoElectronico = false;
		String			Cliente							= "Envio de correo.";
		
		// Recupera los correos del cliente ERP ...
		
		ConsultaODBC 	consultaDatos = new ConsultaODBC( Conecciones.getERPCon() );
		String 			Sentencia = "SELECT XC_DATOS3, XC_DATOS4 FROM MDGCLI11 WHERE XC_NUMCLI = '" + datosDocumento.get( "cliente" ) + "'";
		
		HashMap<String, ArrayList<String>> registrosRecuperados = consultaDatos.ConsultaVariasColumnasODBC( Sentencia );
		
		if ( registrosRecuperados == null ){
			MuestraInformacion( "El cliente no tiene un correo asignado", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
		}
		
		if ( registrosRecuperados.size() == 0 ){
			MuestraInformacion( "El cliente no tiene un correo asignado", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
		}
		
		String from = Utilidades.RecuperaRecurso( "mail_" + datosDocumento.get( "sucursal" ).toLowerCase() );
		
		String to = "";
		
		if ( registrosRecuperados.get("1").get( 0 ) != null ){
			to = registrosRecuperados.get("1").get( 0 );
		}
		if (registrosRecuperados.get("1").get( 1 ) != null ) {
			to = to + registrosRecuperados.get("1").get( 1 );
		} 
		
		// Recupera los correos extra del cliente BARPRO ...
		
		consultaDatos = null;
		
		consultaDatos = new ConsultaODBC( Conecciones.getMySQLLocalBarpro( )  );
		
		
		Sentencia = " 	SELECT 	IFNULL(EM_CFDCC, ''), " +
				"				IFNULL(EM_CFDBCC, '') " +
				"		FROM	BARCLI01" +
				"		WHERE	id_cliente = '" + datosDocumento.get("cliente") + "'" ;
		
		registrosRecuperados.clear();
		
		registrosRecuperados = consultaDatos.ConsultaVariasColumnasMySQL( Sentencia );
		
		String CC 	= registrosRecuperados.get( "1" ).get( 1 );
		String BCC 	= registrosRecuperados.get( "1" ).get( 1 );
	
		
		Correo c = new Correo(from, to);
		if ( CC.length( ) > 0  ){
			c.setCC( CC );
		}
		if ( BCC.length( ) > 0 ){
			c.setBCC( BCC );
		}
		
		c.setSMTPHost( Utilidades.RecuperaRecurso( "smt_host" ) );
		c.setSMPTPort( Utilidades.RecuperaRecurso( "smt_port" ) );
		
		ArrayList<String> attachments = new ArrayList<String>();
		
		attachments.add( RutaPDF );
		
		c.setAttach( attachments );
		
		resultadoEnviaCorreoElectronico = c.enviaCorreo( "Envio de archivos adjuntos, Cancelacion de Factura Electronica: " + datosDocumento.get( "documento" ), "Envio de archivos que soportan la Facturación Electronica: " +datosDocumento.get( "documento" )  );

		if ( !resultadoEnviaCorreoElectronico ){
			MuestraInformacion( c.getError( ), Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
		}
		
		return  resultadoEnviaCorreoElectronico;
	}
	
	private boolean _EnviaCorreoElectronico( HashMap<String, String> datosDocumento, String RutaXML, String RutaPDF )
	{
		boolean 		resultadoEnviaCorreoElectronico = false;
		String			Cliente							= "Envio de correo.";
		
		// Recupera los correos del cliente ERP ...
		
		ConsultaODBC 	consultaDatos = new ConsultaODBC( Conecciones.getERPCon() );
		String 			Sentencia = "SELECT XC_DATOS3, XC_DATOS4 FROM MDGCLI11 WHERE XC_NUMCLI = '" + datosDocumento.get( "cliente" ) + "'";
		
		HashMap<String, ArrayList<String>> registrosRecuperados = consultaDatos.ConsultaVariasColumnasODBC( Sentencia );
		
		if ( registrosRecuperados == null ){
			MuestraInformacion( "El cliente no tiene un correo asignado", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
		}
		
		if ( registrosRecuperados.size() == 0 ){
			MuestraInformacion( "El cliente no tiene un correo asignado", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
		}
		
		String from = Utilidades.RecuperaRecurso( "mail_" + datosDocumento.get( "sucursal" ).toLowerCase() );
		
		String to = "";
		
		if ( registrosRecuperados.get("1").get( 0 ) != null ){
			to = registrosRecuperados.get("1").get( 0 );
		}
		if (registrosRecuperados.get("1").get( 1 ) != null ) {
			to = to + registrosRecuperados.get("1").get( 1 );
		} 
		
		// Recupera los correos extra del cliente BARPRO ...
		
		consultaDatos = null;
		
		consultaDatos = new ConsultaODBC( Conecciones.getMySQLLocalBarpro( )  );
		
		
		Sentencia = " 	SELECT 	IFNULL(EM_CFDCC, ''), " +
				"				IFNULL(EM_CFDBCC, '') " +
				"		FROM	BARCLI01" +
				"		WHERE	id_cliente = '" + datosDocumento.get("cliente") + "'" ;
		
		registrosRecuperados.clear();
		
		registrosRecuperados = consultaDatos.ConsultaVariasColumnasMySQL( Sentencia );
		
		String CC 	= "";
		String BCC 	= "";
		
		if ( !registrosRecuperados.isEmpty( ) ){
			if ( registrosRecuperados.get( "1" ).get( 0 ) != null ){
				CC 	= registrosRecuperados.get( "1" ).get( 0 );
			}
			
			if ( registrosRecuperados.get( "1" ).get( 1 ) != null ){
				BCC = registrosRecuperados.get( "1" ).get( 1 );
			}
		}
	
		
		Correo c = new Correo(from, to);
		if ( CC.length( ) > 0  ){
			c.setCC( CC );
		}
		if ( BCC.length( ) > 0 ){
			c.setBCC( BCC );
		}
		
		c.setSMTPHost( Utilidades.RecuperaRecurso( "smt_host" ) );
		c.setSMPTPort( Utilidades.RecuperaRecurso( "smt_port" ) );
		
		ArrayList<String> attachments = new ArrayList<String>();
		
		attachments.add( RutaXML );
		attachments.add( RutaPDF );
		
		c.setAttach( attachments );
		
		resultadoEnviaCorreoElectronico = c.enviaCorreo( "Envio de archivos adjuntos, Factura Electronica: " + datosDocumento.get( "documento" ), "Envio de archivos que soportan la Facturación Electronica: " +datosDocumento.get( "documento" )  );

		if ( !resultadoEnviaCorreoElectronico ){
			MuestraInformacion( c.getError( ), Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
		}
		
		return  resultadoEnviaCorreoElectronico;
	}
	
	private boolean _SubeInformacionARemoto( String documento, String fecha, String RutaDocumento, HashMap<String, String> datos, documento d )
	{
		boolean resultado = false;
		String	Cliente		= "MySQL Local";
		
		// Tipo de documento
		
		String tipoDeArchivo = "";
		
		switch ( d ) {
			case ARCHIVO_PDF:
				tipoDeArchivo = "application/pdf";
				break;
			case ARCHIVO_XML:
				tipoDeArchivo = "application/octect-stream";
				break;
			default:
				break;
		}
		
		
		try {
			String validaInsercionSQL 	= 	"	SELECT 		* " +
					"	FROM facturas " +
					"	WHERE nombre = '" + documento + "'";

			String insercionSQL			= 	"	INSERT " +
								"	INTO 		facturas 	(	" +
								"								nombre, sl_numcli, sl_numdoc, " +
								"								serie, sl_tipmov, sl_refe, " +
								"								fecha,contenido,tipo," +
								"								enviado" +
								"							)" +
								"	VALUES					('" + documento + "','" + datos.get( "cliente" ) + "','" + datos.get( "suc" ) + datos.get( "documento" ) + "10', " +
								"							'" + datos.get( "serie" ).trim() + datos.get( "sucursal" ) + "','" +  datos.get( "movimiento" ) + "','" + datos.get( "documento" ) + "'," +
								"							'" + fecha + "',?, '" + tipoDeArchivo + "','0')"; 
			
			Blob b = new Blob(Utilidades.RecuperaRecurso( "bdr_host" ), Utilidades.RecuperaRecurso( "bdr_puerto" ), Utilidades.RecuperaRecurso( "bdr_base" ), "" );
			b.setUsuario( Utilidades.RecuperaRecurso( "bdr_user" ) );
			b.setPasswod( Utilidades.RecuperaRecurso( "bdr_password" ));
			
			b.setArchivos( RutaDocumento );
			b.setSentencia( validaInsercionSQL );
			
			if ( !b.validaSentencia() ){
				b.setSentencia( insercionSQL );
				resultado  = b.cargaBlob();
				
				if ( resultado ) {
					resultadoCarga = estadoCarga.CARGADO;
				} else {
					resultadoCarga = estadoCarga.NO_CARGADO;
				}
				
			}  else {
				resultadoCarga = estadoCarga.YA_EXISTE;
			}
			
			
		} catch ( Exception e ) {
			error.clear();
			error.add( "No se pudo cargar la informacion al server remoto");
			error.add( e.toString() );
			MuestraInformacion( error, Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
			resultado = false;
			resultadoCarga = estadoCarga.NO_CARGADO;
		} finally {
			File rd = new File ( RutaDocumento );
			if ( rd.exists ( ) ) {
				rd.delete ( );
			}
		}
		
		
		return resultado;
	}
	
	private boolean _SubeInformacionALocal( String documento, String fecha, String RutaDocumento, HashMap<String, String> datos, documento d )
	{
		boolean resultado = false;
		String	Cliente		= "MySQL Local";
		
		String tipoDeArchivo = "";
		
		switch ( d ) {
			case ARCHIVO_PDF:
				tipoDeArchivo = "application/pdf";
				break;
			case ARCHIVO_XML:
				tipoDeArchivo = "application/octect-stream";
				break;
			default:
				break;
		}
		
		try {
			String validaInsercionSQL 	= 	"	SELECT 		* " +
					"	FROM facturas " +
					"	WHERE nombre = '" + documento + "'";

			String insercionSQL			= 	"	INSERT " +
								"	INTO 		facturas 	(	" +
								"								nombre, sl_numcli, sl_numdoc, " +
								"								serie, sl_tipmov, sl_refe, " +
								"								fecha,contenido,tipo," +
								"								enviado" +
								"							)" +
								"	VALUES					('" + documento + "','" + datos.get( "cliente" ) + "','" + datos.get( "suc" ) + datos.get( "documento" ) + "10', " +
								"							'" + datos.get( "serie" ).trim() + datos.get( "sucursal" ) + "','" +  datos.get( "movimiento" ) + "','" + datos.get( "documento" ) + "'," +
								"							'" + fecha + "',?, '" + tipoDeArchivo + "', '0')"; 
			
			Blob b = new Blob(Utilidades.RecuperaRecurso( "bdlf_host" ), Utilidades.RecuperaRecurso( "bdlf_puerto" ), Utilidades.RecuperaRecurso( "bdlf_base" ), "" );
			b.setUsuario( Utilidades.RecuperaRecurso( "bdlf_user" ) );
			b.setPasswod( Utilidades.RecuperaRecurso( "bdlf_password" ));
			
			b.setArchivos( RutaDocumento );
			b.setSentencia( validaInsercionSQL );
			
			if ( !b.validaSentencia() ){
				b.setSentencia( insercionSQL );
				resultado  = b.cargaBlob();
			}
			
			
		} catch ( Exception e ) {
			error.clear();
			error.add( "No se pudo cargar la informacion al server local");
			error.add( e.toString() );
			MuestraInformacion( error, Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
			resultado = false;
		}
		
		
		return resultado;
	}
	
	private boolean _ActualizaInformacionEnLocal ( String documento, String RutaDocumento )
	{
		boolean resultado = false;
		String	Cliente		= "MySQL Local";
		
		try {
			String validaInsercionSQL 	= 	"	SELECT 		* " +
											"	FROM facturas " +
											"	WHERE nombre = '" + documento + "'";

			String updateSQL			= 	"	UPDATE 		facturas " +
											"	SET 		contenido = ? " +
											"	WHERE 		nombre = '" + documento + "' "; 
			
			Blob b = new Blob( Conecciones.getMySQLLocalFacturas( ) );
			
			b.setArchivos( RutaDocumento );
			b.setSentencia( validaInsercionSQL );
			
			if ( b.validaSentencia() ){
				b.setSentencia( updateSQL );
				resultado  = b.cargaBlob( );
			}
			
		} catch ( Exception e ) {
			error.clear();
			error.add( "No se pudo cargar la informacion al server local");
			error.add( e.toString() );
			MuestraInformacion( error, Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
			resultado = false;
		}
		
		return resultado;
	}
	
	private boolean _ActualizaInformacionEnRemoto ( String documento, String RutaDocumento )
	{
		boolean resultado = false;
		String	Cliente		= "MySQL Local";
		
		try {
			String validaInsercionSQL 	= 	"	SELECT 		* " +
											"	FROM facturas " +
											"	WHERE nombre = '" + documento + "'";

			String updateSQL			= 	"	UPDATE 		facturas " +
											"	SET 		contenido = ? " +
											"	WHERE 		nombre = '" + documento + "' "; 
			
			Blob b = new Blob( Conecciones.getMySQLRemoto( ) );
			
			b.setArchivos( RutaDocumento );
			b.setSentencia( validaInsercionSQL );
			
			if ( b.validaSentencia() ){
				b.setSentencia( updateSQL );
				resultado  = b.cargaBlob( );
			}
			
		} catch ( Exception e ) {
			error.clear();
			error.add( "No se pudo cargar la informacion al server local");
			error.add( e.toString() );
			MuestraInformacion( error, Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
			resultado = false;
		}
		
		return resultado;
	}
	
	private boolean _DescargaDocumentoDeLocal( String Documento, String rutaDescarga )
	{
		boolean resultado = false;
		
		Blob b = new Blob( Conecciones.getMySQLLocalFacturas( ) );
		
		String sentecia = 	"SELECT contenido " +
							"FROM	facturas " +
							"WHERE  nombre = '" + Documento + ".pdf'" ;
		
		b.setSentencia( sentecia );
		
		resultado = b.DescargaArchivo( rutaDescarga );
		
		
		return resultado;
	}
	
	@SuppressWarnings("unused")
	private boolean _DescargaDocumentoDeRemoto( String Documento, String rutaDescarga )
	{
		boolean resultado = false;
		
		Blob b = new Blob( Conecciones.getMySQLRemoto( ) );
		
		String sentecia = 	"SELECT contenido " +
							"FROM	facturas " +
							"WHERE  nombre = '" + Documento + ".pdf" ;
		
		b.setSentencia( sentecia );
		
		resultado = b.DescargaArchivo( rutaDescarga );
		
		return resultado;
	}
	
	private ArrayList<Object> _PreparaInformacion( String documentoRecivido, movimiento m, String Cliente )
	{
		ArrayList<Object> resultado = new ArrayList<Object>( );
		
		//region OPCIONES COMUNES El primero es documento, el siguiente es archvio + opciones especiales
		Documento d = new Documento();
		
		d.RecuperaCampos( documentoRecivido, "movimiento:mov_serie,path:fld_sucursal,suc:suc_sucursal" );
		
		// Guarda el primer objeto
		
		resultado.add( d );
		
		// Se procede a copiar el archivo a local.
		
		ArchivosBarmex a = new ArchivosBarmex( documentoRecivido );
		
		a.validaExistencia( d.getDatos().get( "path" ) );
		
		boolean parar		= false;
		
		boolean existeXml 	= a.getExisteXML ( );
		boolean existePdf 	= a.getExistePDF ( );
		
		boolean archivoXmlCopiado = false;
		boolean archivoPdfCopiado = false;
		
		if ( !existeXml ) {
			MuestraInformacion ( "No se encontro el archivo xml", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
			parar = true;
		} else {
			archivoXmlCopiado = a.CopiaXML (  ServerTCP.folderTemp );
			if ( !archivoXmlCopiado ) {
				MuestraInformacion ( "No se pudo copiar el archivo a TEMP", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
				parar = true;
			}
		}
		
		if ( parar ){
			return null;
		}
		
		if ( !existePdf ){
			MuestraInformacion ( "No se encontro el archivo pdf", Cliente, nivel.ERROR, dondeSeMuestra.AMBOS );
		} else {
			archivoPdfCopiado = a.CopiaPDF ( ServerTCP.folderTemp );
			if ( !archivoPdfCopiado ){
				MuestraInformacion ( "No se pudo copiar el archivo pdf a TEMP", Cliente, nivel.ALARMA, dondeSeMuestra.AMBOS );
			}
		}
		
		resultado.add ( a );
		
		//endregion
		
		//region OPCIONES ESPECIALES, PARA PROCESOS
	
		// Procede a abrir el archivo xml, para recuperar la informacion de fecha.
		
		switch( m ) {
			case INICIO_FACTURACION:
				LectorXML l = new LectorXML( a.getRutaFinalXML() );
				String fecha = l.RecuperaAtributoDeRaiz( "fecha" ).substring( 0, 10 );
				resultado.add( fecha );
				break;
			default:
				resultado.add( null );
				break;
		}
	
		//endregion
		
		
		return resultado;
	}
}
