package com.bmx.srv;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Test;

public class ConeccionesTest 
{

	@Test
	public void testLocalBarpro(){
		
		assertNotNull( "Se esperaba que no fuera nulo", Conecciones.getMySQLLocalBarpro() );
		
	}
	
	@Test
	public void testLocalFacturas(){
		
		assertNotNull( "Se esperaba que no fuera nulo", Conecciones.getMySQLLocalFacturas() );
		
	}

	@Test
	public void testLocalERP(){
		
		assertNotNull( "Se esperaba que no fuera nulo", Conecciones.getMySQLRemoto() );
		
	}
	
	@Test
	public void testCierraConeccion() throws SQLException{
		
		Connection c = Conecciones.getERPCon();
		
		assertNotNull( "Se esperaba que no fuera nulo", c );
		
		Conecciones.Close( c );
		
		assertTrue( "Se esperaba que fuera nulo", c.isClosed() );
		
	}
	
	
}
