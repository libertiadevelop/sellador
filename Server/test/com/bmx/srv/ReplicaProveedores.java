package com.bmx.srv;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;


import org.junit.Test;
import com.bmx.srv.ConsultaODBC;
import com.bmx.srv.Utilidades.tipoDeDato;

public class ReplicaProveedores
{

	HashMap <String, ArrayList <String>> resultado = null;
	
	@Test
	public void validaComparacionOrigenVsCopiaRegistrosNuevosUnaColumnaLlave ( ) {
		ArrayList<String> llaves = new ArrayList <String> ( );
		llaves.add ( "1" );
		
		HashMap <String, ArrayList<String>> origen = generaOrigen ( );
		
		resultado = ConsultaODBC.comparaOrigenVsCopia ( origen , generaCopiaVacia ( ) , llaves );
		
		assertEquals ( "Se esperaba que el tama�o del resultado se igual que el origen", generaOrigen ( ).keySet ( ).size ( ), resultado.keySet ( ).size ( ) );

		for ( String registro : resultado.keySet ( ) ){
			ArrayList<String> valor = resultado.get ( registro );
			
			assertTrue ( "Se esperaba que el arreglo estuviera", origen.containsValue ( valor ) );
			
		}	
	}
	
	@Test
	public void validaComparacionOrigenVsCopiaAlgunosRegistrosUnaColumnaLlave ( ) {
		ArrayList<String> llaves = new ArrayList <String> ( );
		llaves.add ( "1" );
		
		resultado = ConsultaODBC.comparaOrigenVsCopia ( generaOrigen() , generaCopiaParcial ( ) , llaves );
		
		assertEquals ( "Se esperaba que el tama�o del resultado se igual que el origen", 2, resultado.keySet ( ).size ( ) );
	
		for ( String registro : resultado.keySet ( ) ){
			ArrayList<String> valor = resultado.get ( registro );
			
			assertTrue ( "Se esperaba que el arreglo estuviera", registrosFaltantes ( ).containsValue ( valor ) );
			
		}
	}	

	@Test
	public void validaComparacionOrigenVsCopiaRegistrosNuevosDosColumnasLlave ( ) {
		ArrayList<String> llaves = new ArrayList <String> ( );
		llaves.add ( "1" );
		llaves.add ( "3" );
		
		HashMap <String, ArrayList<String>> origen = generaOrigen_2 ( );
		
		resultado = ConsultaODBC.comparaOrigenVsCopia ( origen , generaCopiaVacia ( ) , llaves );
		
		assertEquals ( "Se esperaba que el tama�o del resultado se igual que el origen", origen.keySet ( ).size ( ), resultado.keySet ( ).size ( ) );

		for ( String registro : resultado.keySet ( ) ){
			ArrayList<String> valor = resultado.get ( registro );
			
			assertTrue ( "Se esperaba que el arreglo estuviera", origen.containsValue ( valor ) );
			
		}
	}
	
	@Test
	public void validaComparacionOrigenVsCopiaAlgunosRegistrosDosColumnasLlave ( ) {
		ArrayList<String> llaves = new ArrayList <String> ( );
		llaves.add ( "1" );
		llaves.add ( "3" );
		
		resultado = ConsultaODBC.comparaOrigenVsCopia ( generaOrigen_2() , generaCopiaParcial_2( ) , llaves );
		
		assertEquals ( "Se esperaba que el tama�o del resultado se igual que el origen", 1, resultado.keySet ( ).size ( ) );
	
		for ( String registro : resultado.keySet ( ) ){
			ArrayList<String> valor = resultado.get ( registro );
			
			assertTrue ( "Se esperaba que el arreglo estuviera", registrosFaltantes_2 ( ).containsValue ( valor ) );
			
		}
	}
	
	@Test
	public void validaComparacionValoresCambiaron ( ){
		ArrayList<String> llaves = new ArrayList <String> ( );
		llaves.add ( "1" );
		llaves.add ( "2" );
		llaves.add ( "3" );
		llaves.add ( "4" );
		llaves.add ( "5" );
		
		resultado = ConsultaODBC.comparaOrigenVsCopia ( generaOrigen_3() , generaCopiaParcial_3( ) , llaves );
		
		assertEquals ( "Se esperaba que el tama�o del resultado se igual que el origen", 2, resultado.keySet ( ).size ( ) );
	
		for ( String registro : resultado.keySet ( ) ){
			ArrayList<String> valor = resultado.get ( registro );
			
			assertTrue ( "Se esperaba que el arreglo estuviera", registrosFaltantes_3 ( ).containsValue ( valor ) );
			
		}
	}
	
	@Test
	public void validaBuscaCambiosConLlave ( ) {
		
		ArrayList <String> llaves = new ArrayList <String> ( );
		
		llaves.add ( "1" );
		llaves.add ( "5" );
		
		resultado = ConsultaODBC.buscaModificacionesOrigenVsCopia ( generaOrigen_Modificaciones ( ), generaReplica_Modificaciones ( ), llaves );
		
		assertEquals ( "Se esperaban cuatro registros modificados", 4, resultado.size ( ) );
		
		for ( String llave : resultado.keySet ( ) ) {
			
			ArrayList <String> valoresEnResultado = resultado.get ( llave );
			
			assertTrue ( "Se esperaba que el registro, para la llave: " + llave + ", estuviera. \n" + valoresEnResultado.toString ( ), generaResultado_Modificaciones ( ).containsValue ( valoresEnResultado ) );
			
		}
		
		
	}
	
	@Test
	public void validaSincronizacionDeTablas ( ) throws ClassNotFoundException, SQLException
	{
		HashMap<String, ArrayList <String>> origen = new HashMap <String, ArrayList<String>> ( );
		HashMap<String, ArrayList <String>> replica = new HashMap <String, ArrayList<String>> ( );
		
		HashMap<String, ArrayList <String>> diferencia = new HashMap <String, ArrayList<String>> ( );
		
		ArrayList<String> llaves = new ArrayList <String> ( );
		
		limpiaOrigenBasesPruebaMySQLLocal ( );
		limpiaReplicaBasesPruebaMySQLLocal ( );
		
		cargaDatosEnOrigenPruebaMySQLLocal ( );
		
		ConsultaODBC c = new ConsultaODBC( getLocalCon ( ) );
		
		origen = c.ConsultaVariasColumnasMySQL ( "SELECT * FROM origen" );
		
		assertEquals ( "Se esperaban 10 registros", 10, origen.size ( ) );
		
		// Compara la tabla origen contra replica.
		replica = c.ConsultaVariasColumnasMySQL ( "SELECT * FROM replica" );
		
		assertEquals ( "Se esperaban 0 registros", 0, replica.size ( ) );
		
		llaves.add ( "1" );
		
		diferencia = ConsultaODBC.comparaOrigenVsCopia ( origen, replica, llaves );
		
		assertEquals ( "Se esperaban 10 registros", 10, diferencia.size ( ) );
	
		diferencia = ConsultaODBC.comparaOrigenVsCopia ( origen, diferencia, llaves );
		
		assertEquals ( "Se esperaba que diferencia = origen", 0, diferencia.size ( ) );
		
		// Carga los datos diferentes a la base de replica.
		
		diferencia = ConsultaODBC.comparaOrigenVsCopia ( origen, replica, llaves );
		
		String sentencia = "INSERT INTO replica (id, nombre, apellido, activo, correo) VALUES (?,?,?,?,?)";
		
		c.InsertaRegistroMysql ( sentencia, diferencia, getDatoDeColumna ( ), 50 );
		
		replica = c.ConsultaVariasColumnasMySQL ( "SELECT * FROM replica" );
		
		assertEquals ( "Se esperaban 10 regitros", 10, replica.size ( ) );
		
		diferencia = ConsultaODBC.comparaOrigenVsCopia ( origen, replica, llaves );
		
		assertEquals ( "No se esperaban diferencias", 0, diferencia.size ( ));
		
		// Generando modificaciones
		
		limpiaReplicaBasesPruebaMySQLLocal ( );
		
		modificaDatosEnReplicaPruebaMySQLLocal ( );
		
		replica = c.ConsultaVariasColumnasMySQL ( "SELECT * FROM replica" );
		
		assertEquals ( "Se esperaban 10 regitros", 10, replica.size ( ) );
		
		ArrayList <String> datosJessica = replica.get ( "2" );
		
		assertEquals ( "Se esperaba escarcega, pero salio: " + datosJessica.get ( 2 ) , "escarcega", datosJessica.get ( 2 ) );
		assertEquals ( "Se esperaba correo completo, pero salio: " + datosJessica.get ( 4 ) , "jeesica.escarcega@barmex.com.mx", datosJessica.get ( 4 ) );
		
		// Vuelve a comparar
		
		diferencia = ConsultaODBC.buscaModificacionesOrigenVsCopia ( origen, replica, llaves );
		
		assertEquals ( "Se esperaban 3 modificaciones", 3, diferencia.size ( ) );
		
		for ( String registros : diferencia.keySet ( ) ){
			ArrayList<String> arreglo = diferencia.get ( registros );
			
			assertTrue ( "Se esperaba que los valores existieran: " + arreglo.toString ( ), getRegistrosParaReplicaConModificaciones ( ).containsValue ( arreglo ) );
			
		}
		
		ArrayList<String> nombreDeCampo = new ArrayList <String> ( );
		
		nombreDeCampo.add ( "id" );
		nombreDeCampo.add ( "nombre" );
		nombreDeCampo.add ( "apellido" );
		nombreDeCampo.add ( "activo" );
		nombreDeCampo.add ( "correo" );
		
		int registrosModificados = 0;
		
		registrosModificados = c.updateRegistrosMySQL ( diferencia, nombreDeCampo, getDatoDeColumna ( ), 50, llaves, "replica" );
		
		assertEquals ( "Se esperaban 3 registros modificados", 3, registrosModificados );
		
		replica = c.ConsultaVariasColumnasMySQL ( "SELECT * FROM replica" );
		
		diferencia = ConsultaODBC.comparaOrigenVsCopia ( origen, replica, llaves );
		
		assertEquals ( "Se esperaba que fueran iguales", 0, diferencia.size ( ) );
		
		
		limpiaOrigenBasesPruebaMySQLLocal ( );
		limpiaReplicaBasesPruebaMySQLLocal ( );
		
	}
	
	@Test
	public void validaSyncronizacionBlob ( ) throws ClassNotFoundException, SQLException
	{
		String sent1 		= "SELECT * FROM origenblob";
		String sen2			= "SELECT * FROM replicablob";
		
		HashMap<String, ArrayList <String>> origen 	= new HashMap <String, ArrayList<String>> ( );
		HashMap<String, ArrayList <String>> replica = new HashMap <String, ArrayList<String>> ( );
		
		limpiaOrigenBlob ( );
		limpiaReplicaBlob ( );
		
		cargaBlobOrigen ( );
		
		ConsultaODBC c = new ConsultaODBC( getLocalCon ( ) );
		
		origen = c.ConsultaVariasColumnasMySQL ( sent1 );
		
		assertEquals ( "Se esperaban 50 registros", 50, origen.size ( ) );
		
		String insert = "INSERT INTO replicablob (folio, tipo, formato, nombre, contenido ) VALUES (?,?,?,?,?)";
		
		HashMap <Integer, tipoDeDato> datoDeColumna = new HashMap <Integer, Utilidades.tipoDeDato> ( );
		
		datoDeColumna.put ( 1, tipoDeDato.ENTERO );
		datoDeColumna.put ( 2, tipoDeDato.ENTERO );
		datoDeColumna.put ( 3, tipoDeDato.ENTERO );
		datoDeColumna.put ( 4, tipoDeDato.CADENA );
		datoDeColumna.put ( 5, tipoDeDato.BLOB );
		
		ArrayList <String> llaves = new ArrayList <String> ( );
		
		llaves.add ( "1" );
		llaves.add ( "2" );
		llaves.add ( "3" );
		llaves.add ( "4" );
	
		
		int registros = c.actualizaBlobs ( getLocalCon ( ), "origenblob", "replicablob", insert, datoDeColumna, 10, llaves );
		
		assertEquals ( "Se esperaban 50 registros insertados", 50, registros );
		
		replica = c.ConsultaVariasColumnasMySQL ( sen2 );
		
		assertEquals ( "Se esperaba que devolviera 50 registros", 50, replica.size ( ) );
		
		assertEquals ( "Se esperaba que los blobs fueran iguales", 0, ConsultaODBC.comparaCamposBlob ( getLocalCon(), getLocalCon(), "origenblob", "replicablob", datoDeColumna, llaves ).size ( ) );
		
		limpiaOrigenBlob ( );
		limpiaReplicaBlob ( );
	}
	
	// Siempre se agregan 4 columnas mas, despues de las llaves
	
	//region METODOS PRIVADOS
	
	private HashMap<String, ArrayList<String>> generaOrigen( )
	{
		HashMap<String, ArrayList<String>> resultado = new HashMap <String, ArrayList<String>> ( );
		ArrayList<String> campos = new ArrayList <String> ( );
		
		campos.add ( "1" );
		campos.add ( "2" );
		campos.add ( "3" );
		campos.add ( "4" );
		campos.add ( "5" );
		resultado.put ( "1", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "2" );
		campos.add ( "3" );
		campos.add ( "4" );
		campos.add ( "5" );
		campos.add ( "6" );
		resultado.put ( "2", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "3" );
		campos.add ( "4" );
		campos.add ( "5" );
		campos.add ( "6" );
		campos.add ( "7" );
		resultado.put ( "3", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "4" );
		campos.add ( "5" );
		campos.add ( "6" );
		campos.add ( "7" );
		campos.add ( "8" );
		resultado.put ( "4", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "5" );
		campos.add ( "6" );
		campos.add ( "7" );
		campos.add ( "8" );
		campos.add ( "9" );
		resultado.put ( "5", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		
		return resultado;
	}
	
	private HashMap<String, ArrayList<String>> generaCopiaVacia ( )
	{
		HashMap<String, ArrayList<String>> resultado = new HashMap <String, ArrayList<String>> ( );
		
		return resultado;
	}
	
	private HashMap<String, ArrayList<String>> generaCopiaParcial ( )
	{
		HashMap<String, ArrayList<String>> resultado = new HashMap <String, ArrayList<String>> ( );
		ArrayList<String> campos = new ArrayList <String> ( );
		
		campos.add ( "1" );
		campos.add ( "2" );
		campos.add ( "3" );
		campos.add ( "4" );
		campos.add ( "5" );
		resultado.put ( "1", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		
		campos.add ( "3" );
		campos.add ( "4" );
		campos.add ( "5" );
		campos.add ( "6" );
		campos.add ( "7" );
		resultado.put ( "3", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		
		campos.add ( "5" );
		campos.add ( "6" );
		campos.add ( "7" );
		campos.add ( "8" );
		campos.add ( "9" );
		resultado.put ( "5", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		return resultado;
	}
	
	private HashMap<String, ArrayList<String>> registrosFaltantes ( )
	{
		HashMap<String, ArrayList<String>> resultado = new HashMap <String, ArrayList<String>> ( );
		ArrayList<String> campos = new ArrayList <String> ( );
		campos.add ( "2" );
		campos.add ( "3" );
		campos.add ( "4" );
		campos.add ( "5" );
		campos.add ( "6" );
		resultado.put ( "2", new ArrayList <String> ( campos ) );
		campos.clear ( );

		
		campos.add ( "4" );
		campos.add ( "5" );
		campos.add ( "6" );
		campos.add ( "7" );
		campos.add ( "8" );
		resultado.put ( "4", new ArrayList <String> ( campos ) );
		campos.clear ( );
		return resultado;
	}

	private HashMap<String, ArrayList<String>> generaOrigen_2 ( )
	{
		HashMap<String, ArrayList<String>> resultado = new HashMap <String, ArrayList<String>> ( );
		ArrayList<String> campos = new ArrayList <String> ( );
		
		campos.add ( "1" );
		campos.add ( "2" );
		campos.add ( "1" );
		campos.add ( "4" );
		campos.add ( "5" );
		resultado.put ( "1", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "2" );
		campos.add ( "b" );
		campos.add ( "c" );
		resultado.put ( "2", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "3" );
		campos.add ( "b" );
		campos.add ( "c" );
		resultado.put ( "3", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "4" );
		campos.add ( "b" );
		campos.add ( "c" );
		resultado.put ( "4", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "5" );
		campos.add ( "b" );
		campos.add ( "c" );
		resultado.put ( "5", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		
		return resultado;
	}

	private HashMap<String, ArrayList<String>> generaCopiaParcial_2 ( )
	{
		HashMap<String, ArrayList<String>> resultado = new HashMap <String, ArrayList<String>> ( );
		ArrayList<String> campos = new ArrayList <String> ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "2" );
		campos.add ( "b" );
		campos.add ( "c" );
		resultado.put ( "2", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "3" );
		campos.add ( "b" );
		campos.add ( "c" );
		resultado.put ( "3", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "4" );
		campos.add ( "b" );
		campos.add ( "c" );
		resultado.put ( "4", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "5" );
		campos.add ( "b" );
		campos.add ( "c" );
		resultado.put ( "5", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		
		return resultado;
	}

	private HashMap<String, ArrayList<String>> registrosFaltantes_2 ( )
	{
		HashMap<String, ArrayList<String>> resultado = new HashMap <String, ArrayList<String>> ( );
		ArrayList<String> campos = new ArrayList <String> ( );
		
		campos.add ( "1" );
		campos.add ( "2" );
		campos.add ( "1" );
		campos.add ( "4" );
		campos.add ( "5" );
		resultado.put ( "1", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		
		return resultado;
	}

	private HashMap<String, ArrayList<String>> generaOrigen_3 ( )
	{
		HashMap<String, ArrayList<String>> resultado = new HashMap <String, ArrayList<String>> ( );
		ArrayList<String> campos = new ArrayList <String> ( );
		
		campos.add ( "1" );
		campos.add ( "2" );
		campos.add ( "1" );
		campos.add ( "4" );
		campos.add ( "5" );
		resultado.put ( "1", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "2" );
		campos.add ( "b" );
		campos.add ( "c" );
		resultado.put ( "2", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "3" );
		campos.add ( "b" );
		campos.add ( "c" );
		resultado.put ( "3", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "4" );
		campos.add ( "b" );
		campos.add ( "c" );
		resultado.put ( "4", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "5" );
		campos.add ( "b" );
		campos.add ( "c" );
		resultado.put ( "5", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		
		return resultado;
	}

	private HashMap<String, ArrayList<String>> generaCopiaParcial_3 ( )
	{
		HashMap<String, ArrayList<String>> resultado = new HashMap <String, ArrayList<String>> ( );
		ArrayList<String> campos = new ArrayList <String> ( );
		
		campos.add ( "1" );
		campos.add ( "2" );
		campos.add ( "1" );
		campos.add ( "4" );
		campos.add ( "5" );
		resultado.put ( "1", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "2" );
		campos.add ( "b" );
		campos.add ( "c" );
		resultado.put ( "2", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "3" );
		campos.add ( "b" );
		campos.add ( "c" );
		resultado.put ( "3", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "4" );
		campos.add ( "13" );
		campos.add ( "14" );
		resultado.put ( "4", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "5" );
		campos.add ( "12" );
		campos.add ( "11" );
		resultado.put ( "5", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		
		return resultado;
	}

	private HashMap<String, ArrayList<String>> registrosFaltantes_3 ( )
	{
		HashMap<String, ArrayList<String>> resultado = new HashMap <String, ArrayList<String>> ( );
		ArrayList<String> campos = new ArrayList <String> ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "4" );
		campos.add ( "b" );
		campos.add ( "c" );
		resultado.put ( "4", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "5" );
		campos.add ( "b" );
		campos.add ( "c" );
		resultado.put ( "5", new ArrayList <String> ( campos ) );
		campos.clear ( );
		
		
		return resultado;
	}

	private HashMap<String, ArrayList <String>> generaOrigen_Modificaciones ( ){
		
		HashMap<String, ArrayList<String>> resultado = new HashMap <String, ArrayList<String>> ( );
		ArrayList<String> campos = new ArrayList <String> ( );
		
		int cont = 1;
		
		// Llaves en 1 y 5
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "A" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "B" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "C" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "2" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "A" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "3" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "A" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "3" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "B" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "4" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "A" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "4" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "B" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "5" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "A" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "6" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "A" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
				
		
		
		return resultado;

	}
	
	private HashMap<String, ArrayList <String>> generaReplica_Modificaciones ( ){
		
		HashMap<String, ArrayList<String>> resultado = new HashMap <String, ArrayList<String>> ( );
		ArrayList<String> campos = new ArrayList <String> ( );
		
		int cont = 1;
		
		// Llaves en 1 y 5
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "A" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "A" );						// Se modifia el campo 8
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "B" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "1" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "C" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "2" );
		campos.add ( "a" );
		campos.add ( "A" );					// Se modifica el campo 3
		campos.add ( "B" );					// Se modifica el campo 4
		campos.add ( "A" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "3" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "A" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "3" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "B" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "4" );
		campos.add ( "a" );
		campos.add ( "A" );					// Se modifica el campo 3
		campos.add ( "c" );
		campos.add ( "A" );
		campos.add ( "d" );
		campos.add ( "B" );					// Se modifica el campo 7
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "C" );					// Se modifica el campo 11
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "4" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "B" );
		campos.add ( "A" );					// Se modifica el campo 6
		campos.add ( "B" );					// Se modifica el campo 7
		campos.add ( "C" );					// Se modifica el campo 8
		campos.add ( "1" );					// Se modifica el campo 9
		campos.add ( "2" );					// Se modifica el campo 10
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "5" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "A" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "6" );
		campos.add ( "a" );
		campos.add ( "b" );
		campos.add ( "c" );
		campos.add ( "A" );
		campos.add ( "d" );
		campos.add ( "e" );
		campos.add ( "f" );
		campos.add ( "g" );
		campos.add ( "h" );
		campos.add ( "i" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
				
		
		
		return resultado;

	}
	
	private HashMap<String, ArrayList <String>> generaResultado_Modificaciones ( ){
		
		HashMap<String, ArrayList<String>> resultado = new HashMap <String, ArrayList<String>> ( );
		ArrayList<String> campos = new ArrayList <String> ( );
		
		int cont = 1;
		
		// Llaves en 1 y 5
		
		campos.add ( "1" );
		campos.add ( "" );
		campos.add ( "" );
		campos.add ( "" );
		campos.add ( "A" );
		campos.add ( "" );
		campos.add ( "" );
		campos.add ( "f" );						// Se modifia el campo 8
		campos.add ( "" );
		campos.add ( "" );
		campos.add ( "" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "2" );
		campos.add ( "" );
		campos.add ( "b" );					// Se modifica el campo 3
		campos.add ( "c" );					// Se modifica el campo 4
		campos.add ( "A" );
		campos.add ( "" );
		campos.add ( "" );
		campos.add ( "" );
		campos.add ( "" );
		campos.add ( "" );
		campos.add ( "" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "4" );
		campos.add ( "" );
		campos.add ( "b" );					// Se modifica el campo 3
		campos.add ( "" );
		campos.add ( "A" );
		campos.add ( "" );
		campos.add ( "e" );					// Se modifica el campo 7
		campos.add ( "" );
		campos.add ( "" );
		campos.add ( "" );
		campos.add ( "i" );					// Se modifica el campo 11
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		campos.add ( "4" );
		campos.add ( "" );
		campos.add ( "" );
		campos.add ( "" );
		campos.add ( "B" );
		campos.add ( "d" );					// Se modifica el campo 6
		campos.add ( "e" );					// Se modifica el campo 7
		campos.add ( "f" );					// Se modifica el campo 8
		campos.add ( "g" );					// Se modifica el campo 9
		campos.add ( "h" );					// Se modifica el campo 10
		campos.add ( "" );
		resultado.put ( String.valueOf ( cont ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		cont++;
		
		return resultado;

	}

	private void limpiaOrigenBasesPruebaMySQLLocal ( )
	{
		String sentencia = "DELETE FROM origen";
		limpiaBasesDeDatosPruebaMySQLLocal ( sentencia );
	}
	
	private void limpiaReplicaBasesPruebaMySQLLocal ( )
	{
		String sentencia = "DELETE FROM replica";
		limpiaBasesDeDatosPruebaMySQLLocal ( sentencia );
	}
	
	private void limpiaBasesDeDatosPruebaMySQLLocal ( String sentencia )
	{
		try {
			ConsultaODBC c = new ConsultaODBC(getLocalCon ( ));
			
			c.DeleteRegistrosMySQL ( sentencia );
			
		} catch ( Exception e ){
			System.out.println (e.toString ( ));
		}
	}
	
	private void cargaDatosEnOrigenPruebaMySQLLocal ( )
	{
		try {
			ConsultaODBC c = new ConsultaODBC(getLocalCon ( ));
			
			String sentencia = "INSERT INTO origen (id, nombre, apellido, activo, correo) VALUES (?,?,?,?,?)";
				
			c.InsertaRegistroMysql ( sentencia, getRegistrosParaOrigen ( ), getDatoDeColumna ( ), 50 );
			
		} catch ( Exception e ){
			System.out.println (e.toString ( ));
		}
	}
	
	private void modificaDatosEnReplicaPruebaMySQLLocal ( )
	{
		try {
			ConsultaODBC c = new ConsultaODBC(getLocalCon ( ));
			
			limpiaReplicaBasesPruebaMySQLLocal ( );
			
			String sentencia = "INSERT INTO replica (id, nombre, apellido, activo, correo) VALUES (?,?,?,?,?)";
				
			c.InsertaRegistroMysql ( sentencia, getRegistrosParaReplica ( ), getDatoDeColumna ( ), 50 );
			
		} catch ( Exception e ){
			System.out.println (e.toString ( ));
		}
	}
	
	private Connection getLocalCon() throws ClassNotFoundException, SQLException
	{
		Connection c = null;
		Class.forName ( "com.mysql.jdbc.Driver" );
		
		c = DriverManager.getConnection ( "jdbc:mysql://localhost:3306/test", "root", "root" );
		
		return c;
	}
	
	private HashMap<String, ArrayList<String>> getRegistrosParaOrigen ( )
	{
		HashMap<String,ArrayList <String>> resultado = new HashMap <String, ArrayList<String>> ( );
		
		int contador = 0;
		
		ArrayList <String> campos = new ArrayList <String> ( );
		
		campos.add ( "1" );
		campos.add ( "Andres" );
		campos.add ( "Garcia" );
		campos.add ( "1" );
		campos.add ( "a.g@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "2" );
		campos.add ( "jessica" );
		campos.add ( "escarceba" );
		campos.add ( "1" );
		campos.add ( "j.e@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "3" );
		campos.add ( "Estefany" );
		campos.add ( "Ponce" );
		campos.add ( "1" );
		campos.add ( "e.p@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "4" );
		campos.add ( "Armando" );
		campos.add ( "Ruiz" );
		campos.add ( "1" );
		campos.add ( "a.r@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "5" );
		campos.add ( "Armando" );
		campos.add ( "Velazco" );
		campos.add ( "1" );
		campos.add ( "a.v@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "6" );
		campos.add ( "Eric" );
		campos.add ( "Rueda" );
		campos.add ( "1" );
		campos.add ( "e.r@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "7" );
		campos.add ( "Javier" );
		campos.add ( "Garcia" );
		campos.add ( "1" );
		campos.add ( "j.g@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "8" );
		campos.add ( "Carlos" );
		campos.add ( "Castillo" );
		campos.add ( "1" );
		campos.add ( "c.c@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "9" );
		campos.add ( "Paty" );
		campos.add ( "Pe�a" );
		campos.add ( "1" );
		campos.add ( "p.p@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "10" );
		campos.add ( "Sara" );
		campos.add ( "Duran" );
		campos.add ( "1" );
		campos.add ( "s.d@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		
		return resultado;
	}
	
	private HashMap<String, ArrayList<String>> getRegistrosParaReplica ( )
	{
		HashMap<String,ArrayList <String>> resultado = new HashMap <String, ArrayList<String>> ( );
		
		int contador = 0;
		
		ArrayList <String> campos = new ArrayList <String> ( );
		
		campos.add ( "1" );
		campos.add ( "Andres" );
		campos.add ( "Garcia" );
		campos.add ( "1" );
		campos.add ( "a.g@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "2" );
		campos.add ( "jessica" );
		campos.add ( "escarcega" );									// Cambio de escarceba a escarcega
		campos.add ( "1" );
		campos.add ( "jeesica.escarcega@barmex.com.mx" );			// El correo a jessica.escarcega@barmex.com.mx
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "3" );
		campos.add ( "Estefany" );
		campos.add ( "Ponce" );
		campos.add ( "1" );
		campos.add ( "e.p@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "4" );
		campos.add ( "Armando" );
		campos.add ( "Ruiz" );
		campos.add ( "1" );
		campos.add ( "a.r@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "5" );
		campos.add ( "Armando" );
		campos.add ( "Velasco" );								// De Velazco a Velasco
		campos.add ( "1" );
		campos.add ( "armando@barmex.com.mx" );					// De a.v@... a armando@barmex.com.mx
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "6" );
		campos.add ( "Eric" );
		campos.add ( "Rueda" );
		campos.add ( "0" );										// De 1 a 0
		campos.add ( "e.r@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "7" );
		campos.add ( "Javier" );
		campos.add ( "Garcia" );
		campos.add ( "1" );
		campos.add ( "j.g@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "8" );
		campos.add ( "Carlos" );
		campos.add ( "Castillo" );
		campos.add ( "1" );
		campos.add ( "c.c@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "9" );
		campos.add ( "Paty" );
		campos.add ( "Pe�a" );
		campos.add ( "1" );
		campos.add ( "p.p@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "10" );
		campos.add ( "Sara" );
		campos.add ( "Duran" );
		campos.add ( "1" );
		campos.add ( "s.d@barmex.com.mx" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		
		return resultado;
	}
	
	private HashMap<String, ArrayList<String>> getRegistrosParaReplicaConModificaciones ( )
	{
		HashMap<String,ArrayList <String>> resultado = new HashMap <String, ArrayList<String>> ( );
		
		int contador = 0;
		
		ArrayList <String> campos = new ArrayList <String> ( );
		
		campos.add ( "2" );
		campos.add ( "" );
		campos.add ( "escarceba" );									// Cambio de escarceba a escarcega
		campos.add ( "" );
		campos.add ( "j.e@barmex.com.mx" );							// El correo a jessica.escarcega@barmex.com.mx
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "5" );
		campos.add ( "" );
		campos.add ( "Velazco" );								// De Velazco a Velasco
		campos.add ( "" );
		campos.add ( "a.v@barmex.com.mx" );						// De a.v@... a armando@barmex.com.mx
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
		
		campos.add ( "6" );
		campos.add ( "" );
		campos.add ( "" );
		campos.add ( "1" );										// De 1 a 0
		campos.add ( "" );
		
		resultado.put ( String.valueOf ( contador ), new ArrayList <String> ( campos ) );
		campos.clear ( );
		contador++;
				
		return resultado;
	}
	
	private HashMap<Integer, tipoDeDato> getDatoDeColumna ( )
	{
		HashMap<Integer, tipoDeDato> resultado = new HashMap <Integer, Utilidades.tipoDeDato> ( );
		
		resultado.put ( 1, tipoDeDato.ENTERO );
		resultado.put ( 2, tipoDeDato.CADENA );
		resultado.put ( 3, tipoDeDato.CADENA );
		resultado.put ( 4, tipoDeDato.ENTERO );
		resultado.put ( 5, tipoDeDato.CADENA );
		
		return resultado;
		
	}

	private void limpiaOrigenBlob ( )
	{
		String sentencia = "DELETE FROM origenblob";
		limpiaBasesDeDatosPruebaMySQLLocal ( sentencia );
	}
	
	private void limpiaReplicaBlob ( )
	{
		String sentencia = "DELETE FROM replicablob";
		limpiaBasesDeDatosPruebaMySQLLocal ( sentencia );
	}
	
	private void cargaBlobOrigen ( ) throws ClassNotFoundException, SQLException
	{
		ConsultaODBC c = new ConsultaODBC( getLocalCon ( ) );
		
		String 	path		= "C:\\Users\\andres.garcia\\Documents\\OC\\";
		String 	fileName 	= "OC";
		String 	extencion 	= ".pdf";
		
		
		String sentencia = "INSERT INTO origenblob ( folio, tipo, formato, nombre, contenido ) VALUES (?,?,?,?,?)";
		
		HashMap <Integer, tipoDeDato> datoDeColumna = new HashMap <Integer, Utilidades.tipoDeDato> ( );
		
		datoDeColumna.put ( 1, tipoDeDato.ENTERO );
		datoDeColumna.put ( 2, tipoDeDato.ENTERO );
		datoDeColumna.put ( 3, tipoDeDato.ENTERO );
		datoDeColumna.put ( 4, tipoDeDato.CADENA );
		datoDeColumna.put ( 5, tipoDeDato.BLOB );
		
		HashMap<String, ArrayList <String>> registros = new HashMap <String, ArrayList<String>> ( );
		
		for ( int con = 1; con < 51 ; con++ ){
			
			ArrayList <String> campos = new ArrayList <String> ( );
			
			campos.add ( String.valueOf ( con ) );
			campos.add ( "1" );
			campos.add ( "2" );
			campos.add ( fileName + String.valueOf ( con ) + extencion );
			campos.add ( path + fileName + extencion );
			
			registros.put ( String.valueOf ( con ), campos );
		}
		
		c.InsertaRegistroMysql ( sentencia, registros, datoDeColumna, 10 );
		
		
	}
	
	//endregion

}
