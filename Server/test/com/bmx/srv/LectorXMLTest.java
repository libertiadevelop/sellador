package com.bmx.srv;

import static org.junit.Assert.*;

import org.junit.Test;

public class LectorXMLTest {

	@Test
	public void Inicio() {
		assertTrue( true );
	}

	@Test
	public void ConstructorArchivoNoExiste() {
		LectorXML l = new LectorXML( "hola" );
		
		assertFalse( "Se esperaba falso", l.getResultado() );
	}

	@Test
	public void ConstructorArchivoExiste() {
		LectorXML l = new LectorXML( "C:\\Users\\andres.garcia\\workspace\\PruebaArchivos\\020222024330FACMXI_S.xml" );
		
		assertTrue( "Se esperaba verdadero", l.getResultado() );
	}
	@Test
	public void RecuperaElementoFecha(){
		LectorXML l = new LectorXML( "C:\\Users\\andres.garcia\\workspace\\PruebaArchivos\\020222024330FACMXI_S.xml" );
		
		String esperado = "2014-01-30T11:47:54";

		assertEquals( "Se esperaba verdadera. ", esperado, l.RecuperaAtributoDeRaiz( "fecha" ) );
	}
	
	@Test
	public void RecuperaElementoFechaUTF(){
		LectorXML l = new LectorXML( "C:\\Users\\andres.garcia\\workspace\\PruebaArchivos\\110347118005FACMEX_S.xml" );
		
		String esperado = "2014-07-15T14:14:10";

		assertEquals( "Se esperaba verdadera. ", esperado, l.RecuperaAtributoDeRaiz( "fecha" ) );
	}

}
