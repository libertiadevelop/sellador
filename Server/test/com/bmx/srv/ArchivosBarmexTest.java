package com.bmx.srv;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

public class ArchivosBarmexTest {

	@Test
	public void inicio() {
		assertTrue( true );
	}

	@Test
	public void documentoIncorrecto() {
		ArchivosBarmex a = new ArchivosBarmex( "hola" );
		assertFalse( "Se esperaba falso, el documento es incorrecto.", a.getResultado() );
	}
	@Test
	public void documentoIncorrectoMasDigitos() {
		ArchivosBarmex a = new ArchivosBarmex( "010101020202FACMEX.PDF" );
		assertFalse ( "Se esperaba falso, el documento tiene mas digitos", a.getResultado() );
	}
	@Test
	public void documentoCorrectoNormal() {
		ArchivosBarmex a = new ArchivosBarmex( "010101020202FACMEX" );
		assertTrue ( "Se esperaba Verdadero, el documento es incorrecto.", a.getResultado() );
	}

	@Test
	public void documentoCorrectoPemex() {
		ArchivosBarmex a = new ArchivosBarmex( "010101020202FAPX" );
		assertTrue ( "Se esperaba Verdadero, el documento pemex es correcto.", a.getResultado() );
	}
	@Test
	public void validaExistenciaDeArchivosArchivosInexistentes()
	{
		ArchivosBarmex a = new ArchivosBarmex( "010101020202FACMEX" );
		
		a.validaExistencia( "" );
		
		assertFalse("Se esperaba falso el pdf no existe.", a.getExistePDF() );
		assertFalse("Se esperaba falso el xml no existe.", a.getExisteXML() );
	}
	
	@Test
	public void validaExistenciaDeArchivosArchivosExistentes()
	{
		ArchivosBarmex a = new ArchivosBarmex( "020222024330FACMXI" );
		
		a.validaExistencia( "C:\\Users\\andres.garcia\\workspace\\PruebaArchivos" );
		
		assertTrue("Se esperaba verdadero el pdf existe.", a.getExistePDF() );
		assertTrue("Se esperaba verdadero el xml existe.", a.getExisteXML() );
	}
	@Test
	public void copiaPDF()
	{
		ArchivosBarmex a = new ArchivosBarmex( "020222024330FACMXI" );
		
		a.validaExistencia( "C:\\Users\\andres.garcia\\workspace\\PruebaArchivos" );
		
		a.CopiaPDF( ".\\TEMP" );
		
		File p = new File( ".\\TEMP\\020222024330FACMXI.pdf" );	
		
		assertTrue("Se esperaba verdadero el pdf se debio copiar.", p.exists() );

		if ( p.exists() ) p.delete();
		
	}
	
	
}
