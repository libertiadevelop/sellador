package com.bmx.srv;

import static org.junit.Assert.*;

import org.junit.Test;

public class ReplicaBasesTest
{

	@Test
	public void testReplicaOdbcTableToMySQL ( ) {
		
		assertTrue ( "Se esperaba que fuera verdadero.", ReplicaBases.replicaOdbcTableToMySQL ( ReplicaBases.proceso.ESTADISTICA_07, 100, "2015" ) );
		assertTrue ( "Se esperaba que fuera verdadero.", ReplicaBases.replicaOdbcTableToMySQL ( ReplicaBases.proceso.ESTADISTICA_14, 100, "2015" ) );
	}

}
