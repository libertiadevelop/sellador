package com.bmx.srv;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.junit.Test;
import com.dumbster.smtp.*;

public class CorreoTest {
	
	@Test
	public void aoInicio() {
		assertTrue(true);
	}
	@Test
	public void FromVacio()
	{
		Correo mail = new Correo("", "armando.ruiz@barmex.com.mx");
		assertFalse( mail.getResultado() );
	}
	@Test
	public void FromCorreoIncorrecto()
	{
		Correo mail = new Correo("andres.garcia", "armando.ruiz@barmex.com.mx");
		assertFalse( mail.getResultado() );
	}
	@Test
	public void FromCorreoCorrecto()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		assertTrue( mail.getResultado() );
	}
	@Test
	public void toVacio()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "");
		assertFalse( mail.getResultado() );
	}
	
	@Test
	public void toUnSoloCorreo()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armandor.ruiz@barmex.com.mx");
		assertEquals("Se esperaba un 1 " , 1, mail.getTo().size() ); 
	}
	@Test
	public void toVariosCorreos()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armandor.ruiz@barmex.com.mx, jessica.escarcega@barmex.com.mx, estefany.ponce@barmex.com.mx");
		assertEquals("Se esperaba un 3", 3, mail.getTo().size() );
	}
	@Test
	public void toCorreoIncorrecto()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "xxx");
		assertFalse( mail.getResultado() );
	}
	@Test
	public void toCorreoCorrecto()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		assertTrue( mail.getResultado() );
	}
	@Test
	public void toVariosCorreosUnoInvalido()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "xxx, armando.ruiz@barmex.com.mx");
		assertEquals("Se esperaba un 1: " + mail.getError(), 1, mail.getTo().size() );
		assertTrue( "El error fue: " + mail.getError(), mail.getResultado() );
	}
	@Test
	public void toVariosCorreosTodosValidos()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "jessica.escarcega@barmex.com.mx, armando.ruiz@barmex.com.mx");
		assertEquals("Se esperaba un 2", 2, mail.getTo().size() );
		assertTrue( mail.getResultado() );
	}
	
	@Test
	public void HostVacio()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx","armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "" );
		assertFalse( mail.getResultado() );
	}
	@Test
	public void HostIncorrecto()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx","armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "holamundo" );
		assertFalse( mail.getResultado() );
	}
	@Test
	public void HostCorrecto()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx","armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "mail.barmex.com.mx" );
		assertTrue( mail.getResultado() );
	}
	@Test
	public void HostCorrectoSinDNS()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "holamundo.com.xx" );
		assertFalse( mail.getResultado() );
	}
	@Test
	public void HostCorrectoIP()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		assertTrue( mail.getResultado() );
	}
	@Test
	public void PuertoVacio()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		mail.setSMPTPort("");
		assertFalse( mail.getResultado() );
	}
	@Test
	public void PuertoIncorrectoNoNumerico()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		mail.setSMPTPort("aaa");
		assertFalse( mail.getResultado() );
	}
	@Test
	public void PuertoFueraDelRangoPermitidoPositivo()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		mail.setSMPTPort("1000000");
		assertFalse( mail.getResultado() );
	}
	@Test
	public void PuertoFueraDelRangoPermitidoNegativo()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		mail.setSMPTPort("-1");
		assertFalse( mail.getResultado() );
	}
	@Test
	public void PuertoCorrecto()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		mail.setSMPTPort( "25" );
		assertTrue( "El error fue: " + mail.getError(), mail.getResultado() );
	}
	@Test
	public void CCUnoInvalido()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		mail.setSMPTPort( "25" );
		mail.setCC("saludos");
		assertEquals("Se esperaba cero", 0, mail.getCC().size() );
		assertTrue( "El error fue: " + mail.getError(), mail.getResultado() );
	}
	@Test
	public void CCUnoValido()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		mail.setSMPTPort( "25" );
		mail.setCC("armando.ruiz@barmex.com.mx" );
		assertEquals("Se esperaba 1",1, mail.getCC().size() );
		assertTrue( "El error fue: " + mail.getError(), mail.getResultado() );
	}
	@Test
	public void CCVariosUnoInvalido()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		mail.setSMPTPort( "25" );
		mail.setCC("armando.ruiz@barmex.com.mx,xxx" );
		assertEquals("Se esperaba cero", 0, mail.getCC().size() );
		assertTrue( "El error fue: " + mail.getError(), mail.getResultado() );
	}
	@Test
	public void CCVariosTodosValidos()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		mail.setSMPTPort( "25" );
		mail.setCC("armando.ruiz@barmex.com.mx,andres.garcia@barmex.com.mx" );
		assertEquals("Se esperaba dos", 2, mail.getCC().size() );
		assertTrue( "El error fue: " + mail.getError(), mail.getResultado() );	
	}
	
	@Test
	public void BCCUnoInvalido()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		mail.setSMPTPort( "25" );
		mail.setBCC("saludos");
		assertEquals("Se esperaba cero", 0, mail.getBCC().size() );
		assertTrue( "El error fue: " + mail.getError(), mail.getResultado() );
	}
	@Test
	public void BCCUnoValido()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		mail.setSMPTPort( "25" );
		mail.setBCC("armando.ruiz@barmex.com.mx" );
		assertEquals("Se esperaba 1",1, mail.getBCC().size() );
		assertTrue( "El error fue: " + mail.getError(), mail.getResultado() );
	}
	@Test
	public void BCCVariosUnoInvalido()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		mail.setSMPTPort( "25" );
		mail.setBCC("armando.ruiz@barmex.com.mx,xxx" );
		assertEquals("Se esperaba cero", 0, mail.getBCC().size() );
		assertTrue( "El error fue: " + mail.getError(), mail.getResultado() );
	}
	@Test
	public void BCCVariosTodosValidos()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		mail.setSMPTPort( "25" );
		mail.setBCC("armando.ruiz@barmex.com.mx,andres.garcia@barmex.com.mx" );
		assertEquals("Se esperaba dos", 2, mail.getBCC().size() );
		assertTrue( "El error fue: " + mail.getError(), mail.getResultado() );
		
	}
	@Test
	public void ATTUnArchivoInexistente()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		mail.setSMPTPort( "25" );
		mail.setAttach( "noexisto" );
		assertEquals("Se esperaba dos", 0, mail.getATTACH().size());
		assertTrue( "El error fue: " + mail.getError(), mail.getResultado() );
	}
	@Test
	public void ATTUnArchivoExistente() throws IOException
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		mail.setSMPTPort( "25" );
		String NombreDeArchivo = "F:\\Pruebas\\HolaMundo.txt";
		File f = new File(NombreDeArchivo);
		if ( !f.exists() ){
			f.createNewFile();
		}
		mail.setAttach( NombreDeArchivo );
		assertEquals("Se esperaba 1", 1, mail.getATTACH().size());
		assertTrue( "El error fue: " + mail.getError(), mail.getResultado() );
		
		if ( f.exists() ) f.delete();
		
	}
	@Test
	public void ATTVariosUnArchivoInexistente() throws IOException
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		mail.setSMPTPort( "25" );
		String NombreDeArchivo 	= "F:\\Pruebas\\HolaMundo.txt";
		String NombreDeArchivoI = "xxx";
		File f = new File(NombreDeArchivo);
		if ( !f.exists() ){
			f.createNewFile();
		}
		mail.setAttach( NombreDeArchivo + "," + NombreDeArchivoI );
		assertEquals("Se esperaba 1", 1, mail.getATTACH().size());
		assertTrue( "El error fue: " + mail.getError(), mail.getResultado() );
		
		if ( f.exists() ) f.delete();
	}
	@Test
	public void ATTVariosArchivosExistentes() throws IOException
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx", "armando.ruiz@barmex.com.mx");
		mail.setSMTPHost( "201.148.87.82" );
		mail.setSMPTPort( "25" );
		String NombreDeArchivo 	= "F:\\Pruebas\\HolaMundo.txt";
		String NombreDeArchivoI = "F:\\Pruebas\\HolaMundo1.txt";
		File f 	= new File(NombreDeArchivo);
		File f1 = new File(NombreDeArchivoI); 
		if ( !f.exists() ){
			f.createNewFile();
		}
		if ( !f1.exists() ){
			f1.createNewFile();
		}
		mail.setAttach( NombreDeArchivo + "," + NombreDeArchivoI );
		assertEquals("Se esperaba 2", 2, mail.getATTACH().size());
		assertTrue( "El error fue: " + mail.getError(), mail.getResultado() );
		
		if ( f.exists() ) f.delete();
		if ( f1.exists()) f1.delete();
	}
	@Test
	public void SendMailSinHost()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx","armando.ruiz@barmex.com.mx");
		mail.enviaCorreo("hola", "Cuerpo");
		assertFalse("Se esperaba false",mail.enviaCorreo("Hola", "El que tienes"));
	}
	
	@Test
	public void SendMailSinPort()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx","armando.ruiz@barmex.com.mx");
		mail.setSMTPHost("mail.barmex.com.mx");
		mail.enviaCorreo("Hola", "Cuerpo");
		assertFalse("Se esperaba false",mail.enviaCorreo("Hola", "El que tienes"));
	}
	@Test
	public void SendMailConHostYPortSinEnviar()
	{
		Correo mail = new Correo("andres.garcia@barmex.com.mx","armando.ruiz@barmex.com.mx");
		mail.setSMTPHost("mail.barmex.com.mx");
		mail.setSMPTPort("25");
		mail.enviaCorreo("Hola", "Cuerpo");
		assertTrue("Se esperaba true: " +  mail.getError() ,mail.getResultado());
	}
	@Test
	public void SendMail()
	{
		SimpleSmtpServer 	server 	= SimpleSmtpServer.start();
		Correo				mail 	= new Correo("andres.garcia@barmex.com.mx", "andres.garcia@barmex.com.mx");
		mail.setSMTPHost("127.0.0.1");
		mail.setSMPTPort("25");
		mail.enviaCorreo("Test", "Test Body");
		mail.setDebug( false );
		assertTrue( "Se esperaba verdadero. " + mail.getLog() , mail.enviaCorreo("Test", "Test Body") );
		server.stop();
		if ( server.getReceivedEmailSize() > 0 ){
			@SuppressWarnings("rawtypes")
			Iterator 	emailIter 	= server.getReceivedEmail();
			SmtpMessage	email		= (SmtpMessage)emailIter.next();
			assertTrue( "No llego el subjetc adecuado", email.getHeaderValue( "Subject" ).equals( "Test" ) );
			assertTrue( "No llego el cuerpo adecuado", email.getBody().equals( "Test Body" ) );
		}	
	}
	@Test
	public void SendMailConAttachment()
	{
		SimpleSmtpServer 	server 	= SimpleSmtpServer.start();
		Correo				mail 	= new Correo("andres.garcia@barmex.com.mx", "andres.garcia@barmex.com.mx");
		mail.setSMTPHost("127.0.0.1");
		mail.setSMPTPort("25");
		mail.enviaCorreo("Test", "Test Body");
		mail.setDebug( false );
		assertTrue( "Se esperaba verdadero. " + mail.getLog() , mail.enviaCorreo("Test", "Test Body") );
		server.stop();
		if ( server.getReceivedEmailSize() > 0 ){
			@SuppressWarnings("rawtypes")
			Iterator 	emailIter 	= server.getReceivedEmail();
			SmtpMessage	email		= (SmtpMessage)emailIter.next();
			assertTrue( "No llego el subjetc adecuado", email.getHeaderValue( "Subject" ).equals( "Test" ) );
			assertTrue( "No llego el cuerpo adecuado", email.getBody().equals( "Test Body" ) );
			
		}	
	}
	
}
