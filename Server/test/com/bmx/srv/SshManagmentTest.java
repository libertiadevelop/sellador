package com.bmx.srv;

import static org.junit.Assert.*;

import org.junit.Test;


public class SshManagmentTest
{

	@Test
	public void testConnect ( ) {
		SshManagment s = new SshManagment ( "192.168.10.252", "22" );
		s.Connect ( );
		assertTrue ( "Se esperaba que se conectara", s.getEstado ( ) );
	}
	
	@Test
	public void testDesConnect ( ) {
		SshManagment s = new SshManagment ( "192.168.10.252", "22" );
		s.Connect ( );
		assertTrue ( "Se esperaba que se conectara", s.getEstado ( ) );
		s.Desconecta ( );
		assertTrue ( "Se esperaba que se desconectara", s.getEstado ( ) );
	}
	@Test
	public void testTerminaSession ( ) {
		SshManagment s = new SshManagment ( "192.168.10.252", "22" );
		s.Connect ( );
		assertTrue ( "Se esperaba que se conectara", s.getEstado ( ) );
		s.Desconecta ( );
		assertTrue ( "Se esperaba que se desconectara", s.getEstado ( ) );
		s.TerminaSession ( );
		assertTrue ( "Se esperaba que terminara session", s.getEstado ( ) );
	}

	@Test
	public void testEjecutaProceso ( ) {
		SshManagment s = new SshManagment ( "192.168.10.252", "22" );
		s.EjecutandoProceso ( "export LD_LIBRARY_PATH=\"/XDBC_8/bin\";export GENESIS_HOME=\"/XDBC_8\"; /XDBC_8/bin/asql.sh -r /srv/sentenciaExistencias.txt > /srv/exi.txt" );
	}

	@Test
	public void testDescargaArchivo() {
		SshManagment s = new SshManagment ( "192.168.10.252", "22" );
		s.EjecutandoProceso ( "export LD_LIBRARY_PATH=\"/XDBC_8/bin\";export GENESIS_HOME=\"/XDBC_8\"; /XDBC_8/bin/asql.sh -r /srv/sentenciaExistencias.txt > /srv/exi.txt" );
		s.RecuperaArchivo ( "/srv/exi.txt", "exi.txt" );
	}
	
}
