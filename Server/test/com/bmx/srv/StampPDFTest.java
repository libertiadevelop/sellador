package com.bmx.srv;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import com.itextpdf.text.pdf.PdfReader;

public class StampPDFTest 
{

	@Test
	public void Inicio() {
		assertTrue( true );
	}

	@Test
	public void validaConstructorArchivoInexistente() 
	{
		StampPDF p = new StampPDF( "hola.pdf", "c:\\" );
		
		assertFalse( "Se esperaba falso, el archivo no existe.",  p.getResultado() );
		
	}
	@Test
	public void validaConstructorArchivoIncorrecto() 
	{
		StampPDF p = new StampPDF( "saludos", "C:\\" );
		assertFalse( "Se esperaba falso, el archivo no existe.",  p.getResultado() );
	}
	@Test
	public void validaConstructorArchivoCorrecto()
	{
		
		StampPDF p = new StampPDF( "020222024330FACMXI.pdf", "C:\\Users\\andres.garcia\\workspace\\PruebaArchivos" );
		assertTrue( "Se esperaba verdadero, el archivo existe.",  p.getResultado() );
		
	}
	// validar que documento marcado no se vuelva a marcar
	@Test
	public void marcaDocumento() throws FileNotFoundException, IOException
	{
		String ruta 		= "C:\\Users\\andres.garcia\\workspace\\PruebaArchivos";
		String origen 		= "020222024330FACMXI.pdf";
		String docMarcar	= "docMarcado.pdf";
		Utilidades.CopiaArchivos( ruta + "\\" + docMarcar, ruta + "\\" + origen );
		
		StampPDF p = new StampPDF( docMarcar, ruta );
		
		assertTrue( "Se esperaba verdadero, el archivo debe existir.", p.getResultado() );
		
		p.setMarca( "CANCELADA" );
		
		p.MarcaArchivo();
		
		File m = new File( ruta + "\\" + docMarcar );
		
		assertTrue( "Se esperaba que el documento marcado existiera.", m.exists() );
		
		PdfReader pdfR = new PdfReader( new FileInputStream( m ) );
		
		assertEquals( "Se esperaba encontrar la etiqueta de marcado.", "CANCELADA", pdfR.getInfo().get( "Keywords" ) );
		
		if ( m.exists() ) m.delete();
	}
	@Test
	public void documentoYaMarcado()
	{
		String ruta 		= "C:\\Users\\andres.garcia\\workspace\\PruebaArchivos";
		String docMarcar	= "Marcado.pdf";
		StampPDF p = new StampPDF( docMarcar, ruta );
		
		p.MarcaArchivo();
		
		assertEquals( "Se esperaba que ya fuera marcado.", "DOCUMENTO YA MARCADO", p.getLog() );
	}
	
	
}
