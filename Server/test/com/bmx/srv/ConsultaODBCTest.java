package com.bmx.srv;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

public class ConsultaODBCTest 
{
	
	@Test
	public void inicio() 
	{
		assertTrue( true );
	}

	@Test
	public void ConexionNula()
	{
		ConsultaODBC c = new ConsultaODBC( null );
		
		assertFalse("Se esperaba false", c.getResultado() );
		
	}
	
	@Test
	public void ConexcionCorrecta()
	{
		Connection con = Conecciones.getERPCon();
		
		assertNotNull( "La conexion es nula, no se puede probar", con );
		
		ConsultaODBC c  = new ConsultaODBC( con );
		
		assertTrue( "Se esperaba verdadero", c.getResultado() );
		
	}
	
	@Test
	public void ConsultaABaseDeDatos()
	{
		
		Connection 		con = Conecciones.getERPCon();
		ConsultaODBC	c	= new ConsultaODBC( con );
		
		assertTrue( "Se esperaba verdadero.", c.getResultado() );
		
		String			s	= "SELECT * FROM MDGINV01 WHERE AR_CODART LIKE 'CR 40 1%'";
		
		HashMap<String, ArrayList<String>> resultado = c.ConsultaVariasColumnasODBC( s );
		
		assertEquals( "Se esperaba que devolviera: 5 renglones" , 5, resultado.size() );
		
	}
	
}
