package com.bmx.srv;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.junit.Test;

import com.bmx.srv.Servicios.Servicio;

public class ServiciosTest
{
	private String archivo 		= "servicios.cfg";
	private String archivoDummy	= "no_existo.cfg";
	@Test
	public void testConstructorElArchivoNoExiste()
	{
		Servicios s = new Servicios ( archivoDummy );
		
		assertFalse ( "Se esperaba falso", s.getEstado ( ) );
		assertEquals ( "El mensaje de error es incorrecto", "El archivo no_existo.cfg no existe.", s.getDescripcionError ( ) );
		assertFalse ( "La invocacion debe regresar false", s.getBooleanServicio ( Servicios.Servicio.COMPARA_BASES )) ;
	}
	
	@Test
	public void testConstructorElArchivoExiste() throws FileNotFoundException
	{
		BorraArchivo ( );
		GeneraArchivoConVerdadero ( );
		
		Servicios s = new Servicios( archivo );
		
		assertTrue ( "El archivo si existe,  se espera verdadero", s.getEstado ( ) );
		assertEquals ( "El erro no existe", "", s.getDescripcionError ( ) );
		
		
		BorraArchivo ( );
	}
	
	@Test
	public void testRecuperaTrue() throws FileNotFoundException
	{
		BorraArchivo ( );
		GeneraArchivoConVerdadero ( );
		
		Servicios s = new Servicios( archivo );
		
		assertTrue ( "El archivo si existe,  se espera verdadero", s.getEstado ( ) );
		assertTrue ( "Debe devolver verdadero", s.getBooleanServicio ( Servicio.COMPARA_BASES ) );
		assertEquals ( "El erro no existe", "", s.getDescripcionError ( ) );
		
		BorraArchivo ( );
	}
	
	@Test
	public void testRecuperaFalse()  throws FileNotFoundException
	{
		BorraArchivo ( );
		GeneraArchivoConFalso ( );
		Servicios s = new Servicios( archivo );
		
		assertTrue ( "El archivo si existe,  se espera verdadero", s.getEstado ( ) );
		assertFalse ( "Debe devolver falso", s.getBooleanServicio ( Servicio.COMPARA_BASES ) );
		assertEquals ( "El erro no existe", "", s.getDescripcionError ( ) );
		
		BorraArchivo ( );
	}
	
	@Test
	public void testSetValorAFalso() throws FileNotFoundException
	{
		BorraArchivo ( );
		GeneraArchivoConVerdadero ( );
		
		Servicios s = new Servicios( archivo );
		
		assertTrue ( "El archivo si existe,  se espera verdadero", s.getEstado ( ) );
		assertTrue ( "Debe devolver verdadero", s.getBooleanServicio ( Servicio.COMPARA_BASES ) );
		assertEquals ( "El erro no existe", "", s.getDescripcionError ( ) );
		
		s.setBooleanServicio ( Servicio.COMPARA_BASES, false );
		
		assertFalse ( "Debe devolver falso", s.getBooleanServicio ( Servicio.COMPARA_BASES ) );
		assertEquals ( "El erro no existe", "", s.getDescripcionError ( ) );
		
		BorraArchivo ( );
	}
	
	@Test
	public void testSetValorAVerdadero() throws FileNotFoundException
	{
		BorraArchivo ( );
		GeneraArchivoConFalso ( );
		
		Servicios s = new Servicios( archivo );
		
		assertTrue ( "El archivo si existe,  se espera verdadero", s.getEstado ( ) );
		assertFalse ( "Debe devolver falso", s.getBooleanServicio ( Servicio.COMPARA_BASES ) );
		assertEquals ( "El erro no existe", "", s.getDescripcionError ( ) );
		
		s.setBooleanServicio ( Servicio.COMPARA_BASES, true );
		
		assertTrue ( "Debe devolver verdadero", s.getBooleanServicio ( Servicio.COMPARA_BASES ) );
		assertEquals ( "El erro no existe", "", s.getDescripcionError ( ) );
		BorraArchivo ( );
	}
	
	private void BorraArchivo()
	{
		File c = new File ( archivo );
		if ( c.exists ( ) ){
			c.delete ( );
		}
		
	}
	
	private void GeneraArchivoConVerdadero() throws FileNotFoundException
	{
		PrintWriter p = new PrintWriter( new File ( archivo ) );
		p.println("compara_bases=1");
		p.close();
	}

	private void GeneraArchivoConFalso() throws FileNotFoundException
	{
		PrintWriter p = new PrintWriter( new File ( archivo ) );
		p.println("compara_bases=0");
		p.close();
	}
}
