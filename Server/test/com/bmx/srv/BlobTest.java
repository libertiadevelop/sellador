package com.bmx.srv;
import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import org.junit.Test;


public class BlobTest {

	@Test	
	public void Inicio()
	{
		assertTrue(true);
	}
	@Test
	public void ConstructorValoresVacios()
	{
		Blob b = new Blob( "", "", "", "" );
		
		assertFalse( "Se esperaba false.", b.getResultado() );
	}
	@Test
	public void ConstructorConDatosServerIncorrecto()
	{
		Blob b = new Blob( "hola", "hola", "aaa", "aaa" );
		
		assertFalse( "Se esperaba false. " + b.getLog(), b.getResultado() );
	}
	@Test
	public void ConstructorConDatosServerYPuertoCorrectos()
	{
		Blob b = new Blob ( "localhost", "2280","aaaa", "aaaa" );
		assertTrue( "Se esperaba verdadero" , b.getResultado() );
	}
	
	@Test
	public void SubeInformacionAlBlobSinSentencia()
	{
		Blob b = new Blob( "localhost", "3306", "Test", "PruebaBlob" );
		
		b.cargaBlob();
		
		assertFalse( "Se esperaba falso", b.getResultado()  );	
	}
	@Test
	public void SubeInformacionAlBlobSinArchivos()
	{
		Blob b = new Blob( "localhost", "3306", "Test", "PruebaBlob" );
		
		b.cargaBlob();
		
		assertFalse( "Se esperaba false", b.getResultado() );	
	}
	
	@Test
	public void SubeInformacionAlBlobConArchivosYSentenciaIncogruente()
	{
		Blob b = new Blob( "localhost", "3306", "Test", "PruebaBlob" );
		
		b.setSentencia( "INSERT INTO BASE.TABLA (1,2,3,4) VALUES (?,?,?,?)" );
		b.setArchivos( "1,2,3" );
		
		b.cargaBlob();
		
		assertFalse( "Se esperaba false", b.getResultado() );	
	}
	@Test
	public void SubeInformacionConTodoCorrecto()
	{
		String nombreDeArchivo = "hola.txt";
		//  Crea el archivo a subir.
		try{
			FileWriter fw = new FileWriter( nombreDeArchivo );
			fw.write( "Hola mundo, como estas" );
			fw.flush();
			fw.close();
		} catch ( Exception e ){
			System.out.println( "No se pudo cargar el archivo" );
			System.out.println( e.toString() );
		} 
			
		Blob b = new Blob( "10.0.2.2", "3306", "test", "PruebaBlob" );
		
		b.setSentencia( "INSERT INTO PruebaBlob ( Campo1, Campo2, archivo ) VALUES ('hola','mundo',?)" );
		b.setArchivos( nombreDeArchivo );
		b.setUsuario( "root" );
		b.setPasswod( "root" );
		
		b.cargaBlob();
		
		
		assertTrue( "Se esperaba verdadero, todo esta correcto. " + b.getLog(), b.getResultado() );
		
		// Una vez terminado y validado elimina el archivo.
		
		try {
			File e = new File( nombreDeArchivo );
			if ( e.exists() ){
				e.delete();
			}
		} catch ( Exception e ){
			System.out.println( "El archivo no se pudo borrar, eliminar manualmente" );
			System.out.println( e.toString() );
		}
		
		
	}
	@Test
	public void DescargaInformacionAlBlob() throws IOException
	{
		String nombreDeArchivo = "holamundo.txt";
		
		Blob b = new Blob( "10.0.2.2", "3306", "test", "PruebaBlob" );
		b.setSentencia( "SELECT archivo FROM PruebaBlob WHERE id = 1" );
		b.setUsuario( "root" );
		b.setPasswod( "root" );
	
		b.DescargaArchivo( nombreDeArchivo );
		
		File archivo = new File( nombreDeArchivo );
		
		assertTrue( "Se esperaba verdadero, se debio descargar.", archivo.exists() );
		
		if ( archivo.exists() ){
			FileReader 		fr = new FileReader( archivo );
			BufferedReader 	bf = new BufferedReader( fr );
			
			String linea = bf.readLine();
			
			assertEquals("Se esperaba que el archivo estuviera bien", "Hola mundo, como estas", linea.trim() );
			archivo.delete();
			
			if ( bf != null ) bf.close();
			
		} else {
			assertFalse( "No se pudo validar el contenido", true );
		}
		
	
	}
	
	@Test
	public void ValidaRegistroExistente()
	{
		Blob b = new Blob( "10.0.2.2", "3306", "test", "PruebaBlob" );
		b.setSentencia( "SELECT id FROM PruebaBlob WHERE id = 1" );
		b.setUsuario( "root" );
		b.setPasswod( "root" );
		
		assertTrue( "Se esperaba verdadero el registro existe.", b.validaSentencia() );
	}
	@Test
	public void ValidaRegistroInexistente()
	{
		Blob b = new Blob( "10.0.2.2", "3306", "test", "PruebaBlob" );
		b.setSentencia( "SELECT id FROM PruebaBlob WHERE id = 0" );
		b.setUsuario( "root" );
		b.setPasswod( "root" );
		
		assertFalse( "Se esperaba falso el registro no existe.", b.validaSentencia() );
	}
	
	
}
