package com.bmx.srv;
import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Test;

import com.bmx.srv.Documento;


public class DocumentoTest {

	@Test
	public void testDatos() {
		assertTrue( true );
	}
	@Test
	public void cargaInformacionDelResource()
	{
		Documento d = new Documento( );
		
		d.RecuperaCampos( "123456123456123123" ); 
		
		assertEquals("Se esperaba 4", 4, d.getDatos().size() );	
	}
	@Test
	public void cargaInformacionDeLaCadena()
	{
		//cliente,documento,serie,sucursal
		HashMap<String, String> datosAValidar = new HashMap<String, String>();
		datosAValidar.put( "cliente", "010101" );
		datosAValidar.put( "documento", "020202" );
		datosAValidar.put( "serie", "FAC" );
		datosAValidar.put( "sucursal", "MEX" );
		
		Documento d = new Documento( );
		
		d.RecuperaCampos( datosAValidar.get( "cliente" ) + datosAValidar.get( "documento" ) + datosAValidar.get( "serie" ) + datosAValidar.get( "sucursal" ) );
		
		assertEquals("Se esperaba 4", 4, d.getDatos().size() );
		
		for (String llave :  datosAValidar.keySet() ) {
			assertEquals("Se esperaba: " + datosAValidar.get( llave ) , datosAValidar.get( llave ), d.getDatos().get( llave ));
		}		
	}
	@Test
	public void cargaInformacionDeLaCadenaSegundaOpcion()
	{
		//cliente,documento,serie,sucursal
		HashMap<String, String> datosAValidar = new HashMap<String, String>();
		
		datosAValidar.put( "cliente", "010101" );
		datosAValidar.put( "documento", "020202" );
		datosAValidar.put( "serie", "FA" );
		datosAValidar.put( "sucursal", "PX" );
		
		Documento d = new Documento( );
		
		d.RecuperaCampos( datosAValidar.get( "cliente" ) + datosAValidar.get( "documento" ) + datosAValidar.get( "serie" ) + datosAValidar.get( "sucursal" ) );
		
		assertEquals("Se esperaba 4", 4, d.getDatos().size() );
		
		for ( String llave :  datosAValidar.keySet() ) {
			assertEquals("Se esperaba: " + datosAValidar.get( llave ) , datosAValidar.get( llave ), d.getDatos().get( llave ));
		}		
	}
	@Test
	public void cargaInformacionDeLaCadenaConCamposCalculados()
	{
		//cliente,documento,serie,sucursal
		HashMap<String, String> datosAValidar = new HashMap<String, String>();
		
		datosAValidar.put( "cliente", "010101" );
		datosAValidar.put( "documento", "020202" );
		datosAValidar.put( "serie", "FAC" );
		datosAValidar.put( "sucursal", "MEX" );
		datosAValidar.put( "movimiento", "1" );
		datosAValidar.put( "path","Y:\\Mex\\" );
		datosAValidar.put( "suc", "01" );
		
		Documento d = new Documento( );
		
		d.RecuperaCampos( datosAValidar.get( "cliente" ) + datosAValidar.get( "documento" ) + datosAValidar.get( "serie" ) + datosAValidar.get( "sucursal" ), "movimiento:mov_serie,path:fld_sucursal,suc:suc_sucursal" );
		
		int	contador = 1;
		
		for ( String llave :  datosAValidar.keySet() ) {
			assertEquals("Valor "  + contador + " Se esperaba el valor de llave : " + llave + " " + datosAValidar.get( llave ) , datosAValidar.get( llave ), d.getDatos().get( llave ));
			contador++;
		}		
	}
}
