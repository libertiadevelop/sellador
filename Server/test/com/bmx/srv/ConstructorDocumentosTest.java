package com.bmx.srv;

import static org.junit.Assert.*;

import org.junit.Test;

import com.bmx.srv.Utilidades.tipoDeDocumento;

public class ConstructorDocumentosTest
{

	@Test
	public void testConstructorDocumentos ( ) {
		ConstructorDocumentos c = new ConstructorDocumentos ( "093199", "0111546210", tipoDeDocumento.FACTURA );
		
		assertEquals ( "093199115462FACMEX", c.getNombreDeDocumento ( ) );
		
	}

}
