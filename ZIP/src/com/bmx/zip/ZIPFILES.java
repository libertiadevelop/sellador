package com.bmx.zip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZIPFILES
{
	//region VARIABLES PRIVADAS
	private String	zipFile;
	private boolean resultado;
	private String	error;
	//endregion
	
	//region PROPIEDADES
	public boolean getResultado()
	{
		return resultado;
	}
	
	public String getError()
	{
		return error;
	}
	//endregion
	
	//region CONSTRUCTOR
	public ZIPFILES ( String ZIPFile )
	{
		zipFile = ZIPFile;
	}
	//endregion
	
	//region METODOS PUBLICOS
	public void Descomprime( String Folder )
	{
		if ( _ExisteZIPFile( ) ){
			byte[] 			buffer 	= new byte[1024];
			ZipInputStream	zis	 	= null;
			ZipEntry		ze		= null;
			
			try{
				
				File folder = new File ( Folder );
				
				if ( !folder.exists( ) ){
					folder.mkdir( );
				}
				
				
				zis = new ZipInputStream( new FileInputStream( zipFile ) );
				ze	= zis.getNextEntry( );
				
				while ( ze != null ){
					
					String 	nombreDeArchivo = ze.getName( );
					File	nuevoArchivo	= new File( Folder + File.separator + nombreDeArchivo );
					
					FileOutputStream fos	= new FileOutputStream( nuevoArchivo );
					
					int len;
					
					while ( ( len=zis.read( buffer ) ) > 0 ){
						fos.write( buffer, 0, len );
						fos.flush( );
					}
					
					fos.close( );
					ze = zis.getNextEntry( );
					
				}
				
				zis.closeEntry( );
				zis.close( );
				
				resultado = true;
				
			} catch ( Exception e ){
				resultado = false;
			} finally {
				if ( zis != null ) {
					try{
						zis.closeEntry( );
						zis.close( );
					} catch ( Exception e ){
						error = e.toString( );
					}
				}
			}
					 
		} else {
			resultado = false;
		}
	}
	
	public void Descomprime( )
	{
		if ( _ExisteZIPFile( ) ){
			byte[] 			buffer 	= new byte[1024];
			ZipInputStream	zis	 	= null;
			ZipEntry		ze		= null;
			
			try{
				zis = new ZipInputStream( new FileInputStream( zipFile ) );

				ze	= zis.getNextEntry( );
				
				while ( ze != null ){
					
					String 	nombreDeArchivo = ze.getName( );
					File	nuevoArchivo	= new File( nombreDeArchivo );
					
					FileOutputStream fos	= new FileOutputStream( nuevoArchivo );
					
					int len;
					
					while ( ( len=zis.read( buffer ) ) > 0 ){
						fos.write( buffer, 0, len );
						fos.flush( );
					}
					
					fos.close( );
					ze = zis.getNextEntry( );
					
				}
				
				zis.closeEntry( );
				zis.close( );	
				resultado = true;
				
			} catch ( Exception e ){
				resultado = false;
				error = e.toString ( );
			} finally {
				if ( zis != null ) {
					try{
						zis.closeEntry( );
						zis.close( );
					} catch ( Exception e ){
						error = e.toString( );
					}
				}
			}
					 
		} else {
			resultado = false;
		}
	}
	
	public void DescomprimeStream( InputStream entrada )
	{
		
		if ( entrada != null ) {
			byte[] 			buffer 	= new byte[1024];
			ZipInputStream	zis	 	= null;
			ZipEntry		ze		= null;
			
			try{
				zis = new ZipInputStream( entrada );
	
				ze	= zis.getNextEntry( );
				
				while ( ze != null ){
					
					String 	nombreDeArchivo = ze.getName( );
					File	nuevoArchivo	= new File( nombreDeArchivo );
					
					FileOutputStream fos	= new FileOutputStream( nuevoArchivo );
					
					int len;
					
					while ( ( len=zis.read( buffer ) ) > 0 ){
						fos.write( buffer, 0, len );
						fos.flush( );
					}
					
					fos.close( );
					ze = zis.getNextEntry( );
					
				}
				
				zis.closeEntry( );
				zis.close( );	
				resultado = true;
				
			} catch ( Exception e ){
				resultado = false;
				error = e.toString ( );
			} finally {
				if ( zis != null ) {
					try{
						zis.closeEntry( );
						zis.close( );
					} catch ( Exception e ){
						error = e.toString( );
					}
				}
			}
					 
		} else {
			resultado = false;
		}
	}
	
	public void Comprime ( ArrayList<String> archivos )
	{
		byte[] buffer = new byte[1024];
		
		File d = new File( zipFile );
		
		if ( _ExisteZIPFile( ) ){
			d.delete( );
		}
		
		FileOutputStream 	fos = null;
		ZipOutputStream		zos = null;

		
		try{
			fos = new FileOutputStream( d );
			zos	= new ZipOutputStream( fos );
			
			for ( String archivo : archivos ) {
				
				File a = new File( archivo );
				
				if ( a.exists( ) ){
				
					ZipEntry ze = new ZipEntry( archivo );
					zos.putNextEntry( ze );
					
					FileInputStream in = new FileInputStream( a );
					
					int len;
					
					while( ( len = in.read( buffer ) ) > 0 ){
					
						zos.write( buffer, 0, len );
					}
					
					in.close( );
					zos.closeEntry( );
				
				}
			}
			
			zos.close( );
			fos.close( );
			
			
		} catch ( Exception e ){
			
		}
		
		
	}
	//endregion
	
	//region METODOS PRIVADOS
	private boolean _ExisteZIPFile ()
	{
		boolean resultado = false;
		
		File f = new File( zipFile );
		
		resultado = f.exists( );
		
		return resultado;
	}
	//endregion
}
