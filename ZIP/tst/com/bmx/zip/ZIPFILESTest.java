package com.bmx.zip;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.ArrayList;

import org.junit.Test;

public class ZIPFILESTest
{

	@Test
	public void testComprime ( ) 
	{
		// Crea dos archivos para comprimir
		
		String file1 = "hola.txt";
		String file2 = "hola2.txt";
		
		 
		
		String ZipFile = "h.zip";
		
		File uno 	= new File( file1 );
		File dos 	= new File( file2 );
		File z		= new File( ZipFile );
		
		if ( uno.exists( ) ) 	uno.delete( );
		if ( dos.exists( ) ) 	dos.delete( );
		if ( z.exists( ) )		z.delete( );
		
		CreaArchivo( file1 );
		CreaArchivo( file2 );
		
		ArrayList<String> n = new ArrayList<String>( );
		
		n.add( file1 );
		n.add( file2 );
		
		ZIPFILES zip = new ZIPFILES( ZipFile );
		
		zip.Comprime( n );
		
		assertTrue( "Deberia crearse el archivo", z.exists( ) );
		
		if ( z.exists( ) ) z.delete( );
		
	}

	@Test
	public void testDescomprime ( ) 
	{
		String file1 = "hola.txt";
		String file2 = "hola2.txt";
		
		String ZipFile = "h.zip";
		
		File uno 	= new File( file1 );
		File dos 	= new File( file2 );
		File z		= new File( ZipFile );
		
		if ( uno.exists( ) ) 	uno.delete( );
		if ( dos.exists( ) ) 	dos.delete( );
		if ( z.exists( ) )		z.delete( );
		
		CreaArchivo( file1 );
		CreaArchivo( file2 );
		
		ArrayList<String> n = new ArrayList<String>( );
		
		n.add( file1 );
		n.add( file2 );
		
		ZIPFILES zip = new ZIPFILES( ZipFile );
		
		zip.Comprime( n );
		
		assertTrue( "Deberia crearse el archivo", z.exists( ) );

		uno.delete( );
		dos.delete( );
		
		assertFalse( "No debe exister file1", uno.exists( ) );
		assertFalse( "No deberia existir file2", dos.exists( ) );
		
		// Descomprime el archivo.
		
		zip.Descomprime( );
		
		assertTrue( "Deberia exister file1", uno.exists( ) );
		assertTrue( "Deberia existir file2", dos.exists( ) );

		
	}
	
	@Test
	public void testDescomprimeTmp ()
	{
		
		String zi = "tmp.zip";
		
		File z = new File ( zi );
		
		if ( !z.exists ( ) ){
		
			return;
		}
		
		// Intenta descomprimir este archivo.
		
		ZIPFILES zip = new ZIPFILES ( zi );
		
		zip.Descomprime ( );
		
		
		
	}
	
	@Test
	public void testDescomprimeString ( ) 
	{
		String file1 = "hola.txt";
		String file2 = "hola2.txt";
		
		String ZipFile = "h.zip";
		
		String folder	= "TEMP";
		
		File uno 	= new File( file1 );
		File dos 	= new File( file2 );
		File z		= new File( ZipFile );
		File f		= new File( folder );
		
		
		if ( uno.exists( ) ) 	uno.delete( );
		if ( dos.exists( ) ) 	dos.delete( );
		if ( z.exists( ) )		z.delete( );
		if ( f.exists( ) ) {
			if ( f.list().length == 0 ){
				f.delete( );
			} else {
				String[] archivos = f.list( );
				for ( String string : archivos ) {
					File d = new File( string );
					d.delete( );
				}
			}
		}
		
		CreaArchivo( file1 );
		CreaArchivo( file2 );
		
		ArrayList<String> n = new ArrayList<String>( );
		
		n.add( file1 );
		n.add( file2 );
		
		ZIPFILES zip = new ZIPFILES( ZipFile );
		
		zip.Comprime( n );
		
		assertTrue( "Deberia crearse el archivo", z.exists( ) );

		uno.delete( );
		dos.delete( );
		
		assertFalse( "No debe exister file1", uno.exists( ) );
		assertFalse( "No deberia existir file2", dos.exists( ) );
		
		// Descomprime el archivo.
		
		zip.Descomprime( folder );
		
		uno = new File( folder + File.separator + file1 );
		dos = new File( folder + File.separator + file2 );
		
		assertTrue( "Deberia exister file1", uno.exists( ) );
		assertTrue( "Deberia existir file2", dos.exists( ) );
		
		if ( uno.exists( ) ) uno.delete( );
		if ( dos.exists( ) ) dos.delete( );
		if ( f.exists( ) ) f.delete( );
		if ( z.exists( ) ) z.delete( );
	}

	@Test
	public void testDescomprimeStream ( )
	{
		DataInputStream	 	dis = null;
		DataOutputStream	dos = null;
		
		FileOutputStream	fos = null;
		byte[]				buf = null;
		int					tis	= 0;
		
		String hostName 	= "localhost";
		String puerto		= "5432";
		String tmpFile		= "tmp.zip";
		
		File tmp = new File ( tmpFile );
		if ( tmp.exists ( ) ) tmp.delete();
		
		try{
			Socket com = new Socket( hostName, Integer.parseInt( puerto ) );
			dos = new DataOutputStream( com.getOutputStream( ) );
			dis = new DataInputStream( com.getInputStream( ) );
			
			// Solicitando servicio
			dos.writeInt( 17 );
			// Esperando confirmacion, recive la misma informacion que se solicito
			int Respuesta = dis.readInt( );
			// Envia el n�mero de documento que se esta solicitando.
			dos.writeUTF ( "190254026599FACMTY" );
			// Espera confirmacion;
			Respuesta = dis.readInt ( );
			// Enviando informacion de documento a descargar ( factura o lista de precios )
			dos.writeInt( 3 );
			//Comienza a recivir la informacion.
			
			buf = new byte[1024 * 10000];
			
			// if ( path.length( ) > 0 ) {
			//	/	path = path + "\\";
			//	}
			//	tmpFile = path + tmpFile;
				File tmpF = new File( tmpFile );
				if ( tmpF.exists ( ) ) tmp.delete ( );
				tmpF.createNewFile ( );
				fos = new FileOutputStream( tmpF, true );
				int len;
				while ( (len = dis.read( buf )) != -1 ){
					fos.write ( buf, 0, len );
				}
				fos.close( );
				
				InputStream in = new ByteArrayInputStream ( buf );
				
				
			// termina la conecci�n.
			
		
			
			ZIPFILES zi = new ZIPFILES ( tmpFile );
			
			zi.DescomprimeStream ( in );
			
			System.out.println ( zi.getError ( ) );
			
			com.close( );
			
			// Descomprime el archivo.
			
			ZIPFILES z = new ZIPFILES( tmpFile );
			
			z.Descomprime( );
			if ( !z.getResultado ( ) ){
				System.out.println ( z.getError ( ) );
			}
						
		} catch ( Exception ie ) {
			System.out.println("No se pudo conectar con el servidor");
			System.out.println( ie.toString( ));
		} finally {
			try{
				if ( fos != null ) fos.close( );
			} catch ( Exception e ){
				
			}
		}
	}
	
	private void CreaArchivo ( String Nombre )
	{
		try {
			PrintWriter writer = new PrintWriter( Nombre, "UTF-8" );
			writer.println( "Hola mundo" );
			writer.println( "Hola mundo" );
			writer.println( "Hola mundo" );
			writer.close( );
			
		} catch ( FileNotFoundException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch ( UnsupportedEncodingException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
