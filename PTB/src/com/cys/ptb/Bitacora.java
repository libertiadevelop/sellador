package com.cys.ptb;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import com.cys.ptb.Utilidades;
import com.cys.ptb.Utilidades.mensaje;


public class Bitacora
{
	//region VARIABLES PRIVADAS
	private String 	archivo;
	private boolean creaArchivo;
	private ArrayList<String> error = new ArrayList<String>( );
	//endregion

	//region PROPIEDADES
	public void setAddError ( String mensaje  )
	{
		error.add( mensaje );
	}
	//endregion
	
	//region CONSTRUCTOR
	public Bitacora()
	{
		archivo = "log.t";
		ElArchivoExiste( );
	}
	
	public Bitacora( String Archivo )
	{
		archivo = Archivo;
	}
	//endregion
	
	//region METODOS PUBLICOS
	public void Reporta ( String Cliente, mensaje mensaje )
	{
		Reporta ( error, Cliente, mensaje );
	}
	
	public void Reporta ( ArrayList<String> informacion, String Cliente, mensaje mensaje )
	{
		boolean UsarTabulador = false;
		
		for ( String i : informacion ) {
		
			String cadenaAGuardar = "";
			
			if ( UsarTabulador ){
			
				cadenaAGuardar = FormateaCadena( i, Cliente, Utilidades.mensaje.TABULADOR );
				
			} else {
				
				cadenaAGuardar = FormateaCadena( i, Cliente, mensaje );
			}
			
			EscribeAArchivo( cadenaAGuardar );
			
			if ( !UsarTabulador ) {
				UsarTabulador = true;
			}
		}
	}
	
	public void Reporta ( String informacion, String Cliente, mensaje mensaje )
	{
		String CadenaAGuardar = FormateaCadena( informacion, Cliente, mensaje );
		
		EscribeAArchivo( CadenaAGuardar );
		
	}
	
	public void limpiaError ()
	{
		error.clear( );
	}
	//endregion
	
	//region METODOS PRIVADOS
	private String FormateaCadena ( String informacion, String Cliente, mensaje mensaje )
	{
		
		String marcaDeFecha 	= Utilidades.devuelveFecha( );
		String separador		= " - ";
		String tabulador		= "                 ";
		String cadenaReportada 	= "";
		
		if ( Cliente.trim( ).length( ) == 0 ){
			Cliente = "PTB";
		}
		
		switch ( mensaje ){
			case ALARMA:
				cadenaReportada = marcaDeFecha + separador + Cliente + separador + "alm: ";	
				break;
			case AVISO:
				cadenaReportada = marcaDeFecha + separador + Cliente + separador + "avs: ";	
				break;
			case INFORMACION:
				cadenaReportada = marcaDeFecha + separador + Cliente + separador + "inf: ";	
				break;
			case ERROR:
				cadenaReportada = marcaDeFecha + separador + Cliente + separador + "err: ";	
				break;
			case TABULADOR:
				cadenaReportada = tabulador + separador + Cliente + separador + "     ";	
				break;
			default:
				cadenaReportada = tabulador + separador + Cliente + separador + "     ";	
				break;
		}
		
		
		return cadenaReportada + informacion;
	}
	
	private void EscribeAArchivo( String mensaje )
	{
		
		Writer escritor = null;
		
		ElArchivoExiste( );
		
		try{
			if ( creaArchivo ){
				// Crea el archivo.
				escritor = new BufferedWriter( new FileWriter( archivo ) );
			} else {
				// Lo abre en modo append.
				escritor = new BufferedWriter( new FileWriter( archivo , true ) );
			}
			
			escritor.append( mensaje + "\r\n" );
			escritor.flush( );
			escritor.close( );
			
		} catch ( Exception e ){
			error.clear( );
			error.add( "No se pudo guardar la informacion" );
			error.add( e.toString( ) );
			System.out.println( error.toString( ) );
		}
	}
	
	private void ElArchivoExiste()
	{
		
		File a = new File( archivo );
		if ( a.exists( ) ){
			creaArchivo = false;
		} else {
			creaArchivo = true;
		}
	}
	//endregion
	
}
