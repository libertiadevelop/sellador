package com.cys.ptb;

public interface ErrorLog
{
	public int getError();
	public String getDescripcionDelError();
	public String getLog();
}
