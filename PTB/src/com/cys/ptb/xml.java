package com.cys.ptb;

import java.io.File;

public class xml implements documento, ErrorLog {
	
	//region VARIABLES PRIVADAS
	String 	archivo;
	File 	fArchivo;
	int		error;
	String	dError;
	boolean resultadoOperacion;
	String  Metodo;
	
	StringBuilder log = new StringBuilder ( );
	
	//endregion
	//region PROPIEDADES
	@Override 
	public int getError ( ) {
		return error;
	}

	@Override
	public String getDescripcionDelError ( ) {
		return dError;
	}

	@Override
	public String getLog ( ) {
		return log.toString ( );
	}
	//endregion
	//region CONSTRUCTOR
	//endregion
	//region METODOS PUBLICOS
	@Override
	public boolean carga ( String rutaDelDocumento ) {
		
		inicializaMetodo ( "carga", "Iniciando la carga del documento" );
		
		fArchivo = new File(rutaDelDocumento);
		
		if ( !fArchivo.exists ( ) ){
			setError ( Errores.EL_ARCHIVO_NO_EXISTE.getNum ( ), Errores.EL_ARCHIVO_NO_EXISTE.getDes ( ), getMetodo ( ) );
		}
		
		setLog ( getMetodo ( ),"Finalizada la carga, el resultado fue:" + resultadoOperacion);
		
		setMetodo ( "" );
		
		return resultadoOperacion;
	}
	public boolean validaEsquema( String xsdPath ){
		
		setMetodo ( "Valida esquema" );
		
		if ( !resultadoOperacion ) return resultadoOperacion;
		
		File esquema = new File ( xsdPath );
		
		setLog ( getMetodo(), "Validando la existencia del esquema" );
		
		if ( !esquema.exists ( ) ) {
			// El esquema no existe, borra la informacion.
			resultadoOperacion = false;
			
			setError ( Errores.EL_ARCHIVO_NO_EXISTE.getNum ( ), Errores.EL_ARCHIVO_NO_EXISTE.getDes ( ), getMetodo ( ) );
			
			return resultadoOperacion;
		}
		
		setLog ( getMetodo(), "El esquema existe" );
		
		// Procede a validar el xml
		
		// TODO http://jesus.hernandez.abeleira.com/2014/03/validacion-de-ficheros-xml-contra-un.html
		
		
		return resultadoOperacion;
	}
	
	public boolean validaImportes(){
		if ( !resultadoOperacion ) return resultadoOperacion;
		//TODO Valida los importes
		return resultadoOperacion;
	}
	
	public boolean validaFecha( String FechaLimite ){
		if ( !resultadoOperacion ) return resultadoOperacion;
		//TODO Valida la fecha de expedición
		return resultadoOperacion;
	}
	//endregion
	//region METODOS DEFAULT
	void inicializaMetodo(String Met, String Mensaje){
		resultadoOperacion 	= true;
		cleanError ( );
		setMetodo ( Met );
		setLog ( Metodo, Mensaje );
	}
	void cleanError( ){
		error 	= 0;
		dError 	= "";
	}
	void setError ( int NumError, String DesError, String Metodo ){
		error			 	= NumError;
		dError 				= DesError;
		resultadoOperacion 	= false;
		
		log.append(Metodo + "-" + NumError + "-" + DesError );
	}
	void setLog (String metodo, String mensaje){
		log.append ( Utilidades.devuelveFecha ( ) + ":" + metodo + "-" + mensaje );
	}
	void setMetodo( String Met ){
		Metodo = Met;
	}
	String getMetodo(){
		return Metodo;
	}
	//endregion
	//region METODOS PRIVADOS
	//endregion


}