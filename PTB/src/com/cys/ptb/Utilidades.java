package com.cys.ptb;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utilidades
{
	static enum mensaje 
	{
		ERROR,
		AVISO,
		ALARMA,
		INFORMACION,
		TABULADOR
	}
	
	
	static enum nivelLog
	{
		UNO,
		DOS,
		TRES,
		CUATRO,
		CINCO
	}
	
	public static String devuelveFecha()
	{
		
		DateFormat	formato				= new SimpleDateFormat( "dd/MM/yy HH:mm:ss" );
		Date		fecha				= new Date();
		String		resultado			= formato.format( fecha );

		return resultado;
	}
}
