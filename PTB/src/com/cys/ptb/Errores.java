package com.cys.ptb;

public enum Errores
{
	EL_ARCHIVO_NO_EXISTE(101, "El archivo no se encuentra"),
	EL_XML_NO_ES_VALIDO	(201, "El xml esta mal construido");
	
	private final int 		numeroDeError;
	private final String 	DescripcionDeError;
	
	private Errores (int error, String descripcion )
	{
		this.numeroDeError 		= error;
		this.DescripcionDeError = descripcion;
	}
	
	public int getNum(){
		return numeroDeError;
	}
	public String getDes() {
		return DescripcionDeError;
	}
}
