package com.cys.ptb;

import java.util.ArrayList;

public class Validador
{
	//region VARIABLES PRIVADAS
	private ArrayList<String> resultadoDeValidacion = new ArrayList<String>( );
	//endregion
	
	//region PROPIEDADES
	public ArrayList<String> getValidaci�n()
	{
		return resultadoDeValidacion;
	}
	//endregion
	
	public boolean validaParametrosDeArranque ( String[] args, int NumeroDeParametros )
	{
		boolean resultado = true;
		
		//region 1 NUMERO DE PARAMETROS CORRECTO
		// valida que el n�mero de parametros sea correcto.
		if ( args.length != NumeroDeParametros ){
			resultado = false;
			resultadoDeValidacion.add( "El n�mero de parametros es incorrecto." );
			resultadoDeValidacion.add( "Se pasaron los siguientes parametros: " );
			int numParametro = 0;
			for ( String para : args ) {
				numParametro++;
				resultadoDeValidacion.add( "Parametro " + String.valueOf( numParametro ) + " : " + para );
			}
		}
		//endregion
		
		//region 2 VALIDA QUE LOS ARCHIVOS EXISTAN
		
		//endregion
		
		//region 3 LICENCIA CORRECTA
		
		//endregion
		
			// Si la licencia es valida entonces valida que la caducidad sea correcta, si no no valida esto.
		
			//region 3a VALIDA CADUCIDAD
		
			//endregion
		
		// region 4 VALIDA EL XML
		
		//endregion
		
		//region 5 VALIDA EL CERTIFICADO
		
		
		//region 5a AVISA DE QUE ESTA A PUNTO DE VENCER
		
		//endregion
		
		//endregion
		
		//region 6 VALIDA LOS IMPORTES
		
		//endregion
		
		return resultado;
	}
	
	public boolean validaArchivoXMLVsEsquema ( String xml, String esquema )
	{
		boolean resultadoValidacionXML = true;
		
		
		
		return resultadoValidacionXML;
	}
	
	public boolean validaImportesEnXML ( String xml )
	{
		boolean resultadoValidaImportesEnXML = true;
		
		
		
		return resultadoValidaImportesEnXML;
	}
	
}
