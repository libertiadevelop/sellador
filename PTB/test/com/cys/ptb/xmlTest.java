package com.cys.ptb;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.junit.Test;


public class xmlTest
{
	String ArchivoXMLInexistente 	= "ruta.xml";
	String ArchivoXMLOrigen			= "recursos\\origen.xml";
	String ArchivoXMLExistente		= "000014_0101023500_A_101_000585_20150320.xml";
	String ArchivoXSDOrigen			= "recursos\\cfdiv32.xsd";
	String ArchivoXSD				= "cfdiv32.xsd";
	String ArchivoXSDInexistente	= "noExiste.xsd";
	String ArchivoXMLDefectuoso		= "recursos\\defectuoso.xml";
	@Test
	public void testXmlElArchivoNoExiste ( ) {
		xml x = new xml();
		
		assertFalse ( "Se esperaba false el archivo no existe", x.carga ( "ruta.xml" ) );
		
		assertEquals ( "Se esperaba el error 101", 101, x.getError ( ) );
		
	}
	
	@Test
	public void testXmlElArchivoExiste() {
		xml x = new xml ( );
		borraArchivoXML ( );
		copiaArchivoXML ( );
		
		assertTrue ( "Se esperaba verdadero el archivo existe", x.carga ( ArchivoXMLExistente ) );
		
		assertEquals ( "Se esperaba vacio", 0, x.getError ( ) );
		borraArchivoXML ( );
	}
	
	@Test
	public void testXMLValidandoEsquemaArchivoNoExiste()
	{
		borraArchivoXML ( );
		borraArchivoXSD ( );

		copiaArchivoXML ( );
		
		xml x = new xml ( );
		assertTrue ( "Se esperaba verdadero el archivo existe", x.carga ( ArchivoXMLExistente ));
		assertFalse ( "Se esperaba falso el esquema no existe", x.validaEsquema ( ArchivoXSDInexistente ) );
		
		assertEquals ( "Se esperaba error de archivo no existe", 101, x.getError ( ) );
		
		borraArchivoXSD ( );
		borraArchivoXML ( );
	}
	
	@Test
	public void testXMLValidandoEsquemaArchivoExiste()
	{
		borraArchivoXML ( );
		borraArchivoXSD ( );

		copiaArchivoXML ( );
		copiaArchivoXSD ( );
		
		xml x = new xml ( );
		assertTrue ( "Se esperaba verdadero el archivo existe", x.carga ( ArchivoXMLExistente ));
		assertTrue ( "Se esperaba verdadero el esquema no existe", x.validaEsquema ( ArchivoXSD ) );
		
		assertEquals ( "Se esperaba 0 el archivo existe", 0, x.getError ( ) );
		
		borraArchivoXSD ( );
		borraArchivoXML ( );
	}
	
	@Test
	public void testXMLValidandoEsquemaArchivoErroneo()
	{
		borraArchivoXML ( );
		borraArchivoXSD ( );
		borraXmlEsquemaErroneo ( );
		
		copiaArchivoXSD ( );
		copiaXmlEsquemaErroneo ( );
		
		xml x = new xml ( );
		assertTrue ( "Se esperaba verdadero el archivo existe", x.carga ( ArchivoXMLExistente ));
		assertFalse ( "Se esperaba falso el archivo tiene el esquema mal", x.validaEsquema ( ArchivoXSD ) );
		
		assertEquals ( "Se esperaba 201 el archivo esta mal", 201, x.getError ( ) );
		
		
		borraArchivoXML ( );
		borraArchivoXSD ( );
		borraXmlEsquemaErroneo ( );
	}
	
	@Test
	public void testXMLValidandoEsquemaArchivoCorrecto()
	{
		
	}
	
	private void copiaArchivoXML(){
		CopiaArchivos ( ArchivoXMLExistente, ArchivoXMLOrigen );
	}
	
	private void borraArchivoXML() {
		File d = new File(ArchivoXMLExistente);
		d.delete ( );
	}
	
	private void copiaArchivoXSD(){
		CopiaArchivos ( ArchivoXSD, ArchivoXSDOrigen );
	}
	
	private void borraArchivoXSD() {
		File d = new File(ArchivoXSD);
		d.delete ( );
	}
	
	private void copiaXmlEsquemaErroneo()
	{
		CopiaArchivos ( ArchivoXMLExistente, ArchivoXMLDefectuoso );
	}
	
	private void borraXmlEsquemaErroneo()
	{
		File d = new File(ArchivoXMLDefectuoso);
		d.delete ( );
	}
	
	public boolean CopiaArchivos( String Destino, String archivo )
	{
		boolean resultadoCopiaArchivos = false;
		
		InputStream 	iOrigen = null;
		OutputStream	iDestino = null;
		
		try{
			
			File fOrigen 	= new File( archivo );
			File fDestino 	= new File( Destino ); 
			
			if ( fDestino.exists() ) fDestino.delete();
			
			iOrigen 	= new FileInputStream( fOrigen );
			iDestino	= new FileOutputStream( fDestino );
			
			byte[] buffer = new byte[1024];
			
			int length;
			
			while ( ( length = iOrigen.read( buffer ) )  > 0 ){
				iDestino.write( buffer, 0, length );
			}
			
			iOrigen.close();
			iDestino.close();
						
			resultadoCopiaArchivos = true;
		} catch ( Exception e ){
			resultadoCopiaArchivos = false;
		} finally {
			try {
				if ( iOrigen != null ) iOrigen.close();
				if ( iDestino != null ) iDestino.close();
			} catch ( Exception e ){
				resultadoCopiaArchivos = false;
			}
		}
		
		return resultadoCopiaArchivos;	
	}
	
}
