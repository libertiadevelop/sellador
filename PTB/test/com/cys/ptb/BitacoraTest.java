package com.cys.ptb;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import com.cys.ptb.Utilidades.mensaje;

public class BitacoraTest
{	
	
	@Test
	public void ConstructorVacio ( ) {
		File l = new File( "log.t" );
		// Valida que no exista el archivo
		if ( l.exists( ) ){
			l.delete( );
		}
		
		Bitacora  b = new Bitacora( );
		
		b.Reporta( "Hola mundo", "", mensaje.INFORMACION );
		
		assertTrue( "Se debio crear el archivo", l.exists(  ) );
		// Una vez pasada la prueba elimina el archivo, para no afectar las posteriores pruebas.
		if ( l.exists( ) ){
			l.delete( );
		}
	}

	@Test
	public void ConstructorConArchivo ( ) {
		
		String NombreDeArchivo = "hoy.t";
		
		File l = new File( NombreDeArchivo );
		// Valida que no exista el archivo
		if ( l.exists( ) ){
			l.delete( );
		}
		
		Bitacora  b = new Bitacora( NombreDeArchivo );
		
		b.Reporta( "Hola mundo", "", mensaje.INFORMACION );
		
		assertTrue( "Se debio crear el archivo", l.exists(  ) );
		// Una vez pasada la prueba elimina el archivo, para no afectar las posteriores pruebas.
		if ( l.exists( ) ){
			l.delete( );
		}
	}
	@Test
	public void UnSoloComentarioConstructorDefault ( ) throws IOException {
		// Se usa el constructor default
		File l = new File( "log.t" );
		// Valida que no exista el archivo
		if ( l.exists( ) ){
			l.delete( );
		}
		// Crea los datos de inicio de la prueba
		String UnComentario 			= "Es solo una linea";
		String UnComentarioDebeMostrar 	= Utilidades.devuelveFecha( ) + " - PTB - inf: " + UnComentario ;
		
		Bitacora  b = new Bitacora( );
		
		b.Reporta( UnComentario, "", mensaje.INFORMACION );
		
		assertTrue( "Se debio crear el archivo", l.exists(  ) );
		
		BufferedReader br = new BufferedReader( new FileReader( "log.t" ) );
		String line = br.readLine( );
		
		assertEquals( "La cadena no es correcta", UnComentarioDebeMostrar, line );
		
		// Una vez pasada la prueba elimina el archivo, para no afectar las posteriores pruebas.
		br.close( );
		if ( l.exists( ) ){
			l.delete( );
		}	
	}
	@Test
	public void VariosComentariosConstructorDefault ( ) throws IOException {
		// Se usa el constructor default
		File l = new File( "log.t" );
		// Valida que no exista el archivo
		if ( l.exists( ) ){
			l.delete( );
		}
		// Crea los datos de inicio de la prueba
		
		HashMap<Integer, String> InformacionQueSeCarga		= new HashMap<Integer, String>( );
		HashMap<Integer, String> InformacionQueSeMuestra	= new HashMap<Integer, String>( );
		
		ArrayList<String> CadenaDeInformacionAPasar			= new ArrayList<String>( );
		
		InformacionQueSeCarga.put( 1, "Inicio de comentario" );
		InformacionQueSeMuestra.put(1,Utilidades.devuelveFecha( ) + " - PTB - inf: " + "Inicio de comentario" );
		InformacionQueSeCarga.put( 2, "Segunda linea" );
		InformacionQueSeMuestra.put(2,"                      Segunda linea");
		InformacionQueSeCarga.put( 3, "Tercera linea" );
		InformacionQueSeMuestra.put(3,"                      Tercera linea");
		InformacionQueSeCarga.put( 4, "Cuarta linea" );
		InformacionQueSeMuestra.put(4,"                      Cuarta linea");
		
		for ( int llave : InformacionQueSeCarga.keySet( ) ) {
			CadenaDeInformacionAPasar.add( InformacionQueSeCarga.get( llave ) );
		}
		
		Bitacora  b = new Bitacora( );
		
		b.Reporta( CadenaDeInformacionAPasar, "", mensaje.INFORMACION );
		
		assertTrue( "Se debio crear el archivo", l.exists(  ) );
		
		BufferedReader br = new BufferedReader( new FileReader( "log.t" ) );
		
		String line ="";
		
		while ( ( line = br.readLine( )) != null ){
			
		}
		
		for ( int llave : InformacionQueSeMuestra.keySet( ) ) {			
			
		}
		
		// Una vez pasada la prueba elimina el archivo, para no afectar las posteriores pruebas.
		br.close( );
		if ( l.exists( ) ){
			l.delete( );
		}		
	}
	
	
	
	
	
	

}
