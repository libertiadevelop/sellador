﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Utilidades
{
    public class Cadenas
    {
        public static String AcompletaLongitud ( String cadena, int longitudDeseada, String caracterParaAgregar, bool AlFinal ) {
            String resultado = "";

            int lengthCadena = cadena.Trim().Length;

            int dif = longitudDeseada - lengthCadena;

            if ( dif > 0 ) {

                String prov = cadena.Trim ( );

                for ( int i = 0 ; i < dif ; i++ ) {

                    if ( AlFinal ) {
                        prov += caracterParaAgregar;
                    } else {
                        prov = caracterParaAgregar + prov;
                    }
                }

                resultado = prov;

            } else {
                resultado = cadena.Trim ( );
            }

            return resultado;
        }

        public static bool esNumero ( String cadena ) {
            Regex regex = new Regex ( @"^[0-9]+$" );
            return regex.IsMatch ( cadena );
        }
    }
}
