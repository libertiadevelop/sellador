﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;

namespace DAO
{

    public enum basesDeDatos
    {
        ERP,
        MYSQL
    }

    public class dConexion
    {

        private static String conStringLocal    = "Dsn=ERP;Uid=System;Pwd=Manager;";
        private static String conStringWeb      = "Dsn=barmex;Uid=root;Pwd=root;";

        private static bool     estado;
        private static String   mensaje;

        public static bool getEstado ( ) {
            return estado;
        }

        public static String getMensaje ( ) {
            return mensaje;
        }

        public static OdbcConnection getConexion ( basesDeDatos b ) {

            setEstado ( true, "Iniciando" );

            OdbcConnection m = null;
            switch ( b ) {
                case basesDeDatos.ERP:
                    m = conectaERP ( );
                    break;
                case basesDeDatos.MYSQL:
                    m = conectaMySQL ( );
                    break;
            }

            return m;
        }

        private static OdbcConnection conectaERP ( ) {
            OdbcConnection m = new OdbcConnection ( );

            m.ConnectionString = conStringLocal;

            try {
                m.Open ( );
                setEstado ( true, "Conexion realizada" );
            } catch ( Exception mye ) {
                m = null;
                Console.WriteLine ( mye.ToString ( ) );
                setEstado ( false, mye.ToString ( ) );
            }

            return m;
        }

        private static OdbcConnection conectaMySQL ( ) {
            OdbcConnection m = new OdbcConnection ( );

            m.ConnectionString = conStringWeb;

            try {
                m.Open ( );
                setEstado ( true, "Conexion realizada" );
            } catch ( Exception mye ) {
                m = null;
                Console.WriteLine ( mye.ToString ( ) );
                setEstado ( false, mye.ToString ( ) );
            }

            return m;
        }

        public static bool closeConexion ( OdbcConnection m ) {
            bool resultado = true;

            setEstado ( true, "Cerrando" );

            try {
                m.Close ( );
                setEstado ( true, "Cerrada" );
            } catch ( OdbcException mye ) {
                setEstado ( false, mye.ToString ( ) );
                resultado = false;
            }


            return resultado;
        }

        private static void setEstado ( bool est, String Descripcion ) {
            estado = est;
            mensaje = Descripcion;
        }

    }
}