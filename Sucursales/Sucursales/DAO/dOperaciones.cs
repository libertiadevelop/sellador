﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;

namespace DAO
{
    public class dOperaciones
    {
        OdbcConnection m;

        public enum con
        {
            ERP,
            MYSQL
        }

        enum error
        {
            SIN_ERROR,
            NO_SE_PUDO_CONSULTAR
        }

        private bool    estado;
        private String  mensaje;
        private String  descripcionDeError;

        List<String> Parametros = new List<string> ( );
        List<String> Campos     = new List<string> ( );

        public static DataTable getDistribuidores ( ) {
            DataTable   resultado   = new DataTable ( );
            DataSet     mix         = new DataSet ( );

            resultado.Columns.Add ( "ID", typeof ( Int32 ) );
            resultado.Columns.Add ( "SUC", typeof ( String ) );
            resultado.Columns.Add ( "CLAVE", typeof ( String ) );
            resultado.Columns.Add ( "NOMBRE", typeof ( String ) );
            resultado.Columns.Add ( "USER", typeof ( String ) );
            resultado.Columns.Add ( "PASS", typeof ( String ) );
            resultado.Columns.Add ( "EXISTENCIAS", typeof ( Int16 ) );
            // resultado.Columns.Add ( "FACTURACION", typeof ( Int16 ) );


            var result = from dataRows1 in getDatosFromLocal ( ).AsEnumerable ( )
                         join dataRows2 in getDatosFromWeb ( ).AsEnumerable ( )
                         on dataRows1.Field<String> ( "CL_NUMCLI" ) equals dataRows2.Field<String> ( "CL_NUMCLI" ) into lj
                         from r in lj.DefaultIfEmpty ( )
                         select resultado.LoadDataRow ( new object [ ] 
                         {
                             r == null ? 0   : r.Field<Int32>("ID"),
                             dataRows1.Field<String>("CL_SUCU"),
                             dataRows1.Field<String>("CL_NUMCLI"),
                             dataRows1.Field<String>("CL_NOMCLI"),
                             r == null ? "-" : r.Field<String>("CLAVE_ACCESO"),
                             r == null ? "-" : r.Field<String>("PASSWORD"),
                             r == null ? 0   : r.Field<Int16>("EXISTENCIAS"),
                             //r == null ? 0   : r.Field<Int16>("FACTURACION"),
                         }, false );

            result.CopyToDataTable ( );

            return resultado;
        }

        private static DataTable getDatosFromWeb ( ) {
            DataTable resultado = new DataTable ( );

            String sentencia =  "SELECT      u.idusuario as ID, cl_numcli as CL_NUMCLI, " +
                                            "nombre AS CLAVE_ACCESO, clave AS PASSWORD, " +
                                            "comercio AS EXISTENCIAS, facturacion AS FACTURACION " +
                                "FROM		usuario u " +
                                "INNER JOIN	usuariocliente uc on u.idusuario = uc.idusuario " +
                                "WHERE      nombre LIKE '%E'";

            OdbcDataAdapter adap = new OdbcDataAdapter ( sentencia, dConexion.getConexion ( basesDeDatos.MYSQL ) );

            adap.Fill ( resultado );

            return resultado;
        }

        private static DataTable getDatosFromLocal ( ) {
            DataTable resultado = new DataTable ( );

            String sentencia = "SELECT CL_NUMCLI, CL_NOMCLI, CL_SUCU FROM MDGCLI02 WHERE CL_TIPO = 1";

            OdbcCommand com = new OdbcCommand ( sentencia );
            com.Connection = dConexion.getConexion ( basesDeDatos.ERP );

            OdbcDataAdapter adap = new OdbcDataAdapter ( );
            adap.SelectCommand = com;
            adap.SelectCommand.Connection = dConexion.getConexion ( basesDeDatos.ERP);

            adap.Fill ( resultado );

            return resultado;
        }

        public bool getEstado ( ) {
            return estado;
        }

        public String getMensaje ( ) {
            return mensaje;
        }

        public String getDescripcionDeError ( ) {
            return descripcionDeError;
        }

        public dOperaciones ( con c ) {
            switch ( c ) {
                case con.ERP:
                    m = dConexion.getConexion ( basesDeDatos.ERP );
                    break;
                case con.MYSQL:
                    m = dConexion.getConexion ( basesDeDatos.MYSQL );
                    break;
            }
        }

        public DataTable Consulta ( String consulta ) {
            DataTable resultado = new DataTable ( );

            OdbcCommand com = new OdbcCommand ( consulta );

            com.Connection = m;
            int con = 0;
            foreach ( String llave in Parametros ) {
                com.Parameters.AddWithValue ("@"+Campos[con], llave );
                con++;
                //com.Parameters.Add ( llave );
            }

            OdbcDataAdapter mad = null;

            try {

                setEstado ( true, error.SIN_ERROR, "Iniciando consulta" );

                mad = new OdbcDataAdapter ( com );

                mad.Fill ( resultado );

                setEstado ( true, error.SIN_ERROR, "Fin de consulta" );

            } catch ( OdbcException mye ) {
                setEstado ( false, error.NO_SE_PUDO_CONSULTAR, mye.ToString ( ) );
            } catch ( Exception e ) {
                setEstado ( false, error.NO_SE_PUDO_CONSULTAR, e.ToString ( ) );
            }

            return resultado;
        }

        public int insert ( String sentencia ) {
            OdbcCommand  com = new OdbcCommand ( sentencia );
            com.Connection = m;
            int resultado = 0;
            int con = 0;
            foreach ( String llave in Parametros ) {
                com.Parameters.AddWithValue ( "@" + Campos[con], llave );
                con++;
            }

            try {
                resultado = com.ExecuteNonQuery ( );
            } catch ( Exception e ) {
                setEstado ( false, error.NO_SE_PUDO_CONSULTAR, e.ToString ( ) );
                resultado = -1;
            }
            return resultado;
        }

        public int update ( String sentencia ) {
            OdbcCommand  com = new OdbcCommand ( sentencia );
            com.Connection = m;
            int resultado = 0;
            int c = 0;

            foreach ( String llave in Parametros ) {
                com.Parameters.AddWithValue ( "@" + Campos[0], llave );
                c++;
            }

            try {
                resultado = com.ExecuteNonQuery ( );
            } catch ( Exception e ) {
                setEstado ( false, error.NO_SE_PUDO_CONSULTAR, e.ToString ( ) );
                resultado = -1;
            }
            return resultado;
        }

        public void limpiaArreglos ( ) {
            Parametros.Clear ( );
            Campos.Clear ( );
        }

        public void addNombreCampo ( String nombreDeCampo ) {
            Campos.Add ( nombreDeCampo );
        }

        public void addParametro ( String parametro ) {
            Parametros.Add ( parametro );
        }

        private void setEstado ( bool est, error er, String Mensaje ) {
            estado = est;
            mensaje = Mensaje;
            descripcionDeError = getDescripcionError ( er );
        }

        private String getDescripcionError ( error e ) {
            String resultado = "";
            switch ( e ) {
                case error.SIN_ERROR:
                    resultado = "";
                    break;
                case error.NO_SE_PUDO_CONSULTAR:
                    resultado = "Error al consultar";
                    break;
            }
            return resultado;
        }

        public void closeConnection ( ) {
            if ( m != null ) {
                dConexion.closeConexion ( m );
            }
        }

    }
}
