﻿using System;
using System.Windows.Forms;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlador
{
    public interface iRelacionView
    {
        void setGridSucursales ( DataTable sucursales );
        void setGridAlmacenes ( DataTable almacenes );
    }
}
