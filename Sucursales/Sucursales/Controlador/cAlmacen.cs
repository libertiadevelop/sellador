﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlador
{
    public class cAlmacen
    {
        iAlmacenView _v;

        public cAlmacen ( iAlmacenView vista ) {
            _v = vista;
        }

        public void loadVista ( ) {
        
        }

        public void habilitarAlta ( ) {
            _v.ActivaAlta ( );
        }
        public void habilitaBaja ( ) {
            _v.ActivaBaja ( );
        }
        public void habilitaCambio ( ) {
            _v.ActivaEdicion ( );
        }
        public void Acepta ( ) {
            _v.Acepta ( );
        }
        public void Cancela ( ) {
            _v.Cancela ( );
        }

    }
}
