﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlador
{
    public interface iModo
    {
        void ActivaEdicion ( );
        void ActivaAlta ( );
        void ActivaBaja ( );
        void Acepta ( );
        void Cancela ( );
    }
}
