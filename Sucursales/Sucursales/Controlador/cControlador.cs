﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;

namespace Controlador
{
    public class cControlador
    {
        mUsuario u = null;

        public cControlador ( String ClaveUsuario ) {
            u = new mUsuario ( ClaveUsuario );
        }

        public String getNombre ( ) {
            return ( u.Nombre ) ;
        }
        public String getAlmacen ( ) {
            return u.almacen;
        }
        public String getClave ( ) {
            return u.clave;
        }
        public bool estado ( ) {
            return u.esActivo;
        }
    }
}
