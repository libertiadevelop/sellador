﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;

namespace Controlador
{
    public class cRelacion 
    {

        iRelacionView _vr;

        public cRelacion ( iRelacionView vista ) {
            _vr = vista;
        }

        public void inicializaLaVista ( ) {
            _vr.setGridSucursales ( mSucursal.getSucursales ( "00" ) );
        }

        public void getAlmacenesSucursal ( String sucursal ) {
            _vr.setGridAlmacenes ( ( new mSucursal ( sucursal ) ).getAlmacenes ( ) );
        }

    }
}
