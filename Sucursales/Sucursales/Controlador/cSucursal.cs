﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;

namespace Controlador
{
    public class cSucursal
    {
        iSucursalView   _v;
        String          Clave_sucursal;


        public enum enDondeSeInserta
        {
            EN_ERP,
            EN_MYSQL,
            EN_AMBOS
        }

        public cSucursal ( iSucursalView vista ) {
            _v = vista;
            _v.setController ( this );
        }

        public void loadVista ( ) {
            _v.setGridSucursales ( mSucursal.getSucursales ( "00" ) );
        }

        public void habilitarAlta() {
            _v.ActivaAlta ( );
        }
        public void habilitaBaja ( ) {
            _v.ActivaBaja ( );
        }
        public void habilitaCambio ( ) {
            _v.ActivaEdicion ( );
        }
        public void Acepta ( ) {
            _v.Acepta ( );
        }
        public void Cancela ( ) {
            _v.Cancela ( );
        }

        public void setController ( ) {
        
        }

        public bool insertaRegistro ( enDondeSeInserta d, DataRow datos ) {
            bool resultado = false;
            mSucursal.dondeInserto en = mSucursal.dondeInserto.EN_AMBOS;

            switch ( d ) {
                case enDondeSeInserta.EN_AMBOS:
                    en = mSucursal.dondeInserto.EN_AMBOS;
                    break;
                case enDondeSeInserta.EN_ERP:
                    en = mSucursal.dondeInserto.EN_ERP;
                    break;
                case enDondeSeInserta.EN_MYSQL:
                    en = mSucursal.dondeInserto.EN_MYSQL;
                    break;
            }

            resultado = mSucursal.insertaSucursal( en, datos);

            return resultado;

        }

        public bool actualizaSucursal( String cve_sucursal, Dictionary<String,String> cambios, List<String> anexos ){
            bool resultado = false;

            mSucursal s = new mSucursal ( cve_sucursal );

            resultado = s.actualizaSucursal ( cambios, anexos );

            if ( resultado ) {
                _v.setGridSucursales ( mSucursal.getSucursales ( "00" ) );
            }

            return resultado;
        }

        public bool borraRegistros ( List<String> sucursalesABorrar ) {

            return mSucursal.eliminaSucursales ( sucursalesABorrar );
        }

    }
}
