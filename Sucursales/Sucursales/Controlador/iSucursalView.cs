﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlador
{
    public interface iSucursalView : iModo
    {
        void setGridSucursales ( DataTable Sucursales );
        void setController ( cSucursal c );
    }
}
