﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAO;

namespace Modelo
{
    public class mUsuario
    {

        String  claveUsuario;
        String  NombreDeusuario;
        String  accesoAlmacen;
        bool    activo;

        public String Nombre {
            get {
                return NombreDeusuario;
            }    
        }

        public String almacen {
            get {
                return accesoAlmacen;
            }
        }

        public String clave {
            get {
                return claveUsuario;
            }
        }

        public bool esActivo {
            get {
                return activo;
            }
        }

        public mUsuario ( String ClaveUsuario ) {
            claveUsuario = ClaveUsuario;
            if ( String.IsNullOrEmpty ( claveUsuario ) ) {
                UsuarioInactivo ( );
            } else {
                CargaDatosUsuario();
            }
        }

        private void CargaDatosUsuario ( ) {
            dOperaciones d = new dOperaciones ( dOperaciones.con.MYSQL );
   
            String sentencia = "SELECT NOMUSUARIO, ACCESOSUC, ESTATUS FROM BARUSU01 WHERE CVEUSUARIO = ?";

            d.limpiaArreglos ( );
            d.addNombreCampo ( "CVEUSAURIO" );
            d.addParametro ( claveUsuario );


            DataTable RES = d.Consulta ( sentencia );

            if ( RES.Rows.Count == 1 ) {
                foreach ( DataRow r in RES.Rows ) {
                    NombreDeusuario = r [0].ToString();
                    accesoAlmacen = r [1].ToString ( );
                    activo = ( r [2].ToString ( ).Equals ( "A" ) ) ? true : false;
                }
            } else {
                UsuarioInactivo ( );
            }

        }

        private void UsuarioInactivo ( ) {
            NombreDeusuario = "N/A";
            accesoAlmacen = "01";
            activo = false;
        }

    }
}
