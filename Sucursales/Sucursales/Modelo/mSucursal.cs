﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilidades;
using DAO;

namespace Modelo
{
    public class mSucursal
    {

        #region METODOS ESTATICOS

        public static DataTable getSucursales ( String suc ) {
            
            DataTable resultado = new DataTable ( );

            String consultaErpMaestro       = "SELECT SUBSTR(TB_LLAV,4) AS SUC, TB_INFO FROM MDGDIC01";
            String consultaErpDetalle       = "SELECT * FROM MDGDIC07";
            String consultaMySQL            = "SELECT * FROM BARADM20";

            String sentenciaWhereERPMaestro = "WHERE TB_LLAV LIKE '07.%'";
            String sentenciaWhereERPDetalle = "";
            String sentenciaWhereMySQL      = "";

            if ( !suc.Equals ( "00" ) ) {
                // Se pide una sucursal en particular
                sentenciaWhereERPMaestro    = sentenciaWhereERPMaestro + " AND TB_LLAV = '07." + Utilidades.Cadenas.AcompletaLongitud(suc,2,"0",false) + "'";
                sentenciaWhereERPDetalle    = "WHERE TB07_SUC = '" + Utilidades.Cadenas.AcompletaLongitud ( suc, 2, "0", false ) + "'";
                sentenciaWhereMySQL         = "WHERE CVE_SUCURSAL = " + int.Parse(suc).ToString() + "";
            }

            String e = " ";

            consultaErpMaestro  = consultaErpMaestro    + e + sentenciaWhereERPMaestro  ;
            consultaErpDetalle  = consultaErpDetalle    + e + sentenciaWhereERPDetalle  ;
            consultaMySQL       = consultaMySQL         + e + sentenciaWhereMySQL       ;

            // Se carga la información de las tablas involucradas

            dOperaciones dERP   = new dOperaciones ( dOperaciones.con.ERP );
            dOperaciones dMysql = new dOperaciones ( dOperaciones.con.MYSQL );

            DataTable erpMaestro    = dERP.Consulta ( consultaErpMaestro );
            DataTable erpDetalle    = dERP.Consulta ( consultaErpDetalle );
            DataTable mysqlMaestro  = dMysql.Consulta ( consultaMySQL );
            DataColumn[] keyColumn = new DataColumn [1];
            keyColumn [0] = mysqlMaestro.Columns [0];
            mysqlMaestro.PrimaryKey = keyColumn;

            dERP.closeConnection ( );
            dMysql.closeConnection ( );

            // combina las tablas para mostrar los resultados.

            var erp =   (from maestro in erpMaestro.AsEnumerable ( )
                        join detalle in erpDetalle.AsEnumerable ( ) on maestro.Field<String> ( "SUC" ) equals detalle.Field<String> ( "TB07_SUC" )
                        select new {
                            CVE_SUCURSAL = int.Parse( detalle.Field<string> ( "TB07_SUC" ) ),
                            DESCRIPCION = detalle.Field<string> ( "TB07_DESC" ),
                            TIPO = detalle.Field<Int16> ( "TB07_TIPO" ).ToString(),
                            NOMBRE = detalle.Field<string> ( "TB07_NOMBRE" ),
                            RFC = detalle.Field<string> ( "TB07_RFC" ),
                            CALLE = detalle.Field<string> ( "TB07_CALLE" ),
                            NO_EXT = detalle.Field<string> ( "TB07_NOEXTERIOR" ),
                            NO_INT = detalle.Field<string> ( "TB07_NOINTERIOR" ),
                            COLONIA = detalle.Field<string> ( "TB07_COLONIA" ),
                            REFERENCIA = detalle.Field<string> ( "TB07_REFERENCIA" ),
                            MUNICIPIO = detalle.Field<string> ( "TB07_MUNICIPIO" ),
                            ESTADO = detalle.Field<string> ( "TB07_ESTADO" ),
                            PAIS = detalle.Field<string> ( "TB07_PAIS" ),
                            CODIGO_POSTAL = detalle.Field<string> ( "TB07_CODIGOPOSTAL" ),
                            TELEFONO = detalle.Field<string> ( "TB07_TELEFONO" ),
                            CORREO = detalle.Field<string> ( "TB07_CORREOELECT" ),
                            SITIOWEB = detalle.Field<string> ( "TB07_SITIOWEB" ),
                            DIRSERVIDOR = detalle.Field<string> ( "TB07_DIRSERVIDOR" ),
                            DIRPC_1 = detalle.Field<string> ( "TB07_DIRPC" ),
                            DIRPC_2 = detalle.Field<string> ( "TB07_DIRPC2" ),
                            TB_INFO = maestro.Field<string> ( "TB_INFO" ),
                            TB_LLAVE = maestro.Field<string>("SUC"),
                            RESPONSABLE = ""
                        });

            DataColumn[] llave = new DataColumn[1];

            resultado.Columns.Add ( "CLAVE",typeof(String));              //0
            llave[0] = resultado.Columns[0];
            resultado.Columns.Add ( "DESCRIPCION",typeof(String));        //1
            resultado.Columns.Add ( "TIPO",typeof(Int16));                //2
            resultado.Columns.Add ( "NOMBRE", typeof ( String ) );      //3
            resultado.Columns.Add ( "RFC", typeof ( String ) );         //4
            resultado.Columns.Add ( "CALLE", typeof ( String ) );       //5
            resultado.Columns.Add ( "NO-EXT", typeof ( String ) );      //6
            resultado.Columns.Add ( "NO-INT", typeof ( String ) );      //7
            resultado.Columns.Add ( "COLONIA", typeof ( String ) );     //8
            resultado.Columns.Add ( "REFERENCIA", typeof ( String ) );  //9
            resultado.Columns.Add ( "MUNICIPIO", typeof ( String ) );   //10
            resultado.Columns.Add ( "ESTADO", typeof ( String ) );      //11
            resultado.Columns.Add ( "PAIS", typeof ( String ) );        //12
            resultado.Columns.Add ( "CP", typeof ( String ) );          //13
            resultado.Columns.Add ( "TELEFONO", typeof ( String ) );    //14
            resultado.Columns.Add ( "CORREO", typeof ( String ) );      //15
            resultado.Columns.Add ( "SITIO", typeof ( String ) );       //16
            resultado.Columns.Add ( "SERVIDOR", typeof ( String ) );    //17
            resultado.Columns.Add ( "PC_1", typeof ( String ) );        //18
            resultado.Columns.Add ( "PC_2", typeof ( String ) );        //19
            resultado.Columns.Add ( "CVE MOSTRADOR", typeof ( String ) );        //20
            resultado.Columns.Add ( "CVE SUCURSAL", typeof ( String ) );        //21
            resultado.Columns.Add ( "RESPONSABLE", typeof ( String ) ); //22
            resultado.Columns.Add ( "EN_MYSQL", typeof ( int ) );       //23

            resultado.PrimaryKey = llave;
            
            foreach ( var item in erp ) {
                int cve_sucursal = item.CVE_SUCURSAL;

                bool cargaDatosDeErp = false;

                DataRow f = null;

                if ( mysqlMaestro.Rows.Count == 0 ) {
                    cargaDatosDeErp = true;
                } else {
                    Object[] toFind = new Object [1];
                    toFind [0] = cve_sucursal;
                    f = mysqlMaestro.Rows.Find ( toFind );
                    cargaDatosDeErp = ( f != null ) ? false : true;   
                }

                DataRow nf = resultado.NewRow ( );

                if ( !cargaDatosDeErp ) {

                    Dictionary<mSucursal.camposDic01, String> info = mSucursal.ComponeCamposDic01(f.Field<Int32>("CVE_SUCURSAL").ToString(), f.Field<String>("NOMBRE"), f.Field<String>("CLAVE_MOSTRADOR"), f.Field<String>("CLAVE_SUCURSAL"));
                    // Carga los datos de mysql
                    nf [0] = f.Field<int>("CVE_SUCURSAL");
                    nf [1] = f.Field<String>("DESCRIPCION");
                    nf [2] = f.Field<int>("CVE_TIPO");
                    nf [3] = f.Field<String>("NOMBRE");
                    nf [4] = f.Field<String>("RFC");
                    nf [5] = f.Field<String>("CALLE");
                    nf [6] = f.Field<String>("NO_EXTERIOR");
                    nf [7] = f.Field<String>("NO_INTERIOR");
                    nf [8] = f.Field<String>("COLONIA");
                    nf [9] = f.Field<String>("REFERENCIA");
                    nf [10] = f.Field<String>("MUNICIPIO");
                    nf [11] = f.Field<String>("ESTADO");
                    nf [12] = f.Field<Int32>("CVE_PAIS");
                    nf [13] = f.Field<String>("CODIGO_POSTAL");
                    nf [14] = f.Field<String>("TELEFONO");
                    nf [15] = f.Field<String>("CORREO_ELECTRONICO");
                    nf [16] = f.Field<String>("UBICACION_CFDI_SERVER");
                    nf [17] = f.Field<String>("UBICACION_CFDI_SERVER_BACKUP");
                    nf [18] = f.Field<String>("UBICACION_CFDI_PC");
                    nf [19] = f.Field<String>("UBICACION_CFDI_PC_BACKUP");
                    nf [20] = f.Field<String>("CLAVE_MOSTRADOR" );
                    nf [21] = f.Field<String>("CLAVE_SUCURSAL" );
                    nf [22] = f.Field<String>("RESPONSABLE" );
                    nf [23] = "1";

                } else {
                    // Carga los datos de erp
                    nf [0] =  item.CVE_SUCURSAL;
                    nf [1] =  item.DESCRIPCION;
                    nf [2] =  item.TIPO;
                    nf [3] =  item.NOMBRE;
                    nf [4] =  item.RFC;
                    nf [5] =  item.CALLE;
                    nf [6] =  item.NO_EXT;
                    nf [7] =  item.NO_INT;
                    nf [8] =  item.COLONIA;
                    nf [9] =  item.REFERENCIA;
                    nf [10] = item.MUNICIPIO;
                    nf [11] = item.ESTADO;
                    nf [12] = item.PAIS;
                    nf [13] = item.CODIGO_POSTAL;
                    nf [14] = item.TELEFONO;
                    nf [15] = item.CORREO;
                    nf [16] = item.SITIOWEB;
                    nf [17] = item.DIRSERVIDOR;
                    nf [18] = item.DIRPC_1;
                    nf [19] = item.DIRPC_2;

                    Dictionary<mSucursal.llavesDic01,String> r = mSucursal.DescomponeLlavesDic01 ( item.TB_LLAVE,item.TB_INFO );

                    nf [20] = r [llavesDic01.CLAVE_MOSTRADOR].ToString ( );
                    nf [21] = r [llavesDic01.CLAVE_SUCURSAL].ToString ( );
                    nf [22] = item.RESPONSABLE;
                    nf [23] = "0";

                }

                resultado.Rows.Add ( nf );

            }
           
            return resultado;
        }

        public static bool insertaSucursal ( dondeInserto d, DataRow r ) {
            bool resultado  = false;

            loadMetaDatoERP ( );

            String sentencia = "";
            int registros = 0;
            dOperaciones db = null;

            switch ( d ) {
                case dondeInserto.EN_AMBOS:

                    db = new dOperaciones ( dOperaciones.con.ERP );
                    registros = db.insert ( generaInsert ( tabla.ERP_MAESTRO, r ).Replace ( "\\", "\\\\" ) );

                    if ( registros == 1 ){
                        registros += db.insert ( generaInsert ( tabla.ERP_DETALLE, r ).Replace ( "\\", "\\\\" ) );

                    } else {
                        return false;
                    }

                    db = new dOperaciones ( dOperaciones.con.MYSQL );
                    registros += db.insert ( generaInsert ( tabla.MYSQL, r ).Replace ( "\\", "\\\\" ) );

                    break;
                case dondeInserto.EN_ERP:
                    db = new dOperaciones ( dOperaciones.con.ERP );
                    registros = db.insert ( generaInsert ( tabla.ERP_MAESTRO, r ).Replace ( "\\", "\\\\" ) );

                    if ( registros == 1 ){
                        registros += db.insert ( generaInsert ( tabla.ERP_DETALLE, r ).Replace ( "\\", "\\\\" ) );
                        resultado = ( registros == 2 ) ? true : false;
                    } else {
                        resultado = false;
                    }

                    break;
                case dondeInserto.EN_MYSQL:
                    sentencia = generaInsert ( tabla.MYSQL, r );
                    db = new dOperaciones ( dOperaciones.con.MYSQL );
                    registros = db.insert ( sentencia.Replace("\\","\\\\" ) );
                    resultado = ( registros == 1 ) ? true : false;
                    break;
            }

            resultado = ( registros > 0 ) ? true : false;

            return resultado;
        }

        public static bool eliminaSucursales ( List<String> listaDeSucursales ) {

            int registros = 0;

            foreach ( String cve in listaDeSucursales ) {
                
                String sentenciaMaestro = "DELETE FROM MDGDIC01 WHERE TB_LLAV = '07." + Utilidades.Cadenas.AcompletaLongitud ( cve, 2, "0", false ) + "'";
                String sentenciaDetalle = "DELETE FROM MDGDIC07 WHERE TB07_SUC = '" + Utilidades.Cadenas.AcompletaLongitud ( cve, 2, "0", false ) + "'";
                String sentenciaMysql   = "DELETE FROM BARADM20 WHERE CVE_SUCURSAL = " + cve;

                // Elimina del maestro
                dOperaciones erp = new dOperaciones ( dOperaciones.con.ERP );
                registros = erp.update ( sentenciaMaestro );
                // Elimina del detall
                registros += erp.update ( sentenciaDetalle );
                // Elimina del mysql
                dOperaciones m = new dOperaciones ( dOperaciones.con.MYSQL );
                registros += m.update ( sentenciaMysql );

            }

            return ( registros > 0 ) ? true : false;

        }

        public enum llavesDic01{
            CLAVE,
            NOMBRE,
            CLAVE_MOSTRADOR,
            CLAVE_SUCURSAL
        }

        public enum camposDic01{
            TB_LLAV,
            TB_INFO
        }

        public enum tabla
        {
            ERP_MAESTRO,
            ERP_DETALLE,
            MYSQL
        }

        public static Dictionary<llavesDic01, String> DescomponeLlavesDic01 ( String TBLLAV, String TB_INFO) {
            Dictionary<llavesDic01, String> resultado = new Dictionary<llavesDic01, string> ( );

            resultado.Add( llavesDic01.CLAVE, TBLLAV.Replace("07.","" ).Trim());
            
            int lenTbInfo = TB_INFO.Length;

            if ( lenTbInfo < 35 ) {
                resultado.Add ( llavesDic01.NOMBRE, TB_INFO.Substring ( 0 ).Trim ( ) );
                resultado.Add ( llavesDic01.CLAVE_MOSTRADOR, " " );
                resultado.Add ( llavesDic01.CLAVE_SUCURSAL, " " );
            } else if ( lenTbInfo < 47) {
                resultado.Add ( llavesDic01.NOMBRE, TB_INFO.Substring ( 0, 35 ).Trim ( ) );
                resultado.Add ( llavesDic01.CLAVE_MOSTRADOR, TB_INFO.Substring ( 35 ).Trim ( ) );
                resultado.Add ( llavesDic01.CLAVE_SUCURSAL, " ");
            } else {
                resultado.Add ( llavesDic01.NOMBRE, TB_INFO.Substring ( 0, 35 ).Trim ( ) );
                resultado.Add ( llavesDic01.CLAVE_MOSTRADOR, TB_INFO.Substring ( 35, 12 ).Trim ( ) );
                resultado.Add ( llavesDic01.CLAVE_SUCURSAL, TB_INFO.Substring ( 48 ).Trim ( ) );
            }

            return resultado;
        }

        public static Dictionary<camposDic01, String> ComponeCamposDic01 ( String sucursal, String nombre, String clave_mostrador, String clave_sucursal ) {
            Dictionary<camposDic01, String> resultado = new Dictionary<camposDic01, string> ( );
            
            resultado.Add ( camposDic01.TB_LLAV, "07." + Cadenas.AcompletaLongitud(sucursal, 2, "0", false ));

            resultado.Add( camposDic01.TB_INFO, Cadenas.AcompletaLongitud(nombre, 35, " ", true) + Cadenas.AcompletaLongitud(clave_mostrador,13," ", true) + Cadenas.AcompletaLongitud(clave_sucursal,5," ",true) ); 

            return resultado;
        }

        private static String generaInsert ( tabla t, DataRow r ) {
            String sentenciaInsert = "";

            String claveSucursal    = r[0].ToString ( );
            String descripcion      = r[1].ToString();
            String tipo             = r[2].ToString();
            String nombre           = r[3].ToString();
            String rfc              = r[4].ToString();
            String calle            = r[5].ToString();
            String noExt            = r[6].ToString();
            String noInt            = r[7].ToString();
            String colonia          = r[8].ToString();
            String referencia       = r[9].ToString();
            String municipio        = r[10].ToString();
            String estado           = r[11].ToString();
            String pais             = r[12].ToString();
            String cp               = r[13].ToString();
            String telefono         = r[14].ToString();
            String correo           = r[15].ToString();
            String sitio            = r[16].ToString();
            String servidor         = r [17].ToString ( );
            String PC_1             = r [18].ToString ( );
            String PC_2             = r [19].ToString ( );
            String cve_mostrador    = r[20].ToString();
            String cve_sucursal     = r[21].ToString();
            String responsable      = r[22].ToString();

            switch ( t ) {
                case tabla.ERP_MAESTRO:
                    Dictionary<camposDic01, String> datosAGuardar = ComponeCamposDic01( claveSucursal, nombre, cve_mostrador, cve_sucursal);
                    sentenciaInsert = "INSERT INTO MDGDIC01 (TB_LLAV, TB_INFO) VALUES ('" + datosAGuardar [camposDic01.TB_LLAV] + "','" + datosAGuardar [camposDic01.TB_INFO] + "')";
                    break;
                case tabla.ERP_DETALLE:

                    String prefijo  = "TB07_";

                    sentenciaInsert = "INSERT INTO MDGDIC07 (TB07_SUC, TB07_DESC, TB07_TIPO, " +
                                                            "TB07_NOMBRE, TB07_RFC, TB07_CALLE, " +
                                                            "TB07_NOEXTERIOR, TB07_NOINTERIOR, TB07_COLONIA, " +
                                                            "TB07_REFERENCIA, TB07_MUNICIPIO, TB07_ESTADO, " +
                                                            "TB07_PAIS, TB07_CODIGOPOSTAL, TB07_TELEFONO, " +
                                                            "TB07_CORREOELECT, TB07_SITIOWEB, TB07_DIRSERVIDOR, " +
                                                            "TB07_DIRPC, TB07_DIRPC2) " +
                                                    "VALUES ('" +
                                                            ajustaTextoParaERP ( prefijo + "SUC", Utilidades.Cadenas.AcompletaLongitud ( claveSucursal, 2, "0", false ) ) + "','" +
                                                            ajustaTextoParaERP ( prefijo + "DESC", descripcion ) + "','" +
                                                            ajustaTextoParaERP ( prefijo + "TIPO", tipo ) + "','" +

                                                            ajustaTextoParaERP ( prefijo + "NOMBRE", nombre ) + "','" +
                                                            ajustaTextoParaERP ( prefijo + "RFC", rfc ) + "','" +
                                                            ajustaTextoParaERP ( prefijo + "CALLE", calle ) + "','" +

                                                            ajustaTextoParaERP ( prefijo + "NOEXTERIOR", noExt ) + "','" +
                                                            ajustaTextoParaERP ( prefijo + "NOINTERIOR", noInt ) + "','" +
                                                            ajustaTextoParaERP ( prefijo + "COLONIA", colonia ) + "','" +

                                                            ajustaTextoParaERP ( prefijo + "REFERENCIA", referencia ) + "','" +
                                                            ajustaTextoParaERP ( prefijo + "MUNICIPIO", municipio ) + "','" +
                                                            ajustaTextoParaERP ( prefijo + "ESTADO", estado ) + "','" +

                                                            ajustaTextoParaERP ( prefijo + "PAIS", pais ) + "','" +
                                                            ajustaTextoParaERP ( prefijo + "CODIGOPOSTAL", cp ) + "','" +
                                                            ajustaTextoParaERP ( prefijo + "TELEFONO", telefono ) + "','" +

                                                            ajustaTextoParaERP ( prefijo + "CORREOELECT", correo ) + "','" +
                                                            ajustaTextoParaERP ( prefijo + "SITIOWEB", sitio ) + "','" +
                                                            ajustaTextoParaERP ( prefijo + "DIRSERVIDOR", servidor ) + "','" +

                                                            ajustaTextoParaERP ( prefijo + "DIRPC", PC_1 ) + "','" +
                                                            ajustaTextoParaERP ( prefijo + "DIRPC2", PC_2 ) + "')";

                    break;
                case tabla.MYSQL:
                    // Recupera la informacion del datarow y genera la sentencia insert, para guardar en la base de datos.
                    sentenciaInsert = "INSERT " +
                                        "INTO BARADM20  ( CVE_SUCURSAL, DESCRIPCION, CVE_TIPO, " +
                                                        " NOMBRE, RFC, CALLE, " +
                                                        " NO_EXTERIOR, NO_INTERIOR, COLONIA, " +
                                                        " REFERENCIA, MUNICIPIO, ESTADO, " +
                                                        " CVE_PAIS, CODIGO_POSTAL, TELEFONO, " +
                                                        " CORREO_ELECTRONICO, UBICACION_CFDI_SERVER, UBICACION_CFDI_SERVER_BACKUP, " +
                                                        " UBICACION_CFDI_PC, UBICACION_CFDI_PC_BACKUP, CLAVE_MOSTRADOR, " +
                                                        " CLAVE_SUCURSAL, RESPONSABLE) " +
                                        "VALUES (" +    claveSucursal + ",'" + descripcion + "'," + tipo + ",'" +
                                                        nombre + "','" + rfc + "','" + calle + "','" +
                                                        noExt + "','" + noInt + "','" + colonia + "','" +
                                                        referencia + "','" + municipio + "','" + estado + "','" +
                                                        pais + "','" + cp + "','" + telefono + "','" +
                                                        correo + "','" + sitio + "','" + servidor + "','" +
                                                        PC_1 + "','" + PC_2 + "','" + cve_mostrador + "','" +
                                                        cve_sucursal + "','" + responsable + "')";                    

                    break;
            }
            return sentenciaInsert;
        }

        #region CONFIGURACION DE DATOS EN DIC07
        private static Dictionary<String, int> NomCamposMaestroEnERP   = new Dictionary<String, int> ( );
        private static Dictionary<int, int>    lenCamposMaestroEnERP   = new Dictionary<int, int> ( );

        private static Dictionary<String, int> NomCamposDetalleEnERP   = new Dictionary<String, int> ( );
        private static Dictionary<int, int> lenCamposDetalleEnERP      = new Dictionary<int, int> ( );
        #endregion

        private static void loadMetaDatoERP ( ) {
            NomCamposMaestroEnERP.Add ( "TB_LLAV", 10 );
            NomCamposMaestroEnERP.Add ( "TB_INFO", 80 );

            lenCamposMaestroEnERP.Add ( 1, 10 );
            lenCamposMaestroEnERP.Add ( 2, 80 );

            String prefijo = "TB07_";

            NomCamposDetalleEnERP.Add ( prefijo + "SUC", 2 );
            NomCamposDetalleEnERP.Add ( prefijo + "DESC", 20 );
            NomCamposDetalleEnERP.Add ( prefijo + "TIPO", 1 );
            NomCamposDetalleEnERP.Add ( prefijo + "NOMBRE", 60 );
            NomCamposDetalleEnERP.Add ( prefijo + "RFC", 14 );
            NomCamposDetalleEnERP.Add ( prefijo + "CALLE", 60 );
            NomCamposDetalleEnERP.Add ( prefijo + "NOEXTERIOR", 30 );
            NomCamposDetalleEnERP.Add ( prefijo + "NOINTERIOR", 30 );
            NomCamposDetalleEnERP.Add ( prefijo + "COLONIA", 60 );
            NomCamposDetalleEnERP.Add ( prefijo + "REFERENCIA", 60 );
            NomCamposDetalleEnERP.Add ( prefijo + "MUNICIPIO", 50 );
            NomCamposDetalleEnERP.Add ( prefijo + "ESTADO", 30 );
            NomCamposDetalleEnERP.Add ( prefijo + "PAIS", 20 );
            NomCamposDetalleEnERP.Add ( prefijo + "CODIGOPOSTAL", 10 );
            NomCamposDetalleEnERP.Add ( prefijo + "TELEFONO", 80 );
            NomCamposDetalleEnERP.Add ( prefijo + "CORREOELECT", 80 );
            NomCamposDetalleEnERP.Add ( prefijo + "SITIOWEB", 80 );
            NomCamposDetalleEnERP.Add ( prefijo + "DIRSERVIDOR", 100 );
            NomCamposDetalleEnERP.Add ( prefijo + "DIRPC", 50 );
            NomCamposDetalleEnERP.Add ( prefijo + "DIRPC2", 50 );

            lenCamposDetalleEnERP.Add ( 1, 2 );
            lenCamposDetalleEnERP.Add ( 2, 20 );
            lenCamposDetalleEnERP.Add ( 3, 1 );
            lenCamposDetalleEnERP.Add ( 4, 60 );
            lenCamposDetalleEnERP.Add ( 5, 14 );
            lenCamposDetalleEnERP.Add ( 6, 60 );
            lenCamposDetalleEnERP.Add ( 7, 30 );
            lenCamposDetalleEnERP.Add ( 8, 30 );
            lenCamposDetalleEnERP.Add ( 9, 60 );
            lenCamposDetalleEnERP.Add ( 10, 60 );
            lenCamposDetalleEnERP.Add ( 11, 50 );
            lenCamposDetalleEnERP.Add ( 12, 30 );
            lenCamposDetalleEnERP.Add ( 13, 20 );
            lenCamposDetalleEnERP.Add ( 14, 10 );
            lenCamposDetalleEnERP.Add ( 15, 80 );
            lenCamposDetalleEnERP.Add ( 16, 80 );
            lenCamposDetalleEnERP.Add ( 17, 80 );
            lenCamposDetalleEnERP.Add ( 18, 100 );
            lenCamposDetalleEnERP.Add ( 19, 50 );
            lenCamposDetalleEnERP.Add ( 20, 50 );

        }

        private static String ajustaTextoParaERP ( String campo, String textoAAjustar ) {
            String resultado = "";

            int lenCampo = NomCamposDetalleEnERP [campo];

            if ( textoAAjustar.Length > lenCampo ) {
                resultado = textoAAjustar.Substring ( 0, NomCamposDetalleEnERP [campo] );
            } else {
                resultado = textoAAjustar;
            }

            
            return resultado;
        }

        private static void loadMetaDatoMySQL ( ) {

        }

        #endregion

        #region CONFIGURACION DE DATOS EN MYSQL
        Dictionary<int, String> NomCamposMySQL = new Dictionary<int, string> ( );
        Dictionary<int, int>    lenCamposMySQL = new Dictionary<int, int> ( );
        #endregion

        public enum dondeInserto
        {
            EN_ERP,
            EN_MYSQL,
            EN_AMBOS
        }

        DataTable datosSucursal = new DataTable ( );

        public DataTable getDatos {
            get {
                return datosSucursal;
            }
        }

        String Clave;

        public mSucursal ( String sucursal ) {
            datosSucursal   = mSucursal.getSucursales ( sucursal );
            Clave           = datosSucursal.Rows [0] [0].ToString ( );
        }

        public DataTable getAlmacenes ( ) {
            DataTable resultado = new DataTable ( );

            // Recupera el listado de almacenes del erp

            // Recupera el listado de almacenes de mysql



            return resultado;
        }

        public bool actualizaSucursal ( Dictionary<String, String> cambios, List<String> Anexos ) {
            bool resultado  = false;

            String esp = " ";
            String and = "AND";

            String cambiosERPMaestro = "";
            String cambiosERPDetalle = "";
            String cambiosMySQL      = "";

            int registros = 0;

            Dictionary<String, Dictionary<campoEnBaseDatos,String>> reglas = cargaReglasDeBaseDeDatos();

            bool seAlteroMaestro    = false;
            String nombre           = "";
            String claveMostrador   = "";
            String claveSucursal    = "";

            foreach ( String llave in cambios.Keys ) {
                String valor = cambios [llave];
                Dictionary<campoEnBaseDatos, String> dato = reglas [llave];

                String c_ERP_Maestro = dato [campoEnBaseDatos.ERP_MAESTRO];
                String c_ERP_Detalle = dato [campoEnBaseDatos.ERP_DETALLE];
                String c_MySQL       = dato [campoEnBaseDatos.MYSQL];
                #region CAMBIO EN MAESTRO
                if ( !String.IsNullOrEmpty ( c_ERP_Maestro ) ) {
                    String tipo     = c_ERP_Maestro.Substring ( 0, 2 ).Trim();
                    String campo    = c_ERP_Maestro.Substring ( 2 );

                    seAlteroMaestro = true;

                    claveMostrador  = ( campo.Equals("tClaveMostrador")) ? valor : "";
                    claveSucursal   = ( campo.Equals("tClaveSucursal")) ? valor : "";
                    nombre          = ( campo.Equals ( "tNombre" ) ) ? valor : "";

                }
                #endregion
                #region CAMBIO EN DETALLE
                if ( !String.IsNullOrEmpty ( c_ERP_Detalle ) ) {
                    String tipo     = c_ERP_Detalle.Substring ( 0, 2 ).Trim();
                    String campo    = c_ERP_Detalle.Substring ( 2 );

                    if ( !String.IsNullOrEmpty ( cambiosERPMaestro ) ) {
                        cambiosERPMaestro += and + esp;
                    }

                    switch ( tipo ) {
                        case "I":
                            cambiosERPDetalle += campo + "=" + valor + esp;
                            break;
                        case "S":
                            cambiosERPDetalle += campo + "='" + valor + "'" + esp;
                            break;
                    }
                }
                #endregion
                #region CAMBIO EN MYSQL
                if ( !String.IsNullOrEmpty ( c_MySQL ) ) {
                    String tipo     = c_MySQL.Substring ( 0, 2 ).Trim ( );
                    String campo    = c_MySQL.Substring ( 2 );

                    if ( !String.IsNullOrEmpty ( cambiosERPMaestro ) ) {
                        cambiosMySQL += and + esp;
                    }

                    switch ( tipo ) {
                        case "I":
                            cambiosMySQL += campo + "=" + valor + esp;
                            break;
                        case "S":
                            cambiosMySQL += campo + "='" + valor + "'" + esp;
                            break;
                    }
                }
                #endregion
            }
            #region GENERA MAESTRO
            if ( seAlteroMaestro ) {
                // Carga los datos para el maestro.
                cambiosERPMaestro = mSucursal.ComponeCamposDic01 ( Clave, nombre, claveMostrador, claveSucursal ) [camposDic01.TB_INFO];
            }
            #endregion

            // CARGA LA INFORMACION AL ERP
            if ( cambiosERPMaestro.Length > 0 || cambiosERPDetalle.Length > 0 ) {
                // manda informacion para el erp
                dOperaciones erp = new dOperaciones ( dOperaciones.con.ERP );
                String llaveMaestro     = "WHERE TB_LLAV = '07." + Utilidades.Cadenas.AcompletaLongitud ( Clave, 2, "0", false );
                String llaveDestalle    = "WHERE TB07_SUC = " + Clave;

                if ( cambiosERPMaestro.Length > 0 ) {
                    String sentenciaUpdateMaestro =  "UPDATE MDGDIC01 SET" + esp + cambiosERPMaestro + esp + llaveMaestro;
                    registros = erp.update ( sentenciaUpdateMaestro );
                }

                if ( cambiosERPDetalle.Length > 0 ) {
                    String sentenciaUpdateDetalle = "UPDATE MDGDIC07 SET" + esp + cambiosERPDetalle + esp + llaveDestalle;

                    registros += erp.update ( sentenciaUpdateDetalle );

                }

                erp.closeConnection ( );

            }

            // CARGA LA INFORMACION MYSQL
            if ( cambiosMySQL.Length > 0 ) {
                // manda informacion para el mysql
                dOperaciones mysql = new dOperaciones( dOperaciones.con.MYSQL );
                String llaveMysql   = "WHERE CVE_SUCURSAL = " + Clave;
                String sentenciaUpdateMysql = "UPDATE BARADM20 SET" + esp + cambiosMySQL + esp + llaveMysql;

                registros += mysql.update ( sentenciaUpdateMysql );

                mysql.closeConnection ( );
 
            }

            if ( registros > 0 ) {
                resultado = true;
            } else {
                resultado = false;
            }
            

            return resultado;
        }

        public bool insertaAlmacen ( ) {
            bool resultado = false;


            return resultado;
        }
        
        enum campoEnBaseDatos
        {
            ERP_MAESTRO,
            ERP_DETALLE,
            MYSQL
        }

        Dictionary<campoEnBaseDatos, String> reglaDescarga = new Dictionary<campoEnBaseDatos, string> ( );

        Dictionary<String, Dictionary<campoEnBaseDatos, String>> cargaReglasDeBaseDeDatos() {

            Dictionary<String, Dictionary<campoEnBaseDatos,String>> resultado = new Dictionary<string, Dictionary<campoEnBaseDatos, string>> ( );
            
            // campo en maestro, campo en detalle, campo en mysql

            resultado.Add ( "tClave",           setDatosDeCampos ( "S TB_LLAV",   "S TB07_SUC",             "I CVE_SUCURSAL" ));
            resultado.Add ( "tNombre",          setDatosDeCampos ( "S TB_INFO",   "S TB07_NOMBRE",          "S NOMBRE" ) );
            resultado.Add ( "tDescripcion",     setDatosDeCampos ( "",            "S TB07_DESC",            "S DESCRIPCION" ) );
            resultado.Add ( "tTipo",            setDatosDeCampos ( "",            "I TB07_TIPO",            "I CVE_TIPO" ) );
            resultado.Add ( "tPais",            setDatosDeCampos ( "",            "S TB07_PAIS",            "I CVE_PAIS" ) );
            resultado.Add ( "tEstado",          setDatosDeCampos ( "",            "S TB07_ESTADO",          "S ESTADO" ) );
            resultado.Add ( "tMunicipio",       setDatosDeCampos ( "",            "S TB07_MUNICIPIO",       "S MUNICIPIO" ) );
            resultado.Add ( "tCalle",           setDatosDeCampos ( "",            "S TB07_CALLE",           "S CALLE" ) );
            resultado.Add ( "tNoExt",           setDatosDeCampos ( "",            "I TB07_NOEXTERIOR",      "S NO_EXTERIOR" ) );
            resultado.Add ( "tNoInt",           setDatosDeCampos ( "",            "I TB07_NOINTERIOR",      "S NO_INTERIOR" ) );
            resultado.Add ( "tCP",              setDatosDeCampos ( "",            "S TB07_CODIGOPOSTAL",    "S CODIGO_POSTAL" ) );
            resultado.Add ( "tRFC",             setDatosDeCampos ( "",            "S TB07_RFC",             "S RFC" ) );
            resultado.Add ( "tColonia",         setDatosDeCampos ( "",            "S TB07_COLONIA",         "S COLONIA" ) );
            resultado.Add ( "tReferencia",      setDatosDeCampos ( "",            "S TB07_REFERENCIA",      "S REFERENCIA" ) );
            resultado.Add ( "tTelefono",        setDatosDeCampos ( "",            "S TB07_TELEFONO",        "S TELEFONO" ) );
            resultado.Add ( "tCorreo",          setDatosDeCampos ( "",            "S TB07_CORREOELECT",     "S CORREO_ELECTRONICO" ) );
            resultado.Add ( "tClaveMostrador",  setDatosDeCampos ( "S TB_INFO",   "",                       "S CLAVE_MOSTRADOR" ) );
            resultado.Add ( "tClaveSucursal",   setDatosDeCampos ( "S TB_INFO",   "",                       "S CLAVE_SUCURSAL" ) );
            resultado.Add ( "tDirServer",       setDatosDeCampos ( "",            "S TB07_SITIOWEB",        "S UBICACION_CFDI_SERVER" ) );
            resultado.Add ( "tDirBackServer",   setDatosDeCampos ( "",            "S TB07_DIRSERVIDOR",     "S UBICACION_CFDI_SERVER_BACKUP" ) );
            resultado.Add ( "tCFDIPC",          setDatosDeCampos ( "",            "S TB07_DIRPC",           "S UBICACION_CFDI_PC" ) );
            resultado.Add ( "tCFDIBackPC",      setDatosDeCampos ( "",            "S TB07_DIRPC2",          "S UBICACION_CFDI_PC_BACKUP" ) );
            resultado.Add ( "tResponsable",     setDatosDeCampos ( "",            "",                       "S RESPONSABLE" ) );
            
            return resultado;
        }

        Dictionary<campoEnBaseDatos, String> setDatosDeCampos ( String CampoEnMaestro, String CampoEnDetalle, String CampoEnMysql ) {
            Dictionary<campoEnBaseDatos, String> resultado = new Dictionary<campoEnBaseDatos, string> ( );
            resultado.Add ( campoEnBaseDatos.ERP_MAESTRO, CampoEnMaestro );
            resultado.Add ( campoEnBaseDatos.ERP_DETALLE, CampoEnDetalle );
            resultado.Add ( campoEnBaseDatos.MYSQL, CampoEnMysql );
            return resultado;
        }

    }
}
