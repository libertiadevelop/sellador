﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utilidades;
using DAO;

namespace Modelo
{
    public class mAlmacen
    {

        public enum campos
        {
            CVE_SUCURSAL,
            CVE_ALMACEN,
            CONSECUTIVO_EN_SUCURSAL,
            DESCRIPCION,
            OBSERVACIONES,
            CVE_RESPONSABLE,
            MANEJA_NUMEROS_DE_SERIE,
            MUESTRA_NUMEROS_DE_SERIE,
            ES_ALMACEN_DE_TRASPASO,
            MANEJA_LOTES,
            CUENTA_CONTABLE,
            SE_MUESTRA_EN_WEB,
            SUCURSAL_PARA_DEMANDA,
            CONECTADO,
            PERTENECE_A_LA_SUCURSAL,
            RESERVADO_MAURICIO,
            NO_SE_VISUALIZA,
            SE_CONSIDERA_PARA_ABASTECIMIENTO,
            NO_VENTAS,
            NO_ORDE_DE_COMPRA,
            SPARE
        }

        public enum camposERP
        {
            TB_LLAV,
            TB_INFO
        }

        public static DataTable getAlmacenes ( int Sucursal ) {

            DataTable resultado = new DataTable ( );

            String suc          = Cadenas.AcompletaLongitud ( Sucursal.ToString ( ), 2, "0", false );

            // Recupera el catalogo de almacenes.

            String almacenes    = "SELECT * FROM MDGDIC01 WHERE TB_LLAV LIKE '09." + suc + "'";

            String nombres      = "SELECT * FROM MDGDIC01 WHERE TB_LLAV LIKE '08.%'";

            String fromMySQL    = "SELECT * FROM BARADM21 WHERE CVE_SUCURSAL = " + Sucursal;

            dOperaciones derp   = new dOperaciones ( dOperaciones.con.ERP );
            dOperaciones dmys   = new dOperaciones ( dOperaciones.con.MYSQL );

            DataTable dNombres  = derp.Consulta ( nombres );
            DataTable dRelacion = derp.Consulta ( almacenes );

            DataTable dMySQL    = dmys.Consulta ( fromMySQL );

            derp.closeConnection ( );
            dmys.closeConnection ( );

            // Procede a cargar la informacion a la tabla que se regresa.

            resultado = GetTablaParaEncabezado ( );

            if ( dMySQL.Rows.Count > 0 ) {
                // La informacion la carga del mysql
                foreach ( DataRow r in dMySQL.Rows ) {
                    DataRow n = resultado.NewRow ( );

                    n[1] = r[2];
                    n[2] = r[3];
                    n[3] = r[4];
                    n[4] = r[5];
                    n[5] = r[6];
                    n[6] = r[7];
                    n[7] = r[8];
                    n[8] = r[9];
                    n[9] = r[10];
                    n[10] = r[11];
                    n[10] = r[12];

                    resultado.Rows.Add ( n );
                }

            } else {
                // La informacion la carga del erp

                foreach ( DataRow r in dRelacion.Rows ) {



                }

            }

            return resultado;

        }

        private static DataTable GetTablaParaEncabezado ( ) {
            DataTable resultado = new DataTable ( );

            resultado.Columns.Add ( "CLAVE" );         // ERP ( AHORA ES NUMERICO )
            resultado.Columns.Add ( "NOMBRE" );        // DESCRIPCION
            resultado.Columns.Add ( "OBSERVACIONES" ); // PARA EL MYSQL
            resultado.Columns.Add ( "RESPONSABLE" );   // 
            resultado.Columns.Add ( "MAN" );
            resultado.Columns.Add ( "SERIE" );
            resultado.Columns.Add ( "TIPO" );
            resultado.Columns.Add ( "LOTES" );
            resultado.Columns.Add ( "CTA_CONTABLE" );
            resultado.Columns.Add ( "SUC_DEMANDA" );
            resultado.Columns.Add ( "CONECTADO" );

            return resultado;
        }

        public static Dictionary<campos, String> getEncabezadoAlmacenes ( String TB_LLAV, String TB_INFO ) {
            Dictionary<campos, String> resultado = new Dictionary<campos, string> ( );

            resultado.Add ( campos.CVE_ALMACEN, TB_LLAV.Substring ( 3 ).Trim() );

            resultado.Add ( campos.DESCRIPCION, TB_INFO.Substring ( 0, 26 ).Trim() );
            resultado.Add ( campos.MANEJA_NUMEROS_DE_SERIE, TB_INFO.Substring ( 26, 1 ) );
            resultado.Add ( campos.MUESTRA_NUMEROS_DE_SERIE, TB_INFO.Substring ( 28, 1 ) );
            resultado.Add ( campos.ES_ALMACEN_DE_TRASPASO, TB_INFO.Substring ( 30, 1 ) );
            resultado.Add ( campos.MANEJA_LOTES, TB_INFO.Substring ( 32, 1 ) );
            resultado.Add ( campos.CUENTA_CONTABLE, TB_INFO.Substring ( 34, 8 ).Trim() );
            resultado.Add ( campos.SE_MUESTRA_EN_WEB, TB_INFO.Substring ( 42, 1 ) );

            return resultado;
        }

        public static Dictionary<camposERP, String> setEncabezadoAlmacenes ( mAlmacen m ) {
            Dictionary<camposERP, String> resultado = new Dictionary<camposERP, string> ( );

            resultado.Add ( camposERP.TB_LLAV, "08." + Utilidades.Cadenas.AcompletaLongitud ( m.getCampo ( campos.CVE_ALMACEN ), 3, "0", false ) );

            String cadenaTB_INFO = Cadenas.AcompletaLongitud ( m.getCampo ( campos.DESCRIPCION ), 26, " ", true );
            cadenaTB_INFO += m.getCampo ( campos.MANEJA_NUMEROS_DE_SERIE );
            cadenaTB_INFO += " " + m.getCampo ( campos.MUESTRA_NUMEROS_DE_SERIE );
            cadenaTB_INFO += " " + m.getCampo ( campos.ES_ALMACEN_DE_TRASPASO );
            cadenaTB_INFO += " " + m.getCampo ( campos.MANEJA_LOTES );
            cadenaTB_INFO += " " + Cadenas.AcompletaLongitud(m.getCampo ( campos.CUENTA_CONTABLE ),8," ", true) ;
            cadenaTB_INFO += m.getCampo ( campos.SE_MUESTRA_EN_WEB );

            resultado.Add ( camposERP.TB_INFO, cadenaTB_INFO );

            return resultado;
        }

        public static Dictionary<campos, String> getDatosRelacionAlmacenes ( String TB_LLAV, String TB_INFO ) {
            Dictionary<campos, String> resultado = new Dictionary<campos, string> ( );

            resultado.Add ( campos.CVE_SUCURSAL, TB_LLAV.Substring ( 3, 3 ).Trim ( ) );
            resultado.Add ( campos.CONSECUTIVO_EN_SUCURSAL, TB_LLAV.Substring ( 7 ).Trim ( ) );

            resultado.Add ( campos.CVE_ALMACEN, TB_INFO.Substring ( 0, 3 ).Trim() ) ;
            resultado.Add ( campos.SUCURSAL_PARA_DEMANDA, TB_INFO.Substring ( 5, 2 ) );
            resultado.Add ( campos.CONECTADO, TB_INFO.Substring ( 8, 1 ) );
            resultado.Add ( campos.PERTENECE_A_LA_SUCURSAL, TB_INFO.Substring ( 10, 1 ) );
            resultado.Add ( campos.RESERVADO_MAURICIO, TB_INFO.Substring ( 11, 1  ) );
            resultado.Add ( campos.NO_SE_VISUALIZA, TB_INFO.Substring (12, 1 ) );
            resultado.Add ( campos.SE_CONSIDERA_PARA_ABASTECIMIENTO, TB_INFO.Substring( 13, 1 ) );

            // Esto es un comentario que quiero se muestre en git.

            return resultado;
        }

        public static Dictionary<camposERP, String> setDatosRelacionAlmacenes ( mAlmacen m ) {
            Dictionary<camposERP, String> resultado = new Dictionary<camposERP, string> ( );

            resultado.Add ( camposERP.TB_LLAV, "09." + Cadenas.AcompletaLongitud ( m.getCampo ( campos.CVE_SUCURSAL ), 6, " ", true ) + m.getCampo( campos.CONSECUTIVO_EN_SUCURSAL ) );
            
            String cadTb_Info = Cadenas.AcompletaLongitud( m.getCampo( campos.CVE_ALMACEN), 5, " ", true );
            cadTb_Info += m.getCampo(campos.SUCURSAL_PARA_DEMANDA);
            cadTb_Info += " " + m.getCampo ( campos.CONECTADO );
            cadTb_Info += " " + m.getCampo( campos.PERTENECE_A_LA_SUCURSAL );
            cadTb_Info += m.getCampo(campos.RESERVADO_MAURICIO);
            cadTb_Info += m.getCampo( campos.NO_SE_VISUALIZA );
            cadTb_Info += m.getCampo( campos.SE_CONSIDERA_PARA_ABASTECIMIENTO );

            resultado.Add ( camposERP.TB_INFO, cadTb_Info );

            return resultado;
        }

        public static Dictionary<campos, String> getOtrosDatos ( DataRow d ) {
            Dictionary<campos, String> resultado = new Dictionary<campos, string> ( );

            resultado.Add ( campos.NO_VENTAS, d["TB09_NO_VTA"].ToString() );
            resultado.Add ( campos.NO_ORDE_DE_COMPRA, d["TB09_NO_OC"].ToString() );
            resultado.Add ( campos.SPARE, d["TB09_DISPONIBLE"].ToString ( ) );

            return resultado;
        }

        public static DataTable SetOtrosDatos ( mAlmacen m ) {
            DataTable resultado = new DataTable ( );
            resultado.Columns.Add ( "TB09_ALM" );
            resultado.Columns.Add ( "TB09_SUC" );
            resultado.Columns.Add ( "TB09_PERT1" );
            resultado.Columns.Add ( "TB09_PERT2" );
            resultado.Columns.Add ( "TB09_PERT3" );
            resultado.Columns.Add ( "TB09_DESC" );
            resultado.Columns.Add ( "TB09_PERT4" );
            resultado.Columns.Add ( "TB09_NO_VTA" );
            resultado.Columns.Add ( "TB09_NO_OC" );
            resultado.Columns.Add ( "TB09_DISPONIBLE" );

            DataRow nuevo = resultado.NewRow ( );

            nuevo[0] = m.getCampo( campos.CVE_ALMACEN );
            nuevo[1] = m.getCampo( campos.CVE_SUCURSAL );
            nuevo[2] = m.getCampo( campos.PERTENECE_A_LA_SUCURSAL );
            nuevo[3] = m.getCampo( campos.RESERVADO_MAURICIO );
            nuevo[4] = m.getCampo( campos.NO_SE_VISUALIZA );
            nuevo[5] = m.getCampo( campos.DESCRIPCION );
            nuevo[6] = m.getCampo( campos.SE_CONSIDERA_PARA_ABASTECIMIENTO );
            nuevo[7] = m.getCampo( campos.NO_VENTAS );
            nuevo[8] = m.getCampo( campos.NO_ORDE_DE_COMPRA );
            nuevo[9] = m.getCampo( campos.SPARE );

            resultado.Rows.Add ( nuevo );

            return resultado;
        }

        private Dictionary<campos, String> valoresDeAlmacen = new Dictionary<campos, string> ( );

        public mAlmacen ( Dictionary<campos, String> encabezado, Dictionary<campos, String> relacion, Dictionary<campos, String> dic09 ) {

            // ENCABEZADO

            valoresDeAlmacen[campos.CVE_ALMACEN]                = encabezado[campos.CVE_ALMACEN];
            valoresDeAlmacen[campos.DESCRIPCION]                = encabezado[campos.DESCRIPCION];
            valoresDeAlmacen[campos.MANEJA_NUMEROS_DE_SERIE]    = encabezado[campos.MANEJA_NUMEROS_DE_SERIE];
            valoresDeAlmacen[campos.MUESTRA_NUMEROS_DE_SERIE]   = encabezado[campos.MUESTRA_NUMEROS_DE_SERIE];
            valoresDeAlmacen[campos.ES_ALMACEN_DE_TRASPASO]     = encabezado[campos.ES_ALMACEN_DE_TRASPASO];
            valoresDeAlmacen[campos.MANEJA_LOTES]               = encabezado[campos.MANEJA_LOTES];
            valoresDeAlmacen[campos.CUENTA_CONTABLE]            = encabezado[campos.CUENTA_CONTABLE];
            valoresDeAlmacen[campos.SE_MUESTRA_EN_WEB]          = encabezado[campos.SE_MUESTRA_EN_WEB];

            // DETALLE
            
            valoresDeAlmacen[campos.CVE_SUCURSAL]                       = relacion[campos.CVE_SUCURSAL];
            valoresDeAlmacen[campos.CONSECUTIVO_EN_SUCURSAL]            = relacion[campos.CONSECUTIVO_EN_SUCURSAL];
          //valoresDeAlmacen[campos.CVE_ALMACEN]                        = relacion[campos.CVE_ALMACEN];
            valoresDeAlmacen[campos.SUCURSAL_PARA_DEMANDA]              = relacion[campos.SUCURSAL_PARA_DEMANDA];
            valoresDeAlmacen[campos.CONECTADO]                          = relacion[campos.CONECTADO];
            valoresDeAlmacen[campos.PERTENECE_A_LA_SUCURSAL]            = relacion[campos.PERTENECE_A_LA_SUCURSAL];
            valoresDeAlmacen[campos.RESERVADO_MAURICIO]                 = relacion[campos.RESERVADO_MAURICIO];
            valoresDeAlmacen[campos.NO_SE_VISUALIZA]                    = relacion[campos.NO_SE_VISUALIZA];
            valoresDeAlmacen[campos.SE_CONSIDERA_PARA_ABASTECIMIENTO]   = relacion[campos.SE_CONSIDERA_PARA_ABASTECIMIENTO];


            // OTROS

            valoresDeAlmacen[campos.NO_VENTAS]                          = dic09[campos.NO_VENTAS];
            valoresDeAlmacen[campos.NO_ORDE_DE_COMPRA]                  = dic09[campos.NO_ORDE_DE_COMPRA];
            valoresDeAlmacen[campos.SPARE]                              = dic09[campos.SPARE];

        }

        public String getCampo ( campos c ) {
            return valoresDeAlmacen[c];
        }


    }
}
