﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Controlador;

namespace Sucursales
{
    public partial class frmContenedor : Form
    {
        String NombreDeUsuario;
        String version = "1.0.0.0";

        cControlador u = null;

        formas actual;

        public frmContenedor ( String ClaveUsuario ) {
            u = new cControlador ( ClaveUsuario );
            NombreDeUsuario = u.getNombre ( );
            NombreDeUsuario += " Controlador (" + version + ") ";
            InitializeComponent ( );
        }

        enum formas
        {
            RELACION,
            SUCURSALES,
            ALMACENES
        }

        enum aciones
        {
            ALTA,
            BAJA,
            CAMBIO
        }

        enum respuestas
        {
            ACEPTAR,
            CANCELAR
        }

        cSucursal cs = null;
        cAlmacen  ca = null;


        private void Form1_Load ( object sender, EventArgs e ) {
            lanzaForma ( formas.RELACION );
        }

        private void salirToolStripMenuItem_Click ( object sender, EventArgs e ) {
            this.Close ( );
        }

        #region LANZA FORMAS

        private void lanzaForma ( formas f ) {

            foreach ( Form fo in this.MdiChildren ) {
                fo.Close ( );
            }

            bool EstadoInicial = true;

            tsbRelacion.Enabled = EstadoInicial;
            tsbSucursal.Enabled = EstadoInicial;
            tsbAlmacen.Enabled  = EstadoInicial;

            String caption = "";

            actual = f;

            switch ( f ) {
                case formas.RELACION:
                    relacion    r = new relacion ( );
                    cRelacion   c = new cRelacion ( r );
                    tsbRelacion.Enabled = false;
                    habilitarModo ( false );
                    habilitarGuardar ( false );
                    caption = "Relacion sucursal - almacen";
                    c.inicializaLaVista ( );
                    r.MdiParent = this;
                    r.WindowState = FormWindowState.Maximized;
                    r.Show ( );
                    break;
                case formas.SUCURSALES:
                    Sucursal    s   = new Sucursal ( );
                    cs  = new cSucursal ( s );
                    tsbSucursal.Enabled = false;
                    habilitarModo ( true );
                    habilitarGuardar ( false );
                    caption = "Sucursales";
                    cs.loadVista ( );
                    s.MdiParent = this;
                    s.WindowState = FormWindowState.Maximized;
                    s.Show ( );
                    break;
                case formas.ALMACENES:
                    almacen     a   = new almacen ( );
                    ca = new cAlmacen ( a );
                    tsbAlmacen.Enabled = false;
                    habilitarModo ( true );
                    habilitarGuardar ( false );
                    ca.loadVista ( );
                    a.MdiParent = this;
                    a.WindowState = FormWindowState.Maximized;
                    a.Show ( );
                    caption = "Almacenes";
                    break;
            }

            this.Text = NombreDeUsuario + caption;

        }
        
        private void tsbRelacion_Click ( object sender, EventArgs e ) {
            lanzaForma ( formas.RELACION );
        }

        private void tsbSucursal_Click ( object sender, EventArgs e ) {
            lanzaForma ( formas.SUCURSALES );
        }

        private void tsbAlmacen_Click ( object sender, EventArgs e ) {
            lanzaForma ( formas.ALMACENES );
        }
        #endregion

        #region MANDA ACCIONES

        private void tsbAlta_Click ( object sender, EventArgs e ) {
            mandaAccion( aciones.ALTA );
        }

        private void tsbBaja_Click ( object sender, EventArgs e ) {
            mandaAccion( aciones.BAJA );
        }

        private void tsbCambio_Click ( object sender, EventArgs e ) {
            mandaAccion( aciones.CAMBIO );
        }

        void mandaAccion ( aciones a ) {
            switch ( actual ) {
                case formas.SUCURSALES:

                    if ( cs != null ) {
                        switch ( a ) {
                            case aciones.ALTA:
                                cs.habilitarAlta ( );
                                break;
                            case aciones.BAJA:
                                cs.habilitaBaja ( );
                                break;
                            case aciones.CAMBIO:
                                cs.habilitaCambio();
                                break;
                        }
                    } 
                    break;
                case formas.ALMACENES:

                    if ( ca != null ) {
                        switch ( a ) {
                            case aciones.ALTA:
                                ca.habilitarAlta ( );
                                break;
                            case aciones.BAJA:
                                ca.habilitaBaja ( );
                                break;
                            case aciones.CAMBIO:
                                ca.habilitaCambio ( );
                                break;
                        }
                    }

                    break;
            }
            // Bloque todos los demas botones

            tsbAlta.Enabled     = false;
            tsbBaja.Enabled     = false;
            tsbCambio.Enabled   = false;

            switch ( a ) {
                case aciones.ALTA:
                    tsbAlta.Enabled = true;
                    break;
                case aciones.BAJA:
                    tsbBaja.Enabled = true;
                    break;
                case aciones.CAMBIO:
                    tsbCambio.Enabled = true;
                    break;
            }
            

            // Cambia backGround de botones, para indicar que estan habilitados.

            habilitarGuardar ( true );
        }
        #endregion

        #region MANDA RESPUESTAS
        private void tsbAcepta_Click ( object sender, EventArgs e ) {
            mandaRespuesta ( respuestas.ACEPTAR );
        }

        private void tsbCancela_Click ( object sender, EventArgs e ) {
            mandaRespuesta ( respuestas.CANCELAR );
        }

        void mandaRespuesta ( respuestas r ) {
            switch ( actual ) {
                case formas.SUCURSALES:

                    if ( cs != null ) {
                        switch ( r ) {
                            case respuestas.ACEPTAR:
                                cs.Acepta();
                                break;
                            case respuestas.CANCELAR:
                                cs.Cancela();
                                break;
                        }
                    } 
                    break;
                case formas.ALMACENES:

                    if ( ca != null ) {
                        switch ( r ) {
                            case respuestas.ACEPTAR:
                                ca.Acepta();
                                break;
                            case respuestas.CANCELAR:
                                ca.Cancela();
                                break;
                        }
                    }

                    break;
            }
            habilitarGuardar ( false );
            habilitarModo ( true );
       }
        #endregion
        
        private void habilitarModo ( bool habilita ) {
            tsbAlta.Enabled     = habilita;
            tsbBaja.Enabled     = habilita;
            tsbCambio.Enabled   = habilita;
        }

        private void habilitarGuardar ( bool habilita ) {
            tsbAcepta.Enabled = habilita;
            tsbCancela.Enabled = habilita;

            if ( habilita ) {
                tsbAcepta.BackColor     = Color.LightGreen;
                tsbCancela.BackColor    = Color.LightPink;
            } else {
                tsbAcepta.BackColor     = SystemColors.Control;
                tsbCancela.BackColor    = SystemColors.Control;
            }

        }

    }
}
