﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Controlador;


namespace Sucursales
{
    public partial class Sucursal : Form, iSucursalView
    {

        cSucursal _c;

        BindingSource bs = new BindingSource ( );
        bool cargaTerminada;

        int ajusteDel;

        enum edicionText
        {
            INICIO,
            FINAL
        }

        enum accionEnviada
        {
            MODIFICACION,
            ALTA,
            BAJA,
            OPERACION
        }

        Dictionary<String, String> antes    = new Dictionary<string, string> ( );
        Dictionary<String, String> despues  = new Dictionary<string, string> ( );

        accionEnviada accionEnCurso = accionEnviada.OPERACION;

        public Sucursal ( ) {
            InitializeComponent ( );
            estadoText ( false );
        }

        private void Sucursal_Shown ( object sender, EventArgs e ) {
            // Una vez que termina de carga entonces ya puede ver que row tiene el foco
            estadoInicial ( );
            setDataInTextBox ( 0, ajusteDel );
        }

        private void Sucursal_Load ( object sender, EventArgs e ) {

        }

        public void setController ( cSucursal c ) {
            _c = c;
        }

        public void setGridSucursales ( DataTable Sucursales ) {

            bs.DataSource = Sucursales.DefaultView;
            dataGridView1.DataSource = bs;
            foreach ( DataGridViewColumn c in dataGridView1.Columns ) {
                c.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }

            estadoInicial ( );

        }

        public void ActivaEdicion ( ) {
            accionEnCurso = accionEnviada.MODIFICACION;
            getTextBox ( edicionText.INICIO );
            estadoText ( true );
            cargaTerminada = false;
            tClave.Focus ( );

        }

        public void ActivaAlta ( ) {
            limpiaTex ( );
            estadoText ( true );
            dataGridView1.AllowUserToAddRows = true;
            cargaTerminada = false;
            accionEnCurso = accionEnviada.ALTA;
        }

        public void ActivaBaja ( ) {
            estadoText ( false );

            ajusteDel = 1;

            accionEnCurso = accionEnviada.BAJA;
            DataGridViewCheckBoxColumn c;
            c = new DataGridViewCheckBoxColumn ( );
            dataGridView1.Columns.Insert ( 0, c );
            dataGridView1.Columns [0].HeaderText = "DEL";
            dataGridView1.Columns [0].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
        }

        public void Acepta ( ) {
            switch ( accionEnCurso ) {
                case accionEnviada.MODIFICACION:

                    getTextBox ( edicionText.FINAL );

                    Dictionary<String,String> cambios = getDiferencias_para_actualizar_bd ( );
                    if ( cambios.Count > 0 ) {
                        // MANDA LA ACTUALIZACION
                        String claveSucursal = tClave.Text;

                        List<String> anexo = new List<string> ( );
                        anexo.Add ( tNombre.Text );
                        anexo.Add ( tClaveMostrador.Text );
                        anexo.Add ( tClaveSucursal.Text );

                        if ( _c.actualizaSucursal ( claveSucursal, cambios, anexo ) ) {
                            estadoInicial ( );
                        } else{
                            // Regresa al estado anterior.
                            Dictionary<String, String> regresar = getDiferencias_para_regresar_valor ( );
                            cargaValoresATextDesdeDictionary ( regresar );
                        }

                    }
                    break;
                case accionEnviada.ALTA:
                    // manda el llamado para el alta de una sucursal
                    if ( _c.insertaRegistro ( cSucursal.enDondeSeInserta.EN_AMBOS, getTextBoxToDataRow ( ) ) ) {
                        _c.loadVista ( );
                    } else {
                        Cancela ( );
                    }
                    break;

                case accionEnviada.BAJA:
                    List<String> paraDarDeBaja = new List<string> ( );
                    foreach ( DataGridViewRow r in dataGridView1.Rows ) {
                        if ( Convert.ToBoolean( r.Cells [0].Value ) == true ) {
                            paraDarDeBaja.Add ( r.Cells [1].Value.ToString ( ) );
                        }
                    }

                    if ( paraDarDeBaja.Count > 0 ) {
                        if ( _c.borraRegistros ( paraDarDeBaja ) ) {
                            dataGridView1.Columns.RemoveAt ( 0 );
                            ajusteDel = 0;
                            _c.loadVista ( );
                        } else {
                            Cancela ( );
                        }
                    }


                    break;
            }

            accionEnCurso = accionEnviada.OPERACION;
        }

        public void Cancela ( ) {

            switch ( accionEnCurso ) {
                case accionEnviada.MODIFICACION:

                    getTextBox ( edicionText.FINAL );
                    // Regresa el valor anterior

                    Dictionary<String,String> cambios = getDiferencias_para_regresar_valor ( );
                    if ( cambios.Count > 0 ) {
                        cargaValoresATextDesdeDictionary ( cambios );
                    }

                    break;
                case accionEnviada.ALTA:
                    limpiaTex ( );
                    break;
                case accionEnviada.BAJA:
                    ajusteDel = 0;
                    limpiaTex ( );
                    dataGridView1.Columns.RemoveAt ( 0 );
                    break;

            }

            accionEnCurso = accionEnviada.OPERACION;
            estadoInicial ( );
        }

        private void dataGridView1_CellClick ( object sender, DataGridViewCellEventArgs e ) {
            
            if ( cargaTerminada ) {
                int i = dataGridView1.CurrentCell.RowIndex;
                setDataInTextBox ( i, ajusteDel );

                if ( accionEnCurso == accionEnviada.BAJA ) {

                    int col = dataGridView1.CurrentCell.ColumnIndex;

                    if ( col == 0 ) {

                        dataGridView1.CurrentCell.Value = ( Convert.ToBoolean( dataGridView1.CurrentCell.Value ) == true ) ? false : true;

                    }
                }

            }
        }

        private void dataGridView1_RowEnter ( object sender, DataGridViewCellEventArgs e ) {
            if ( cargaTerminada ) {
                int i = dataGridView1.CurrentCell.RowIndex;
                setDataInTextBox ( i, ajusteDel );
            }
        }

        private void setDataInTextBox ( int row, int ajuste ) {

            tClave.Text             = dataGridView1.Rows [row].Cells [0 + ajuste].Value.ToString ( );
            tNombre.Text            = dataGridView1.Rows [row].Cells [3 + ajuste].Value.ToString ( );
            tDescripcion.Text       = dataGridView1.Rows [row].Cells [1 + ajuste].Value.ToString ( );
            tTipo.Text              = dataGridView1.Rows [row].Cells [2 + ajuste].Value.ToString ( );
            tRFC.Text               = dataGridView1.Rows [row].Cells [4 + ajuste].Value.ToString ( );
            tCalle.Text             = dataGridView1.Rows [row].Cells [5 + ajuste].Value.ToString ( );
            tNoExt.Text             = dataGridView1.Rows [row].Cells [6+ ajuste].Value.ToString ( );
            tNoInt.Text             = dataGridView1.Rows [row].Cells [7+ ajuste].Value.ToString ( );
            tColonia.Text           = dataGridView1.Rows [row].Cells [8+ ajuste].Value.ToString ( );
            tReferencia.Text        = dataGridView1.Rows [row].Cells [9+ ajuste].Value.ToString ( );
            tMunicipio.Text         = dataGridView1.Rows [row].Cells [10+ ajuste].Value.ToString ( );
            tEstado.Text            = dataGridView1.Rows [row].Cells [11+ ajuste].Value.ToString ( );
            tPais.Text              = dataGridView1.Rows [row].Cells [12+ ajuste].Value.ToString ( );
            tCP.Text                = dataGridView1.Rows [row].Cells [13+ ajuste].Value.ToString ( );
            tTelefono.Text          = dataGridView1.Rows [row].Cells [14+ ajuste].Value.ToString ( );
            tCorreo.Text            = dataGridView1.Rows [row].Cells [15+ ajuste].Value.ToString ( );
            tDirServer.Text         = dataGridView1.Rows [row].Cells [16+ ajuste].Value.ToString ( );
            tDirBackServer.Text     = dataGridView1.Rows [row].Cells [17+ ajuste].Value.ToString ( );
            tCFDIPC.Text            = dataGridView1.Rows [row].Cells [18+ ajuste].Value.ToString ( );
            tCFDIBackPC.Text        = dataGridView1.Rows [row].Cells [19+ ajuste].Value.ToString ( );
            tClaveMostrador.Text    = dataGridView1.Rows [row].Cells [20+ ajuste].Value.ToString ( );
            tClaveSucursal.Text     = dataGridView1.Rows [row].Cells [21+ ajuste].Value.ToString ( );
            tResponsable.Text       = dataGridView1.Rows [row].Cells [22+ ajuste].Value.ToString ( );
            if ( dataGridView1.Rows [row].Cells [23+ ajuste].Value.ToString().Equals("0")) {
                btnActBarpro.Enabled = true;
                btnActBarpro.BackColor = Color.LightGreen;
            } else {
                btnActBarpro.Enabled = false;
                btnActBarpro.BackColor = SystemColors.Control;
            }

            estadoText ( false );
        }

        private void estadoText ( bool estado ) {
            tClave.Enabled = estado;
            tNombre.Enabled = estado;
            tDescripcion.Enabled = estado;
            tTipo.Enabled = estado;
            tRFC.Enabled = estado;
            tCalle.Enabled = estado;
            tNoExt.Enabled = estado;
            tNoInt.Enabled = estado;
            tColonia.Enabled = estado;
            tReferencia.Enabled = estado;
            tMunicipio.Enabled = estado;
            tEstado.Enabled = estado;
            tPais.Enabled = estado;
            tCP.Enabled = estado;
            tTelefono.Enabled = estado;
            tCorreo.Enabled = estado;
            tDirServer.Enabled = estado;
            tDirBackServer.Enabled = estado;
            tCFDIPC.Enabled = estado;
            tCFDIBackPC.Enabled = estado;
            tClaveMostrador.Enabled = estado;
            tClaveSucursal.Enabled = estado;
            tResponsable.Enabled = estado;
        }

        private void limpiaTex ( ) {
            tClave.Text = "";
            tNombre.Text = "";
            tDescripcion.Text = "";
            tTipo.Text = "";
            tRFC.Text = "";
            tCalle.Text = "";
            tNoExt.Text = "";
            tNoInt.Text = "";
            tColonia.Text = "";
            tReferencia.Text = "";
            tMunicipio.Text = "";
            tEstado.Text = "";
            tPais.Text = "";
            tCP.Text = "";
            tTelefono.Text = "";
            tCorreo.Text = "";
            tDirServer.Text = "";
            tDirBackServer.Text = "";
            tCFDIPC.Text = "";
            tCFDIBackPC.Text = "";
            tClaveMostrador.Text = "";
            tClaveSucursal.Text = "";
            tResponsable.Text = "";
        }

        private void estadoInicial ( ) {
            accionEnCurso = accionEnviada.OPERACION;
            estadoText ( false );
            dataGridView1.AllowUserToAddRows = false;
            foreach ( DataGridViewRow r in dataGridView1.Rows ) {
                r.ReadOnly = true;
            }
            cargaTerminada = true;
            btnActBarpro.BackColor = SystemColors.Control;
        }

        #region IDENTIFICA CAMBIOS EN VALORES

        private void getTextBox ( edicionText e ) {
            Dictionary<String,String> resultado = new Dictionary<string, string> ( );

            foreach ( Control c in this.Controls ) {
                if ( c is TextBox ) {
                    resultado.Add ( c.Name, c.Text );
                }            
            }

            switch ( e ) {
                case Sucursal.edicionText.INICIO:
                    antes = resultado;
                    break;
                case Sucursal.edicionText.FINAL:
                    despues = resultado;
                    break;
            }
        }

        private DataRow getTextBoxToDataRow ( ) {
            // Dependiendo del nombre del campo es en donde los va a guardar dentro del datarow

            DataTable resultado = new DataTable ( );

            DataColumn[] llave = new DataColumn [1];

            resultado.Columns.Add ( "CLAVE", typeof ( String ) );              //0
            llave [0] = resultado.Columns [0];
            resultado.Columns.Add ( "DESCRIPCION", typeof ( String ) );        //1
            resultado.Columns.Add ( "TIPO", typeof ( Int16 ) );                //2
            resultado.Columns.Add ( "NOMBRE", typeof ( String ) );      //3
            resultado.Columns.Add ( "RFC", typeof ( String ) );         //4
            resultado.Columns.Add ( "CALLE", typeof ( String ) );       //5
            resultado.Columns.Add ( "NO-EXT", typeof ( String ) );      //6
            resultado.Columns.Add ( "NO-INT", typeof ( String ) );      //7
            resultado.Columns.Add ( "COLONIA", typeof ( String ) );     //8
            resultado.Columns.Add ( "REFERENCIA", typeof ( String ) );  //9
            resultado.Columns.Add ( "MUNICIPIO", typeof ( String ) );   //10
            resultado.Columns.Add ( "ESTADO", typeof ( String ) );      //11
            resultado.Columns.Add ( "PAIS", typeof ( String ) );        //12
            resultado.Columns.Add ( "CP", typeof ( String ) );          //13
            resultado.Columns.Add ( "TELEFONO", typeof ( String ) );    //14
            resultado.Columns.Add ( "CORREO", typeof ( String ) );      //15
            resultado.Columns.Add ( "SITIO", typeof ( String ) );       //16
            resultado.Columns.Add ( "SERVIDOR", typeof ( String ) );    //17
            resultado.Columns.Add ( "PC_1", typeof ( String ) );        //18
            resultado.Columns.Add ( "PC_2", typeof ( String ) );        //19
            resultado.Columns.Add ( "CVE MOSTRADOR", typeof ( String ) );        //20
            resultado.Columns.Add ( "CVE SUCURSAL", typeof ( String ) );        //21
            resultado.Columns.Add ( "RESPONSABLE", typeof ( String ) ); //22

            DataRow r = resultado.NewRow ( );

            r[0]    = tClave.Text;
            r[1]    = tDescripcion.Text;
            r[2]    = tTipo.Text;
            r[3]    = tNombre.Text;
            r[4]    = tRFC.Text;
            r[5]    = tCalle.Text;
            r[6]    = tNoExt.Text;
            r[7]    = tNoInt.Text;
            r[8]    = tColonia.Text;
            r[9]    = tReferencia.Text;
            r[10]   = tMunicipio.Text;
            r[11]   = tEstado.Text;
            r[12]   = tPais.Text;
            r[13]   = tCP.Text;
            r[14]   = tTelefono.Text;
            r[15]   = tCorreo.Text;
            r[16]   = tDirServer.Text;
            r[17]   = tDirBackServer.Text;
            r[18]   = tCFDIPC.Text;
            r[19]   = tCFDIBackPC.Text;
            r[20]   = tClaveMostrador.Text;
            r[21]   = tClaveSucursal.Text;
            r[22]   = tResponsable.Text;

            resultado.Rows.Add ( r );


            return resultado.Rows [0];
        }

        private Dictionary<String, String> getDiferencias_para_actualizar_bd( ) {
            return despues.Except ( antes ).ToDictionary ( x => x.Key, x => x.Value );
        }

        private Dictionary<String, String> getDiferencias_para_regresar_valor ( ) {
            return antes.Except ( despues ).ToDictionary ( x => x.Key, x => x.Value );
        }

        private void cargaValoresATextDesdeDictionary ( Dictionary<String, String> datos ) {
            foreach ( String textControl in datos.Keys ) {
                setValueOnText ( textControl, datos [textControl] );
            }

        }

        private void setValueOnText ( String textName, String value ) {
            foreach ( Control c in this.Controls ) {
                if ( c.Name.Equals ( textName ) ) {
                    c.Text = value;
                }
            }
        }

        #endregion

        private void insertaRegistro ( cSucursal.enDondeSeInserta e, DataRow d ) {
            _c.insertaRegistro ( e, d );            
        }

        private void btnActBarpro_Click ( object sender, EventArgs e ) {
            int i = dataGridView1.CurrentCell.RowIndex;

            DataView d = (DataView) bs.DataSource;

            DataTable r = d.ToTable ( );
           
            insertaRegistro ( cSucursal.enDondeSeInserta.EN_MYSQL, r.Rows[i] );

        }

        private void tClave_Leave ( object sender, EventArgs e ) {
            String clave = ( (TextBox)sender ).Text;

            if ( accionEnCurso == accionEnviada.ALTA ) {
                if ( clave.Length == 0 ) {
                    errProvider.SetError ( tClave, String.Empty );
                } else {
                    errProvider.Clear ( );
                }

                if ( !( Utilidades.Cadenas.esNumero( clave  ))) {
                    errProvider.SetError (tClave, "La clave debe ser numerica");
                } else {
                    errProvider.Clear ( );
                }

                List<String> claves = getClaves ( );
                bool yaexiste = ( claves.Contains(clave) ) ? true : false;

                if ( yaexiste ) {
                    errProvider.SetError( tClave, "La clave ya existe");

                } else {
                    errProvider.Clear ( );
                }

            }

        }

        private List<String> getClaves ( ) {
            List<String> resultado = new List<string> ( );
            foreach ( DataGridViewRow r in dataGridView1.Rows ) {
                if ( !r.IsNewRow ) {
                    resultado.Add ( r.Cells [0].Value.ToString ( ) );
                }
            }
            return resultado;
        }

        private void tClaveMostrador_TextChanged ( object sender, EventArgs e ) {
            String text = ( (TextBox)sender ).Text;

            if ( text.Length > 9 ) {
                MessageBox.Show ( "La clave no puede ser mayor a 8 digitos.", "BARMEX - ALTA DE SUCURSALES", MessageBoxButtons.OK );
                text = text.Substring ( 0, 9 );
                tClaveMostrador.Text = text;
                tClaveMostrador.SelectAll ( );
            }

        }

        private void tClaveSucursal_TextChanged ( object sender, EventArgs e ) {
            String text = ( (TextBox)sender ).Text;

            if ( text.Length > 6 ) {
                MessageBox.Show ( "La clave no puede ser mayor a 6 digitos.", "BARMEX - ALTA DE SUCURSALES", MessageBoxButtons.OK );
                text = text.Substring ( 0, 6 );
                tClaveSucursal.Text = text;
                tClaveSucursal.SelectAll ( );
            }
        }

    }
}
