﻿namespace Sucursales
{
    partial class Sucursal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose ( bool disposing ) {
            if ( disposing && ( components != null ) ) {
                components.Dispose ( );
            }
            base.Dispose ( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ( ) {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tClave = new System.Windows.Forms.TextBox();
            this.tPais = new System.Windows.Forms.TextBox();
            this.tEstado = new System.Windows.Forms.TextBox();
            this.tCorreo = new System.Windows.Forms.TextBox();
            this.tDirServer = new System.Windows.Forms.TextBox();
            this.tCP = new System.Windows.Forms.TextBox();
            this.tRFC = new System.Windows.Forms.TextBox();
            this.tTelefono = new System.Windows.Forms.TextBox();
            this.tReferencia = new System.Windows.Forms.TextBox();
            this.tColonia = new System.Windows.Forms.TextBox();
            this.tCalle = new System.Windows.Forms.TextBox();
            this.tMunicipio = new System.Windows.Forms.TextBox();
            this.tNoExt = new System.Windows.Forms.TextBox();
            this.tTipo = new System.Windows.Forms.TextBox();
            this.tDescripcion = new System.Windows.Forms.TextBox();
            this.tNombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tNoInt = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tClaveMostrador = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.tClaveSucursal = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.tDirBackServer = new System.Windows.Forms.TextBox();
            this.tCFDIPC = new System.Windows.Forms.TextBox();
            this.tCFDIBackPC = new System.Windows.Forms.TextBox();
            this.btnActBarpro = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.tResponsable = new System.Windows.Forms.TextBox();
            this.errProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 228);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(885, 181);
            this.dataGridView1.TabIndex = 24;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_RowEnter);
            // 
            // tClave
            // 
            this.tClave.Location = new System.Drawing.Point(65, 6);
            this.tClave.Name = "tClave";
            this.tClave.Size = new System.Drawing.Size(67, 20);
            this.tClave.TabIndex = 1;
            this.tClave.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tClave.Leave += new System.EventHandler(this.tClave_Leave);
            // 
            // tPais
            // 
            this.tPais.Location = new System.Drawing.Point(208, 35);
            this.tPais.Name = "tPais";
            this.tPais.Size = new System.Drawing.Size(100, 20);
            this.tPais.TabIndex = 5;
            this.tPais.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tEstado
            // 
            this.tEstado.Location = new System.Drawing.Point(404, 35);
            this.tEstado.Name = "tEstado";
            this.tEstado.Size = new System.Drawing.Size(124, 20);
            this.tEstado.TabIndex = 6;
            this.tEstado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tCorreo
            // 
            this.tCorreo.Location = new System.Drawing.Point(339, 120);
            this.tCorreo.Name = "tCorreo";
            this.tCorreo.Size = new System.Drawing.Size(188, 20);
            this.tCorreo.TabIndex = 16;
            // 
            // tDirServer
            // 
            this.tDirServer.Location = new System.Drawing.Point(129, 149);
            this.tDirServer.Name = "tDirServer";
            this.tDirServer.Size = new System.Drawing.Size(274, 20);
            this.tDirServer.TabIndex = 19;
            // 
            // tCP
            // 
            this.tCP.Location = new System.Drawing.Point(655, 62);
            this.tCP.Name = "tCP";
            this.tCP.Size = new System.Drawing.Size(100, 20);
            this.tCP.TabIndex = 11;
            this.tCP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tRFC
            // 
            this.tRFC.Location = new System.Drawing.Point(812, 62);
            this.tRFC.Name = "tRFC";
            this.tRFC.Size = new System.Drawing.Size(85, 20);
            this.tRFC.TabIndex = 12;
            this.tRFC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tTelefono
            // 
            this.tTelefono.Location = new System.Drawing.Point(91, 120);
            this.tTelefono.Name = "tTelefono";
            this.tTelefono.Size = new System.Drawing.Size(162, 20);
            this.tTelefono.TabIndex = 15;
            // 
            // tReferencia
            // 
            this.tReferencia.Location = new System.Drawing.Point(419, 90);
            this.tReferencia.Name = "tReferencia";
            this.tReferencia.Size = new System.Drawing.Size(478, 20);
            this.tReferencia.TabIndex = 14;
            // 
            // tColonia
            // 
            this.tColonia.Location = new System.Drawing.Point(80, 90);
            this.tColonia.Name = "tColonia";
            this.tColonia.Size = new System.Drawing.Size(228, 20);
            this.tColonia.TabIndex = 13;
            // 
            // tCalle
            // 
            this.tCalle.Location = new System.Drawing.Point(65, 62);
            this.tCalle.Name = "tCalle";
            this.tCalle.Size = new System.Drawing.Size(243, 20);
            this.tCalle.TabIndex = 8;
            // 
            // tMunicipio
            // 
            this.tMunicipio.Location = new System.Drawing.Point(625, 35);
            this.tMunicipio.Name = "tMunicipio";
            this.tMunicipio.Size = new System.Drawing.Size(272, 20);
            this.tMunicipio.TabIndex = 7;
            // 
            // tNoExt
            // 
            this.tNoExt.Location = new System.Drawing.Point(387, 62);
            this.tNoExt.Name = "tNoExt";
            this.tNoExt.Size = new System.Drawing.Size(67, 20);
            this.tNoExt.TabIndex = 9;
            this.tNoExt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tTipo
            // 
            this.tTipo.Location = new System.Drawing.Point(65, 35);
            this.tTipo.Name = "tTipo";
            this.tTipo.Size = new System.Drawing.Size(67, 20);
            this.tTipo.TabIndex = 4;
            this.tTipo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tDescripcion
            // 
            this.tDescripcion.Location = new System.Drawing.Point(504, 6);
            this.tDescripcion.Name = "tDescripcion";
            this.tDescripcion.Size = new System.Drawing.Size(393, 20);
            this.tDescripcion.TabIndex = 3;
            // 
            // tNombre
            // 
            this.tNombre.Location = new System.Drawing.Point(208, 6);
            this.tNombre.Name = "tNombre";
            this.tNombre.Size = new System.Drawing.Size(195, 20);
            this.tNombre.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "CLAVE:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(324, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "ESTADO:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(543, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "MUNICIPIO:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(622, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "CP:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(324, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "REFERENCIA:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(474, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "NO INT:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(324, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "NO EXT:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(138, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "PAIS:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 179);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "CFDI EN PC:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(9, 152);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(114, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "CFDI EN SERVER:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(9, 123);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "TELEFONO:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(9, 93);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 13);
            this.label13.TabIndex = 29;
            this.label13.Text = "COLONIA:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(9, 65);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 30;
            this.label14.Text = "CALLE:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(9, 38);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 13);
            this.label15.TabIndex = 31;
            this.label15.Text = "TIPO:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(409, 9);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(95, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "DESCRIPCION:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(138, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 13);
            this.label17.TabIndex = 33;
            this.label17.Text = "NOMBRE:";
            // 
            // tNoInt
            // 
            this.tNoInt.Location = new System.Drawing.Point(534, 62);
            this.tNoInt.Name = "tNoInt";
            this.tNoInt.Size = new System.Drawing.Size(67, 20);
            this.tNoInt.TabIndex = 10;
            this.tNoInt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(771, 65);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 13);
            this.label19.TabIndex = 35;
            this.label19.Text = "RFC:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(270, 123);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(63, 13);
            this.label18.TabIndex = 23;
            this.label18.Text = "CORREO:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(416, 179);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(112, 13);
            this.label7.TabIndex = 36;
            this.label7.Text = "CFDI BACK EN PC";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(543, 123);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(90, 13);
            this.label20.TabIndex = 36;
            this.label20.Text = "MOSTRADOR:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(756, 123);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(36, 13);
            this.label21.TabIndex = 36;
            this.label21.Text = "SUC:";
            // 
            // tClaveMostrador
            // 
            this.tClaveMostrador.Location = new System.Drawing.Point(657, 120);
            this.tClaveMostrador.Name = "tClaveMostrador";
            this.tClaveMostrador.Size = new System.Drawing.Size(88, 20);
            this.tClaveMostrador.TabIndex = 17;
            this.tClaveMostrador.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tClaveMostrador.TextChanged += new System.EventHandler(this.tClaveMostrador_TextChanged);
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(809, 120);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(88, 20);
            this.textBox19.TabIndex = 37;
            // 
            // tClaveSucursal
            // 
            this.tClaveSucursal.Location = new System.Drawing.Point(809, 120);
            this.tClaveSucursal.Name = "tClaveSucursal";
            this.tClaveSucursal.Size = new System.Drawing.Size(88, 20);
            this.tClaveSucursal.TabIndex = 18;
            this.tClaveSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tClaveSucursal.TextChanged += new System.EventHandler(this.tClaveSucursal_TextChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(533, 123);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(118, 13);
            this.label22.TabIndex = 36;
            this.label22.Text = "CVE MOSTRADOR:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(416, 152);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(129, 13);
            this.label23.TabIndex = 38;
            this.label23.Text = "CFDI BACK SERVER:";
            // 
            // tDirBackServer
            // 
            this.tDirBackServer.Location = new System.Drawing.Point(551, 149);
            this.tDirBackServer.Name = "tDirBackServer";
            this.tDirBackServer.Size = new System.Drawing.Size(274, 20);
            this.tDirBackServer.TabIndex = 20;
            // 
            // tCFDIPC
            // 
            this.tCFDIPC.Location = new System.Drawing.Point(129, 176);
            this.tCFDIPC.Name = "tCFDIPC";
            this.tCFDIPC.Size = new System.Drawing.Size(274, 20);
            this.tCFDIPC.TabIndex = 21;
            // 
            // tCFDIBackPC
            // 
            this.tCFDIBackPC.Location = new System.Drawing.Point(551, 176);
            this.tCFDIBackPC.Name = "tCFDIBackPC";
            this.tCFDIBackPC.Size = new System.Drawing.Size(274, 20);
            this.tCFDIBackPC.TabIndex = 22;
            // 
            // btnActBarpro
            // 
            this.btnActBarpro.Enabled = false;
            this.btnActBarpro.Location = new System.Drawing.Point(831, 149);
            this.btnActBarpro.Name = "btnActBarpro";
            this.btnActBarpro.Size = new System.Drawing.Size(66, 68);
            this.btnActBarpro.TabIndex = 24;
            this.btnActBarpro.Text = "ACT BARPRO";
            this.btnActBarpro.UseVisualStyleBackColor = true;
            this.btnActBarpro.Click += new System.EventHandler(this.btnActBarpro_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(12, 204);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(101, 13);
            this.label24.TabIndex = 43;
            this.label24.Text = "RESPONSABLE:";
            // 
            // tResponsable
            // 
            this.tResponsable.Location = new System.Drawing.Point(129, 200);
            this.tResponsable.Name = "tResponsable";
            this.tResponsable.Size = new System.Drawing.Size(423, 20);
            this.tResponsable.TabIndex = 23;
            // 
            // errProvider
            // 
            this.errProvider.ContainerControl = this;
            // 
            // Sucursal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 421);
            this.Controls.Add(this.tResponsable);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.btnActBarpro);
            this.Controls.Add(this.tCFDIBackPC);
            this.Controls.Add(this.tCFDIPC);
            this.Controls.Add(this.tDirBackServer);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.tClaveSucursal);
            this.Controls.Add(this.textBox19);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.tNoInt);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tNombre);
            this.Controls.Add(this.tDescripcion);
            this.Controls.Add(this.tTipo);
            this.Controls.Add(this.tNoExt);
            this.Controls.Add(this.tMunicipio);
            this.Controls.Add(this.tCalle);
            this.Controls.Add(this.tColonia);
            this.Controls.Add(this.tReferencia);
            this.Controls.Add(this.tTelefono);
            this.Controls.Add(this.tRFC);
            this.Controls.Add(this.tCP);
            this.Controls.Add(this.tClaveMostrador);
            this.Controls.Add(this.tDirServer);
            this.Controls.Add(this.tCorreo);
            this.Controls.Add(this.tEstado);
            this.Controls.Add(this.tPais);
            this.Controls.Add(this.tClave);
            this.Controls.Add(this.dataGridView1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Sucursal";
            this.Text = "Sucursal";
            this.Load += new System.EventHandler(this.Sucursal_Load);
            this.Shown += new System.EventHandler(this.Sucursal_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox tClave;
        private System.Windows.Forms.TextBox tPais;
        private System.Windows.Forms.TextBox tEstado;
        private System.Windows.Forms.TextBox tCorreo;
        private System.Windows.Forms.TextBox tDirServer;
        private System.Windows.Forms.TextBox tCP;
        private System.Windows.Forms.TextBox tRFC;
        private System.Windows.Forms.TextBox tTelefono;
        private System.Windows.Forms.TextBox tReferencia;
        private System.Windows.Forms.TextBox tColonia;
        private System.Windows.Forms.TextBox tCalle;
        private System.Windows.Forms.TextBox tMunicipio;
        private System.Windows.Forms.TextBox tNoExt;
        private System.Windows.Forms.TextBox tTipo;
        private System.Windows.Forms.TextBox tDescripcion;
        private System.Windows.Forms.TextBox tNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tNoInt;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tClaveMostrador;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox tClaveSucursal;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tDirBackServer;
        private System.Windows.Forms.TextBox tCFDIPC;
        private System.Windows.Forms.TextBox tCFDIBackPC;
        private System.Windows.Forms.Button btnActBarpro;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tResponsable;
        private System.Windows.Forms.ErrorProvider errProvider;

    }
}