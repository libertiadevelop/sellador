﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Controlador;

namespace Sucursales
{
    public partial class relacion : Form , iRelacionView
    {

        BindingSource bs = new BindingSource ( );

        public relacion ( ) {
            InitializeComponent ( );
        }

        private void relacion_Load ( object sender, EventArgs e ) {

        }

        public void setGridSucursales ( DataTable sucursales ) {
            bs.DataSource = sucursales.DefaultView;
            dataGridView1.DataSource = bs;

            foreach ( DataGridViewColumn c in dataGridView1.Columns ) {
                c.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }


        }

        public void setGridAlmacenes ( DataTable almacenes ) {
            //throw new NotImplementedException ( );
        }
    }
}
