﻿namespace Sucursales
{
    partial class frmContenedor
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose ( bool disposing ) {
            if ( disposing && ( components != null ) ) {
                components.Dispose ( );
            }
            base.Dispose ( disposing );
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent ( ) {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmContenedor));
            this.mnsGeneral = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tlsControl = new System.Windows.Forms.ToolStrip();
            this.tsbRelacion = new System.Windows.Forms.ToolStripButton();
            this.tsbSucursal = new System.Windows.Forms.ToolStripButton();
            this.tsbAlmacen = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbAlta = new System.Windows.Forms.ToolStripButton();
            this.tsbBaja = new System.Windows.Forms.ToolStripButton();
            this.tsbCambio = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbAcepta = new System.Windows.Forms.ToolStripButton();
            this.tsbCancela = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mnsGeneral.SuspendLayout();
            this.tlsControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnsGeneral
            // 
            this.mnsGeneral.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.ayudaToolStripMenuItem});
            this.mnsGeneral.Location = new System.Drawing.Point(0, 0);
            this.mnsGeneral.Name = "mnsGeneral";
            this.mnsGeneral.Size = new System.Drawing.Size(909, 24);
            this.mnsGeneral.TabIndex = 1;
            this.mnsGeneral.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(96, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.ayudaToolStripMenuItem.Text = "Ayuda";
            // 
            // tlsControl
            // 
            this.tlsControl.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbRelacion,
            this.tsbSucursal,
            this.tsbAlmacen,
            this.toolStripSeparator1,
            this.tsbAlta,
            this.tsbBaja,
            this.tsbCambio,
            this.toolStripSeparator2,
            this.tsbAcepta,
            this.tsbCancela});
            this.tlsControl.Location = new System.Drawing.Point(0, 24);
            this.tlsControl.Name = "tlsControl";
            this.tlsControl.Size = new System.Drawing.Size(909, 25);
            this.tlsControl.TabIndex = 2;
            this.tlsControl.Text = "toolStrip1";
            // 
            // tsbRelacion
            // 
            this.tsbRelacion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbRelacion.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tsbRelacion.Image = ((System.Drawing.Image)(resources.GetObject("tsbRelacion.Image")));
            this.tsbRelacion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRelacion.Name = "tsbRelacion";
            this.tsbRelacion.Size = new System.Drawing.Size(58, 22);
            this.tsbRelacion.Text = "Relacion";
            this.tsbRelacion.Click += new System.EventHandler(this.tsbRelacion_Click);
            // 
            // tsbSucursal
            // 
            this.tsbSucursal.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbSucursal.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tsbSucursal.Image = ((System.Drawing.Image)(resources.GetObject("tsbSucursal.Image")));
            this.tsbSucursal.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSucursal.Name = "tsbSucursal";
            this.tsbSucursal.Size = new System.Drawing.Size(69, 22);
            this.tsbSucursal.Text = "Sucursales";
            this.tsbSucursal.Click += new System.EventHandler(this.tsbSucursal_Click);
            // 
            // tsbAlmacen
            // 
            this.tsbAlmacen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbAlmacen.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tsbAlmacen.Image = ((System.Drawing.Image)(resources.GetObject("tsbAlmacen.Image")));
            this.tsbAlmacen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAlmacen.Name = "tsbAlmacen";
            this.tsbAlmacen.Size = new System.Drawing.Size(71, 22);
            this.tsbAlmacen.Text = "Almacenes";
            this.tsbAlmacen.Click += new System.EventHandler(this.tsbAlmacen_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbAlta
            // 
            this.tsbAlta.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbAlta.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tsbAlta.Image = ((System.Drawing.Image)(resources.GetObject("tsbAlta.Image")));
            this.tsbAlta.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAlta.Name = "tsbAlta";
            this.tsbAlta.Size = new System.Drawing.Size(33, 22);
            this.tsbAlta.Text = "Alta";
            this.tsbAlta.Click += new System.EventHandler(this.tsbAlta_Click);
            // 
            // tsbBaja
            // 
            this.tsbBaja.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbBaja.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tsbBaja.Image = ((System.Drawing.Image)(resources.GetObject("tsbBaja.Image")));
            this.tsbBaja.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBaja.Name = "tsbBaja";
            this.tsbBaja.RightToLeftAutoMirrorImage = true;
            this.tsbBaja.Size = new System.Drawing.Size(34, 22);
            this.tsbBaja.Text = "Baja";
            this.tsbBaja.Click += new System.EventHandler(this.tsbBaja_Click);
            // 
            // tsbCambio
            // 
            this.tsbCambio.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbCambio.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tsbCambio.Image = ((System.Drawing.Image)(resources.GetObject("tsbCambio.Image")));
            this.tsbCambio.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCambio.Name = "tsbCambio";
            this.tsbCambio.Size = new System.Drawing.Size(52, 22);
            this.tsbCambio.Text = "Cambio";
            this.tsbCambio.Click += new System.EventHandler(this.tsbCambio_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbAcepta
            // 
            this.tsbAcepta.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbAcepta.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tsbAcepta.Image = ((System.Drawing.Image)(resources.GetObject("tsbAcepta.Image")));
            this.tsbAcepta.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAcepta.Name = "tsbAcepta";
            this.tsbAcepta.Size = new System.Drawing.Size(55, 22);
            this.tsbAcepta.Text = "Aceptar";
            this.tsbAcepta.Click += new System.EventHandler(this.tsbAcepta_Click);
            // 
            // tsbCancela
            // 
            this.tsbCancela.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbCancela.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tsbCancela.Image = ((System.Drawing.Image)(resources.GetObject("tsbCancela.Image")));
            this.tsbCancela.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCancela.Name = "tsbCancela";
            this.tsbCancela.Size = new System.Drawing.Size(58, 22);
            this.tsbCancela.Text = "Cancelar";
            this.tsbCancela.Click += new System.EventHandler(this.tsbCancela_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(909, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "dummy";
            this.menuStrip1.Visible = false;
            // 
            // frmContenedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 479);
            this.Controls.Add(this.tlsControl);
            this.Controls.Add(this.mnsGeneral);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.mnsGeneral;
            this.MaximumSize = new System.Drawing.Size(925, 518);
            this.Name = "frmContenedor";
            this.Text = "CONTROLADOR";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.mnsGeneral.ResumeLayout(false);
            this.mnsGeneral.PerformLayout();
            this.tlsControl.ResumeLayout(false);
            this.tlsControl.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnsGeneral;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.ToolStrip tlsControl;
        private System.Windows.Forms.ToolStripButton tsbRelacion;
        private System.Windows.Forms.ToolStripButton tsbSucursal;
        private System.Windows.Forms.ToolStripButton tsbAlmacen;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbAlta;
        private System.Windows.Forms.ToolStripButton tsbBaja;
        private System.Windows.Forms.ToolStripButton tsbCambio;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbAcepta;
        private System.Windows.Forms.ToolStripButton tsbCancela;
    }
}

