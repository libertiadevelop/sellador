﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace Sucursales
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main ( ) {
            Application.EnableVisualStyles ( );
            Application.SetCompatibleTextRenderingDefault ( false );

            String[] argumentos = Environment.GetCommandLineArgs ( );

            if ( argumentos.Length != 3 ) {
                return;
            }


            Application.Run ( new frmContenedor ( argumentos[1] ) );
        }
    }
}
