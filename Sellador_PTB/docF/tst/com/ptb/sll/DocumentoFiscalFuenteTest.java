package com.ptb.sll;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ptb.sll.DocumentoFiscalFuente.validaciones;

import java.util.ArrayList;
import java.io.*;
import java.util.HashMap;

import com.ptb.sll.VersionSat;

public class DocumentoFiscalFuenteTest {

	
	String 	PathXML 	= "000014_0101027300_A_101_000623_20150518.xml";
	String 	PathXSD2	= "cfdv2.xsd";
	String  PathXSD3	= "cfd32.xsd";
	String 	PathXSD		= "";
	String 	PathCert 	= "aaa010101aaa.cer";
	String 	PathKey		= "aaa010101aaa.key";
	String	PathXSLT	= "";
	String 	PathXSLT3	= "cadenaoriginal_3_2.xslt";
	String  PathXSLT2	= "cadenaoriginal_2_0.xslt";
	
	String	rutaPrueba	= "documentosDePrueba/";
	
	String	Password	= "a0123456789";
	
	
	String PasswordB	= "BAR810309";
	
	
	
	HashMap<String, Boolean> erroresEsperados = new HashMap<String, Boolean>();
	
	
	DocumentoFiscalFuente dff;
	
	@Test
	public void test() {
		assertTrue(true);
	}
	
	@Test
	public void testId() {
		
		LimpiaInformacion();
		
		DocumentoFiscalFuente df = new DocumentoFiscalFuente( PathXML, PathXSD );
		
		assertEquals("Se esperaba el id = 105", "105", df.getID() );
		
	}
	
	public void testVersion() {
		
		LimpiaInformacion();
		
		DocumentoFiscalFuente df = new DocumentoFiscalFuente ( PathXML, PathXSD );
		
		assertEquals("Se esperaba la version 0.0.0.0" , "0.0.0.0", df.getVersion() );
		
	}
	
	@Test
	public void testValidaConstructor() throws IOException
	{
		
		LimpiaInformacion();
		
		// Valida cada uno de los constructores;
		
		dff = new DocumentoFiscalFuente( "","" );
		
		erroresEsperados.clear();
		erroresEsperados.put("VALIDA-XML", false );
		erroresEsperados.put("VALIDA-XSD", false );
		
		assertFalse("Tiene que ser falso, cadenas vacias", dff.getEstado() );
		assertEquals("validando matriz de errores", erroresEsperados, dff.getResultadoDeLasValidaciones() );
		
		dff = new DocumentoFiscalFuente("no.x", "no.y" );
		
		erroresEsperados.clear();
		erroresEsperados.put("VALIDA-XML", false );
		erroresEsperados.put("VALIDA-XSD", false );
		
		assertFalse("Tiene que ser falso, archivos inexistentes", dff.getEstado() );
		assertEquals("validando matriz de errores",erroresEsperados, dff.getResultadoDeLasValidaciones() );
		
		PathXSD 	= PathXSD2;
		PathXSLT	= PathXSLT3;
		
		copiaArchivosDePrueba();
		
		dff = new DocumentoFiscalFuente( PathXML, PathXSD );
		
		erroresEsperados.clear();
		erroresEsperados.put("VALIDA-XML", true );
		erroresEsperados.put("VALIDA-XSD", true );
		
		assertTrue("Tiene que ser falso, archivos inexistentes", dff.getEstado() );
		assertEquals("validando matriz de errores",erroresEsperados, dff.getResultadoDeLasValidaciones() );
		
		
		LimpiaInformacion();
		
		
	}
	
	@Test
	public void testValidaUTF8() throws IOException {
		
		LimpiaInformacion();
		
		PathXSD 	= PathXSD3;
		
		PathXSLT	= PathXSLT3;
		
		copiaArchivosDePrueba();
		
		dff = new DocumentoFiscalFuente( PathXML, PathXSD );
		
		dff.valida( validaciones.UTF8 );
		
		assertTrue("Se esperaba que no hubiera errores", dff.getEstado() );
		
		LimpiaInformacion();
		
	}
	
	@Test
	public void testValidaEsquema() throws IOException
	{
		LimpiaInformacion();
		
		PathXSD 	= PathXSD3;
		PathXSLT	= PathXSLT3;
		
		copiaArchivosDePrueba();
		
		dff = new DocumentoFiscalFuente( PathXML, PathXSD );
		
		dff.valida( validaciones.UTF8 );
		
		assertTrue("Se esperaba que no hubiera errores", dff.getEstado() );
		
		dff.setVersionXML( VersionSat.VERSION_3_2 );
		
		dff.valida( validaciones.ESQUEMA );
		
		assertTrue("Se esperaba que no hubiera errores", dff.getEstado() );
		
		LimpiaInformacion();
	}
	
	@Test
	public void testValidaImportes() throws IOException
	{
		LimpiaInformacion();
		
		PathXSD = PathXSD3;
		PathXSLT	= PathXSLT3;
		
		copiaArchivosDePrueba();
		
		dff = new DocumentoFiscalFuente( PathXML, PathXSD );
		
		dff.valida( validaciones.UTF8 );
		
		assertTrue("Se esperaba que no hubiera errores utf-8", dff.getEstado() );
		
		dff.setVersionXML( VersionSat.VERSION_3_2 );
		
		dff.valida( validaciones.ESQUEMA );
		
		assertTrue("Se esperaba que no hubiera errores esquema", dff.getEstado() );
		
		dff.valida( validaciones.IMPORTES );
		
		assertTrue("Se esperaba que no hubiera errores importes", dff.getEstado() );
		
		LimpiaInformacion();
	}
	
	@Test 
	public void testSello() throws IOException
	{
		LimpiaInformacion();
		
		PathXSD 	= PathXSD3;
		PathXSLT	= PathXSLT3;
		
		PathXML		= "240486153837FACMEX.xml";
		PathCert	= "00001000000201624125.cer";
		PathKey		= "bar810309n12_1207241841s.key";
		Password	= "BAR810309";
		
		
		copiaArchivosDePrueba();
		
		dff = new DocumentoFiscalFuente ( PathXML, PathXSD );
		
		dff.valida( validaciones.UTF8 );
		
		assertTrue("Se esperaba que no hubiera errores", dff.getEstado() );
		
		dff.setVersionXML( VersionSat.VERSION_3_2 );
		
		dff.valida( validaciones.ESQUEMA );
		
		assertTrue("Se esperaba que no hubiera errores", dff.getEstado() );
		
		dff.valida( validaciones.IMPORTES );
		
		assertTrue("Se esperaba que no hubiera errores", dff.getEstado() );
		
		dff.SellaDocumento(PathCert, PathKey, Password, PathXSLT);
		
		assertTrue("Se esperaba que no hubiera errores", dff.getEstado() );
		
		com.ptb.xsd.sat.v32.Comprobante c = (com.ptb.xsd.sat.v32.Comprobante) dff.getXMlObject();
		
		assertEquals("Se esperaba que el no certificado fuera el mismo", "00001000000201624125", c.getNoCertificado());
		
		assertEquals("Se esperaba que el sello fuera el mismo", "Jnoh2pWEqgbf61lYk13Y6kd/WbZWxcmn9IfR4FOCEZWZkepca/5pvuriX6HUIzLjFxPL+ASqWuTkGlzMSbT/LHYrTtBxN06cL77SxkhVFl+HJz1mbUGx9tYEgfeLrofImUSCn2WdgUzgZcgbfTFh71d4hgQD9cxBwW63raooMog=", c.getSello());
													
		assertEquals("Se esperaba que el certificado fuera el mismo", "MIIEZTCCA02gAwIBAgIUMDAwMDEwMDAwMDAyMDE2MjQxMjUwDQYJKoZIhvcNAQEFBQAwggGVMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMSEwHwYJKoZIhvcNAQkBFhJhc2lzbmV0QHNhdC5nb2IubXgxJjAkBgNVBAkMHUF2LiBIaWRhbGdvIDc3LCBDb2wuIEd1ZXJyZXJvMQ4wDAYDVQQRDAUwNjMwMDELMAkGA1UEBhMCTVgxGTAXBgNVBAgMEERpc3RyaXRvIEZlZGVyYWwxFDASBgNVBAcMC0N1YXVodMOpbW9jMRUwEwYDVQQtEwxTQVQ5NzA3MDFOTjMxPjA8BgkqhkiG9w0BCQIML1Jlc3BvbnNhYmxlOiBDZWNpbGlhIEd1aWxsZXJtaW5hIEdhcmPDrWEgR3VlcnJhMB4XDTEyMDcyNDIzMzgzOFoXDTE2MDcyNDIzMzgzOFowgaYxGDAWBgNVBAMTD0JBUk1FWCBTQSBERSBDVjEYMBYGA1UEKRMPQkFSTUVYIFNBIERFIENWMRgwFgYDVQQKEw9CQVJNRVggU0EgREUgQ1YxJTAjBgNVBC0THEJBUjgxMDMwOU4xMiAvIExPRkM2MzA3MjdFWTUxHjAcBgNVBAUTFSAvIExPRkM2MzA3MjdNREZQTFQwMzEPMA0GA1UECxMGQkFSTUVYMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC3NnANfhQmudVV+Nh+/hvgbTdcJI34TaMPqMPDqLONgt7a6LTKlw+b4HI/Uve6M8dnaHs/IHYYTuLrKWU+xXvoc3frPoSNb7U4iXirHMO2sCU5zhvC5KdSYRv6Er3iGHCEIQALibrNBkPxvb2me7KgrQTZpwjSzh4nP8OvQrKR0QIDAQABox0wGzAMBgNVHRMBAf8EAjAAMAsGA1UdDwQEAwIGwDANBgkqhkiG9w0BAQUFAAOCAQEAgQ9yHmJrX8lphPHfQv1DPgykhZKk+4ZT5mmQ713dv64Tqdhemw4DS5WBWBDhZvvSHWuGoCwmv4WUtnY/8V9pYCl2/nU5crg3jLB9S+Xrjk/zgOL/AUyG965ssVjmZwkgpB9LEDJQH+uzNDjz2X36eHF/ZZlLtCw0iCgyIDlFTLQLudpA4mfkdQfEveNpdS34aKHc5sM4rLFNL1NcmdcnObUjyyvry8RddDMwgXmyjP2ICsjctYsV3PVs7k080w//J9Gtpbp7WgFGLoumJJOA+s+VAYAkFs3tEgmhc6Rl1g5k0vlY8BgWVR/iI5HQGL1WhXgeQoSr+gQ4SIRjnCHQqA==", c.getCertificado());
		
		
		LimpiaInformacion();
		
		PathXML 	= "000014_0101027300_A_101_000623_20150518.xml";
		PathCert 	= "aaa010101aaa.cer";
		PathKey		= "aaa010101aaa.key";
		Password	= "a0123456789";
	}
	
	private void copiaArchivosDePrueba() throws IOException {
		
		ArrayList<String> archivos = new ArrayList<String>();
		
		archivos.add( PathXML );
		archivos.add( PathXSD );
		archivos.add( PathCert );
		archivos.add( PathKey );
		archivos.add( PathXSLT );
		
		for ( String a : archivos ){
			CopyFile( rutaPrueba + a, a );
		}
		
	}
	
	private void CopyFile ( String Source, String Dest ) throws IOException{
		
		File source = new File( Source );
		File dest	= new File( Dest );
		
		InputStream 	is = null;
		OutputStream	os = null;
		
		try {
			
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			
			byte[] buffer = new byte[1024];
			int length = 0;
			
			while (( length = is.read(buffer) ) > 0 ) {
				os.write( buffer, 0, length );
			}
			
		} finally {
			is.close();
			os.close();
		}
		
	}
	
	private void LimpiaInformacion(){
		
		// Elimina todos los archivos de prueba
		
		ArrayList<String> archivos = new ArrayList<String>();
		
		archivos.add( PathXML );
		archivos.add( PathXSD );
		archivos.add( PathCert );
		archivos.add( PathKey );
		archivos.add( PathXSLT );
		
		for( String a : archivos ){
			File f = new File ( a  );
			if ( f.exists() ) {
				f.delete();
			}
		}
		
		
	}
	

}
