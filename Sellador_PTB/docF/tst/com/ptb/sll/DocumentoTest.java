package com.ptb.sll;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;


import java.awt.Desktop;
import java.io.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class DocumentoTest {

	String RFC 			= "GACA760103F70";
	String archivo		= "factura.xml";
	String xmlOrigen	= "resultado.xml";
	
	@Test
	public void A_testDocumento() {
		assertTrue( true );
	}
	
	@Test
	public void T_RecuperaUnTextoFile() throws IOException{
		// Primero copia el archivo de documentosDePrueba a el dir raiz.
		
		String archivoDeTexto = "010087137696FACMEX_S.INI";
		
		File docTexto = new File( archivoDeTexto );
		
		if ( docTexto.exists() ){
			docTexto.delete();
		}
		
		// Copia el archivo
		InputStream 	is = new FileInputStream("documentosDePrueba" + File.separator + archivoDeTexto );
		OutputStream 	os = new FileOutputStream(docTexto);
		
		byte[] buffer = new byte[1024];
		int length;
		while ( (length = is.read(buffer)) > 0 ) {
			os.write(buffer, 0, length);
		}
		is.close();
		os.close();
		
		// Realiza la prueba
		
		String respuesta = ""; //recuperaInformacion.recupera(archivoDeTexto, "noCertificado");   
		
		
		assertTrue("Se esperaba verdadero", respuesta.equals("00001000000201624125"));
		
		//Borra la informacion residual
		
		if ( docTexto.exists() ){
			docTexto.delete();
		}
		
	}
	
	@Test
	public void T_RecuperaUnTextoXMLAtributo() throws IOException{
		String archivoXML = "010087137696FACMEX_S.xml";
		
		File docXML = new File( archivoXML );
		
		if ( docXML.exists() ){
			docXML.delete();
		}
		
		// Copia el archivo
		InputStream 	is = new FileInputStream("documentosDePrueba" + File.separator + archivoXML );
		OutputStream 	os = new FileOutputStream(docXML);
		
		byte[] buffer = new byte[1024];
		int length;
		while ( (length = is.read(buffer)) > 0 ) {
			os.write(buffer, 0, length);
		}
		is.close();
		os.close();
		
		// Realiza la prueba
		
		String respuesta = ""; //recuperaInformacion.recupera(archivoXML, "//*/@serie");
		
		assertTrue("Se esperaba verdadero", respuesta.equals("FACMEX"));
		
		//Borra la informacion residual
		
		if ( docXML.exists() ){
			docXML.delete();
		}
	}
	
	@Test
	public void T_RecuperaUnTextoXMLAtributoInside() throws IOException{
		String archivoXML = "010087137696FACMEX_S.xml";
		
		File docXML = new File( archivoXML );
		
		if ( docXML.exists() ){
			docXML.delete();
		}
		
		// Copia el archivo
		InputStream 	is = new FileInputStream("documentosDePrueba" + File.separator + archivoXML );
		OutputStream 	os = new FileOutputStream(docXML);
		
		byte[] buffer = new byte[1024];
		int length;
		while ( (length = is.read(buffer)) > 0 ) {
			os.write(buffer, 0, length);
		}
		is.close();
		os.close();
		
		// Realiza la prueba
		
		String respuesta = ""; //recuperaInformacion.recupera(archivoXML, "//Emisor/@rfc");
		
		assertTrue("Se esperaba verdadero", respuesta.equals("BAR810309N12"));
		
		//Borra la informacion residual
		
		if ( docXML.exists() ){
			docXML.delete();
		}
	}
	
	@Test
	public void T_RecuperaVariasPartidas() throws IOException {
		String archivoXML = "010087137696FACMEX_S.xml";
		
		File docXML = new File( archivoXML );
		
		if ( docXML.exists() ){
			docXML.delete();
		}
		
		// Copia el archivo
		InputStream 	is = new FileInputStream("documentosDePrueba" + File.separator + archivoXML );
		OutputStream 	os = new FileOutputStream(docXML);
		
		byte[] buffer = new byte[1024];
		int length;
		while ( (length = is.read(buffer)) > 0 ) {
			os.write(buffer, 0, length);
		}
		is.close();
		os.close();
		
		// Realiza la prueba
		
		Map<String, List<String>> respuesta = null; //recuperaInformacion.recupera(archivoXML, "//Conceptos/*", "noIdentificacion");
		
		assertEquals("Se esperaba el numero: "  , 3, respuesta.size());
		
		String[] llaves 	= {"25TAC62BSUC10PN7","CR 40 1R", "CR 50 1R"};
		Set<String> llav = new HashSet<String>(Arrays.asList(llaves));
		
		Set<String> enMap 		= respuesta.keySet();
		
		enMap.removeAll(llav);
		
		assertEquals("Se esperaba cero", 0, enMap.size());
		
		//Borra la informacion residual
		
		if ( docXML.exists() ){
			docXML.delete();
		}
	}
}
