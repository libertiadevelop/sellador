package com.ptb.sll;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import org.junit.Test;

public class DocumentoFiscalTest {
	
	String 	PathXML 	= "000014_0101027300_A_101_000623_20150518.xml";
	String 	PathXML_S 	= "000014_0101027300_A_101_000623_20150518_S.xml";
	String 	PathXSD2	= "cfdv2.xsd";
	String  PathXSD3	= "cfd32.xsd";
	String 	PathXSD		= "";
	String 	PathCert 	= "aaa010101aaa.cer";
	String 	PathKey		= "aaa010101aaa.key";
	String	PathXSLT	= "";
	String 	PathXSLT3	= "cadenaoriginal_3_2.xslt";
	String  PathXSLT2	= "cadenaoriginal_2_0.xslt";
	
	String	rutaPrueba	= "documentosDePrueba/";
	
	String	Password	= "a0123456789";
	
	
	String PasswordB	= "BAR810309";
	

	@Test
	public void testSellaDocumento() throws IOException {
		PathXSD 	= PathXSD3;
		PathXSLT	= PathXSLT3;
		
		PathXML		= "240486153837FACMEX.xml";
		PathXML_S	= "240486153837FACMEX_S.xml";
		PathCert	= "00001000000201624125.cer";
		PathKey		= "bar810309n12_1207241841s.key";
		Password	= "BAR810309";
		LimpiaInformacion();
		copiaArchivosDePrueba();
		
		DocumentoFiscal d = new DocumentoFiscal(PathXML, PathXML_S, PathKey, PathCert, Password, PathXSD3, PathXSLT3, "3.2", true );
		
		d.sellaDocumento();
		
		assertTrue("Se esperaba que fuera verdadero " + d.getDescripcionDeError(), d.getEstado() );
		
		LimpiaInformacion();
		
	}

	private void copiaArchivosDePrueba() throws IOException {
		
		ArrayList<String> archivos = new ArrayList<String>();
		
		archivos.add( PathXML );
		archivos.add( PathXSD );
		archivos.add( PathCert );
		archivos.add( PathKey );
		archivos.add( PathXSLT );
		
		for ( String a : archivos ){
			CopyFile( rutaPrueba + a, a );
		}
		
	}
	
	private void CopyFile ( String Source, String Dest ) throws IOException{
		
		File source = new File( Source );
		File dest	= new File( Dest );
		
		InputStream 	is = null;
		OutputStream	os = null;
		
		try {
			
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			
			byte[] buffer = new byte[1024];
			int length = 0;
			
			while (( length = is.read(buffer) ) > 0 ) {
				os.write( buffer, 0, length );
			}
			
		} finally {
			is.close();
			os.close();
		}
		
	}
	
	private void LimpiaInformacion(){
		
		// Elimina todos los archivos de prueba
		
		ArrayList<String> archivos = new ArrayList<String>();
		
		archivos.add( PathXML );
		archivos.add( PathXSD );
		archivos.add( PathCert );
		archivos.add( PathKey );
		archivos.add( PathXSLT );
		
		for( String a : archivos ){
			File f = new File ( a  );
			if ( f.exists() ) {
				f.delete();
			}
		}
		
		
	}
	
	
}
