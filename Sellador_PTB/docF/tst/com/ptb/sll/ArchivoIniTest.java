package com.ptb.sll;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.FixMethodOrder;
import org.junit.Test;
@FixMethodOrder
public class ArchivoIniTest {

	private String pathToIni = "000001000002FACMEX_S.INI";
	
	@Test
	public void testGetVersion() {
		
		ArchivoIni ai = new ArchivoIni( pathToIni );
		assertEquals("La version, no corresponde", "0.0.0.0", ai.getVersion());
		EliminaIniDePrueba();
	}

	@Test
	public void testGetID() {
		ArchivoIni ai = new ArchivoIni( pathToIni );
		assertEquals("El id no corresponde", "100", ai.getID());
		EliminaIniDePrueba();
	}

	@Test
	public void testSetValor() throws IOException {
		EliminaIniDePrueba();
		
		ArchivoIni ai = new ArchivoIni( pathToIni );
		
		ai.setValor("Uno", "Este es un ejemplo");
		ai.setValor("Dos", "Este es un ejemplo");
		
		assertEquals("Se esperagan solo dos registros", 2, regresaDatosDelArchivoIni().size() );
		
		
		EliminaIniDePrueba();
	}
	
	@Test
	public void testSetValorDuplicado() throws IOException {
		EliminaIniDePrueba();
		
		ArchivoIni ai = new ArchivoIni( pathToIni );
		
		ai.setValor("Uno", "Este es un ejemplo");
		ai.setValor("Uno", "Este es un ejemplo");
		
		assertEquals("Se debe manda un error de duplicidad", "111", ai.getCodError() );
		assertEquals("Solo se guardo un registro", 1, regresaDatosDelArchivoIni().size() );
		
		
		EliminaIniDePrueba();
	}
	
	@Test
	public void testEliminaArchivoPrevio() throws IOException{
		EliminaIniDePrueba();
		creaIniDummy();
		
		ArchivoIni ai = new ArchivoIni( pathToIni );
		
		ai.setValor("ejemplo", "Esto es un ejemplo");
		
		List<String> prueba = new ArrayList<String>();
		prueba.add("ejemplo=Esto es un ejemplo");
		
		List<String> datosEnIni = regresaDatosDelArchivoIni();
		
		List<String> Resultado = new ArrayList<String>();
		
		Resultado.addAll(prueba);
		Resultado.removeAll(datosEnIni);
		
		// El resultado esperado es 0, ya que se borra el archivo anterior.
		
		assertEquals("Se debio eliminar el archivo previo", 0, Resultado.size() );
		
		EliminaIniDePrueba();
	}
	
	@Test
	public void testGetValor() throws FileNotFoundException {
		EliminaIniDePrueba();
		
		ArchivoIni ai = new ArchivoIni( pathToIni );
		
		ai.setValor("Cadena", "||3.2|2015-07-01T09:06:33|ingreso|PAGO EN UNA SOLA EXHIBICION|MONEDA: MXP TIPO DE CAMBIO: 1.0000|4154.000|0.000|1.0000|MXN|4818.640|TRANSFERENC BANCARIA|DELG. CUAUHTEMOC, MEXICO, D.F.|0000|BAR810309N12|BARMEX, S.A. DE C.V.|CDA. DE CEDRO|NO. 501 Y 509|ATLAMPA|DELG. CUAUHTEMOC|MEXICO, D.F|MEXICO|06450|CDA. DE CEDRO|NO. 501 Y 509|COL. ATLAMPA|DELG. CUAUHTEMOC|MEXICO, D.F.|MEXICO|06450|REGIMEN GENERAL DE LEY PERSONAS MORALES|MAI131217843|MAQUINADOS Y AUTOMATIZACION INDUSTRIAL ACAMSA,SA DE CV|MARACAIBO NO.27|SAN PEDRO ZACATENCO GUSTAVO A. MADERO|MEXICO, D.F.|MEXICO|07360|5.000|PZ|D40BTB18H (MAR)|SPROCKET|692.000|3460.000|1.000|PZ|D40BTB19H (MAR)|SPROCKET|694.000|694.000|IVA|16.00|664.640|664.640||");
		ai.setValor("Sello", "Xjb3I60xGEGKloYM/XiUgHxt9WBZ4dhi6vSWyA30ABLlxM2/IzjIlU12/8L92xhf\nP8KEIhR47nvG7rHh1fd7qtzl2xx8ma25DNO6TqmzcMbO0gLqeyu4XQc680CNQoVv\nJj4n284NQhFR35RhtgruPb3Sn7IHLCF7Gd0ReKy97xs=\n");
		ai.setValor("noCertificado", "00001000000201624125");
		ai.setValor("Certificado", "MIIEZTCCA02gAwIBAgIUMDAwMDEwMDAwMDAyMDE2MjQxMjUwDQYJKoZIhvcNAQEFBQAwggGVMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMSEwHwYJKoZIhvcNAQkBFhJhc2lzbmV0QHNhdC5nb2IubXgxJjAkBgNVBAkMHUF2LiBIaWRhbGdvIDc3LCBDb2wuIEd1ZXJyZXJvMQ4wDAYDVQQRDAUwNjMwMDELMAkGA1UEBhMCTVgxGTAXBgNVBAgMEERpc3RyaXRvIEZlZGVyYWwxFDASBgNVBAcMC0N1YXVodMOpbW9jMRUwEwYDVQQtEwxTQVQ5NzA3MDFOTjMxPjA8BgkqhkiG9w0BCQIML1Jlc3BvbnNhYmxlOiBDZWNpbGlhIEd1aWxsZXJtaW5hIEdhcmPDrWEgR3VlcnJhMB4XDTEyMDcyNDIzMzgzOFoXDTE2MDcyNDIzMzgzOFowgaYxGDAWBgNVBAMTD0JBUk1FWCBTQSBERSBDVjEYMBYGA1UEKRMPQkFSTUVYIFNBIERFIENWMRgwFgYDVQQKEw9CQVJNRVggU0EgREUgQ1YxJTAjBgNVBC0THEJBUjgxMDMwOU4xMiAvIExPRkM2MzA3MjdFWTUxHjAcBgNVBAUTFSAvIExPRkM2MzA3MjdNREZQTFQwMzEPMA0GA1UECxMGQkFSTUVYMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC3NnANfhQmudVV+Nh+/hvgbTdcJI34TaMPqMPDqLONgt7a6LTKlw+b4HI/Uve6M8dnaHs/IHYYTuLrKWU+xXvoc3frPoSNb7U4iXirHMO2sCU5zhvC5KdSYRv6Er3iGHCEIQALibrNBkPxvb2me7KgrQTZpwjSzh4nP8OvQrKR0QIDAQABox0wGzAMBgNVHRMBAf8EAjAAMAsGA1UdDwQEAwIGwDANBgkqhkiG9w0BAQUFAAOCAQEAgQ9yHmJrX8lphPHfQv1DPgykhZKk+4ZT5mmQ713dv64Tqdhemw4DS5WBWBDhZvvSHWuGoCwmv4WUtnY/8V9pYCl2/nU5crg3jLB9S+Xrjk/zgOL/AUyG965ssVjmZwkgpB9LEDJQH+uzNDjz2X36eHF/ZZlLtCw0iCgyIDlFTLQLudpA4mfkdQfEveNpdS34aKHc5sM4rLFNL1NcmdcnObUjyyvry8RddDMwgXmyjP2ICsjctYsV3PVs7k080w//J9Gtpbp7WgFGLoumJJOA+s+VAYAkFs3tEgmhc6Rl1g5k0vlY8BgWVR/iI5HQGL1WhXgeQoSr+gQ4SIRjnCHQqA==");
		ai.setValor("selloCFD", "Xjb3I60xGEGKloYM/XiUgHxt9WBZ4dhi6vSWyA30ABLlxM2/IzjIlU12/8L92xhfP8KEIhR47nvG7rHh1fd7qtzl2xx8ma25DNO6TqmzcMbO0gLqeyu4XQc680CNQoVvJj4n284NQhFR35RhtgruPb3Sn7IHLCF7Gd0ReKy97xs=");
		ai.setValor("version", "1.0");
		ai.setValor("FechaTimbrado", "2015-07-01T09:10:02");
		ai.setValor("UUID", "F3524E44-732A-46C5-84FE-CF075CB55B98");
		ai.setValor("selloSAT", "ETBBH+qAkHmr8Lc8vs9hmsF6aL8pHCpDWv0oRtfABqWvIMMeG+eizWCrMnW3/yXThmf7nEvDH7eJF/nRy/L+Mol74RRs1cG2u3pqy3ABTdY+DFx0z35dQNcVnpPIBgi5BsEDEzqIchqQdNlvXzzUShrty4pr96cQql6Wc3QqbIM=");
		ai.setValor("noCertificadoSAT", "00001000000203220546");
		ai.setValor("CadenaTimbre", "||1.0|F3524E44-732A-46C5-84FE-CF075CB55B98|2015-07-01T09:10:02|Xjb3I60xGEGKloYM/XiUgHxt9WBZ4dhi6vSWyA30ABLlxM2/IzjIlU12/8L92xhfP8KEIhR47nvG7rHh1fd7qtzl2xx8ma25DNO6TqmzcMbO0gLqeyu4XQc680CNQoVvJj4n284NQhFR35RhtgruPb3Sn7IHLCF7Gd0ReKy97xs=|00001000000203220546||");
		ai.setValor("Pac", "FEL");
		
		
		String resultado = ai.getValor("noCertificado");
		
		assertEquals("Se esperaba el valor del certificado", "00001000000201624125", resultado);
		
		EliminaIniDePrueba();
		
	}

	@Test
	public void testGetLlaves() throws FileNotFoundException {
		EliminaIniDePrueba();
		
		ArchivoIni ai = new ArchivoIni( pathToIni );
		creaIniDePrueba();
		
		List<String> llavesRecuperadas = ai.getLlaves();
		
		List<String> compara = new ArrayList<String>();
		compara.addAll(llavesRecuperadas);
		
		compara.removeAll( GeneraLineasDeValidacionLlaves() );
		
		assertEquals("Se esperaba que las llaves se recuperaran", 0, compara.size());
		
		
		EliminaIniDePrueba();
	}

	@Test
	public void testGetPares() throws FileNotFoundException {
		EliminaIniDePrueba();
		ArchivoIni ai = new ArchivoIni( pathToIni );
		
		ai.setValor("Cadena", "||3.2|2015-07-01T09:06:33|ingreso|PAGO EN UNA SOLA EXHIBICION|MONEDA: MXP TIPO DE CAMBIO: 1.0000|4154.000|0.000|1.0000|MXN|4818.640|TRANSFERENC BANCARIA|DELG. CUAUHTEMOC, MEXICO, D.F.|0000|BAR810309N12|BARMEX, S.A. DE C.V.|CDA. DE CEDRO|NO. 501 Y 509|ATLAMPA|DELG. CUAUHTEMOC|MEXICO, D.F|MEXICO|06450|CDA. DE CEDRO|NO. 501 Y 509|COL. ATLAMPA|DELG. CUAUHTEMOC|MEXICO, D.F.|MEXICO|06450|REGIMEN GENERAL DE LEY PERSONAS MORALES|MAI131217843|MAQUINADOS Y AUTOMATIZACION INDUSTRIAL ACAMSA,SA DE CV|MARACAIBO NO.27|SAN PEDRO ZACATENCO GUSTAVO A. MADERO|MEXICO, D.F.|MEXICO|07360|5.000|PZ|D40BTB18H (MAR)|SPROCKET|692.000|3460.000|1.000|PZ|D40BTB19H (MAR)|SPROCKET|694.000|694.000|IVA|16.00|664.640|664.640||");
		ai.setValor("Sello", "Xjb3I60xGEGKloYM/XiUgHxt9WBZ4dhi6vSWyA30ABLlxM2/IzjIlU12/8L92xhf\\nP8KEIhR47nvG7rHh1fd7qtzl2xx8ma25DNO6TqmzcMbO0gLqeyu4XQc680CNQoVv\\nJj4n284NQhFR35RhtgruPb3Sn7IHLCF7Gd0ReKy97xs=\\n");
		ai.setValor("noCertificado", "00001000000201624125");
		ai.setValor("Certificado", "MIIEZTCCA02gAwIBAgIUMDAwMDEwMDAwMDAyMDE2MjQxMjUwDQYJKoZIhvcNAQEFBQAwggGVMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMSEwHwYJKoZIhvcNAQkBFhJhc2lzbmV0QHNhdC5nb2IubXgxJjAkBgNVBAkMHUF2LiBIaWRhbGdvIDc3LCBDb2wuIEd1ZXJyZXJvMQ4wDAYDVQQRDAUwNjMwMDELMAkGA1UEBhMCTVgxGTAXBgNVBAgMEERpc3RyaXRvIEZlZGVyYWwxFDASBgNVBAcMC0N1YXVodMOpbW9jMRUwEwYDVQQtEwxTQVQ5NzA3MDFOTjMxPjA8BgkqhkiG9w0BCQIML1Jlc3BvbnNhYmxlOiBDZWNpbGlhIEd1aWxsZXJtaW5hIEdhcmPDrWEgR3VlcnJhMB4XDTEyMDcyNDIzMzgzOFoXDTE2MDcyNDIzMzgzOFowgaYxGDAWBgNVBAMTD0JBUk1FWCBTQSBERSBDVjEYMBYGA1UEKRMPQkFSTUVYIFNBIERFIENWMRgwFgYDVQQKEw9CQVJNRVggU0EgREUgQ1YxJTAjBgNVBC0THEJBUjgxMDMwOU4xMiAvIExPRkM2MzA3MjdFWTUxHjAcBgNVBAUTFSAvIExPRkM2MzA3MjdNREZQTFQwMzEPMA0GA1UECxMGQkFSTUVYMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC3NnANfhQmudVV+Nh+/hvgbTdcJI34TaMPqMPDqLONgt7a6LTKlw+b4HI/Uve6M8dnaHs/IHYYTuLrKWU+xXvoc3frPoSNb7U4iXirHMO2sCU5zhvC5KdSYRv6Er3iGHCEIQALibrNBkPxvb2me7KgrQTZpwjSzh4nP8OvQrKR0QIDAQABox0wGzAMBgNVHRMBAf8EAjAAMAsGA1UdDwQEAwIGwDANBgkqhkiG9w0BAQUFAAOCAQEAgQ9yHmJrX8lphPHfQv1DPgykhZKk+4ZT5mmQ713dv64Tqdhemw4DS5WBWBDhZvvSHWuGoCwmv4WUtnY/8V9pYCl2/nU5crg3jLB9S+Xrjk/zgOL/AUyG965ssVjmZwkgpB9LEDJQH+uzNDjz2X36eHF/ZZlLtCw0iCgyIDlFTLQLudpA4mfkdQfEveNpdS34aKHc5sM4rLFNL1NcmdcnObUjyyvry8RddDMwgXmyjP2ICsjctYsV3PVs7k080w//J9Gtpbp7WgFGLoumJJOA+s+VAYAkFs3tEgmhc6Rl1g5k0vlY8BgWVR/iI5HQGL1WhXgeQoSr+gQ4SIRjnCHQqA==");
		ai.setValor("selloCFD", "Xjb3I60xGEGKloYM/XiUgHxt9WBZ4dhi6vSWyA30ABLlxM2/IzjIlU12/8L92xhfP8KEIhR47nvG7rHh1fd7qtzl2xx8ma25DNO6TqmzcMbO0gLqeyu4XQc680CNQoVvJj4n284NQhFR35RhtgruPb3Sn7IHLCF7Gd0ReKy97xs=");
		ai.setValor("version", "1.0");
		ai.setValor("FechaTimbrado", "2015-07-01T09:10:02");
		ai.setValor("UUID", "F3524E44-732A-46C5-84FE-CF075CB55B98");
		ai.setValor("selloSAT", "ETBBH+qAkHmr8Lc8vs9hmsF6aL8pHCpDWv0oRtfABqWvIMMeG+eizWCrMnW3/yXThmf7nEvDH7eJF/nRy/L+Mol74RRs1cG2u3pqy3ABTdY+DFx0z35dQNcVnpPIBgi5BsEDEzqIchqQdNlvXzzUShrty4pr96cQql6Wc3QqbIM=");
		ai.setValor("noCertificadoSAT", "00001000000203220546");
		ai.setValor("CadenaTimbre", "||1.0|F3524E44-732A-46C5-84FE-CF075CB55B98|2015-07-01T09:10:02|Xjb3I60xGEGKloYM/XiUgHxt9WBZ4dhi6vSWyA30ABLlxM2/IzjIlU12/8L92xhfP8KEIhR47nvG7rHh1fd7qtzl2xx8ma25DNO6TqmzcMbO0gLqeyu4XQc680CNQoVvJj4n284NQhFR35RhtgruPb3Sn7IHLCF7Gd0ReKy97xs=|00001000000203220546||");
		ai.setValor("Pac", "FEL");
		
		Map<String, String> compara = ai.getPares();
		
		compara.entrySet().removeAll( GeneraLineasDeValidacionPares().entrySet() );
		
		assertEquals( "Se esperaba que los maps fueran iguales", 0, compara.size() );
		
		EliminaIniDePrueba();
	}


	private void EliminaIniDePrueba() {
		File ip = new File ( pathToIni ) ;
		ip.delete();
	}
	
	private void creaIniDummy() throws FileNotFoundException{
		PrintWriter p = new PrintWriter( new File ( pathToIni ) );
		p.println("no=sss");
		p.close();
	}
	
	private void creaIniDePrueba() throws FileNotFoundException{
		
		PrintWriter p = new PrintWriter( new File ( pathToIni ) );
		p.println("Cadena=||3.2|2015-07-01T09:06:33|ingreso|PAGO EN UNA SOLA EXHIBICION|MONEDA: MXP TIPO DE CAMBIO: 1.0000|4154.000|0.000|1.0000|MXN|4818.640|TRANSFERENC BANCARIA|DELG. CUAUHTEMOC, MEXICO, D.F.|0000|BAR810309N12|BARMEX, S.A. DE C.V.|CDA. DE CEDRO|NO. 501 Y 509|ATLAMPA|DELG. CUAUHTEMOC|MEXICO, D.F|MEXICO|06450|CDA. DE CEDRO|NO. 501 Y 509|COL. ATLAMPA|DELG. CUAUHTEMOC|MEXICO, D.F.|MEXICO|06450|REGIMEN GENERAL DE LEY PERSONAS MORALES|MAI131217843|MAQUINADOS Y AUTOMATIZACION INDUSTRIAL ACAMSA,SA DE CV|MARACAIBO NO.27|SAN PEDRO ZACATENCO GUSTAVO A. MADERO|MEXICO, D.F.|MEXICO|07360|5.000|PZ|D40BTB18H (MAR)|SPROCKET|692.000|3460.000|1.000|PZ|D40BTB19H (MAR)|SPROCKET|694.000|694.000|IVA|16.00|664.640|664.640||");
		p.println("Sello=Xjb3I60xGEGKloYM/XiUgHxt9WBZ4dhi6vSWyA30ABLlxM2/IzjIlU12/8L92xhf\nP8KEIhR47nvG7rHh1fd7qtzl2xx8ma25DNO6TqmzcMbO0gLqeyu4XQc680CNQoVv\nJj4n284NQhFR35RhtgruPb3Sn7IHLCF7Gd0ReKy97xs=\n");
		p.println("noCertificado=00001000000201624125");
		p.println("certificado=MIIEZTCCA02gAwIBAgIUMDAwMDEwMDAwMDAyMDE2MjQxMjUwDQYJKoZIhvcNAQEFBQAwggGVMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMSEwHwYJKoZIhvcNAQkBFhJhc2lzbmV0QHNhdC5nb2IubXgxJjAkBgNVBAkMHUF2LiBIaWRhbGdvIDc3LCBDb2wuIEd1ZXJyZXJvMQ4wDAYDVQQRDAUwNjMwMDELMAkGA1UEBhMCTVgxGTAXBgNVBAgMEERpc3RyaXRvIEZlZGVyYWwxFDASBgNVBAcMC0N1YXVodMOpbW9jMRUwEwYDVQQtEwxTQVQ5NzA3MDFOTjMxPjA8BgkqhkiG9w0BCQIML1Jlc3BvbnNhYmxlOiBDZWNpbGlhIEd1aWxsZXJtaW5hIEdhcmPDrWEgR3VlcnJhMB4XDTEyMDcyNDIzMzgzOFoXDTE2MDcyNDIzMzgzOFowgaYxGDAWBgNVBAMTD0JBUk1FWCBTQSBERSBDVjEYMBYGA1UEKRMPQkFSTUVYIFNBIERFIENWMRgwFgYDVQQKEw9CQVJNRVggU0EgREUgQ1YxJTAjBgNVBC0THEJBUjgxMDMwOU4xMiAvIExPRkM2MzA3MjdFWTUxHjAcBgNVBAUTFSAvIExPRkM2MzA3MjdNREZQTFQwMzEPMA0GA1UECxMGQkFSTUVYMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC3NnANfhQmudVV+Nh+/hvgbTdcJI34TaMPqMPDqLONgt7a6LTKlw+b4HI/Uve6M8dnaHs/IHYYTuLrKWU+xXvoc3frPoSNb7U4iXirHMO2sCU5zhvC5KdSYRv6Er3iGHCEIQALibrNBkPxvb2me7KgrQTZpwjSzh4nP8OvQrKR0QIDAQABox0wGzAMBgNVHRMBAf8EAjAAMAsGA1UdDwQEAwIGwDANBgkqhkiG9w0BAQUFAAOCAQEAgQ9yHmJrX8lphPHfQv1DPgykhZKk+4ZT5mmQ713dv64Tqdhemw4DS5WBWBDhZvvSHWuGoCwmv4WUtnY/8V9pYCl2/nU5crg3jLB9S+Xrjk/zgOL/AUyG965ssVjmZwkgpB9LEDJQH+uzNDjz2X36eHF/ZZlLtCw0iCgyIDlFTLQLudpA4mfkdQfEveNpdS34aKHc5sM4rLFNL1NcmdcnObUjyyvry8RddDMwgXmyjP2ICsjctYsV3PVs7k080w//J9Gtpbp7WgFGLoumJJOA+s+VAYAkFs3tEgmhc6Rl1g5k0vlY8BgWVR/iI5HQGL1WhXgeQoSr+gQ4SIRjnCHQqA==");
		p.println("selloCFD=Xjb3I60xGEGKloYM/XiUgHxt9WBZ4dhi6vSWyA30ABLlxM2/IzjIlU12/8L92xhfP8KEIhR47nvG7rHh1fd7qtzl2xx8ma25DNO6TqmzcMbO0gLqeyu4XQc680CNQoVvJj4n284NQhFR35RhtgruPb3Sn7IHLCF7Gd0ReKy97xs=");
		p.println("version=1.0");
		p.println("FechaTimbrado=2015-07-01T09:10:02");
		p.println("UUID=F3524E44-732A-46C5-84FE-CF075CB55B98");
		p.println("selloSAT=ETBBH+qAkHmr8Lc8vs9hmsF6aL8pHCpDWv0oRtfABqWvIMMeG+eizWCrMnW3/yXThmf7nEvDH7eJF/nRy/L+Mol74RRs1cG2u3pqy3ABTdY+DFx0z35dQNcVnpPIBgi5BsEDEzqIchqQdNlvXzzUShrty4pr96cQql6Wc3QqbIM=");
		p.println("noCertificadoSAT=00001000000203220546");
		p.println("CadenaTimbre=||1.0|F3524E44-732A-46C5-84FE-CF075CB55B98|2015-07-01T09:10:02|Xjb3I60xGEGKloYM/XiUgHxt9WBZ4dhi6vSWyA30ABLlxM2/IzjIlU12/8L92xhfP8KEIhR47nvG7rHh1fd7qtzl2xx8ma25DNO6TqmzcMbO0gLqeyu4XQc680CNQoVvJj4n284NQhFR35RhtgruPb3Sn7IHLCF7Gd0ReKy97xs=|00001000000203220546||");
		p.println("Pac=FEL");
		p.close();
		
	}
	
	private List<String> GeneraLineasDeValidacionCompletas()
	{
		List<String> resultado = new ArrayList<String>();
		resultado.add("Cadena=||3.2|2015-07-01T09:06:33|ingreso|PAGO EN UNA SOLA EXHIBICION|MONEDA: MXP TIPO DE CAMBIO: 1.0000|4154.000|0.000|1.0000|MXN|4818.640|TRANSFERENC BANCARIA|DELG. CUAUHTEMOC, MEXICO, D.F.|0000|BAR810309N12|BARMEX, S.A. DE C.V.|CDA. DE CEDRO|NO. 501 Y 509|ATLAMPA|DELG. CUAUHTEMOC|MEXICO, D.F|MEXICO|06450|CDA. DE CEDRO|NO. 501 Y 509|COL. ATLAMPA|DELG. CUAUHTEMOC|MEXICO, D.F.|MEXICO|06450|REGIMEN GENERAL DE LEY PERSONAS MORALES|MAI131217843|MAQUINADOS Y AUTOMATIZACION INDUSTRIAL ACAMSA,SA DE CV|MARACAIBO NO.27|SAN PEDRO ZACATENCO GUSTAVO A. MADERO|MEXICO, D.F.|MEXICO|07360|5.000|PZ|D40BTB18H (MAR)|SPROCKET|692.000|3460.000|1.000|PZ|D40BTB19H (MAR)|SPROCKET|694.000|694.000|IVA|16.00|664.640|664.640||");
		resultado.add("Sello=Xjb3I60xGEGKloYM/XiUgHxt9WBZ4dhi6vSWyA30ABLlxM2/IzjIlU12/8L92xhf\nP8KEIhR47nvG7rHh1fd7qtzl2xx8ma25DNO6TqmzcMbO0gLqeyu4XQc680CNQoVv\nJj4n284NQhFR35RhtgruPb3Sn7IHLCF7Gd0ReKy97xs=\n");
		resultado.add("noCertificado=00001000000201624125");
		resultado.add("certificado=MIIEZTCCA02gAwIBAgIUMDAwMDEwMDAwMDAyMDE2MjQxMjUwDQYJKoZIhvcNAQEFBQAwggGVMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMSEwHwYJKoZIhvcNAQkBFhJhc2lzbmV0QHNhdC5nb2IubXgxJjAkBgNVBAkMHUF2LiBIaWRhbGdvIDc3LCBDb2wuIEd1ZXJyZXJvMQ4wDAYDVQQRDAUwNjMwMDELMAkGA1UEBhMCTVgxGTAXBgNVBAgMEERpc3RyaXRvIEZlZGVyYWwxFDASBgNVBAcMC0N1YXVodMOpbW9jMRUwEwYDVQQtEwxTQVQ5NzA3MDFOTjMxPjA8BgkqhkiG9w0BCQIML1Jlc3BvbnNhYmxlOiBDZWNpbGlhIEd1aWxsZXJtaW5hIEdhcmPDrWEgR3VlcnJhMB4XDTEyMDcyNDIzMzgzOFoXDTE2MDcyNDIzMzgzOFowgaYxGDAWBgNVBAMTD0JBUk1FWCBTQSBERSBDVjEYMBYGA1UEKRMPQkFSTUVYIFNBIERFIENWMRgwFgYDVQQKEw9CQVJNRVggU0EgREUgQ1YxJTAjBgNVBC0THEJBUjgxMDMwOU4xMiAvIExPRkM2MzA3MjdFWTUxHjAcBgNVBAUTFSAvIExPRkM2MzA3MjdNREZQTFQwMzEPMA0GA1UECxMGQkFSTUVYMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC3NnANfhQmudVV+Nh+/hvgbTdcJI34TaMPqMPDqLONgt7a6LTKlw+b4HI/Uve6M8dnaHs/IHYYTuLrKWU+xXvoc3frPoSNb7U4iXirHMO2sCU5zhvC5KdSYRv6Er3iGHCEIQALibrNBkPxvb2me7KgrQTZpwjSzh4nP8OvQrKR0QIDAQABox0wGzAMBgNVHRMBAf8EAjAAMAsGA1UdDwQEAwIGwDANBgkqhkiG9w0BAQUFAAOCAQEAgQ9yHmJrX8lphPHfQv1DPgykhZKk+4ZT5mmQ713dv64Tqdhemw4DS5WBWBDhZvvSHWuGoCwmv4WUtnY/8V9pYCl2/nU5crg3jLB9S+Xrjk/zgOL/AUyG965ssVjmZwkgpB9LEDJQH+uzNDjz2X36eHF/ZZlLtCw0iCgyIDlFTLQLudpA4mfkdQfEveNpdS34aKHc5sM4rLFNL1NcmdcnObUjyyvry8RddDMwgXmyjP2ICsjctYsV3PVs7k080w//J9Gtpbp7WgFGLoumJJOA+s+VAYAkFs3tEgmhc6Rl1g5k0vlY8BgWVR/iI5HQGL1WhXgeQoSr+gQ4SIRjnCHQqA==");
		resultado.add("selloCFD=Xjb3I60xGEGKloYM/XiUgHxt9WBZ4dhi6vSWyA30ABLlxM2/IzjIlU12/8L92xhfP8KEIhR47nvG7rHh1fd7qtzl2xx8ma25DNO6TqmzcMbO0gLqeyu4XQc680CNQoVvJj4n284NQhFR35RhtgruPb3Sn7IHLCF7Gd0ReKy97xs=");
		resultado.add("version=1.0");
		resultado.add("FechaTimbrado=2015-07-01T09:10:02");
		resultado.add("UUID=F3524E44-732A-46C5-84FE-CF075CB55B98");
		resultado.add("selloSAT=ETBBH+qAkHmr8Lc8vs9hmsF6aL8pHCpDWv0oRtfABqWvIMMeG+eizWCrMnW3/yXThmf7nEvDH7eJF/nRy/L+Mol74RRs1cG2u3pqy3ABTdY+DFx0z35dQNcVnpPIBgi5BsEDEzqIchqQdNlvXzzUShrty4pr96cQql6Wc3QqbIM=");
		resultado.add("noCertificadoSAT=00001000000203220546");
		resultado.add("CadenaTimbre=||1.0|F3524E44-732A-46C5-84FE-CF075CB55B98|2015-07-01T09:10:02|Xjb3I60xGEGKloYM/XiUgHxt9WBZ4dhi6vSWyA30ABLlxM2/IzjIlU12/8L92xhfP8KEIhR47nvG7rHh1fd7qtzl2xx8ma25DNO6TqmzcMbO0gLqeyu4XQc680CNQoVvJj4n284NQhFR35RhtgruPb3Sn7IHLCF7Gd0ReKy97xs=|00001000000203220546||");
		resultado.add("Pac=FEL");
		return resultado;
	}
	
	private Map<String, String> GeneraLineasDeValidacionPares()
	{
		Map<String, String> resultado = new HashMap<String, String>();
		
		resultado.put("Cadena", "||3.2|2015-07-01T09:06:33|ingreso|PAGO EN UNA SOLA EXHIBICION|MONEDA: MXP TIPO DE CAMBIO: 1.0000|4154.000|0.000|1.0000|MXN|4818.640|TRANSFERENC BANCARIA|DELG. CUAUHTEMOC, MEXICO, D.F.|0000|BAR810309N12|BARMEX, S.A. DE C.V.|CDA. DE CEDRO|NO. 501 Y 509|ATLAMPA|DELG. CUAUHTEMOC|MEXICO, D.F|MEXICO|06450|CDA. DE CEDRO|NO. 501 Y 509|COL. ATLAMPA|DELG. CUAUHTEMOC|MEXICO, D.F.|MEXICO|06450|REGIMEN GENERAL DE LEY PERSONAS MORALES|MAI131217843|MAQUINADOS Y AUTOMATIZACION INDUSTRIAL ACAMSA,SA DE CV|MARACAIBO NO.27|SAN PEDRO ZACATENCO GUSTAVO A. MADERO|MEXICO, D.F.|MEXICO|07360|5.000|PZ|D40BTB18H (MAR)|SPROCKET|692.000|3460.000|1.000|PZ|D40BTB19H (MAR)|SPROCKET|694.000|694.000|IVA|16.00|664.640|664.640||");
		resultado.put("Sello", "Xjb3I60xGEGKloYM/XiUgHxt9WBZ4dhi6vSWyA30ABLlxM2/IzjIlU12/8L92xhf\\nP8KEIhR47nvG7rHh1fd7qtzl2xx8ma25DNO6TqmzcMbO0gLqeyu4XQc680CNQoVv\\nJj4n284NQhFR35RhtgruPb3Sn7IHLCF7Gd0ReKy97xs=\\n");
		resultado.put("noCertificado", "00001000000201624125");
		resultado.put("Certificado", "MIIEZTCCA02gAwIBAgIUMDAwMDEwMDAwMDAyMDE2MjQxMjUwDQYJKoZIhvcNAQEFBQAwggGVMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMSEwHwYJKoZIhvcNAQkBFhJhc2lzbmV0QHNhdC5nb2IubXgxJjAkBgNVBAkMHUF2LiBIaWRhbGdvIDc3LCBDb2wuIEd1ZXJyZXJvMQ4wDAYDVQQRDAUwNjMwMDELMAkGA1UEBhMCTVgxGTAXBgNVBAgMEERpc3RyaXRvIEZlZGVyYWwxFDASBgNVBAcMC0N1YXVodMOpbW9jMRUwEwYDVQQtEwxTQVQ5NzA3MDFOTjMxPjA8BgkqhkiG9w0BCQIML1Jlc3BvbnNhYmxlOiBDZWNpbGlhIEd1aWxsZXJtaW5hIEdhcmPDrWEgR3VlcnJhMB4XDTEyMDcyNDIzMzgzOFoXDTE2MDcyNDIzMzgzOFowgaYxGDAWBgNVBAMTD0JBUk1FWCBTQSBERSBDVjEYMBYGA1UEKRMPQkFSTUVYIFNBIERFIENWMRgwFgYDVQQKEw9CQVJNRVggU0EgREUgQ1YxJTAjBgNVBC0THEJBUjgxMDMwOU4xMiAvIExPRkM2MzA3MjdFWTUxHjAcBgNVBAUTFSAvIExPRkM2MzA3MjdNREZQTFQwMzEPMA0GA1UECxMGQkFSTUVYMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC3NnANfhQmudVV+Nh+/hvgbTdcJI34TaMPqMPDqLONgt7a6LTKlw+b4HI/Uve6M8dnaHs/IHYYTuLrKWU+xXvoc3frPoSNb7U4iXirHMO2sCU5zhvC5KdSYRv6Er3iGHCEIQALibrNBkPxvb2me7KgrQTZpwjSzh4nP8OvQrKR0QIDAQABox0wGzAMBgNVHRMBAf8EAjAAMAsGA1UdDwQEAwIGwDANBgkqhkiG9w0BAQUFAAOCAQEAgQ9yHmJrX8lphPHfQv1DPgykhZKk+4ZT5mmQ713dv64Tqdhemw4DS5WBWBDhZvvSHWuGoCwmv4WUtnY/8V9pYCl2/nU5crg3jLB9S+Xrjk/zgOL/AUyG965ssVjmZwkgpB9LEDJQH+uzNDjz2X36eHF/ZZlLtCw0iCgyIDlFTLQLudpA4mfkdQfEveNpdS34aKHc5sM4rLFNL1NcmdcnObUjyyvry8RddDMwgXmyjP2ICsjctYsV3PVs7k080w//J9Gtpbp7WgFGLoumJJOA+s+VAYAkFs3tEgmhc6Rl1g5k0vlY8BgWVR/iI5HQGL1WhXgeQoSr+gQ4SIRjnCHQqA==");
		resultado.put("selloCFD", "Xjb3I60xGEGKloYM/XiUgHxt9WBZ4dhi6vSWyA30ABLlxM2/IzjIlU12/8L92xhfP8KEIhR47nvG7rHh1fd7qtzl2xx8ma25DNO6TqmzcMbO0gLqeyu4XQc680CNQoVvJj4n284NQhFR35RhtgruPb3Sn7IHLCF7Gd0ReKy97xs=");
		resultado.put("version", "1.0");
		resultado.put("FechaTimbrado", "2015-07-01T09:10:02");
		resultado.put("UUID", "F3524E44-732A-46C5-84FE-CF075CB55B98");
		resultado.put("selloSAT", "ETBBH+qAkHmr8Lc8vs9hmsF6aL8pHCpDWv0oRtfABqWvIMMeG+eizWCrMnW3/yXThmf7nEvDH7eJF/nRy/L+Mol74RRs1cG2u3pqy3ABTdY+DFx0z35dQNcVnpPIBgi5BsEDEzqIchqQdNlvXzzUShrty4pr96cQql6Wc3QqbIM=");
		resultado.put("noCertificadoSAT", "00001000000203220546");
		resultado.put("CadenaTimbre", "||1.0|F3524E44-732A-46C5-84FE-CF075CB55B98|2015-07-01T09:10:02|Xjb3I60xGEGKloYM/XiUgHxt9WBZ4dhi6vSWyA30ABLlxM2/IzjIlU12/8L92xhfP8KEIhR47nvG7rHh1fd7qtzl2xx8ma25DNO6TqmzcMbO0gLqeyu4XQc680CNQoVvJj4n284NQhFR35RhtgruPb3Sn7IHLCF7Gd0ReKy97xs=|00001000000203220546||");
		resultado.put("Pac", "FEL");
		

		return resultado;
	}
	
	
	private List<String> GeneraLineasDeValidacionLlaves()
	{
		List<String> resultado = new ArrayList<String>();
		resultado.add("Cadena");
		resultado.add("Sello");
		resultado.add("noCertificado");
		resultado.add("certificado");
		resultado.add("selloCFD");
		resultado.add("version");
		resultado.add("FechaTimbrado");
		resultado.add("UUID");
		resultado.add("selloSAT");
		resultado.add("noCertificadoSAT");
		resultado.add("CadenaTimbre");
		resultado.add("Pac");
		return resultado;
	}
	
	private List<String> regresaDatosDelArchivoIni() throws IOException
	{
		List<String> resultadoNumeroDeLineas = new ArrayList<String>();
		
		BufferedReader br = new BufferedReader( new FileReader( pathToIni ) );
		
		String s;
		
		while ( (s = br.readLine()) != null  ){
			if ( s.trim().length() > 0 ) {
				resultadoNumeroDeLineas.add(s);
			}
		}
		
		br.close();
		
		return resultadoNumeroDeLineas;
	}
	
}
