package com.ptb.sll;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import org.junit.FixMethodOrder;
import org.junit.Test;

@FixMethodOrder
																																																																					
public class BitacoraTest {

	String pathLogDefault 	= "log.t";
	String pathLogPrueba	= "prueba.t"; 
	String pathConfigFile	= "confLog.cfg";
	
	@Test
	public void testGetVersion() {
		Bitacora b = new Bitacora(true, true, Bitacora.DebugMode.BASICO);
		b.setAvanzado("000", "prueba", "hola,1", "void");
		assertEquals("No se pudo recuperar la version", "0.0.0.0", b.getVersion());
	}

	@Test
	public void testGetID() {
		Bitacora b = new Bitacora(true, true, Bitacora.DebugMode.BASICO);
		assertEquals("No se pudo recuperar el id", "101", b.getID());
	}
	
	
	@Test
	public void testSetRegistroBasico() throws IOException {
		
		borraArchivo(pathLogDefault);
		delFileConfig();
		
		Bitacora b = new Bitacora(true, false, Bitacora.DebugMode.BASICO);
		b.setAvanzado("000", "prueba", "hola,1", "void");
		b.setRegistro(Bitacora.Severidad.TAG, "Ejemplo");
		
		List<String> prueba = new ArrayList<String>();
		prueba.add("BASICO inf_t: Ejemplo");
		
		List<String> prov = arrayWithoutDateTime( numeroDeLineas(pathLogDefault) );
		
		List<String> res = prov;
		
		res.removeAll( prueba );
		
		assertEquals("La cadena para debug basico, no concuerda.", 0, res.size());
		
		
		borraArchivo(pathLogDefault);
	}
	
	@Test
	public void testSetRegistroAvanzado() throws IOException {
		
		borraArchivo(pathLogDefault);
		
		Bitacora b = new Bitacora(true, false, Bitacora.DebugMode.AVANZADO);
		b.setAvanzado("000", "prueba", "hola,1", "void");
		b.setRegistro(Bitacora.Severidad.TAG, "Este es un ejemplo");
		
		List<String> prueba = new ArrayList<String>();
		prueba.add("AVANZADO inf_t: [000] [prueba] [hola,1] [void] Este es un ejemplo");
		            
		List<String> prov = arrayWithoutDateTime( numeroDeLineas(pathLogDefault) );
		
		List<String> res = prov;
		
		res.removeAll( prueba );
		
		
		// Se omite la fecha, por la marca de tiempo.
		
		
		assertEquals("La cadena para debug avanzado, no concuerda.", 0, res.size());
		
		borraArchivo(pathLogDefault);
	}

	@Test
	public void testSetoAvanzadoTodosLosTags() throws IOException {
		
		borraArchivo(pathLogDefault);
		
		Bitacora b = new Bitacora(true, false, Bitacora.DebugMode.AVANZADO);
		b.setAvanzado("000", "prueba", "hola,1", "void");
		
		b.setRegistro(Bitacora.Severidad.TAG, "Este es un ejemplo");
		b.setRegistro(Bitacora.Severidad.ALARMA, "Este es un ejemplo");
		b.setRegistro(Bitacora.Severidad.ERROR, "Este es un ejemplo");
		
		
		List<String> prueba = new ArrayList<String>();
		prueba.add("AVANZADO inf: [000] [prueba] [hola,1] [void] Este es un ejemplo");
		prueba.add("AVANZADO inf: [000] [prueba] [hola,1] [void] Este es un ejemplo");
		prueba.add("AVANZADO alm: [000] [prueba] [hola,1] [void] Este es un ejemplo");
		prueba.add("AVANZADO err: [000] [prueba] [hola,1] [void] Este es un ejemplo");
		prueba.add("AVANZADO inf: [000] [prueba] [hola,1] [void] Este es un ejemplo");
		
		List<String> prov = arrayWithoutDateTime( numeroDeLineas(pathLogDefault) );
		
		List<String> res = prov;
		
		res.removeAll( prueba );
			
		assertEquals("La cadena para todos los tags, no concuerda.", 0, res.size());
		
		borraArchivo(pathLogDefault);
	}
	
	
	//TODO colocar el archivo de configuracion
	
	@Test
	public void testArchivoDeConfiguracion() throws IOException
	{
		delFileConfig();
		setFileConfig();
		borraArchivo(pathLogDefault);
		
		setFileDemo(pathLogDefault);
		
		Bitacora b = new Bitacora( false, false, Bitacora.DebugMode.AVANZADO);
		
		// Debe eliminar el archivo, debe permanecer un solo registro, mostar en consola y mostrar un log sencillo
		
		b.setRegistro(Bitacora.Severidad.TAG, "hola mundo");
		
		assertEquals("No concuerdan las lineas, eliminando", 1, numeroDeLineas(pathLogDefault).size());
		
		borraArchivo(pathLogDefault);
		delFileConfig();
		
	}
	
	
	
	@Test
	public void testEliminaPrevios() throws IOException{
		
		borraArchivo(pathLogDefault);
		setFileDemo(pathLogDefault);
		// Carga un documento nuevo, solo tiene una linea
		
		Bitacora b = new Bitacora( true, false, Bitacora.DebugMode.BASICO );
	
		b.setRegistro(Bitacora.Severidad.TAG, "hola mundo");
		
		assertEquals("No concuerdan las lineas, eliminando", 1, numeroDeLineas(pathLogDefault).size());
		
		borraArchivo(pathLogDefault);
	}
	
	@Test
	public void testSinEliminarPrevios() throws IOException{
		// Carga un registro, agrega una linea a la previa
		
		borraArchivo(pathLogDefault);
		setFileDemo(pathLogDefault);
		delFileConfig();
		// Carga un documento nuevo, solo tiene una linea
		
		Bitacora b = new Bitacora( false, false, Bitacora.DebugMode.BASICO );
	
		b.setRegistro(Bitacora.Severidad.TAG, "hola mundo");
		
		assertEquals("No concuerdan las lineas, sin eliminar.", 2, numeroDeLineas(pathLogDefault).size());
		
		borraArchivo(pathLogDefault);
		
	}
	
	private List<String> numeroDeLineas( String path ) throws IOException
	{
		List<String> resultadoNumeroDeLineas = new ArrayList<String>();
		
		BufferedReader br = new BufferedReader( new FileReader( path ) );
		
		String s;
		
		while ( (s = br.readLine()) != null  ){
			if ( s.trim().length() > 0 ) {
				resultadoNumeroDeLineas.add(s);
			}
		}
		
		br.close();
		
		return resultadoNumeroDeLineas;
	}
	
	private void setFileDemo( String path ) throws IOException 
	{
		PrintWriter p = new PrintWriter( new File ( path ) );
		p.println("AVANZADO inf_i: [000] [prueba] [hola,1] [void] Este es un ejemplo");
		p.close();
	}
	
	private void setFileConfig( ) throws IOException 
	{
		
		String path = pathConfigFile;
		
		PrintWriter p = new PrintWriter( new File ( path ) );

		p.println("EliminaPrevios=1");
		p.println("MostrarEnConsola=0");
		p.println("DebugMode=Avanzado");
		p.println("GuardarEnDB=0");
		p.close();
		
	}
	
	private void delFileConfig()
	{
		File fp = new File ( pathConfigFile );
		if ( fp.exists() ){
			fp.delete();
		}
	}
	
	private void borraArchivo( String path ) throws IOException
	{
		File f = new File( path );
		
		if ( f.exists() ){
			f.delete();
		}
	}
	
	private List<String> arrayWithoutDateTime( List<String> arreglo )
	{
		List<String> prov = new ArrayList<String>();
	
		for ( String linea : arreglo ) {
			prov.add( getLineWithoutDateTime( linea ) );
		}
		return prov;
	}
	
	private String getLineWithoutDateTime( String line )
	{
		String resultado = line.substring(20);
		return resultado;
	}

}
