package com.ptb.sll;

import static org.junit.Assert.*;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.junit.Test;

public class ControlTest {
	
	String Server	= "ptb.com.mx";
	String Puerto	= "3306";
	String DataBase	= "ptb_cfdi";
	String Usuario	= "ptb_bitcfdi";
	String Password = "9575a518cbab51474e97068686f2c2c7";
	String RFCM		= "PTV070409A22";
	String RFCMBad	= "GACA760103F70";
	
	

	@Test
	public void testMandaInformacion() {
		fail("Not yet implemented");
	}

	@Test
	public void testExisteRFCMaestro() {
		Control c = new Control( Server, Puerto, DataBase, Usuario, Password );
		assertTrue("Se esperaba que se pudiera conectar", c.getEstado());
		
		assertTrue("Se esperaba que fuera verdadero el rfc existe", c.existeRFCMaestro(RFCM));
	}

	@Test
	public void testExisteRFCMaestroBad(){
		Control c = new Control( Server, Puerto, DataBase, Usuario, Password );
		assertTrue("Se esperaba que se pudiera conectar", c.getEstado());
		
		assertFalse("Se esperaba que fuera verdadero el rfc existe", c.existeRFCMaestro(RFCMBad));
	}
	
	@Test
	public void testTimbresDisponibles() {
		Control c = new Control( Server, Puerto, DataBase, Usuario, Password);
		assertTrue("Se esperaba que se pudiera conectar", c.getEstado());
		
		// Se esperaba 11.
		
		assertEquals("Se esperaba que devolviera 11", 11, c.timbresDisponibles(RFCM));
		
	}
	
	@Test
	public void testTimbresDisponiblesBad() {
		Control c = new Control( Server, Puerto, DataBase, Usuario, Password);
		assertTrue("Se esperaba que se pudiera conectar", c.getEstado());
		
		// Se esperaba 11.
		
		assertNotEquals("Se esperaba que devolviera 22", 21, c.timbresDisponibles(RFCM));
		
	}

	@Test
	public void testOrdenDeTimbrado() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetVersion() {
		Control c = new Control(Server, Puerto, DataBase, Usuario, Password);
		assertEquals("Se esperaba 0.0.0.0", "0.0.0.0", c.getVersion());
	}

	@Test
	public void testGetID() {
		Control c = new Control(Server, Puerto, DataBase, Usuario, Password);
		assertEquals("Se esperaba 106", "106", c.getID());
	}
	
	@Test
	public void testConstructor() {
		Control c = new Control(Server, Puerto, DataBase, Usuario, Password);
		assertTrue("Se esperaba que se pudiera conectar", c.getEstado());
	}
	
	@Test
	public void testGetFecha() {
		Control c = new Control( Server, Puerto, DataBase, Usuario, Password );
		assertTrue("Se esperaba que se pudiera conectar", c.getEstado());
		
		assertEquals("Se esperaba que la fecha fuera igual", getFechaNow().substring(0,13), c.getFecha().substring(0,13));
		
	}
	
	@Test
	public void testHostName() {
		String nombre = Utilidades.getPcName();
		assertEquals("Se esperaba un nombre:  ", "sistemasbarmex8", nombre);
	}
	
	//region METODOS PRIVADOS
	private String getFechaNow()
	{
		Calendar f = Calendar.getInstance();
		
		java.util.Date date = f.getTime();             
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format1.format(date).substring(0,16); 
		 
	}
	//endregion
}
