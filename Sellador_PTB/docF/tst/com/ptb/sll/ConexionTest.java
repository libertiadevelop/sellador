package com.ptb.sll;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ptb.sll.Conexion.rdbms;

public class ConexionTest {

	private String server 		= "localhost";
	private String puerto		= "3306";
	private String baseDeDatos	= "test";
	private String usuario		= "root";
	private String password		= "root";
	
	private String sentenciaConsulta		= "";
	private String sentenciaInsert			= "";
	private String sentenciaUpdate			= "";
	private String setenciaDelete			= "";
	
	
	@Test
	public void testConsulta() {
		fail("Not yet implemented");
	}

	@Test
	public void testInsertaRegistros() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdateRegistro() {
		fail("Not yet implemented");
	}

	@Test
	public void testOpenConexion() {
		fail("Not yet implemented");
	}

	@Test
	public void testCloseConexion() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetBatch() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetBatch() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetVersion() {
		Conexion c = new Conexion(server, puerto, baseDeDatos, usuario, password );
		
		assertEquals("Se esperaba la version de la clase", "0.0.0.0", c.getVersion() );
		
	}

	@Test
	public void testGetID() {
		Conexion c = new Conexion( server, puerto, baseDeDatos, usuario, password );
		
		assertEquals("Se esperaba el id de la clas 103", "103", c.getID() );
		
	}

	@Test
	public void testOpenConexionValida () {
		
		Conexion c = new Conexion(server, puerto, baseDeDatos, usuario, password);
		
		
		assertTrue("Se esperaba que la conexion fuera verdadera", c.OpenConexion( con.rdbms.MYSQL ) );
		assertTrue("Se esperaba que la conexion fuera verdadera", c.getEstado() );
		assertEquals("Se esperaba que no hubiera errores", "", c.getDesError() );
	}
	
	@Test
	public void testOpenConexionInvalida () {
		
		Conexion c = new Conexion("", puerto, baseDeDatos, usuario, password);
		
		assertFalse("Se esperaba falso, los datos son incompletos", c.getEstado() );
		assertEquals("Se esperaba el error 103001", 103001, Integer.parseInt( c.getCodError() ));
		assertEquals("Se esperba el mensaje de erro datos invalidos", "Faltan datos, para relizar la conexion.", c.getDesError());
		
		c = new Conexion(server, puerto, "", usuario, password);
		
		assertFalse("Se esperaba falso, los datos son incompletos", c.getEstado() );
		assertEquals("Se esperaba el error 103001", 103001, Integer.parseInt( c.getCodError() ));
		assertEquals("Se esperba el mensaje de erro datos invalidos", "Faltan datos, para relizar la conexion.", c.getDesError());
		
		c = new Conexion("192.168.9.0", puerto, baseDeDatos, usuario, password);
		
		c.OpenConexion( con.rdbms.MYSQL );
		
		assertFalse("Se esperaba falso, El server no existe", c.getEstado() );
		assertEquals("Se esperaba el error 103001", 103002, Integer.parseInt( c.getCodError() ));
		assertEquals("Se esperba el mensaje de erro datos invalidos", "Error al intentar conectar con la base de datos.", c.getDesError());
		
	}
	
	
	
	@Test
	public void testGetEstado() {
		fail("Not yet implemented");
		
	}
	
	@Test
	public void testGetEstado_false() {
		fail("Not yet implemented");
	}
	
	

	@Test
	public void testGetCodError() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetDesError() {
		fail("Not yet implemented");
	}

	
}

