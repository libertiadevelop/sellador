/**
 * Timbre.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.TES_TFD_Negocios;

public class Timbre  implements java.io.Serializable {
    private java.lang.String estado;

    private java.util.Calendar fechaTimbrado;

    private java.lang.String numeroCertificadoSAT;

    private java.lang.String selloCFD;

    private java.lang.String selloSAT;

    private java.lang.String UUID;

    public Timbre() {
    }

    public Timbre(
           java.lang.String estado,
           java.util.Calendar fechaTimbrado,
           java.lang.String numeroCertificadoSAT,
           java.lang.String selloCFD,
           java.lang.String selloSAT,
           java.lang.String UUID) {
           this.estado = estado;
           this.fechaTimbrado = fechaTimbrado;
           this.numeroCertificadoSAT = numeroCertificadoSAT;
           this.selloCFD = selloCFD;
           this.selloSAT = selloSAT;
           this.UUID = UUID;
    }


    /**
     * Gets the estado value for this Timbre.
     * 
     * @return estado
     */
    public java.lang.String getEstado() {
        return estado;
    }


    /**
     * Sets the estado value for this Timbre.
     * 
     * @param estado
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }


    /**
     * Gets the fechaTimbrado value for this Timbre.
     * 
     * @return fechaTimbrado
     */
    public java.util.Calendar getFechaTimbrado() {
        return fechaTimbrado;
    }


    /**
     * Sets the fechaTimbrado value for this Timbre.
     * 
     * @param fechaTimbrado
     */
    public void setFechaTimbrado(java.util.Calendar fechaTimbrado) {
        this.fechaTimbrado = fechaTimbrado;
    }


    /**
     * Gets the numeroCertificadoSAT value for this Timbre.
     * 
     * @return numeroCertificadoSAT
     */
    public java.lang.String getNumeroCertificadoSAT() {
        return numeroCertificadoSAT;
    }


    /**
     * Sets the numeroCertificadoSAT value for this Timbre.
     * 
     * @param numeroCertificadoSAT
     */
    public void setNumeroCertificadoSAT(java.lang.String numeroCertificadoSAT) {
        this.numeroCertificadoSAT = numeroCertificadoSAT;
    }


    /**
     * Gets the selloCFD value for this Timbre.
     * 
     * @return selloCFD
     */
    public java.lang.String getSelloCFD() {
        return selloCFD;
    }


    /**
     * Sets the selloCFD value for this Timbre.
     * 
     * @param selloCFD
     */
    public void setSelloCFD(java.lang.String selloCFD) {
        this.selloCFD = selloCFD;
    }


    /**
     * Gets the selloSAT value for this Timbre.
     * 
     * @return selloSAT
     */
    public java.lang.String getSelloSAT() {
        return selloSAT;
    }


    /**
     * Sets the selloSAT value for this Timbre.
     * 
     * @param selloSAT
     */
    public void setSelloSAT(java.lang.String selloSAT) {
        this.selloSAT = selloSAT;
    }


    /**
     * Gets the UUID value for this Timbre.
     * 
     * @return UUID
     */
    public java.lang.String getUUID() {
        return UUID;
    }


    /**
     * Sets the UUID value for this Timbre.
     * 
     * @param UUID
     */
    public void setUUID(java.lang.String UUID) {
        this.UUID = UUID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Timbre)) return false;
        Timbre other = (Timbre) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.estado==null && other.getEstado()==null) || 
             (this.estado!=null &&
              this.estado.equals(other.getEstado()))) &&
            ((this.fechaTimbrado==null && other.getFechaTimbrado()==null) || 
             (this.fechaTimbrado!=null &&
              this.fechaTimbrado.equals(other.getFechaTimbrado()))) &&
            ((this.numeroCertificadoSAT==null && other.getNumeroCertificadoSAT()==null) || 
             (this.numeroCertificadoSAT!=null &&
              this.numeroCertificadoSAT.equals(other.getNumeroCertificadoSAT()))) &&
            ((this.selloCFD==null && other.getSelloCFD()==null) || 
             (this.selloCFD!=null &&
              this.selloCFD.equals(other.getSelloCFD()))) &&
            ((this.selloSAT==null && other.getSelloSAT()==null) || 
             (this.selloSAT!=null &&
              this.selloSAT.equals(other.getSelloSAT()))) &&
            ((this.UUID==null && other.getUUID()==null) || 
             (this.UUID!=null &&
              this.UUID.equals(other.getUUID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEstado() != null) {
            _hashCode += getEstado().hashCode();
        }
        if (getFechaTimbrado() != null) {
            _hashCode += getFechaTimbrado().hashCode();
        }
        if (getNumeroCertificadoSAT() != null) {
            _hashCode += getNumeroCertificadoSAT().hashCode();
        }
        if (getSelloCFD() != null) {
            _hashCode += getSelloCFD().hashCode();
        }
        if (getSelloSAT() != null) {
            _hashCode += getSelloSAT().hashCode();
        }
        if (getUUID() != null) {
            _hashCode += getUUID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Timbre.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "Timbre"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "Estado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaTimbrado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "FechaTimbrado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroCertificadoSAT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "NumeroCertificadoSAT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("selloCFD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "SelloCFD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("selloSAT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "SelloSAT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("UUID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "UUID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
