/**
 * RegistroTimbre.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.TES_TFD_Negocios;

public class RegistroTimbre  implements java.io.Serializable {
    private java.lang.String estado;

    private java.util.Calendar fechaTimbrado;

    private java.lang.Integer noFila;

    private java.lang.String RFCEmisor;

    private java.lang.String RFCReceptor;

    private java.lang.String UUID;

    public RegistroTimbre() {
    }

    public RegistroTimbre(
           java.lang.String estado,
           java.util.Calendar fechaTimbrado,
           java.lang.Integer noFila,
           java.lang.String RFCEmisor,
           java.lang.String RFCReceptor,
           java.lang.String UUID) {
           this.estado = estado;
           this.fechaTimbrado = fechaTimbrado;
           this.noFila = noFila;
           this.RFCEmisor = RFCEmisor;
           this.RFCReceptor = RFCReceptor;
           this.UUID = UUID;
    }


    /**
     * Gets the estado value for this RegistroTimbre.
     * 
     * @return estado
     */
    public java.lang.String getEstado() {
        return estado;
    }


    /**
     * Sets the estado value for this RegistroTimbre.
     * 
     * @param estado
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }


    /**
     * Gets the fechaTimbrado value for this RegistroTimbre.
     * 
     * @return fechaTimbrado
     */
    public java.util.Calendar getFechaTimbrado() {
        return fechaTimbrado;
    }


    /**
     * Sets the fechaTimbrado value for this RegistroTimbre.
     * 
     * @param fechaTimbrado
     */
    public void setFechaTimbrado(java.util.Calendar fechaTimbrado) {
        this.fechaTimbrado = fechaTimbrado;
    }


    /**
     * Gets the noFila value for this RegistroTimbre.
     * 
     * @return noFila
     */
    public java.lang.Integer getNoFila() {
        return noFila;
    }


    /**
     * Sets the noFila value for this RegistroTimbre.
     * 
     * @param noFila
     */
    public void setNoFila(java.lang.Integer noFila) {
        this.noFila = noFila;
    }


    /**
     * Gets the RFCEmisor value for this RegistroTimbre.
     * 
     * @return RFCEmisor
     */
    public java.lang.String getRFCEmisor() {
        return RFCEmisor;
    }


    /**
     * Sets the RFCEmisor value for this RegistroTimbre.
     * 
     * @param RFCEmisor
     */
    public void setRFCEmisor(java.lang.String RFCEmisor) {
        this.RFCEmisor = RFCEmisor;
    }


    /**
     * Gets the RFCReceptor value for this RegistroTimbre.
     * 
     * @return RFCReceptor
     */
    public java.lang.String getRFCReceptor() {
        return RFCReceptor;
    }


    /**
     * Sets the RFCReceptor value for this RegistroTimbre.
     * 
     * @param RFCReceptor
     */
    public void setRFCReceptor(java.lang.String RFCReceptor) {
        this.RFCReceptor = RFCReceptor;
    }


    /**
     * Gets the UUID value for this RegistroTimbre.
     * 
     * @return UUID
     */
    public java.lang.String getUUID() {
        return UUID;
    }


    /**
     * Sets the UUID value for this RegistroTimbre.
     * 
     * @param UUID
     */
    public void setUUID(java.lang.String UUID) {
        this.UUID = UUID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RegistroTimbre)) return false;
        RegistroTimbre other = (RegistroTimbre) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.estado==null && other.getEstado()==null) || 
             (this.estado!=null &&
              this.estado.equals(other.getEstado()))) &&
            ((this.fechaTimbrado==null && other.getFechaTimbrado()==null) || 
             (this.fechaTimbrado!=null &&
              this.fechaTimbrado.equals(other.getFechaTimbrado()))) &&
            ((this.noFila==null && other.getNoFila()==null) || 
             (this.noFila!=null &&
              this.noFila.equals(other.getNoFila()))) &&
            ((this.RFCEmisor==null && other.getRFCEmisor()==null) || 
             (this.RFCEmisor!=null &&
              this.RFCEmisor.equals(other.getRFCEmisor()))) &&
            ((this.RFCReceptor==null && other.getRFCReceptor()==null) || 
             (this.RFCReceptor!=null &&
              this.RFCReceptor.equals(other.getRFCReceptor()))) &&
            ((this.UUID==null && other.getUUID()==null) || 
             (this.UUID!=null &&
              this.UUID.equals(other.getUUID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEstado() != null) {
            _hashCode += getEstado().hashCode();
        }
        if (getFechaTimbrado() != null) {
            _hashCode += getFechaTimbrado().hashCode();
        }
        if (getNoFila() != null) {
            _hashCode += getNoFila().hashCode();
        }
        if (getRFCEmisor() != null) {
            _hashCode += getRFCEmisor().hashCode();
        }
        if (getRFCReceptor() != null) {
            _hashCode += getRFCReceptor().hashCode();
        }
        if (getUUID() != null) {
            _hashCode += getUUID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RegistroTimbre.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "RegistroTimbre"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "Estado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaTimbrado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "FechaTimbrado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("noFila");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "NoFila"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RFCEmisor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "RFCEmisor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RFCReceptor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "RFCReceptor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("UUID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "UUID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
