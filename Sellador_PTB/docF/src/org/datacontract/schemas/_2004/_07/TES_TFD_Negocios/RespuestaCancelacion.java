/**
 * RespuestaCancelacion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.TES_TFD_Negocios;

public class RespuestaCancelacion  implements java.io.Serializable {
    private org.datacontract.schemas._2004._07.TES_TFD_Negocios.DetalleCancelacion[] detallesCancelacion;

    private java.lang.String mensajeError;

    private java.lang.String mensajeErrorDetallado;

    private java.lang.Boolean operacionExitosa;

    private java.lang.String XMLAcuse;

    public RespuestaCancelacion() {
    }

    public RespuestaCancelacion(
           org.datacontract.schemas._2004._07.TES_TFD_Negocios.DetalleCancelacion[] detallesCancelacion,
           java.lang.String mensajeError,
           java.lang.String mensajeErrorDetallado,
           java.lang.Boolean operacionExitosa,
           java.lang.String XMLAcuse) {
           this.detallesCancelacion = detallesCancelacion;
           this.mensajeError = mensajeError;
           this.mensajeErrorDetallado = mensajeErrorDetallado;
           this.operacionExitosa = operacionExitosa;
           this.XMLAcuse = XMLAcuse;
    }


    /**
     * Gets the detallesCancelacion value for this RespuestaCancelacion.
     * 
     * @return detallesCancelacion
     */
    public org.datacontract.schemas._2004._07.TES_TFD_Negocios.DetalleCancelacion[] getDetallesCancelacion() {
        return detallesCancelacion;
    }


    /**
     * Sets the detallesCancelacion value for this RespuestaCancelacion.
     * 
     * @param detallesCancelacion
     */
    public void setDetallesCancelacion(org.datacontract.schemas._2004._07.TES_TFD_Negocios.DetalleCancelacion[] detallesCancelacion) {
        this.detallesCancelacion = detallesCancelacion;
    }


    /**
     * Gets the mensajeError value for this RespuestaCancelacion.
     * 
     * @return mensajeError
     */
    public java.lang.String getMensajeError() {
        return mensajeError;
    }


    /**
     * Sets the mensajeError value for this RespuestaCancelacion.
     * 
     * @param mensajeError
     */
    public void setMensajeError(java.lang.String mensajeError) {
        this.mensajeError = mensajeError;
    }


    /**
     * Gets the mensajeErrorDetallado value for this RespuestaCancelacion.
     * 
     * @return mensajeErrorDetallado
     */
    public java.lang.String getMensajeErrorDetallado() {
        return mensajeErrorDetallado;
    }


    /**
     * Sets the mensajeErrorDetallado value for this RespuestaCancelacion.
     * 
     * @param mensajeErrorDetallado
     */
    public void setMensajeErrorDetallado(java.lang.String mensajeErrorDetallado) {
        this.mensajeErrorDetallado = mensajeErrorDetallado;
    }


    /**
     * Gets the operacionExitosa value for this RespuestaCancelacion.
     * 
     * @return operacionExitosa
     */
    public java.lang.Boolean getOperacionExitosa() {
        return operacionExitosa;
    }


    /**
     * Sets the operacionExitosa value for this RespuestaCancelacion.
     * 
     * @param operacionExitosa
     */
    public void setOperacionExitosa(java.lang.Boolean operacionExitosa) {
        this.operacionExitosa = operacionExitosa;
    }


    /**
     * Gets the XMLAcuse value for this RespuestaCancelacion.
     * 
     * @return XMLAcuse
     */
    public java.lang.String getXMLAcuse() {
        return XMLAcuse;
    }


    /**
     * Sets the XMLAcuse value for this RespuestaCancelacion.
     * 
     * @param XMLAcuse
     */
    public void setXMLAcuse(java.lang.String XMLAcuse) {
        this.XMLAcuse = XMLAcuse;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RespuestaCancelacion)) return false;
        RespuestaCancelacion other = (RespuestaCancelacion) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.detallesCancelacion==null && other.getDetallesCancelacion()==null) || 
             (this.detallesCancelacion!=null &&
              java.util.Arrays.equals(this.detallesCancelacion, other.getDetallesCancelacion()))) &&
            ((this.mensajeError==null && other.getMensajeError()==null) || 
             (this.mensajeError!=null &&
              this.mensajeError.equals(other.getMensajeError()))) &&
            ((this.mensajeErrorDetallado==null && other.getMensajeErrorDetallado()==null) || 
             (this.mensajeErrorDetallado!=null &&
              this.mensajeErrorDetallado.equals(other.getMensajeErrorDetallado()))) &&
            ((this.operacionExitosa==null && other.getOperacionExitosa()==null) || 
             (this.operacionExitosa!=null &&
              this.operacionExitosa.equals(other.getOperacionExitosa()))) &&
            ((this.XMLAcuse==null && other.getXMLAcuse()==null) || 
             (this.XMLAcuse!=null &&
              this.XMLAcuse.equals(other.getXMLAcuse())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDetallesCancelacion() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDetallesCancelacion());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDetallesCancelacion(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMensajeError() != null) {
            _hashCode += getMensajeError().hashCode();
        }
        if (getMensajeErrorDetallado() != null) {
            _hashCode += getMensajeErrorDetallado().hashCode();
        }
        if (getOperacionExitosa() != null) {
            _hashCode += getOperacionExitosa().hashCode();
        }
        if (getXMLAcuse() != null) {
            _hashCode += getXMLAcuse().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RespuestaCancelacion.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "RespuestaCancelacion"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("detallesCancelacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "DetallesCancelacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "DetalleCancelacion"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "DetalleCancelacion"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensajeError");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "MensajeError"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensajeErrorDetallado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "MensajeErrorDetallado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operacionExitosa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "OperacionExitosa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("XMLAcuse");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "XMLAcuse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
