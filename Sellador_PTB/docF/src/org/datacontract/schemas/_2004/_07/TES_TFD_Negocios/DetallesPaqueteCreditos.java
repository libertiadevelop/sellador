/**
 * DetallesPaqueteCreditos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.TES_TFD_Negocios;

public class DetallesPaqueteCreditos  implements java.io.Serializable {
    private java.lang.Boolean enUso;

    private java.util.Calendar fechaActivacion;

    private java.util.Calendar fechaVencimiento;

    private java.lang.String paquete;

    private java.lang.Integer timbres;

    private java.lang.Integer timbresRestantes;

    private java.lang.Integer timbresUsados;

    private java.lang.Boolean vigente;

    public DetallesPaqueteCreditos() {
    }

    public DetallesPaqueteCreditos(
           java.lang.Boolean enUso,
           java.util.Calendar fechaActivacion,
           java.util.Calendar fechaVencimiento,
           java.lang.String paquete,
           java.lang.Integer timbres,
           java.lang.Integer timbresRestantes,
           java.lang.Integer timbresUsados,
           java.lang.Boolean vigente) {
           this.enUso = enUso;
           this.fechaActivacion = fechaActivacion;
           this.fechaVencimiento = fechaVencimiento;
           this.paquete = paquete;
           this.timbres = timbres;
           this.timbresRestantes = timbresRestantes;
           this.timbresUsados = timbresUsados;
           this.vigente = vigente;
    }


    /**
     * Gets the enUso value for this DetallesPaqueteCreditos.
     * 
     * @return enUso
     */
    public java.lang.Boolean getEnUso() {
        return enUso;
    }


    /**
     * Sets the enUso value for this DetallesPaqueteCreditos.
     * 
     * @param enUso
     */
    public void setEnUso(java.lang.Boolean enUso) {
        this.enUso = enUso;
    }


    /**
     * Gets the fechaActivacion value for this DetallesPaqueteCreditos.
     * 
     * @return fechaActivacion
     */
    public java.util.Calendar getFechaActivacion() {
        return fechaActivacion;
    }


    /**
     * Sets the fechaActivacion value for this DetallesPaqueteCreditos.
     * 
     * @param fechaActivacion
     */
    public void setFechaActivacion(java.util.Calendar fechaActivacion) {
        this.fechaActivacion = fechaActivacion;
    }


    /**
     * Gets the fechaVencimiento value for this DetallesPaqueteCreditos.
     * 
     * @return fechaVencimiento
     */
    public java.util.Calendar getFechaVencimiento() {
        return fechaVencimiento;
    }


    /**
     * Sets the fechaVencimiento value for this DetallesPaqueteCreditos.
     * 
     * @param fechaVencimiento
     */
    public void setFechaVencimiento(java.util.Calendar fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }


    /**
     * Gets the paquete value for this DetallesPaqueteCreditos.
     * 
     * @return paquete
     */
    public java.lang.String getPaquete() {
        return paquete;
    }


    /**
     * Sets the paquete value for this DetallesPaqueteCreditos.
     * 
     * @param paquete
     */
    public void setPaquete(java.lang.String paquete) {
        this.paquete = paquete;
    }


    /**
     * Gets the timbres value for this DetallesPaqueteCreditos.
     * 
     * @return timbres
     */
    public java.lang.Integer getTimbres() {
        return timbres;
    }


    /**
     * Sets the timbres value for this DetallesPaqueteCreditos.
     * 
     * @param timbres
     */
    public void setTimbres(java.lang.Integer timbres) {
        this.timbres = timbres;
    }


    /**
     * Gets the timbresRestantes value for this DetallesPaqueteCreditos.
     * 
     * @return timbresRestantes
     */
    public java.lang.Integer getTimbresRestantes() {
        return timbresRestantes;
    }


    /**
     * Sets the timbresRestantes value for this DetallesPaqueteCreditos.
     * 
     * @param timbresRestantes
     */
    public void setTimbresRestantes(java.lang.Integer timbresRestantes) {
        this.timbresRestantes = timbresRestantes;
    }


    /**
     * Gets the timbresUsados value for this DetallesPaqueteCreditos.
     * 
     * @return timbresUsados
     */
    public java.lang.Integer getTimbresUsados() {
        return timbresUsados;
    }


    /**
     * Sets the timbresUsados value for this DetallesPaqueteCreditos.
     * 
     * @param timbresUsados
     */
    public void setTimbresUsados(java.lang.Integer timbresUsados) {
        this.timbresUsados = timbresUsados;
    }


    /**
     * Gets the vigente value for this DetallesPaqueteCreditos.
     * 
     * @return vigente
     */
    public java.lang.Boolean getVigente() {
        return vigente;
    }


    /**
     * Sets the vigente value for this DetallesPaqueteCreditos.
     * 
     * @param vigente
     */
    public void setVigente(java.lang.Boolean vigente) {
        this.vigente = vigente;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DetallesPaqueteCreditos)) return false;
        DetallesPaqueteCreditos other = (DetallesPaqueteCreditos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.enUso==null && other.getEnUso()==null) || 
             (this.enUso!=null &&
              this.enUso.equals(other.getEnUso()))) &&
            ((this.fechaActivacion==null && other.getFechaActivacion()==null) || 
             (this.fechaActivacion!=null &&
              this.fechaActivacion.equals(other.getFechaActivacion()))) &&
            ((this.fechaVencimiento==null && other.getFechaVencimiento()==null) || 
             (this.fechaVencimiento!=null &&
              this.fechaVencimiento.equals(other.getFechaVencimiento()))) &&
            ((this.paquete==null && other.getPaquete()==null) || 
             (this.paquete!=null &&
              this.paquete.equals(other.getPaquete()))) &&
            ((this.timbres==null && other.getTimbres()==null) || 
             (this.timbres!=null &&
              this.timbres.equals(other.getTimbres()))) &&
            ((this.timbresRestantes==null && other.getTimbresRestantes()==null) || 
             (this.timbresRestantes!=null &&
              this.timbresRestantes.equals(other.getTimbresRestantes()))) &&
            ((this.timbresUsados==null && other.getTimbresUsados()==null) || 
             (this.timbresUsados!=null &&
              this.timbresUsados.equals(other.getTimbresUsados()))) &&
            ((this.vigente==null && other.getVigente()==null) || 
             (this.vigente!=null &&
              this.vigente.equals(other.getVigente())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEnUso() != null) {
            _hashCode += getEnUso().hashCode();
        }
        if (getFechaActivacion() != null) {
            _hashCode += getFechaActivacion().hashCode();
        }
        if (getFechaVencimiento() != null) {
            _hashCode += getFechaVencimiento().hashCode();
        }
        if (getPaquete() != null) {
            _hashCode += getPaquete().hashCode();
        }
        if (getTimbres() != null) {
            _hashCode += getTimbres().hashCode();
        }
        if (getTimbresRestantes() != null) {
            _hashCode += getTimbresRestantes().hashCode();
        }
        if (getTimbresUsados() != null) {
            _hashCode += getTimbresUsados().hashCode();
        }
        if (getVigente() != null) {
            _hashCode += getVigente().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DetallesPaqueteCreditos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "DetallesPaqueteCreditos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("enUso");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "EnUso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaActivacion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "FechaActivacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaVencimiento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "FechaVencimiento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paquete");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "Paquete"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timbres");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "Timbres"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timbresRestantes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "TimbresRestantes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timbresUsados");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "TimbresUsados"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vigente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "Vigente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
