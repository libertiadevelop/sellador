/**
 * RespuestaTFD.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.TES_TFD_Negocios;

public class RespuestaTFD  implements java.io.Serializable {
    private java.lang.String codigoRespuesta;

    private java.lang.Integer creditosRestantes;

    private java.lang.String mensajeError;

    private java.lang.String mensajeErrorDetallado;

    private java.lang.Boolean operacionExitosa;

    private java.lang.String PDFResultado;

    private org.datacontract.schemas._2004._07.TES_TFD_Negocios.Timbre timbre;

    private java.lang.String XMLResultado;

    public RespuestaTFD() {
    }

    public RespuestaTFD(
           java.lang.String codigoRespuesta,
           java.lang.Integer creditosRestantes,
           java.lang.String mensajeError,
           java.lang.String mensajeErrorDetallado,
           java.lang.Boolean operacionExitosa,
           java.lang.String PDFResultado,
           org.datacontract.schemas._2004._07.TES_TFD_Negocios.Timbre timbre,
           java.lang.String XMLResultado) {
           this.codigoRespuesta = codigoRespuesta;
           this.creditosRestantes = creditosRestantes;
           this.mensajeError = mensajeError;
           this.mensajeErrorDetallado = mensajeErrorDetallado;
           this.operacionExitosa = operacionExitosa;
           this.PDFResultado = PDFResultado;
           this.timbre = timbre;
           this.XMLResultado = XMLResultado;
    }


    /**
     * Gets the codigoRespuesta value for this RespuestaTFD.
     * 
     * @return codigoRespuesta
     */
    public java.lang.String getCodigoRespuesta() {
        return codigoRespuesta;
    }


    /**
     * Sets the codigoRespuesta value for this RespuestaTFD.
     * 
     * @param codigoRespuesta
     */
    public void setCodigoRespuesta(java.lang.String codigoRespuesta) {
        this.codigoRespuesta = codigoRespuesta;
    }


    /**
     * Gets the creditosRestantes value for this RespuestaTFD.
     * 
     * @return creditosRestantes
     */
    public java.lang.Integer getCreditosRestantes() {
        return creditosRestantes;
    }


    /**
     * Sets the creditosRestantes value for this RespuestaTFD.
     * 
     * @param creditosRestantes
     */
    public void setCreditosRestantes(java.lang.Integer creditosRestantes) {
        this.creditosRestantes = creditosRestantes;
    }


    /**
     * Gets the mensajeError value for this RespuestaTFD.
     * 
     * @return mensajeError
     */
    public java.lang.String getMensajeError() {
        return mensajeError;
    }


    /**
     * Sets the mensajeError value for this RespuestaTFD.
     * 
     * @param mensajeError
     */
    public void setMensajeError(java.lang.String mensajeError) {
        this.mensajeError = mensajeError;
    }


    /**
     * Gets the mensajeErrorDetallado value for this RespuestaTFD.
     * 
     * @return mensajeErrorDetallado
     */
    public java.lang.String getMensajeErrorDetallado() {
        return mensajeErrorDetallado;
    }


    /**
     * Sets the mensajeErrorDetallado value for this RespuestaTFD.
     * 
     * @param mensajeErrorDetallado
     */
    public void setMensajeErrorDetallado(java.lang.String mensajeErrorDetallado) {
        this.mensajeErrorDetallado = mensajeErrorDetallado;
    }


    /**
     * Gets the operacionExitosa value for this RespuestaTFD.
     * 
     * @return operacionExitosa
     */
    public java.lang.Boolean getOperacionExitosa() {
        return operacionExitosa;
    }


    /**
     * Sets the operacionExitosa value for this RespuestaTFD.
     * 
     * @param operacionExitosa
     */
    public void setOperacionExitosa(java.lang.Boolean operacionExitosa) {
        this.operacionExitosa = operacionExitosa;
    }


    /**
     * Gets the PDFResultado value for this RespuestaTFD.
     * 
     * @return PDFResultado
     */
    public java.lang.String getPDFResultado() {
        return PDFResultado;
    }


    /**
     * Sets the PDFResultado value for this RespuestaTFD.
     * 
     * @param PDFResultado
     */
    public void setPDFResultado(java.lang.String PDFResultado) {
        this.PDFResultado = PDFResultado;
    }


    /**
     * Gets the timbre value for this RespuestaTFD.
     * 
     * @return timbre
     */
    public org.datacontract.schemas._2004._07.TES_TFD_Negocios.Timbre getTimbre() {
        return timbre;
    }


    /**
     * Sets the timbre value for this RespuestaTFD.
     * 
     * @param timbre
     */
    public void setTimbre(org.datacontract.schemas._2004._07.TES_TFD_Negocios.Timbre timbre) {
        this.timbre = timbre;
    }


    /**
     * Gets the XMLResultado value for this RespuestaTFD.
     * 
     * @return XMLResultado
     */
    public java.lang.String getXMLResultado() {
        return XMLResultado;
    }


    /**
     * Sets the XMLResultado value for this RespuestaTFD.
     * 
     * @param XMLResultado
     */
    public void setXMLResultado(java.lang.String XMLResultado) {
        this.XMLResultado = XMLResultado;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RespuestaTFD)) return false;
        RespuestaTFD other = (RespuestaTFD) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codigoRespuesta==null && other.getCodigoRespuesta()==null) || 
             (this.codigoRespuesta!=null &&
              this.codigoRespuesta.equals(other.getCodigoRespuesta()))) &&
            ((this.creditosRestantes==null && other.getCreditosRestantes()==null) || 
             (this.creditosRestantes!=null &&
              this.creditosRestantes.equals(other.getCreditosRestantes()))) &&
            ((this.mensajeError==null && other.getMensajeError()==null) || 
             (this.mensajeError!=null &&
              this.mensajeError.equals(other.getMensajeError()))) &&
            ((this.mensajeErrorDetallado==null && other.getMensajeErrorDetallado()==null) || 
             (this.mensajeErrorDetallado!=null &&
              this.mensajeErrorDetallado.equals(other.getMensajeErrorDetallado()))) &&
            ((this.operacionExitosa==null && other.getOperacionExitosa()==null) || 
             (this.operacionExitosa!=null &&
              this.operacionExitosa.equals(other.getOperacionExitosa()))) &&
            ((this.PDFResultado==null && other.getPDFResultado()==null) || 
             (this.PDFResultado!=null &&
              this.PDFResultado.equals(other.getPDFResultado()))) &&
            ((this.timbre==null && other.getTimbre()==null) || 
             (this.timbre!=null &&
              this.timbre.equals(other.getTimbre()))) &&
            ((this.XMLResultado==null && other.getXMLResultado()==null) || 
             (this.XMLResultado!=null &&
              this.XMLResultado.equals(other.getXMLResultado())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodigoRespuesta() != null) {
            _hashCode += getCodigoRespuesta().hashCode();
        }
        if (getCreditosRestantes() != null) {
            _hashCode += getCreditosRestantes().hashCode();
        }
        if (getMensajeError() != null) {
            _hashCode += getMensajeError().hashCode();
        }
        if (getMensajeErrorDetallado() != null) {
            _hashCode += getMensajeErrorDetallado().hashCode();
        }
        if (getOperacionExitosa() != null) {
            _hashCode += getOperacionExitosa().hashCode();
        }
        if (getPDFResultado() != null) {
            _hashCode += getPDFResultado().hashCode();
        }
        if (getTimbre() != null) {
            _hashCode += getTimbre().hashCode();
        }
        if (getXMLResultado() != null) {
            _hashCode += getXMLResultado().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RespuestaTFD.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "RespuestaTFD"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoRespuesta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "CodigoRespuesta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditosRestantes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "CreditosRestantes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensajeError");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "MensajeError"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensajeErrorDetallado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "MensajeErrorDetallado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operacionExitosa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "OperacionExitosa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PDFResultado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "PDFResultado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timbre");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "Timbre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "Timbre"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("XMLResultado");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "XMLResultado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
