/**
 * RespuestaReporte.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.TES_TFD_Negocios;

public class RespuestaReporte  implements java.io.Serializable {
    private org.datacontract.schemas._2004._07.TES_TFD_Negocios.RegistroTimbre[] listaComprobantes;

    private java.lang.String mensajeError;

    private java.lang.Boolean operacionExitosa;

    private java.lang.Integer totalComprobantesPeriodo;

    public RespuestaReporte() {
    }

    public RespuestaReporte(
           org.datacontract.schemas._2004._07.TES_TFD_Negocios.RegistroTimbre[] listaComprobantes,
           java.lang.String mensajeError,
           java.lang.Boolean operacionExitosa,
           java.lang.Integer totalComprobantesPeriodo) {
           this.listaComprobantes = listaComprobantes;
           this.mensajeError = mensajeError;
           this.operacionExitosa = operacionExitosa;
           this.totalComprobantesPeriodo = totalComprobantesPeriodo;
    }


    /**
     * Gets the listaComprobantes value for this RespuestaReporte.
     * 
     * @return listaComprobantes
     */
    public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RegistroTimbre[] getListaComprobantes() {
        return listaComprobantes;
    }


    /**
     * Sets the listaComprobantes value for this RespuestaReporte.
     * 
     * @param listaComprobantes
     */
    public void setListaComprobantes(org.datacontract.schemas._2004._07.TES_TFD_Negocios.RegistroTimbre[] listaComprobantes) {
        this.listaComprobantes = listaComprobantes;
    }


    /**
     * Gets the mensajeError value for this RespuestaReporte.
     * 
     * @return mensajeError
     */
    public java.lang.String getMensajeError() {
        return mensajeError;
    }


    /**
     * Sets the mensajeError value for this RespuestaReporte.
     * 
     * @param mensajeError
     */
    public void setMensajeError(java.lang.String mensajeError) {
        this.mensajeError = mensajeError;
    }


    /**
     * Gets the operacionExitosa value for this RespuestaReporte.
     * 
     * @return operacionExitosa
     */
    public java.lang.Boolean getOperacionExitosa() {
        return operacionExitosa;
    }


    /**
     * Sets the operacionExitosa value for this RespuestaReporte.
     * 
     * @param operacionExitosa
     */
    public void setOperacionExitosa(java.lang.Boolean operacionExitosa) {
        this.operacionExitosa = operacionExitosa;
    }


    /**
     * Gets the totalComprobantesPeriodo value for this RespuestaReporte.
     * 
     * @return totalComprobantesPeriodo
     */
    public java.lang.Integer getTotalComprobantesPeriodo() {
        return totalComprobantesPeriodo;
    }


    /**
     * Sets the totalComprobantesPeriodo value for this RespuestaReporte.
     * 
     * @param totalComprobantesPeriodo
     */
    public void setTotalComprobantesPeriodo(java.lang.Integer totalComprobantesPeriodo) {
        this.totalComprobantesPeriodo = totalComprobantesPeriodo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RespuestaReporte)) return false;
        RespuestaReporte other = (RespuestaReporte) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.listaComprobantes==null && other.getListaComprobantes()==null) || 
             (this.listaComprobantes!=null &&
              java.util.Arrays.equals(this.listaComprobantes, other.getListaComprobantes()))) &&
            ((this.mensajeError==null && other.getMensajeError()==null) || 
             (this.mensajeError!=null &&
              this.mensajeError.equals(other.getMensajeError()))) &&
            ((this.operacionExitosa==null && other.getOperacionExitosa()==null) || 
             (this.operacionExitosa!=null &&
              this.operacionExitosa.equals(other.getOperacionExitosa()))) &&
            ((this.totalComprobantesPeriodo==null && other.getTotalComprobantesPeriodo()==null) || 
             (this.totalComprobantesPeriodo!=null &&
              this.totalComprobantesPeriodo.equals(other.getTotalComprobantesPeriodo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getListaComprobantes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getListaComprobantes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getListaComprobantes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMensajeError() != null) {
            _hashCode += getMensajeError().hashCode();
        }
        if (getOperacionExitosa() != null) {
            _hashCode += getOperacionExitosa().hashCode();
        }
        if (getTotalComprobantesPeriodo() != null) {
            _hashCode += getTotalComprobantesPeriodo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RespuestaReporte.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "RespuestaReporte"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("listaComprobantes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "ListaComprobantes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "RegistroTimbre"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "RegistroTimbre"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensajeError");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "MensajeError"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operacionExitosa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "OperacionExitosa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalComprobantesPeriodo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "TotalComprobantesPeriodo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
