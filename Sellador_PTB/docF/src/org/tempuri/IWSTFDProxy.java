package org.tempuri;

public class IWSTFDProxy implements org.tempuri.IWSTFD {
  private String _endpoint = null;
  private org.tempuri.IWSTFD iWSTFD = null;
  
  public IWSTFDProxy() {
    _initIWSTFDProxy();
  }
  
  public IWSTFDProxy(String endpoint) {
    _endpoint = endpoint;
    _initIWSTFDProxy();
  }
  
  private void _initIWSTFDProxy() {
    try {
      iWSTFD = (new org.tempuri.WSTFDLocator()).getsoapHttpEndpoint();
      if (iWSTFD != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iWSTFD)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iWSTFD)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iWSTFD != null)
      ((javax.xml.rpc.Stub)iWSTFD)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.tempuri.IWSTFD getIWSTFD() {
    if (iWSTFD == null)
      _initIWSTFDProxy();
    return iWSTFD;
  }
  
  public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD timbrarCFDI(java.lang.String usuario, java.lang.String password, java.lang.String cadenaXML, java.lang.String referencia) throws java.rmi.RemoteException{
    if (iWSTFD == null)
      _initIWSTFDProxy();
    return iWSTFD.timbrarCFDI(usuario, password, cadenaXML, referencia);
  }
  
  public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaCancelacion cancelarCFDI(java.lang.String usuario, java.lang.String password, java.lang.String rFCEmisor, java.lang.String[] listaCFDI, java.lang.String clavePrivada_Base64, java.lang.String passwordClavePrivada) throws java.rmi.RemoteException{
    if (iWSTFD == null)
      _initIWSTFDProxy();
    return iWSTFD.cancelarCFDI(usuario, password, rFCEmisor, listaCFDI, clavePrivada_Base64, passwordClavePrivada);
  }
  
  public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD timbrarCFDTestSectorPrimario(java.lang.String usuario, java.lang.String password, java.lang.String cadenaXML, java.lang.String referencia) throws java.rmi.RemoteException{
    if (iWSTFD == null)
      _initIWSTFDProxy();
    return iWSTFD.timbrarCFDTestSectorPrimario(usuario, password, cadenaXML, referencia);
  }
  
  public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD obtenerPDF(java.lang.String usuario, java.lang.String password, java.lang.String uUID, java.lang.String logoBase64) throws java.rmi.RemoteException{
    if (iWSTFD == null)
      _initIWSTFDProxy();
    return iWSTFD.obtenerPDF(usuario, password, uUID, logoBase64);
  }
  
  public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD obtenerAcuseEnvio(java.lang.String usuario, java.lang.String password, java.lang.String uUID) throws java.rmi.RemoteException{
    if (iWSTFD == null)
      _initIWSTFDProxy();
    return iWSTFD.obtenerAcuseEnvio(usuario, password, uUID);
  }
  
  public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD obtenerAcuseCancelacion(java.lang.String usuario, java.lang.String password, java.lang.String uUID) throws java.rmi.RemoteException{
    if (iWSTFD == null)
      _initIWSTFDProxy();
    return iWSTFD.obtenerAcuseCancelacion(usuario, password, uUID);
  }
  
  public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD cambiarPassword(java.lang.String usuario, java.lang.String passwordActual, java.lang.String passwordNuevo) throws java.rmi.RemoteException{
    if (iWSTFD == null)
      _initIWSTFDProxy();
    return iWSTFD.cambiarPassword(usuario, passwordActual, passwordNuevo);
  }
  
  public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD consultarComplementoTimbre(java.lang.String usuario, java.lang.String password, java.lang.String uUID) throws java.rmi.RemoteException{
    if (iWSTFD == null)
      _initIWSTFDProxy();
    return iWSTFD.consultarComplementoTimbre(usuario, password, uUID);
  }
  
  public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD consultarTimbrePorReferencia(java.lang.String usuario, java.lang.String password, java.lang.String referencia) throws java.rmi.RemoteException{
    if (iWSTFD == null)
      _initIWSTFDProxy();
    return iWSTFD.consultarTimbrePorReferencia(usuario, password, referencia);
  }
  
  public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaCreditos consultarCreditos(java.lang.String usuario, java.lang.String password) throws java.rmi.RemoteException{
    if (iWSTFD == null)
      _initIWSTFDProxy();
    return iWSTFD.consultarCreditos(usuario, password);
  }
  
  public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaReporte consultarComprobantes(java.lang.String usuario, java.lang.String password, java.util.Calendar fechaInicial, java.util.Calendar fechaFinal, java.lang.Integer filaInicial) throws java.rmi.RemoteException{
    if (iWSTFD == null)
      _initIWSTFDProxy();
    return iWSTFD.consultarComprobantes(usuario, password, fechaInicial, fechaFinal, filaInicial);
  }
  
  public java.lang.Object[] obtenerPaquetesClientes(java.lang.String[] LClientes) throws java.rmi.RemoteException{
    if (iWSTFD == null)
      _initIWSTFDProxy();
    return iWSTFD.obtenerPaquetesClientes(LClientes);
  }
  
  
}