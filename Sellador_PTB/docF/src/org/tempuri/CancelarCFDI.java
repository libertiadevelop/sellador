/**
 * CancelarCFDI.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class CancelarCFDI  implements java.io.Serializable {
    private java.lang.String usuario;

    private java.lang.String password;

    private java.lang.String rFCEmisor;

    private java.lang.String[] listaCFDI;

    private java.lang.String clavePrivada_Base64;

    private java.lang.String passwordClavePrivada;

    public CancelarCFDI() {
    }

    public CancelarCFDI(
           java.lang.String usuario,
           java.lang.String password,
           java.lang.String rFCEmisor,
           java.lang.String[] listaCFDI,
           java.lang.String clavePrivada_Base64,
           java.lang.String passwordClavePrivada) {
           this.usuario = usuario;
           this.password = password;
           this.rFCEmisor = rFCEmisor;
           this.listaCFDI = listaCFDI;
           this.clavePrivada_Base64 = clavePrivada_Base64;
           this.passwordClavePrivada = passwordClavePrivada;
    }


    /**
     * Gets the usuario value for this CancelarCFDI.
     * 
     * @return usuario
     */
    public java.lang.String getUsuario() {
        return usuario;
    }


    /**
     * Sets the usuario value for this CancelarCFDI.
     * 
     * @param usuario
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }


    /**
     * Gets the password value for this CancelarCFDI.
     * 
     * @return password
     */
    public java.lang.String getPassword() {
        return password;
    }


    /**
     * Sets the password value for this CancelarCFDI.
     * 
     * @param password
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }


    /**
     * Gets the rFCEmisor value for this CancelarCFDI.
     * 
     * @return rFCEmisor
     */
    public java.lang.String getRFCEmisor() {
        return rFCEmisor;
    }


    /**
     * Sets the rFCEmisor value for this CancelarCFDI.
     * 
     * @param rFCEmisor
     */
    public void setRFCEmisor(java.lang.String rFCEmisor) {
        this.rFCEmisor = rFCEmisor;
    }


    /**
     * Gets the listaCFDI value for this CancelarCFDI.
     * 
     * @return listaCFDI
     */
    public java.lang.String[] getListaCFDI() {
        return listaCFDI;
    }


    /**
     * Sets the listaCFDI value for this CancelarCFDI.
     * 
     * @param listaCFDI
     */
    public void setListaCFDI(java.lang.String[] listaCFDI) {
        this.listaCFDI = listaCFDI;
    }


    /**
     * Gets the clavePrivada_Base64 value for this CancelarCFDI.
     * 
     * @return clavePrivada_Base64
     */
    public java.lang.String getClavePrivada_Base64() {
        return clavePrivada_Base64;
    }


    /**
     * Sets the clavePrivada_Base64 value for this CancelarCFDI.
     * 
     * @param clavePrivada_Base64
     */
    public void setClavePrivada_Base64(java.lang.String clavePrivada_Base64) {
        this.clavePrivada_Base64 = clavePrivada_Base64;
    }


    /**
     * Gets the passwordClavePrivada value for this CancelarCFDI.
     * 
     * @return passwordClavePrivada
     */
    public java.lang.String getPasswordClavePrivada() {
        return passwordClavePrivada;
    }


    /**
     * Sets the passwordClavePrivada value for this CancelarCFDI.
     * 
     * @param passwordClavePrivada
     */
    public void setPasswordClavePrivada(java.lang.String passwordClavePrivada) {
        this.passwordClavePrivada = passwordClavePrivada;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CancelarCFDI)) return false;
        CancelarCFDI other = (CancelarCFDI) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.usuario==null && other.getUsuario()==null) || 
             (this.usuario!=null &&
              this.usuario.equals(other.getUsuario()))) &&
            ((this.password==null && other.getPassword()==null) || 
             (this.password!=null &&
              this.password.equals(other.getPassword()))) &&
            ((this.rFCEmisor==null && other.getRFCEmisor()==null) || 
             (this.rFCEmisor!=null &&
              this.rFCEmisor.equals(other.getRFCEmisor()))) &&
            ((this.listaCFDI==null && other.getListaCFDI()==null) || 
             (this.listaCFDI!=null &&
              java.util.Arrays.equals(this.listaCFDI, other.getListaCFDI()))) &&
            ((this.clavePrivada_Base64==null && other.getClavePrivada_Base64()==null) || 
             (this.clavePrivada_Base64!=null &&
              this.clavePrivada_Base64.equals(other.getClavePrivada_Base64()))) &&
            ((this.passwordClavePrivada==null && other.getPasswordClavePrivada()==null) || 
             (this.passwordClavePrivada!=null &&
              this.passwordClavePrivada.equals(other.getPasswordClavePrivada())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUsuario() != null) {
            _hashCode += getUsuario().hashCode();
        }
        if (getPassword() != null) {
            _hashCode += getPassword().hashCode();
        }
        if (getRFCEmisor() != null) {
            _hashCode += getRFCEmisor().hashCode();
        }
        if (getListaCFDI() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getListaCFDI());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getListaCFDI(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getClavePrivada_Base64() != null) {
            _hashCode += getClavePrivada_Base64().hashCode();
        }
        if (getPasswordClavePrivada() != null) {
            _hashCode += getPasswordClavePrivada().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CancelarCFDI.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">CancelarCFDI"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuario");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "usuario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RFCEmisor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "rFCEmisor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("listaCFDI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "listaCFDI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clavePrivada_Base64");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "clavePrivada_Base64"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passwordClavePrivada");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "passwordClavePrivada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
