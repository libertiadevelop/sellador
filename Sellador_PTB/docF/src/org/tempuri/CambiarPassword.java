/**
 * CambiarPassword.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class CambiarPassword  implements java.io.Serializable {
    private java.lang.String usuario;

    private java.lang.String passwordActual;

    private java.lang.String passwordNuevo;

    public CambiarPassword() {
    }

    public CambiarPassword(
           java.lang.String usuario,
           java.lang.String passwordActual,
           java.lang.String passwordNuevo) {
           this.usuario = usuario;
           this.passwordActual = passwordActual;
           this.passwordNuevo = passwordNuevo;
    }


    /**
     * Gets the usuario value for this CambiarPassword.
     * 
     * @return usuario
     */
    public java.lang.String getUsuario() {
        return usuario;
    }


    /**
     * Sets the usuario value for this CambiarPassword.
     * 
     * @param usuario
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }


    /**
     * Gets the passwordActual value for this CambiarPassword.
     * 
     * @return passwordActual
     */
    public java.lang.String getPasswordActual() {
        return passwordActual;
    }


    /**
     * Sets the passwordActual value for this CambiarPassword.
     * 
     * @param passwordActual
     */
    public void setPasswordActual(java.lang.String passwordActual) {
        this.passwordActual = passwordActual;
    }


    /**
     * Gets the passwordNuevo value for this CambiarPassword.
     * 
     * @return passwordNuevo
     */
    public java.lang.String getPasswordNuevo() {
        return passwordNuevo;
    }


    /**
     * Sets the passwordNuevo value for this CambiarPassword.
     * 
     * @param passwordNuevo
     */
    public void setPasswordNuevo(java.lang.String passwordNuevo) {
        this.passwordNuevo = passwordNuevo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CambiarPassword)) return false;
        CambiarPassword other = (CambiarPassword) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.usuario==null && other.getUsuario()==null) || 
             (this.usuario!=null &&
              this.usuario.equals(other.getUsuario()))) &&
            ((this.passwordActual==null && other.getPasswordActual()==null) || 
             (this.passwordActual!=null &&
              this.passwordActual.equals(other.getPasswordActual()))) &&
            ((this.passwordNuevo==null && other.getPasswordNuevo()==null) || 
             (this.passwordNuevo!=null &&
              this.passwordNuevo.equals(other.getPasswordNuevo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUsuario() != null) {
            _hashCode += getUsuario().hashCode();
        }
        if (getPasswordActual() != null) {
            _hashCode += getPasswordActual().hashCode();
        }
        if (getPasswordNuevo() != null) {
            _hashCode += getPasswordNuevo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CambiarPassword.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">CambiarPassword"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuario");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "usuario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passwordActual");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "passwordActual"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passwordNuevo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "passwordNuevo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
