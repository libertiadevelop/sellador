/**
 * WSTFD.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface WSTFD extends javax.xml.rpc.Service {
    public java.lang.String getsoapHttpEndpointAddress();

    public org.tempuri.IWSTFD getsoapHttpEndpoint() throws javax.xml.rpc.ServiceException;

    public org.tempuri.IWSTFD getsoapHttpEndpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getsoapHttpEndpointHttpsAddress();

    public org.tempuri.IWSTFD getsoapHttpEndpointHttps() throws javax.xml.rpc.ServiceException;

    public org.tempuri.IWSTFD getsoapHttpEndpointHttps(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
