/**
 * IWSTFD.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface IWSTFD extends java.rmi.Remote {
    public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD timbrarCFDI(java.lang.String usuario, java.lang.String password, java.lang.String cadenaXML, java.lang.String referencia) throws java.rmi.RemoteException;
    public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaCancelacion cancelarCFDI(java.lang.String usuario, java.lang.String password, java.lang.String rFCEmisor, java.lang.String[] listaCFDI, java.lang.String clavePrivada_Base64, java.lang.String passwordClavePrivada) throws java.rmi.RemoteException;
    public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD timbrarCFDTestSectorPrimario(java.lang.String usuario, java.lang.String password, java.lang.String cadenaXML, java.lang.String referencia) throws java.rmi.RemoteException;
    public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD obtenerPDF(java.lang.String usuario, java.lang.String password, java.lang.String uUID, java.lang.String logoBase64) throws java.rmi.RemoteException;
    public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD obtenerAcuseEnvio(java.lang.String usuario, java.lang.String password, java.lang.String uUID) throws java.rmi.RemoteException;
    public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD obtenerAcuseCancelacion(java.lang.String usuario, java.lang.String password, java.lang.String uUID) throws java.rmi.RemoteException;
    public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD cambiarPassword(java.lang.String usuario, java.lang.String passwordActual, java.lang.String passwordNuevo) throws java.rmi.RemoteException;
    public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD consultarComplementoTimbre(java.lang.String usuario, java.lang.String password, java.lang.String uUID) throws java.rmi.RemoteException;
    public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD consultarTimbrePorReferencia(java.lang.String usuario, java.lang.String password, java.lang.String referencia) throws java.rmi.RemoteException;
    public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaCreditos consultarCreditos(java.lang.String usuario, java.lang.String password) throws java.rmi.RemoteException;
    public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaReporte consultarComprobantes(java.lang.String usuario, java.lang.String password, java.util.Calendar fechaInicial, java.util.Calendar fechaFinal, java.lang.Integer filaInicial) throws java.rmi.RemoteException;
    public java.lang.Object[] obtenerPaquetesClientes(java.lang.String[] LClientes) throws java.rmi.RemoteException;
}
