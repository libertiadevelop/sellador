/**
 * ConsultarComplementoTimbreResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class ConsultarComplementoTimbreResponse  implements java.io.Serializable {
    private org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD consultarComplementoTimbreResult;

    public ConsultarComplementoTimbreResponse() {
    }

    public ConsultarComplementoTimbreResponse(
           org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD consultarComplementoTimbreResult) {
           this.consultarComplementoTimbreResult = consultarComplementoTimbreResult;
    }


    /**
     * Gets the consultarComplementoTimbreResult value for this ConsultarComplementoTimbreResponse.
     * 
     * @return consultarComplementoTimbreResult
     */
    public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD getConsultarComplementoTimbreResult() {
        return consultarComplementoTimbreResult;
    }


    /**
     * Sets the consultarComplementoTimbreResult value for this ConsultarComplementoTimbreResponse.
     * 
     * @param consultarComplementoTimbreResult
     */
    public void setConsultarComplementoTimbreResult(org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD consultarComplementoTimbreResult) {
        this.consultarComplementoTimbreResult = consultarComplementoTimbreResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ConsultarComplementoTimbreResponse)) return false;
        ConsultarComplementoTimbreResponse other = (ConsultarComplementoTimbreResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.consultarComplementoTimbreResult==null && other.getConsultarComplementoTimbreResult()==null) || 
             (this.consultarComplementoTimbreResult!=null &&
              this.consultarComplementoTimbreResult.equals(other.getConsultarComplementoTimbreResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getConsultarComplementoTimbreResult() != null) {
            _hashCode += getConsultarComplementoTimbreResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ConsultarComplementoTimbreResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">ConsultarComplementoTimbreResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consultarComplementoTimbreResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ConsultarComplementoTimbreResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "RespuestaTFD"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
