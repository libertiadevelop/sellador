/**
 * ObtenerPDFResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class ObtenerPDFResponse  implements java.io.Serializable {
    private org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD obtenerPDFResult;

    public ObtenerPDFResponse() {
    }

    public ObtenerPDFResponse(
           org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD obtenerPDFResult) {
           this.obtenerPDFResult = obtenerPDFResult;
    }


    /**
     * Gets the obtenerPDFResult value for this ObtenerPDFResponse.
     * 
     * @return obtenerPDFResult
     */
    public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD getObtenerPDFResult() {
        return obtenerPDFResult;
    }


    /**
     * Sets the obtenerPDFResult value for this ObtenerPDFResponse.
     * 
     * @param obtenerPDFResult
     */
    public void setObtenerPDFResult(org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaTFD obtenerPDFResult) {
        this.obtenerPDFResult = obtenerPDFResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ObtenerPDFResponse)) return false;
        ObtenerPDFResponse other = (ObtenerPDFResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.obtenerPDFResult==null && other.getObtenerPDFResult()==null) || 
             (this.obtenerPDFResult!=null &&
              this.obtenerPDFResult.equals(other.getObtenerPDFResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getObtenerPDFResult() != null) {
            _hashCode += getObtenerPDFResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ObtenerPDFResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">ObtenerPDFResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obtenerPDFResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ObtenerPDFResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "RespuestaTFD"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
