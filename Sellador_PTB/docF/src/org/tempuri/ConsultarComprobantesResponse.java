/**
 * ConsultarComprobantesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class ConsultarComprobantesResponse  implements java.io.Serializable {
    private org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaReporte consultarComprobantesResult;

    public ConsultarComprobantesResponse() {
    }

    public ConsultarComprobantesResponse(
           org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaReporte consultarComprobantesResult) {
           this.consultarComprobantesResult = consultarComprobantesResult;
    }


    /**
     * Gets the consultarComprobantesResult value for this ConsultarComprobantesResponse.
     * 
     * @return consultarComprobantesResult
     */
    public org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaReporte getConsultarComprobantesResult() {
        return consultarComprobantesResult;
    }


    /**
     * Sets the consultarComprobantesResult value for this ConsultarComprobantesResponse.
     * 
     * @param consultarComprobantesResult
     */
    public void setConsultarComprobantesResult(org.datacontract.schemas._2004._07.TES_TFD_Negocios.RespuestaReporte consultarComprobantesResult) {
        this.consultarComprobantesResult = consultarComprobantesResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ConsultarComprobantesResponse)) return false;
        ConsultarComprobantesResponse other = (ConsultarComprobantesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.consultarComprobantesResult==null && other.getConsultarComprobantesResult()==null) || 
             (this.consultarComprobantesResult!=null &&
              this.consultarComprobantesResult.equals(other.getConsultarComprobantesResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getConsultarComprobantesResult() != null) {
            _hashCode += getConsultarComprobantesResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ConsultarComprobantesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">ConsultarComprobantesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consultarComprobantesResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ConsultarComprobantesResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/TES.TFD.Negocios", "RespuestaReporte"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
