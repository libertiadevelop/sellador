package com.ptb.sll;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.io.Writer;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.HashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.*;
import javax.xml.XMLConstants;

import org.mozilla.universalchardet.UniversalDetector;

import com.ptb.sll.VersionSat;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import java.math.BigInteger;
import java.nio.*;
import java.nio.charset.Charset;

import javax.xml.validation.SchemaFactory;

import org.apache.commons.ssl.*;

public class DocumentoFiscalFuente implements Error, id, docFisFue {

	String id 		= "105";
	String version 	= "0.0.0.0";
	
	String pathXml;
	String pathXsd;
	String pathCert;
	String pathKey;
	String claveKey;
	
	boolean 	estado;
	String		desError;
	String		MesDesError;
	
	File		xml;
	File		cert;
	File		key;
	File 		xsd;
	File		xslt;
	
	String 		xmlOrigen;
	String		xmlSellado;
	
	VersionSat	versionSat;
	
	Object		xmlObject;
	
	HashMap<String, Boolean> resultadoDeValidaciones = new HashMap<String, Boolean>();
	
	enum error 
	{
		SIN_ERROR(0),
		SE_PASO_UNA_CADENA_VACIA(1),
		EL_ARCHIVO_NO_EXISTE(2),
		NO_SE_PASO_EL_PASSWORD(3),
		NO_SE_DETECTO_CODIFICACION(4),
		NO_SE_PUDO_CERRAR_EL_ARCHIVO(5),
		XML_INVALIDO(6),
		IMPORTE_INCORRECTOS(7),
		NO_SE_PUDO_OBTENER_EL_CERTIFICADO(8),
		ERROR_AL_OBTENER_DATOS_DEL_CERTIFICADO(9),
		NO_SE_PUDO_GUARDAR_LOS_DATOS_DE_CERT(10),
		NO_SE_PUDO_GENERAR_EL_SELLO(11);
		;
		
		int e;
		private error( int er ){
			e = er;
		}
		
		public String getDesError(){
			String resultado = "";
			switch ( e ){
			case 0:
				resultado = "";
				break;
			case 1:
				resultado = "No se paso un path";
				break;
			case 2:
				resultado = "El archivo no existe";
				break;
			case 3:
				resultado = "No se paso el password";
				break;
			case 4:
				resultado = "Codificaciòn invalida";
				break;
			case 5:
				resultado = "No se pudo cerrar el archivo";
				break;
			case 6:
				resultado = "El xml pasado es invalido";
				break;
			case 7:
				resultado = "Importe invalido";
				break;
			case 8:
				resultado = "No se pudo obtener el certificado";
				break;
			case 9:
				resultado = "No se pudieron obtener los datos del certificado";
				break;
			case 10:
				resultado = "No se pudieron grabar los datos del certificado";
				break;
			case 11:
				resultado = "No se pudo generar el sello";
				break;
			}
			return resultado;
		}
		
		
	}
	
	enum camposAGuardar{
		CERTIFICADO,
		NO_CERTIFICADO,
		SELLO;
	}
	
	public enum validaciones
	{
		
		XML(0), XSD(1), CERT(2), KEY(3), ESQUEMA(4), UTF8(5), IMPORTES(6), XSLT(7);
		
		int v;
		private validaciones ( int validacion )
		{
			v = validacion;
		}
		
		public String getValidacion()
		{
			String resultado = "";
			
			switch ( v ){
			case ( 0 ):
				resultado = "VALIDA-XML";
				break;
			case ( 1 ):
				resultado = "VALIDA-XSD";
				break;
			case ( 2 ):
				resultado = "VALIDA-CERT";
				break;
 			case ( 3 ):
 				resultado = "VALIDA-KEY";
 				break;
 			case ( 4 ):
 				resultado = "VALIDA-ESQUEMA";
 				break;
 			case ( 5 ):
 				resultado = "VALIDA-CODIFICACION";
 				break;
 			case ( 6 ):
 				resultado = "VALIDA-IMPORTES";
 				break;
 			case ( 7 ):
 				resultado = "VALIDA - TRANSFORMACION";
 				break;
			}
			
			return resultado;
		}
		
	}
	
	public enum versionXML
	{
		VERSION_2, VERSION_3_2;
	}
	
	public DocumentoFiscalFuente ( String PathXML, String PathXSD )
	{
		String Modulo = "Constructor";
		setEstado(true, error.SIN_ERROR, "", Modulo);
		
		pathXml = PathXML;
		pathXsd = PathXSD;
		
		// Validando archivos
		
		xml 	= ValidaArchivo ( pathXml, Modulo, validaciones.XML.getValidacion() );
		xsd		= ValidaArchivo ( pathXsd, Modulo, validaciones.XSD.getValidacion() );
		
	}
	
	public void valida( validaciones v  ) {
		
		switch (v) {
		case UTF8:
			convierteAUTF8( v );
			break;
		case ESQUEMA:
			validaEsquema( v );
			break;
		case IMPORTES:
			validaImportes ( v );
			break;
		default:
			break;
		}
		
	}

	public void SellaDocumento( String pathCert, String pathKey, String password, String pathXslt ) {
		
		String Modulo = "Sellar";
		
		cert	= ValidaArchivo ( pathCert, Modulo, validaciones.CERT.getValidacion() );
		key		= ValidaArchivo ( pathKey, Modulo, validaciones.KEY.getValidacion() );
		xslt	= ValidaArchivo (pathXslt, Modulo, validaciones.XSLT.getValidacion());
		
		
		if ( estado ) {
			// Procede a sellar el documento
			
			X509Certificate c 		= null;
			String 			cer64 	= "";
			String 			noCer	= "";
			
			setEstado(true, error.SIN_ERROR, "", Modulo);
			
			try {
			
				c 		= getX509Certificate(cert);
				cer64 	= getCertificadoBase64( c );
				noCer	= getNoCertificado( c );
				setEstado(true, error.SIN_ERROR, "", Modulo);
			} catch ( Exception e ) {
				setEstado(false, error.ERROR_AL_OBTENER_DATOS_DEL_CERTIFICADO, e.toString(), Modulo);
			}
			
			if ( !estado ) return;
			
			ActualizaXml(camposAGuardar.CERTIFICADO, cer64.replaceAll("[\n]", ""));
			ActualizaXml(camposAGuardar.NO_CERTIFICADO, noCer);
			
				
			// Procede a sellar el documento
			
			setEstado(true, error.SIN_ERROR,"Iniciando sellado", Modulo);
		
			String proceso = "";
			String cadenaOriginal = "";
			
			try {
				proceso	= "Cadena Original";
				cadenaOriginal = generaCadenaOriginal();
				setEstado(true, error.SIN_ERROR, "Cadena generada", Modulo + proceso);
				proceso = "Llave privada";
				PrivateKey 	llavePrivada 	= getPrivateKey ( key, password );
				setEstado(true, error.SIN_ERROR, "Llave recuperada", Modulo + proceso);
				proceso = "sello";
				String 		sello	 		= generaSelloDigital( llavePrivada, cadenaOriginal );
				setEstado(true, error.SIN_ERROR, "Sello generado", Modulo + proceso);
				ActualizaXml(camposAGuardar.SELLO, sello.replaceAll("[\n]", ""));
			} catch ( Exception e ){
				setEstado(false, error.NO_SE_PUDO_GENERAR_EL_SELLO, e.toString(), Modulo );
			}
		
		}

	}

	private String generaSelloDigital(PrivateKey llavePrivada,String cadenaOriginal) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
		
		java.security.Signature sign = java.security.Signature.getInstance("SHA1withRSA");
		sign.initSign(llavePrivada, new SecureRandom());
		sign.update(cadenaOriginal.getBytes());
		byte[] signature = sign.sign();
		
		return new String(Base64.encode(signature));
	}

	private PrivateKey getPrivateKey(File key2, String password) throws GeneralSecurityException, IOException {
		
		FileInputStream fis = new FileInputStream( key );
		
		PKCS8Key k = new PKCS8Key(fis, password.toCharArray());
		
		byte[] decrypted = k.getDecryptedBytes();
		
		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(decrypted);
		
		PrivateKey pk = null;
		
		if( k.isDSA() ){
			pk = KeyFactory.getInstance("DSA").generatePrivate( spec );
		} else if ( k.isRSA() ) {
			pk = KeyFactory.getInstance("RSA").generatePrivate( spec );
		}
		
		pk = k.getPrivateKey();
		
		return pk;
		
	}

	private String generaCadenaOriginal() throws TransformerException {
		String resultado = "";
		
		StreamSource 			ss 	= new StreamSource( xslt );
		TransformerFactory		tf	= TransformerFactory.newInstance();
		tf.setURIResolver( new UriResolver() );
		
		Transformer				t	= tf.newTransformer( ss );
		
		
		ByteArrayOutputStream 	o	= new ByteArrayOutputStream();
		
		t.transform( new StreamSource( new ByteArrayInputStream( xmlOrigen.getBytes() ) ), new StreamResult( o ) );
		
		resultado =  o.toString();
		
		return resultado;
	}

	private void ActualizaXml( camposAGuardar campo, String valor) {
		
		
		String Modulo = "Guarda datos ";
		
		StringWriter 					sw 	= new StringWriter();
		JAXBContext						jc	= null;
		javax.xml.bind.Marshaller		m	= null;
		
		
		boolean							g = false;
		
		switch (versionSat) {
		case VERSION_2:
			com.ptb.xsd.sat.v2.Comprobante xmlPrevio = ( com.ptb.xsd.sat.v2.Comprobante ) xmlObject;
			
			
			switch ( campo ){
			case CERTIFICADO:
				xmlPrevio.setCertificado(valor);
				Modulo += "Certificado";
				break;
			case NO_CERTIFICADO:
				xmlPrevio.setNoCertificado(valor);
				Modulo += "No Certificado";
				break;
			case SELLO:
				xmlPrevio.setSello(valor);
				Modulo += "Sello";
				break;
			}
						
			try {
			
				setEstado(true, error.SIN_ERROR, "Intentando convertir a cadena", Modulo);
				
				jc 	= JAXBContext.newInstance(com.ptb.xsd.sat.v2.Comprobante.class);
				m	= jc.createMarshaller();
				
				m.marshal(xmlPrevio, sw);
				
				g = true;
			
			} catch ( Exception e ){
				setEstado(false, error.NO_SE_PUDO_GUARDAR_LOS_DATOS_DE_CERT, e.toString(), Modulo);
			}

			xmlObject = xmlPrevio;
			
			
			
			break;
		case VERSION_3_2:
			com.ptb.xsd.sat.v32.Comprobante xmlPrevio2 = ( com.ptb.xsd.sat.v32.Comprobante ) xmlObject;
			
			switch ( campo ){
			case CERTIFICADO:
				xmlPrevio2.setCertificado(valor);
				Modulo += "Certificado";
				break;
			case NO_CERTIFICADO:
				xmlPrevio2.setNoCertificado(valor);
				Modulo += "No Certificado";
				break;
			case SELLO:
				xmlPrevio2.setSello(valor);
				Modulo += "Sello";
				break;
			}
			
			try {
				
				setEstado(true, error.SIN_ERROR, "Intentando convertir a cadena", Modulo);
				
				jc 	= JAXBContext.newInstance(com.ptb.xsd.sat.v32.Comprobante.class);
				m	= jc.createMarshaller();
				
				m.marshal(xmlPrevio2, sw);
				
				g = true;
			
			} catch ( Exception e ){
				setEstado(false, error.NO_SE_PUDO_GUARDAR_LOS_DATOS_DE_CERT, e.toString(), Modulo);
			}
			
			xmlObject = xmlPrevio2;
			
		}
		
		
		if ( sw != null && g  ){
			if ( campo == camposAGuardar.SELLO ) {
				xmlSellado = sw.toString();
			} else {
				xmlOrigen = sw.toString();
			}
		}
		
	}

	public String getXmlOriginal() {
		return xmlOrigen;
	}

	public String getXmlSellado() {
		return xmlSellado;
	}
	
	public void setVersionXML( VersionSat vs )
	{
		versionSat = vs;
	}

	public HashMap<String, Boolean> getResultadoDeLasValidaciones() {
		return resultadoDeValidaciones;
	}

	public String getVersion() {
		return version;
	}

	public String getID() {
		
		return id;
	}

	public boolean getEstado() {
		return estado;
	}

	public String getCodError() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getDesError() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Object getXMlObject() {
		return xmlObject;
	}
	
	private void setEstado ( boolean est, error sinError , String mensaje, String Modulo ){
		
		estado 		= est;
		desError	= Modulo + " - " + sinError.getDesError();
		MesDesError	= mensaje;
		
	}
	
	private void setEstado ( boolean est, error sinError , String mensaje, String Modulo, String Validacion ){
		
		estado 		= est;
		desError	= Modulo + " - " + sinError.getDesError();
		MesDesError	= mensaje;
		
		resultadoDeValidaciones.put(Validacion, est);
	}
	
	private File ValidaArchivo ( String path, String Modulo, String validacion ) {
		
		File resultado = null;
		
		if ( path == null ){
			
			setEstado( false, error.SE_PASO_UNA_CADENA_VACIA, "Se paso una cadena vacia", Modulo, validacion );
			
		} else if ( path.trim().length() == 0) {
			
			setEstado( false, error.SE_PASO_UNA_CADENA_VACIA, "Se paso una cadena vacia", Modulo, validacion );
			
		} else {
			resultado = new File( path );
			if ( !resultado.exists() ){
				setEstado(false, error.EL_ARCHIVO_NO_EXISTE, "El archivo " + path + " no existe", Modulo, validacion );
				resultado = null;
			} 
		}
		
		if ( resultado != null ){
			setEstado(true, error.SIN_ERROR, "", Modulo, validacion);
		}
		
		
		return resultado;
	}
	
	private void convierteAUTF8( validaciones v ){
		
		String Modulo = "UTF8";
		
		setEstado(true, error.SIN_ERROR, "Iniciando validaciòn de codificacion", Modulo); 
		
		String codificacion = getEncoding( Modulo );
		
		if ( codificacion == null ){
			
			// intenta con algunos juegos.
			
			codificacion = "cp852";
			
		}
		
		
		if ( codificacion.equals( "UTF-8" ) ) {
			// El archivo se encuentra en UTF-8 no hay nada que hacer 
			
			setEstado(true, error.SIN_ERROR, "El archivo se encuentra en UTF-8", Modulo, validaciones.UTF8.getValidacion());
			
		} else {
			
			final Charset ori	= Charset.forName( codificacion );
			final Charset des	= Charset.forName( "UTF-8" );
			
			String resultado = "";
			
			Writer out = null;
			
			try {
			
				RandomAccessFile f = new RandomAccessFile(xml, "r" );
				byte[] b = new byte[(int) f.length()];
				
				f.read(b);
	
				f.close();
				
				final CharBuffer oriEnc = ori.decode( ByteBuffer.wrap( b ) );
				
				final byte[] utfEncode = des.encode(oriEnc).array() ;
				
				resultado = new String( utfEncode, des.displayName() );
				
				//resultado = unescapeHtml4( resultado.replace("&apos;", "\'" ) );
						
				// Una vez guardado el contenido borra el original
				
				xml.delete();
				
				// Ahora lo guarda con utf-8
				
				out =  new OutputStreamWriter( new FileOutputStream(xml), "UTF-8" );
				
				out.write( resultado.trim() );
				
				out.flush();
				
				out.close();
				
				setEstado(true, error.SIN_ERROR, "Codificacion detectada", Modulo, validaciones.UTF8.getValidacion());
				
			} catch ( Exception e ) {
				
				setEstado(false, error.NO_SE_DETECTO_CODIFICACION, e.toString(), Modulo, validaciones.UTF8.getValidacion());
				
			} finally {
				
				if ( out != null ){
					try {
						out.close();
					} catch ( Exception e ){
						setEstado(false,  error.NO_SE_PUDO_CERRAR_EL_ARCHIVO, "En utf-8", Modulo);
					}
				}
				
			}
		}
		
		
	}
	
	private void validaEsquema( validaciones v ){
		
		String Modulo = "Valida esquema";
		
		SchemaFactory 	sf 	= null;
		Schema			s 	= null;
		Validator		va	= null;
		
		try {
			
			setEstado(true, error.SIN_ERROR, "Iniciando validacion de esquema", Modulo);
			
			sf 	= SchemaFactory.newInstance( XMLConstants.W3C_XML_SCHEMA_NS_URI  );
			s	= sf.newSchema( new StreamSource(xsd));
			va	= s.newValidator();

			va.validate( new StreamSource( xml ) ) ;
			
			setEstado(true, error.SIN_ERROR , "XML Validado", Modulo, validaciones.ESQUEMA.getValidacion());
			
			xmlOrigen = new String();
			
			// Carga el xml, como objecto, y lo prepara.
			
			xmlObject = CargaXSD.getDatosFrom( xml.getAbsolutePath(), versionSat );
			
			
		} catch ( Exception e ){
			
			setEstado(false, error.XML_INVALIDO, e.toString(), Modulo, validaciones.ESQUEMA.getValidacion());
			
		}
		
		
		
		
	}
	
	private void validaImportes( validaciones v ) {
		
		switch (versionSat) {
		case VERSION_2:
			importesV2();
			break;
		case VERSION_3_2:
			importesV32();
			break;
		}
		
	}
	
	private void importesV2(){
	
		String Modulo = "Valida importes 2";
		boolean resultado = false;
		
		com.ptb.xsd.sat.v2.Comprobante c = (com.ptb.xsd.sat.v2.Comprobante) xmlObject;
		
		Double subTotal 		= 0.0;
		Double impRetenidos		= 0.0;
		Double impTrasladados	= 0.0;
		
		
		if ( c.getImpuestos().getTotalImpuestosRetenidos() != null ){
			impRetenidos = c.getImpuestos().getTotalImpuestosRetenidos().doubleValue();
		}
		
		if ( c.getImpuestos().getTotalImpuestosTrasladados() != null ){
			impTrasladados = c.getImpuestos().getTotalImpuestosTrasladados().doubleValue();
		}
		
		Double impuestos	= impRetenidos + impTrasladados;
		
		Double descuentos	= 0.0;
		
		if ( c.getDescuento() != null ){
			descuentos = c.getDescuento().doubleValue();
		}
		
		
		Double total		= c.getTotal().doubleValue();
		
		
		for ( com.ptb.xsd.sat.v2.Comprobante.Conceptos.Concepto pa : c.getConceptos().getConcepto() ){
			subTotal += pa.getImporte().doubleValue();
		}
		
		// Realiza la suma de toda la informacion
		
		if ( total == (subTotal + impuestos - descuentos ) ){
			resultado = true;
			setEstado(resultado, error.SIN_ERROR, "Importes correctos", Modulo, validaciones.IMPORTES.getValidacion());
		} else {
			resultado = false;
			setEstado(resultado, error.IMPORTE_INCORRECTOS, "Importes incorrectos", Modulo, validaciones.IMPORTES.getValidacion());
		}
		
		
		
	}
	
	private void importesV32() {
		String Modulo = "Valida importes 3.2";
		boolean resultado = false;
		
		com.ptb.xsd.sat.v32.Comprobante c = (com.ptb.xsd.sat.v32.Comprobante) xmlObject;
		
		Double subTotal 		= 0.0;
		Double impRetenidos		= 0.0;
		Double impTrasladados	= 0.0;
		
		
		if ( c.getImpuestos().getTotalImpuestosRetenidos() != null ){
			impRetenidos = c.getImpuestos().getTotalImpuestosRetenidos().doubleValue();
		}
		
		if ( c.getImpuestos().getTotalImpuestosTrasladados() != null ){
			impTrasladados = c.getImpuestos().getTotalImpuestosTrasladados().doubleValue();
		}
		
		Double impuestos	= impRetenidos + impTrasladados;
		
		Double descuentos	= 0.0;
		
		if ( c.getDescuento() != null ){
			descuentos = c.getDescuento().doubleValue();
		}
		
		Double total		= c.getTotal().doubleValue();
		
		
		for ( com.ptb.xsd.sat.v32.Comprobante.Conceptos.Concepto pa : c.getConceptos().getConcepto() ){
			subTotal += pa.getImporte().doubleValue();
		}
		
		// Realiza la suma de toda la informacion
		
		if ( total == (subTotal + impuestos - descuentos ) ){
			resultado = true;
			setEstado(resultado, error.SIN_ERROR, "Importes correctos", Modulo, validaciones.IMPORTES.getValidacion());
		} else {
			resultado = false;
			setEstado(resultado, error.IMPORTE_INCORRECTOS, "Importes incorrectos", Modulo, validaciones.IMPORTES.getValidacion());
		}
		
	}
	
	private String getEncoding ( String Modulo ) {
		
		String encoding = "";
		
		setEstado(true, error.SIN_ERROR, "Iniciando detection de codificacion", Modulo);
		
		//TODO Archivo de configuracion, si existe, desde aqui manda el tipo de codificacion.
		
		
		byte[] 				buf 		= new byte[4096];
		UniversalDetector 	detector 	= new UniversalDetector(null);
		FileInputStream		fis			= null;
		
		try  {
			fis = new java.io.FileInputStream( pathXml );

			int nread;
			while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
			  detector.handleData(buf, 0, nread);
			}
			
			detector.dataEnd();
			
		    encoding = detector.getDetectedCharset();
		    
		    fis.close();
		    
		} catch ( Exception e ) {
			
		} finally {
			if ( fis != null ){
				try {
					fis.close();
				} catch ( IOException ioe ){
					setEstado(true, error.NO_SE_PUDO_CERRAR_EL_ARCHIVO, "Error al cerra archivo", Modulo);
				}
			}
		}
	    
	    if ( encoding == null ) {
	    	setEstado( false , error.NO_SE_DETECTO_CODIFICACION, "No se pudo detectar la codificacion", Modulo);
	    } else {
	    	setEstado(true, error.SIN_ERROR, "Coodificacion detectada", Modulo);
	    }

	    detector.reset();

		
		return encoding;
		
	}
	
	private X509Certificate getX509Certificate( final File certificado ){
		
		String Modulo = "Certificado";
		
		FileInputStream 	fis = null;
		CertificateFactory 	cf 	= null;
		X509Certificate		r 	= null;
		
		setEstado(true, error.SIN_ERROR,"Obteniendo certificado", Modulo);
		
		try {
			fis 		= new FileInputStream(certificado);
			cf			= CertificateFactory.getInstance("X.509");
			r 			= (X509Certificate) cf.generateCertificate(fis);
			
			fis.close();
			
		} catch ( Exception e ){
			setEstado(false, error.NO_SE_PUDO_OBTENER_EL_CERTIFICADO, e.toString(), Modulo);
		} finally {
			if ( fis != null ){
				try{
					fis.close();
				} catch (Exception e ) {
					setEstado ( false, error.NO_SE_PUDO_CERRAR_EL_ARCHIVO, e.toString(), Modulo );
				}
			}
		}
		
		return r;
	}

	private String getCertificadoBase64( final X509Certificate cert ) throws CertificateEncodingException {
		return new String ( Base64.encode(cert.getEncoded() ) );
	}
	
	private String getNoCertificado ( final X509Certificate cert ){
		BigInteger serial = cert.getSerialNumber();
		byte[] sArr = serial.toByteArray();
		StringBuffer buffer = new StringBuffer();
		for ( int i=0 ; i < sArr.length; i++ ){
			buffer.append( (char) sArr[i]);
		}
		
		return buffer.toString();
		
	}
	
}
