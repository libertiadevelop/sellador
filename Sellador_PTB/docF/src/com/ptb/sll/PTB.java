package com.ptb.sll;

import java.util.HashMap;

import com.ptb.sll.Bitacora.DebugMode;

public class PTB {

	enum tipoValidacion {
		PARAMETROS(0),
		ARCHIVOS(1)
		;
		int v = 0;
		private tipoValidacion( int i )
		{
			v = i;
		}	
	}
	
	enum archivos{
		ARCHIVO_ORIGEN(0),
		ARCHIVO_DESTINO(1),
		ARCHIVO_KEY(2),
		ARCHIVO_CERT(3)
		;
		int ar;
		private archivos( int a )
		{
			ar = a;
		}
		
		public String getDescripcionDeArchivo()
		{
			String resultado = "";
			switch ( ar ) {
			case 0:
				resultado = "Archivo origen";
				break;
			case 1:
				resultado = "Archivo destino";
				break;
			case 2:
				resultado = "Archivo key";
				break;
			case 3:
				resultado = "Archivo cert";
				break;
			}
			
			return resultado;
		}
	}
	
	int 					numeroDeParametros 	= 34;
	boolean 				continuar 			= true;
	Bitacora				bitGen				= null;

	HashMap<archivos, String> archivosSellado	= new HashMap<archivos, String>(); 
	
	public static void main(String[] args) 
	{
		int 	numeroDeParametros 	= 34;
		boolean continuar	 		= true;

		Bitacora b = new Bitacora(continuar, continuar, null);
		
		b.guardaRegistro("Iniciando sellado");
		
		b.guardaRegistro( "Validando numero de parametros" );
		if ( args.length != numeroDeParametros ) {
			b.guardaRegistro( "No se pasaron los parametros necesarios" );
			int cont = 0;
			
			for ( String param :  args ){
				b.guardaRegistro( ++cont + " - " + param );
			}
			continuar = false;
		}
		
		// Valida la informacion pasada.
		if ( continuar ){
			
		}
		
		
	
		b.guardaRegistro("Terminando sellado");
	}
	
	public PTB( int NumeroDeParametros, String[] args ){
		
		numeroDeParametros 	= NumeroDeParametros;
		bitGen 				= new Bitacora("log.t", false, false, DebugMode.BASICO);
		
		setMapArchivos();
		
		bitGen.setRegistro("Iniciando sellado");
		bitGen.setRegistro("Iniciando validacion de parametros");
		valida ( args );
		bitGen.setRegistro("Finalizando validacion de parametros");
		
	}
	
	public void valida( String[] args ){
		
		bitGen.setRegistro("");
		
		for ( tipoValidacion t : tipoValidacion.values() ){
			
			if ( !validaciones(t, args) ) {
				if ( continuar ) {
					continuar = false;
				}
			}
		}
		
	}
	
	
	private boolean validaciones ( tipoValidacion t, String[] args ){
		boolean resultado = false;
		
		switch ( t ){
		case PARAMETROS:
			bitGen.setRegistro("Iniciando validacion de parametros");
			resultado = numeroDeParametros( args );
			bitGen.setRegistro("Finalizando validacion de parametros, el resultado fue: " + resultado );
			break;
		case ARCHIVOS:
			bitGen.setRegistro("Iniciando validacion de archivos");
			resultado = archivos( args );
			bitGen.setRegistro("Finalizando validacion de archivos, el resultado fue: " + resultado );
			break;
		}
		
		return resultado;
		
	}
	
	// VALIDACIONES
	
	private boolean numeroDeParametros ( String[] args)
	{
		boolean resultado = false;
		
		if ( numeroDeParametros != args.length ){
			bitGen.setRegistro("El numero de parametros pasados no es el correcto" );
			int cont = 0;
			for( String param : args ) {
				bitGen.setRegistro( "Parametro " + ++cont + " " + param ) ;
			}
		} else {
			bitGen.setRegistro( "El numero de parametros pasados es correcto" );
		}
		
		return resultado;
	}
	
	private boolean archivos( String[] args )
	{
		boolean resultado = false;
		// Valida los archivos del parametro 1 al 4
		archivosSellado.put( archivos.ARCHIVO_ORIGEN, args[0]);
		archivosSellado.put(archivos.ARCHIVO_DESTINO, args[1]);
		archivosSellado.put(archivos.ARCHIVO_KEY, args[2]);
		archivosSellado.put(archivos.ARCHIVO_CERT, args[3]);
		
		return resultado;
	}
	
	
	// VARIOS
	
	private void setMapArchivos()
	{
		for ( archivos a : archivos.values() ){
			archivosSellado.put(a, "");
		}
	}
}

