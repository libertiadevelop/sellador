package com.ptb.sll;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Bitacora implements Error, id, bit {

	private String version 	= "0.0.0.0";
	private String id		= "101";
	
	private boolean	estado				= true;
	private String 	codigoDeError		= "";
	private String	descripcionError 	= "";
	
	private String cfgFile 		= "confLog.cfg";
	private String logFile		= "log.t";
	
	File fileLog				= null;
	
	private Boolean 	eliminaPrevio 		= false;
	private Boolean 	muestraEnConsola	= false;
	private DebugMode 	debug				= DebugMode.BASICO;
	
	private String idClase				= "";
	private String metodo				= "";
	private String valoresPasados		= "";
	private String valoresADevolver		= "";
	private String stackTrace			= "";
	
	public static enum Severidad {
		TAG(2), ALARMA(4), ERROR(5);
		
		private int severidad;
		
		private Severidad( int i ){
			severidad = i;
		}
		
		public String getSeveridad(){
			String sSeveridad = "";
			switch (severidad) {
			case 2:
				sSeveridad = "inf";
				break;
			case 4:
				sSeveridad = "alm";
				break;
			case 5:
				sSeveridad = "err";
				break;

			}
			
			return sSeveridad;
		}
		
		
	}
	
	public static enum DebugMode {
		BASICO(1), AVANZADO(2);
		
		private int modo;
		private DebugMode(int dm ){
			modo = dm;
		}
		
		public String getModo (){
			String sModo = "";
			switch (modo) {
			case 1:
				sModo = "BASICO";
				break;
			case 2:
				sModo = "AVANZADO";
				break;

			}
			
			return sModo;
		}
		
	}
	

	public void setErrorStackTrace ( String stack ){
		stackTrace = stack;
	}
	
	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public String getID() {
		return id;
	}

	@Override
	public boolean getEstado() {
		return estado;
	}

	@Override
	public String getCodError() {

		return codigoDeError;
	}

	@Override
	public String getDesError() {
	
		return descripcionError;
	}
	
	
	public Bitacora ( Boolean EliminaPrevio, Boolean MuestraEnConsola, DebugMode Debug)
	{
		if ( configFileExist() ){
			setValoresBitacora();
		} else {
			setValoresBitacora(EliminaPrevio, MuestraEnConsola, Debug);
		}
		
		eliminaArchivosSiSeRequiere();
		estado = true;
	}
	public Bitacora ( String path, Boolean EliminaPrevio, Boolean MuestraEnConsola, DebugMode Debug )
	{
		if ( configFileExist() ){
			setValoresBitacora();
		} else {
			setValoresBitacora(EliminaPrevio, MuestraEnConsola, Debug);
		}
		
		logFile = path;
		
		eliminaArchivosSiSeRequiere();
		estado = true;
	}
	
	public void setAvanzado  ( String IdClase, String Metodo, String ValoresPasados, String ValoresADevolver){
		idClase 			= IdClase;
		metodo 				= Metodo;
		valoresPasados		= ValoresPasados;
		valoresADevolver	= ValoresADevolver;
	}
	
	@Override
	public void setRegistro(String Descripcion) 
	{
		String CadenaAGuardar = debug.getModo() + " " + Severidad.TAG.getSeveridad() + ": " + 
								getDatosDebugAvanzado() + Descripcion + " " + stackTrace ;
		
		guardaRegistro(CadenaAGuardar);
	}
	
	public void setRegistro ( Severidad severidad, String Descripcion )
	{
		String CadenaAGuardar = debug.getModo() + " " + severidad.getSeveridad() + ": " + 
				getDatosDebugAvanzado() + Descripcion + " " + stackTrace ;

		guardaRegistro(CadenaAGuardar);
	}
	
	public void guardaRegistro( String CadenaAGuardar )
	{
		CadenaAGuardar = CadenaAGuardar.trim();
		
		PrintWriter p = null;
		
		CadenaAGuardar = getDateTime() + " " + CadenaAGuardar;
 		
		try {
			p = new PrintWriter( new BufferedWriter( new FileWriter( logFile, true )) );
			p.println( CadenaAGuardar);
			p.close();
		} catch ( Exception ge ){
			estado 			= false;
			codigoDeError	= "102";
			descripcionError = "No se pudo guardar. " + ge.toString();			
		} finally {
			if ( p != null ) p.close();
		}
		
		if ( muestraEnConsola ){
			System.out.println( CadenaAGuardar );
		}
	}

	private String getDatosDebugAvanzado(){
		String resultado = "";
		if ( debug == DebugMode.AVANZADO ) {
			resultado = "[" + idClase + "] [" + metodo + "] [" + valoresPasados + "] [" + valoresADevolver + "] ";
		} 
		return resultado;
	}
	
	private String getDateTime()
	{
		String timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
		return timeStamp;	
	}
	
	private boolean configFileExist()
	{
		boolean existe = false;
		
		File cf = new File( cfgFile );
		
		if ( cf.exists() ){
			existe = true;
		} else{
			existe = false;
		}
		
		return existe;
	}
	
	private void setValoresBitacora( Boolean EliminaPrevio, Boolean MuestraConsola, DebugMode Debug )
	{
		eliminaPrevio 		= EliminaPrevio;
		muestraEnConsola	= MuestraConsola;
		debug				= Debug;
	}
	
	private void setValoresBitacora(  )
	{
		// Lee el archivo y procede a cargar los valores por default
		try {
			BufferedReader br = new BufferedReader( new FileReader( cfgFile ) );
			
			String s;
			
			while ( (s = br.readLine()) != null  ){
				if ( s.trim().length() > 0 ) {
					String llave = s.substring(0, s.indexOf('=')  );
					String valor = s.substring( s.indexOf('=') + 1 );
					
					if ( llave.equals("EliminaPrevios") ){
						eliminaPrevio = convierteTextoABoolean(valor);
					}
					
					if ( llave.equals("MostrarEnConsola") ){
						muestraEnConsola = convierteTextoABoolean(valor);
					}
					
					if ( llave.equals("DebugMode") ){
						if ( valor == "Basico" ){
							debug = DebugMode.BASICO;
						} else {
							debug = DebugMode.AVANZADO;
						}
					}
					
					
					
				}
			}
			
			br.close();
			
			estado = true;
			
		} catch ( Exception e ){
			estado 				= true;
			codigoDeError 		= "002";
			descripcionError	= "El archivo no se pudo leer. " + e.toString();
		}
		
		
	}
	
	private boolean convierteTextoABoolean ( String valor )
	{
		boolean resultado = false;
		
		if ( valor.equals( "1" ) ){
			resultado = true;
		} else {
			resultado = false;
		}
		
		return resultado;
	}
	
	private void eliminaArchivosSiSeRequiere()
	{
		if ( eliminaPrevio ){
			File lf = new File( logFile );
			if ( lf.exists() ){
				lf.delete();
			}
		}
	}
}
