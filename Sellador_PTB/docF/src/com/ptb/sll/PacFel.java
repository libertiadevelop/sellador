package com.ptb.sll;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.datacontract.schemas._2004._07.TES_TFD_Negocios.*;
import org.tempuri.TimbrarCFDI;

public class PacFel implements Ipac {

	RespuestaTFD respuesta;
	
	public boolean getEstado() {

		return est.getValue();
	}

	@Override
	public String getCodError() {
		return String.valueOf(e.getCodError());
	}

	@Override
	public String getDesError() {
		
		return e.getDescripcionDelError();
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public String getID() {

		return id;
	}

	@Override
	public boolean Timbrar(String usuario, String password, String pathXmlFile) {
		
		boolean resultado = false;
		
		return resultado;
	}

	@Override
	public boolean Cancela(String usuario, String password, String RFC_Emisor, 	String[] UUID, String Certificado, String passwordCertificado) {
		// TODO Auto-generated method stub
		return false;
	}

	//region VARIABLES PRIVADAS
	
	String version 	= "0.0.0.0"	;	
	String id 		= "110";
	estado est		= estado.OK;
	
	error e ;
	
	String 	usuario;
	String 	password;
	String	pathToXMl;
	
	File 	xmlSellado;
	
	String 	cadenaXmlSellado;
	String 	addenda;
	
	RespuestaTFD timbre;
	
	//endregion
	
	//region Manejo de errores
	private enum error {
		SIN_ERROR(0),
		USUARIO_VACIO(1),
		PASSWORD_VACIO(2),
		ARCHIVO_VACIO(3),
		EL_ARCHIVO_NO_EXISTE(4),
		NO_SE_PUDO_LEER_EL_ARCHIVO_XML(5)
		;
		
		int e;
		private error( int err ){
			e = err;
		}
		
		public int getCodError(){
			return e;
		}
		
		public String getDescripcionDelError() {
			String resultado = "";
			switch (e) {
			case 0:
				resultado = "";
				break;
			case 1:
				resultado = "LA CADENA DEL USUARIO ESTA VACIO";
				break;
			case 2:
				resultado = "LA CADENA DE PASSWORD ESTA VACIO";
				break;
			case 3:
				resultado = "LA CADENA DE ARCHIVO ESTA VACIA";
				break;
			case 4:
				resultado = "EL ARCHIVO NO EXISTE";
				break;
			case 5:
				resultado = "NO SE PUDO OBTENER LA CADENA DEL XML";
				break;
			}
			return resultado;
		}
		
		public estado getEstado(){
			estado resultado = estado.INDEFINIDO;
			if ( e == 0 ){
				resultado = estado.OK;
			} else {
				resultado = estado.ERROR;
			}
			return resultado;
		}
		
	}
	//endregion
	
	private enum estado {
		ERROR(0),
		OK(1),
		INDEFINIDO(2)
		;
		int v;
		private estado(int vv) {
			v = vv;
		}
		public boolean getValue () {
			boolean resultado = false;
			
			switch (v) {
			case 0:
				resultado = false;
				break;
			case 1:
				resultado = true;
				break;
			}
				
			return resultado;
		}
	}
	
	
	public PacFel( String Usuario, String Password, String PathToXMl ) {
	
		// Valida la informacion que se le esta pasando a fel.
		
		validaCadenas(Usuario, error.USUARIO_VACIO);
		
		if ( e != error.SIN_ERROR ){
			return;
		} else{
			usuario = Usuario;
		}
		
		validaCadenas(Password, error.PASSWORD_VACIO);
		
		if ( e != error.SIN_ERROR ){
			return;
		} else {
			password  = Password;
		}
		
		validaCadenas(PathToXMl, error.ARCHIVO_VACIO);
		
		if ( e != error.SIN_ERROR ){
			return;
		} else {
			pathToXMl = PathToXMl;
		}
		
		xmlSellado = new File(PathToXMl);
		
		if ( !xmlSellado.exists() ) {
			setError( error.EL_ARCHIVO_NO_EXISTE );
		}
		
	}
	
	public boolean timbra(){
		boolean resultado = false;
		
		// Recupera la informacion que se encuentra en el archivo xml
		
		// Valida que se encuentre la adenda, en caso de existir la extrae
		
		// Manda a timbrar
		
		// Si la adenda existio se guarda la informacion
		
		// Genera un archivo xml con el resultado obtenido.
		
		return resultado;
		
	}
	
	private void getStringFromXMl(){
		try {
			BufferedReader 	lector 	= new BufferedReader( new FileReader( pathToXMl) );
			String 			line 	= "";
			StringBuilder 	sb 		= new StringBuilder();
			
			String			ls		= System.getProperty("line.separator");
			
			while ( (line = lector.readLine()) != null ){
				sb.append(line);
				sb.append(ls);
			}
			
			cadenaXmlSellado = sb.toString();
			
			e = error.SIN_ERROR;
			
		} catch ( Exception ioe ) {
			e = error.NO_SE_PUDO_LEER_EL_ARCHIVO_XML;
			cadenaXmlSellado = "";
		}
		
	}
	
	private void getAdenda() {
		
	}
	
	private void setAdenda() {
		
	}
	
	private void validaCadenas ( String cadena, error errorARepotar ){
		
		setError( ( ( cadena.trim().length() == 0 ) ? errorARepotar : error.SIN_ERROR) ); 
		
	}
	
	private void setError ( error er ){
		e 	= er;
		est = e.getEstado();
	}
	
	
}
