package com.ptb.sll;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

class Borde extends PdfPageEventHelper{
	
	private float anchoDeLinea = 1;
	private float ajusteInicio = 1;
	
	public void setAnchoDeLinea ( float ancho ) {
		anchoDeLinea = ancho;
	}
	
	public void onEndPage( PdfWriter writer, Document document ) {
		PdfContentByte 	canvas 		= writer.getDirectContent();
		Rectangle		rectProv	= document.getPageSize(); 
		

		
		float izq 	= document.leftMargin();
		float der	= document.rightMargin();
		float sup 	= document.topMargin();
		float inf	= document.bottomMargin();
		
		float ajuste = anchoDeLinea + ajusteInicio;
		
		Rectangle rect = new Rectangle(izq - ajuste, inf - ajuste, rectProv.getWidth() - der - ajuste ,rectProv.getHeight() - sup - ajuste );
		
		rect.setBorder( Rectangle.BOX );
		
		rect.setBorderWidth( anchoDeLinea );
		
		rect.setBorderColor( BaseColor.BLACK );
		rect.setUseVariableBorders(true);
		canvas.rectangle(rect);
	}	
	
}
