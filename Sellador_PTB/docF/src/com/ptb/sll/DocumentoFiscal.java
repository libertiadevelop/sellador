package com.ptb.sll;
import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.ptb.sll.Bitacora;
import com.ptb.sll.Bitacora.DebugMode;
import com.ptb.sll.Bitacora.Severidad;
import com.ptb.sll.DocumentoFiscalFuente.validaciones;

public class DocumentoFiscal
{
	
	String 		id		= "109";
	String 		version = "0.0.0.0";
	
	String 		mensaje;							// Regresa informacion anexa
	
	private List<String> log = new ArrayList<String>();
	
	Bitacora	bitGlb 	= null;
	Bitacora	bitDoc	= null;
	
	DebugMode	d;
	
	String _valoresPasados;
	String _valoresDevueltos;
	
	String documento;
	
	error  _er;
	flujo  _fl;
	estado _es;
	
	String pathXmlOrigen;
	String pathXmlDestino;
	String pathKey;
	String pahtCert;
	String pathXsd;
	String pathXslt;
	
	String passwordKey;
	
	VersionSat _versionSat;
	
	HashMap<archivos, String> paths ;
	
	public boolean getEstado() {
		return _es.getValue();
	}
	
	public int getCodigoDeError(){
		return _er.getCodError();
	}
	
	public String getDescripcionDeError(){
		return _er.getDescripcionDelError();
	}
	
	public String getInformacionExtra(){
		return mensaje;
	}
	
	public List<String> getLog(){
		return log;
	}
	
	
	private enum flujo{
		
		INICIO(10),
		VALIDANDO_EXISTENCIA_DE_ARCHIVOS(20),
				VALIDANDO_XML(21),
				VALIDANDO_KEY(22),
				VALIDANDO_CER(23),
				VALIDANDO_DEF(24),
				VALIDANDO_ARCHIVO_DE_SALIDA_XML(25),
				VALIDANDO_ARCHIVO_DE_SALIDA_PDF(26),
				VALIDANDO_ARCHIVO_XSD(27),
				VALIDANDO_ARCHIVO_XSLT(28),
				VALIDANDO_PASSWORD(29),
		VALIDANDO_DATOS_KEY_CERT(30),
		VALIDANDO_VERSION_SAT(31),
		VALIDANDO_ESQUEMA(40),
		GENERANDO_SELLO(50),
		GUARDANDO_ARCHIVO_XML_SELLADO(51),
		TIMBRANDO(60),
		GENERANDO_REPORTES(70),
		GENERANDO_PDF(71),
		GENERANDO_IMPRESION(72),
		FINALIZAR(80)
		;
		
		int f;
		private flujo( int fl){
			f = fl;
		}
		
		public String getDescripcionDeFlujo(){
			return getDesFlujo().trim();
		}
		
		@SuppressWarnings("unused")
		public String getDescripcionDeFlujo( String mensaje ){
			String resultado = getDesFlujo();
			if ( !mensaje.isEmpty() ) {
				resultado = resultado + mensaje;
			}
			return resultado.trim();
		}
		
		private String getDesFlujo(){
			String resultado = "";
			switch (f) {
			case 10:
				resultado = "Iniciando";
				break;
			case 20:
				resultado = "Validando la existencia de archivos";
				break;
			case 21:
				resultado = "Validando la existencia de Xml de entrada";
				break;
			case 22:
				resultado = "Validando la existencias de Key";
				break;
			case 23:
				resultado = "Validando la existencias de Cer";
				break;
			case 24:
				resultado = "Validando la existencias de xsd";
				break;
			case 25:
				resultado = "Validando existencia de Xml timbrado";
				break;
			case 26:
				resultado = "Validando existencia de PDF";
				break;
			case 27:
				resultado = "Validando existencia de archivo xsd";
				break;
			case 28:
				resultado = "Validando existencia de archivo xslt";
				break;
			case 29:
				resultado = "Validando que se haya pasado contraseña, para el key";
				break;
			case 30:
				resultado = "Validando los datos fiscales";
				break;
			case 31:
				resultado = "Validando la version del sat";
				break;
			case 40:
				resultado = "Validando con el esquema";
				break;
			case 50:
				resultado = "Sellando documento";
				break;
			case 51:
				resultado = "Guardando archivo xml sellado";
				break;
			case 60:
				resultado = "Timbrando";
				break;
			case 70:
				resultado = "Generando reportes";
				break;
			case 71:
				resultado = "Generando PDF";
				break;
			case 72:
				resultado = "Generando Impresion";
				break;
			case 80:
				resultado = "Finalizando";
				break;
			}
			return resultado + ". ";
		}
		
		private String getMetodo(){
			String resultado = "";
			if ( f < 30 ){
				resultado = "Constructor";
			} else if (f < 60 ) {
				resultado = "Sellador";
			} else if ( f < 70 ){
				resultado = "Timbrador";
			} else if ( f < 80 ){
				resultado = "Reporteador";
			} else {
				resultado = "No definido";
			}
			return resultado ;
		}
		
	}
	
	private enum error {
		SIN_ERROR(0),
		PATH_VACIO(1),
		NO_EXISTE_EL_ARCHIVO(2),
		NO_SE_PUDO_BORRAR_EL_ARCHIVO(3),
		NO_SE_PASO_PASSWORD(4),
		NO_SE_PUEDE_CONTINUAR_ERROR_ANTERIOR(5),
		LA_VERSION_DEL_SAT_ESTA_VACIA(6),
		LA_VERSION_DEL_SAT_NO_ES_NUMERICA(7),
		ERROR_AL_VALIDAR_UTF8(8),
		ERROR_AL_VALIDAR_ESQUEMA(9),
		ERROR_AL_VALIDAR_IMPORTES(10),
		ERROR_AL_GENERAR_EL_SELLO(11),
		NO_SE_PUDO_GUARDAR_EL_ARCHIVO_XML_SELLADO(12)
		;
		int e;
		private error( int er ){
			e = er;
		}
		
		public int getCodError(){
			return e;
		}
		public String getDescripcionDelError(){
			String resultado = "";
			
			switch (e) {
			case 0:
				resultado = "No hay error";
				break;
			case 1:
				resultado = "El path esta vacio";
				break;
			case 2:
				resultado = "El archivo no existe";
				break;
			case 3:
				resultado = "No se pudo eliminar el archivo";
				break;
			case 4:
				resultado = "No se paso el password";
				break;
			case 5:
				resultado = "Existe un error previo";
				break;
			case 6:
				resultado = "La version del sat, esta vacia";
				break;
			case 7:
				resultado = "La version del sat no es numerica";
				break;
			case 8:
				resultado = "Problemas al validar UTF8";
				break;
			case 9:
				resultado = "Problemas al validar el esquema";
				break;
			case 10:
				resultado = "Los importes no concuerdan";
				break;
			case 11:
				resultado = "Error al intentar generar el sello";
				break;
			case 12:
				resultado = "No se pudo guardar el xml sellado";
				break;
			}
			
			return resultado;
		}
		
		public Severidad getSeveridad(){
			Severidad s = Severidad.TAG;
			switch (e) {
			case 0:
				s = Severidad.TAG;
				break;
			default:
				s = Severidad.ERROR;
				break;
			}
	
			return s;
		}
		
		public estado getEstado(){
			estado esta = estado.INDEFINIDO;
			switch ( e ){
			case 0:
				esta = estado.OK;
				break;
			default:
				esta = estado.ER;
				break;
			}
			return esta;
		}
		
	}
	
	private enum estado {
		ER(0),
		OK(1),
		INDEFINIDO(2)
		;
		int v;
		private estado (int va){
			v= va;
		}
		
		public boolean getValue(){
			boolean resultado = false;
			switch (v) {
			case 0:
				resultado = false;
				break;
			case 1:
				resultado = true;
				break;
			}
			
			return resultado;
		}
		public String getStringDescripcion(){
			String resultado = "";
			switch (v) {
			case 0:
				resultado = "Mal";
				break;
			case 1:
				resultado = "Bien";
				break;
			}
			
			return resultado;
		}
	}
	
	private enum dondeReporta{
		GLOBAL, 
		DOCUMENTO,
		AMBAS
		;
	}
	
	private enum archivos{
		XML_ORIGEN,
		XML_DESTINO,
		KEY,
		CERT,
		XSD,
		XSLT,
		PDF_DESTINO
		;
	}
		
	public DocumentoFiscal ( String xmlOrigen, String xmlDestino, String Key, String Cert, String password, String xsd, String xslt, String versionSat, boolean reporta )
	{
		// Inicia con la bitacora
		
		setDatosAvanzado( xmlOrigen + "," + xmlDestino + "," + Key + "," + Cert + "," + password + "," + xsd + "," + xslt + "," + reporta , "void" );
		
		d = ( reporta ) ? DebugMode.AVANZADO : DebugMode.BASICO;
		
		setError(error.SIN_ERROR);
		
		paths = new HashMap<DocumentoFiscal.archivos, String>();
		
		// Valida la informacion de entrada archivos 
		_fl = flujo.VALIDANDO_XML;
		paths.put(archivos.XML_ORIGEN, validaArchivo(xmlOrigen, false, dondeReporta.GLOBAL) );
		
		_fl = flujo.VALIDANDO_ARCHIVO_DE_SALIDA_XML;
		paths.put(archivos.XML_DESTINO, validaArchivoNoExiste(xmlDestino, dondeReporta.GLOBAL) );
		
		String nombrePdf = xmlDestino.substring(0, xmlDestino.lastIndexOf(".") ) + "pdf";
		documento = xmlDestino.substring(xmlDestino.lastIndexOf("\\") + 1, xmlDestino.lastIndexOf(".") );
		
		_fl = flujo.VALIDANDO_ARCHIVO_DE_SALIDA_PDF;
		paths.put(archivos.PDF_DESTINO, validaArchivoNoExiste(nombrePdf, dondeReporta.GLOBAL) );
		
		_fl = flujo.VALIDANDO_KEY;
		paths.put(archivos.KEY, validaArchivo( Key, false, dondeReporta.GLOBAL) );
		
		_fl = flujo.VALIDANDO_CER;
		paths.put(archivos.CERT, validaArchivo(Cert, false, dondeReporta.GLOBAL) );
		
		_fl = flujo.VALIDANDO_ARCHIVO_XSD;
		paths.put( archivos.XSD, validaArchivo(xsd, false, dondeReporta.GLOBAL) );
		
		_fl = flujo.VALIDANDO_ARCHIVO_XSLT;
		paths.put(archivos.XSLT, validaArchivo(xslt, false, dondeReporta.GLOBAL) );
		
		_fl = flujo.VALIDANDO_PASSWORD;	
		setError( (password.trim().isEmpty()) ? error.NO_SE_PASO_PASSWORD : error.SIN_ERROR );
		if ( _er == error.SIN_ERROR ) {
			passwordKey = password;
		}
		setLog("", dondeReporta.GLOBAL );
		
		_fl = flujo.VALIDANDO_VERSION_SAT;
		setError( (versionSat.trim().isEmpty() ) ? error.LA_VERSION_DEL_SAT_ESTA_VACIA : error.SIN_ERROR );
		setLog( "", dondeReporta.GLOBAL );
		if ( _er == error.SIN_ERROR ) {
			setError( ( !Utilidades.isNumeric(versionSat) ) ? error.LA_VERSION_DEL_SAT_NO_ES_NUMERICA : error.SIN_ERROR );
			setLog( "", dondeReporta.GLOBAL );
			if ( _er == error.SIN_ERROR ){
				setVersionSat(versionSat);
			}
		}
		
	}
	
	public void sellaDocumento(){
		if ( _es == estado.ER ) {
			_er = error.NO_SE_PUEDE_CONTINUAR_ERROR_ANTERIOR;
			setLog("No se puede continuar", dondeReporta.GLOBAL );
			return;
		}
		
		// Empieza la validacion de informacion se creara la validacion por documento
		
		_fl = flujo.GENERANDO_SELLO;
		
		DocumentoFiscalFuente df = new DocumentoFiscalFuente( paths.get(archivos.XML_ORIGEN), paths.get(archivos.XSD));
		
		df.setVersionXML(_versionSat);
		
		df.valida(validaciones.UTF8);
		setError( (!df.estado) ? error.ERROR_AL_VALIDAR_UTF8 : error.SIN_ERROR );
		setLog( df.getDesError(), dondeReporta.DOCUMENTO );
		df.valida(validaciones.ESQUEMA);
		setError( (!df.getEstado()) ? error.ERROR_AL_VALIDAR_ESQUEMA : error.SIN_ERROR );
		setLog( df.getDesError(), dondeReporta.DOCUMENTO);
		df.valida(validaciones.IMPORTES);
		setError( (!df.getEstado()) ? error.ERROR_AL_VALIDAR_IMPORTES : error.SIN_ERROR);
		setLog(df.getDesError(), dondeReporta.DOCUMENTO);
		
		if ( _er == error.SIN_ERROR ) {
			df.SellaDocumento(paths.get(archivos.CERT) , paths.get(archivos.KEY), passwordKey, paths.get(archivos.XSLT));
			setError( !df.getEstado() ? error.ERROR_AL_GENERAR_EL_SELLO : error.SIN_ERROR );
			setLog( df.getDesError(), dondeReporta.AMBAS);
		}
		
		if ( _er == error.SIN_ERROR ){
			// Procede a guardar el documento
			_fl = flujo.GUARDANDO_ARCHIVO_XML_SELLADO;
			guardaXml( df.getXmlSellado() );
		}
		
	}
	
	public void timbraDocumento () {
		
		if ( _es == estado.ER ) {
			_er = error.NO_SE_PUEDE_CONTINUAR_ERROR_ANTERIOR;
			setLog("No se puede continuar", dondeReporta.GLOBAL );
			return;
		}
		
		// Manda el sellador ...
		
		
		
		
		
		
		_fl = flujo.TIMBRANDO;
	}
	
	public void imprimeComprobante() {
		if ( _es == estado.ER ) {
			_er = error.NO_SE_PUEDE_CONTINUAR_ERROR_ANTERIOR;
			setLog("No se puede continuar", dondeReporta.GLOBAL );
			return;
		}
		
		_fl = flujo.GENERANDO_PDF;
		
		_fl = flujo.GENERANDO_IMPRESION;
		
		_fl = flujo.FINALIZAR;
	}
	
	private void setLog( String Message, dondeReporta dr ){
		
		mensaje					= Message;
		
		switch (dr) {
		case GLOBAL:
			setBitacoraGlobal( );
			break;
		case DOCUMENTO:
			setBitacoraDocumento( );
			break;
		case AMBAS:
			setBitacoraGlobal( );
			setBitacoraDocumento( );
			break;
		}

	}
	
	private void setBitacoraGlobal( ){
		if ( bitGlb == null ){
			bitGlb = new Bitacora( "ptb.log", false, false, d ); 
		}
		
		
		if ( d == DebugMode.AVANZADO ){
			bitGlb.setAvanzado(id, _fl.getMetodo(), _valoresPasados, _valoresDevueltos );
		}
		
		bitGlb.setRegistro( _er.getSeveridad(), construyeMensaje() );
		
	}
	
	private void setBitacoraDocumento( ){
		if ( bitDoc == null ){
			bitDoc = new Bitacora( documento + ".log", true, false, d );
		}
		if ( d == DebugMode.AVANZADO ){
			bitDoc.setAvanzado(id, _fl.getMetodo(), _valoresPasados, _valoresDevueltos );
		}
		
		bitDoc.setRegistro( _er.getSeveridad(), construyeMensaje() );
		
	}
	
	private void setDatosAvanzado( String datosPasados, String datosDevueltos ){
		_valoresPasados 	= datosPasados;
		_valoresDevueltos 	= datosDevueltos;
	}
	
	private String construyeMensaje ( )	{
		// Flujo(operacion) - resultado - Error - mensaje(stackTrace)
		
		String espacio = "-";
		
		return _fl.getDescripcionDeFlujo() + espacio + _es.getStringDescripcion() + espacio + _er.getDescripcionDelError() + espacio + mensaje;
				
	}
	
	private String validaArchivo ( String path, boolean borraSiexiste, dondeReporta d ) {
		
		setError ( ( path.isEmpty() ) ? error.PATH_VACIO : error.SIN_ERROR );
		setLog("Se paso el path:" + path, d);
		if ( _er != error.SIN_ERROR ) {
			return "";
		}
		
		setError ( (Utilidades.ExisteElArchivo(path)) ? error.SIN_ERROR: error.NO_EXISTE_EL_ARCHIVO );
		setLog("El archivo " + path + ( (_er == error.SIN_ERROR ) ? " existe" : " no existe" ), d);
		if ( _er != error.SIN_ERROR ){
			return "";
		} 
		
		if ( borraSiexiste ) {
			setError ( ( Utilidades.borraArchivo(path) ) ? error.SIN_ERROR : error.NO_SE_PUDO_BORRAR_EL_ARCHIVO );
			setLog("", d);
		}
		
		return path;
		
	}
	
	private String validaArchivoNoExiste ( String path, dondeReporta d ) {
		
		setError ( ( path.isEmpty() ) ? error.PATH_VACIO : error.SIN_ERROR );
		setLog("Se paso el path:" + path, d);
		if ( _er != error.SIN_ERROR ) {
			return "";
		}
	
		if ( Utilidades.ExisteElArchivo(path) ) {
			setError ( ( Utilidades.borraArchivo(path) ? error.SIN_ERROR : error.NO_SE_PUDO_BORRAR_EL_ARCHIVO ) );
			setLog( "El path " + path + ( ( _er == error.SIN_ERROR ) ? " Se borro " : " No se pudo borrar" ) , dondeReporta.GLOBAL );
		}  else {
			setError( error.SIN_ERROR );
			setLog( " El path " + path + " se creara mas adelante" , dondeReporta.GLOBAL);
		}
		
		return path;
		
	}
	
	private void setVersionSat( String version ) {
		
		if ( version.equals("2") ){
			_versionSat = VersionSat.VERSION_2;
		} else if ( version.equals("3.2") ) {
			_versionSat = VersionSat.VERSION_3_2;
		}
		
		setLog("La version del sat es: " + version, dondeReporta.GLOBAL);
		
	}
	
	private void setError ( error er ) {
		// Si la clase cayo en error, no se puede cambiar este estado.
		_er = er;
		
		_es = ( _es != estado.ER ) ? _er.getEstado() : estado.ER;
		
	}
	
	private void guardaXml ( String cadenaXMl ) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder builder = factory.newDocumentBuilder();
		    Document doc = builder.parse(new InputSource(new StringReader(cadenaXMl)));
	
		    // Write the parsed document to an xml file
		    TransformerFactory transformerFactory = TransformerFactory.newInstance();
		    Transformer transformer = transformerFactory.newTransformer();
		    DOMSource source = new DOMSource(doc);
	
		    StreamResult result =  new StreamResult(new File( paths.get(archivos.XML_DESTINO)));
		    transformer.transform(source, result);
		    setError( error.SIN_ERROR );
		    setLog( "", dondeReporta.DOCUMENTO);
		} catch ( Exception e ){
			setError( error.NO_SE_PUDO_GUARDAR_EL_ARCHIVO_XML_SELLADO );
			setLog( e.toString(), dondeReporta.AMBAS);
		}
	}
	
}