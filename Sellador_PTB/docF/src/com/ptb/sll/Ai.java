package com.ptb.sll;

import java.util.List;
import java.util.Map;

public interface Ai {
	String 				getValor( String llave);
	void 				setValor( String llave, String valor);
	List<String> 		getLlaves();
	Map<String, String> getPares();
}
