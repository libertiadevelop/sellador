package com.ptb.sll;

import java.util.ArrayList;
import java.util.HashMap;

import com.ptb.sll.Utilidades.formatosDefecha;
import com.ptb.sll.con.Tipo;

public class Control implements Error, id, IControl 
{
	
	error 	_er;
	estado 	_es;
	flujo	_fl;
	
	//region ENUMERACIONES
	enum parametros{
		SERVER(0),
		PUERTO(1),
		BASE_DE_DATOS(2),
		USUARIO(3),
		PASSWORD(4),
		XML_DE_ENTRADA(5),
		XML_TIMBRADO(6),
		LOG_SELLADOR(7),
		LOG_DOCUMENTO(8)
		;
		int p;
		private parametros ( int pa ){
			p = pa;
		}
		public String getDescripcionParametro()
		{
			String resultado = "";
			switch (p) {
			case 0:
				resultado = "Server";
				break;
			case 1:
				resultado = "Puerto";
				break;
			case 2:
				resultado = "Base de datos";
				break;
			case 3:
				resultado = "Usuario";
				break;
			case 4:
				resultado = "Password";
				break;
			case 5:
				resultado = "Xml de entrada";
				break;
			case 6:
				resultado = "Xml timbrado";
				break;
			case 7:
				resultado = "Log sellador";
				break;
			case 8:
				resultado = "Log documenot";
				break;
			}
			return resultado;
		}
	}
	
	enum error {
		SIN_ERROR(0),
		PARAMETRO_VACIO(1),
		NO_SE_PASO_PARAMETRO(2),
		NO_SE_PUDO_CONSULTAR_BASE(3),
		XML_DE_ENTRADA_NO_EXISTE(4),
		XML_TIMBRADO_NO_EXISTE5(5),
		LOG_SELLADO_NO_EXISTR(6),
		LOG_DOCUMENTO_NO_EXISTE(7)
		;
		int e;
		private error ( int ie ){
			e = ie;
		}
		public String getDescripcionDelError( String info ) {
			
			String resultado 	= "";
			String addicional 	= "";
			if ( info.trim().length() > 0 ) {
				addicional = " " + info;
			}
			
			
			switch (e) {
			case 0:
				resultado = "";
				break;
			case 1:
				resultado = "El parametro" + addicional + " esta vacio";
				break;
			case 2:
				resultado = "El parametro" + addicional + " es cadena cero";
				break;
			case 3:
				resultado = "Error al consultar la base de datos";
				break;
			case 4:
				resultado = "El archivo de entrada xml " + addicional + ", no existe";
				break;
			case 5:
				resultado = "El archivo xml timbrado " + addicional + ", no existe";
				break;
			case 6:
				resultado = "El archivo log del sellador " + addicional + ", no se encuentra";
				break;
			case 7:
				resultado = "El archivo log del documento " + addicional + ", no se encuentra";
				break;
			}
			return resultado;
		}
		public String getCodigoDeError()
		{
			String resultado = "";
			switch (e) {
			case 0:
				resultado = "106";
				break;
			case 1:
				resultado ="1061";
				break;
			case 2:
				resultado ="1062";
				break;
			case 3:
				resultado = "1063";
				break;
			case 4:
				resultado = "1064";
				break;
			case 5:
				resultado = "1065";
				break;
			case 6:
				resultado = "1066";
				break;
			case 7:
				resultado = "1067";
				break;
			}
			return resultado;
		}
		
	}
	
	private enum flujo {
		INICIO(0)
		;
		int f;
		private flujo ( int fl ){
			f = fl;
		}
		
		public String getDescripcionDeFlujo() {
			return getDesFlujo().trim();
		}
		
		public String getDescripcionDeFLujo ( String mensaje ){
			String resultado = getDesFlujo();
			resultado = ( !mensaje.isEmpty() ) ? resultado + mensaje : resultado.trim() ;
			return resultado;
		}
		
		private String getDesFlujo() {
			String resultado ="";
			switch (f) {
			case 0:
				resultado = "Iniciando";
				break;
			}
			return resultado + ". ";
		}
		
		private String getMetodo() {
			String resultado ="";
			if ( f < 10 ){
				resultado = "Constructo";
			}
			return resultado;
		}
		
	}
	
	private enum estado {
		ER(0),
		OK(1),
		INDEFINIDO(2)
		;
		int v;
		private estado( int va ){
			v = va;
		}
		
		public boolean getValue() {
			boolean resultado = false;
			switch (v) {
			case 0:
				resultado = false;
				break;
			case 1:
				resultado = true;
				break;
			}
			
			return resultado;
		}
		
		public String getStringDescripcion() {
			String resultado ="";
			switch (v) {
			case 0:
				resultado = "Mal";
				break;
			case 1:
				resultado = "Bien";
				break;
			}
			return resultado;
		}
	}
	
	//endregion
	
	public void setRFC_Maestro ( String rfc ){
		RFC_Maestro = rfc;
	}
	public void set_RFC_Emisor ( String rfc ){
		RFC_Emisor = rfc;
	}
	public void setFolio ( String f ){
		folio = f;
	}
	public void setSerie ( String s ){
		serie = s;
	}
	

	public boolean mandaInformacion(String xmlDeEntrada, String xmlTimbrado,
			String error, int seTimbro, String Pac, String tipoDeDocumentoFiscal, String logSellado, String logDocumento) {
			
		boolean resultado = false;
		String espacio = " ";
		
		if ( !estado ) {
			return resultado;
		}
		
		String nombrePc = Utilidades.getPcName();
		String fecha	= Utilidades.getFecha(formatosDefecha.bitacoraTimbres);

		// Recupera los datos de los xml de entrada.
		
		if ( !elRegistroExiste(Pac, tipoDeDocumentoFiscal) ){
			String sentencia = 	"INSERT "															+
							 	"INTO 			timbreb		" 										+
							 				   "(RFC, RFCEmisor, serie, " 							+
							 				   "folio, PAC, tipo,  " 								+
							 				   "PC, xmlEntrada, xmlSellado, "						+
							 				   "error, fecha, logGeneral, "							+
							 				   "logDocumento, documentoTimbrado ) "					+
							 			"VALUES (?,?,?, " +
							 				    "?,?,?, " +
							 				    "?,?,?,	" +
							 				    "?,?,?,	" +
							 				    "?,?)";
			
			ArrayList<String> datosAPasar = new ArrayList<String>();
			datosAPasar.add(RFC_Maestro);
			datosAPasar.add(RFC_Emisor);
			datosAPasar.add(serie);
			datosAPasar.add(folio);
			datosAPasar.add(Pac);
			datosAPasar.add(tipoDeDocumentoFiscal);
			datosAPasar.add(nombrePc);
			datosAPasar.add(xmlDeEntrada);
			datosAPasar.add(xmlTimbrado);
			datosAPasar.add(error);
			datosAPasar.add("now()");
			datosAPasar.add(logSellado);
			datosAPasar.add(logDocumento);
			datosAPasar.add( String.valueOf( seTimbro ) );
			int cont = 1;
			HashMap<Integer, Tipo> columnas = new HashMap<Integer, con.Tipo>();	
			columnas.put(cont++, Tipo.CADENA);		//MAESTRO
			columnas.put(cont++, Tipo.CADENA);		//EMISOR
			columnas.put(cont++, Tipo.CADENA);		//SERIE
			columnas.put(cont++, Tipo.CADENA);		//FOLIO
			columnas.put(cont++, Tipo.CADENA);		//PAC
			columnas.put(cont++, Tipo.CADENA);		//TIPO DE DOCUMENTO FISCAL
			columnas.put(cont++, Tipo.CADENA);		//NOMBRE PC
			columnas.put(cont++, Tipo.BLOB);		// XML DE ENTRAD
			columnas.put(cont++, Tipo.BLOB);		// XML TIMBRADO
			columnas.put(cont++, Tipo.CADENA);		// ERROR
			columnas.put(cont++, Tipo.CADENA);		// FECHA
			columnas.put(cont++, Tipo.BLOB);		// LOG SELLADO
			columnas.put(cont++, Tipo.BLOB);		// LOG DOCUMENTO
			columnas.put(cont++, Tipo.ENTERO);		// SE TIMBRO
			
			int insertados = c.InsertaRegistros(sentencia, datosAPasar, columnas);
			
			if ( insertados == 1 ){
				resultado = true;
			} else {
				resultado = false;
			}
			
		} 
		
		return resultado;
	}	

	public boolean existeRFCMaestro(String RFCMaestro) {
		
		boolean resultado 	= false;
		String Sentencia 	= "SELECT COUNT(*) FROM encabezado WHERE RFC ='" + RFCMaestro + "'";
		String recuperado 	= "";
		
		r.clear();
		r = c.Consulta(Sentencia);
		
		if ( r.size() > 0 ){
			setEstado(true, error.SIN_ERROR.getCodigoDeError(), error.SIN_ERROR.getDescripcionDelError(""), "");
			recuperado = r.get("1").get(0);
			if ( recuperado.equals("1") ){
				resultado = true;
			} else {
				resultado = false;
			}
		} else {
			resultado = false;
			setEstado(false, error.NO_SE_PUDO_CONSULTAR_BASE.getCodigoDeError(), error.NO_SE_PUDO_CONSULTAR_BASE.getDescripcionDelError(""), c.getDesError());
		}
		
		return resultado;
		
	}

	@Override
	public int timbresDisponibles(String RFC) {
		
		String Sentencia = "SELECT ( TimbresComprados + ROUND(TimbresComprados * ( PorcentajeDeGracia / 100 ) ,0) ) - TimbresConsumidos  FROM encabezado WHERE RFC LIKE '" + RFC + "'";
		String resultado = "";
		
		r.clear();
		
		r = c.Consulta(Sentencia);
		
		if ( r.size() > 0 ){
			setEstado(true, error.SIN_ERROR.getCodigoDeError(), error.SIN_ERROR.getDescripcionDelError(""), "");
			resultado = r.get("1").get(0);
		} else {
			setEstado(false, error.NO_SE_PUDO_CONSULTAR_BASE.getCodigoDeError(), error.NO_SE_PUDO_CONSULTAR_BASE.getDescripcionDelError(""), c.getDesError());
		}
		
		int res = 0;
		
		if ( Utilidades.isNumeric(resultado) ) {
			res = Integer.parseInt(resultado);
		}
		
		
		return res;
	}

	@Override
	public HashMap<String, HashMap<String, String>> OrdenDeTimbrado() {
		HashMap<String , HashMap<String, String>> resultado = new HashMap<String, HashMap<String,String>>();
		
		String sentencia = 	"SELECT		 	 prioridad, pac.pac, usuario, " 		+
											"contraseña, cancelacion " 				+	
							"FROM		 	 datosDeAcceso " 						+
								"INNER JOIN  pac ON pac.pac = datosDeAcceso.pac " 	+
							"WHERE			 RFC = '" + RFC_Maestro  + "'";
		
		r.clear();
		
		r = c.Consulta(sentencia);
		
		setEstado(true, error.SIN_ERROR.getCodigoDeError(), error.SIN_ERROR.getDescripcionDelError(""), "");
		
		if ( r.size() > 0 ) {
			
			for ( String clave : r.keySet() ){
				ArrayList<String> 		datos		= new ArrayList<String>();
				HashMap<String, String> aGuardar 	= new HashMap<String, String>();
				
				datos = r.get(clave);
				
				String prioridad 	= datos.get(0);
				String pac			= datos.get(1);
				String usuario		= datos.get(2);
				String contraseña	= datos.get(3);
				String cancelacion	= datos.get(4);
				
				aGuardar.put("pac",		 	pac);
				aGuardar.put("usuario", 	usuario);
				aGuardar.put("contraseña", 	contraseña);
				aGuardar.put("cancelacion",	cancelacion);
				
				
				resultado.put(prioridad, aGuardar);
			}
			
		} else {
			setEstado(false, error.NO_SE_PUDO_CONSULTAR_BASE.getCodigoDeError(), error.NO_SE_PUDO_CONSULTAR_BASE.getDescripcionDelError(""), c.getDesError() );
		}
		
		return resultado;
	}

	public boolean getEstado() {		
		return estado;
	}

	public String getCodError() {
		return codigoDeError;
	}

	public String getDesError() {
		return descripcionDeError;
	}
	
	
	String server;
	String puerto;
	String password;
	String usuario;
	String dataBase;
	
	final String id = "106";
	final String version = "0.0.0.0";
	
	boolean estado;
	String	codigoDeError;
	String	descripcionDeError;
	String	mensaje;
	
	String RFC_Emisor;
	String RFC_Maestro;
	String serie;
	String folio;
	
	
	Conexion c = null;
	
	String Sentencia;
	HashMap<String, ArrayList<String>> r = new HashMap<String, ArrayList<String>>();
	
	//region CONSTRUCTOR
	public Control( String Server, String Puerto, String DataBase, String Usuario, String Password )
	{
		validaParametro(parametros.SERVER, Server);
		validaParametro(parametros.PUERTO, Puerto);
		validaParametro(parametros.BASE_DE_DATOS, DataBase);
		validaParametro(parametros.USUARIO, Usuario);
		validaParametro(parametros.PASSWORD, Password);
		c = new Conexion(Server, Puerto, DataBase, Usuario, Password);
		c.OpenConexion( com.ptb.sll.con.rdbms.MYSQL );
		setEstado(c.getEstado(), c.getCodError(), c.getDesError(), "");
		
	}
	
	//endregion
	
	//region METODOS PUBLICOS
	
	public String getVersion() {
		return version;
	}

	public String getID() {
		return id;
	}
	
	public String getFecha() {
		
		String resultado = "";
		
		Sentencia = "SELECT NOW()";
		r.clear();
		r = c.Consulta(Sentencia);
		
		if ( r.size() > 0 ){
			setEstado(true, error.SIN_ERROR.getCodigoDeError(), error.SIN_ERROR.getDescripcionDelError(""), "");
			resultado = r.get("1").get(0).substring(0,16);
		} else {
			setEstado(false, error.NO_SE_PUDO_CONSULTAR_BASE.getCodigoDeError(), error.NO_SE_PUDO_CONSULTAR_BASE.getDescripcionDelError(""), c.getDesError());
		}
		
		return resultado;
		
	}
	//endregion
	
	//region METODOS PRIVADOS
	public void validaParametro( parametros p, String valor )
	{
		if ( valor == null ){
			setEstado(false, error.NO_SE_PASO_PARAMETRO.getCodigoDeError(), error.NO_SE_PASO_PARAMETRO.getDescripcionDelError(p.getDescripcionParametro()), "");
		} else if ( valor.trim().length() == 0 ) {
			setEstado(false, error.NO_SE_PASO_PARAMETRO.getCodigoDeError(), error.NO_SE_PASO_PARAMETRO.getDescripcionDelError(p.getDescripcionParametro()), "");
		} else {
			setEstado(true, error.SIN_ERROR.getCodigoDeError(), error.SIN_ERROR.getDescripcionDelError(""), "");
		}
		
	}
	
	public void setEstado( boolean Est, String string, String DesError, String Mensaje ) 
	{
		estado 				= Est;
		codigoDeError		= string;
		descripcionDeError	= DesError;
		mensaje				= Mensaje;
	}
	
	//endregion

	private boolean elRegistroExiste ( String PAC, String tipo ){
		boolean resultado = false;
		
		String sentencia = "	SELECT 			COUNT(*) " +
							"	FROM			timbreb " +
							"	WHERE			RFC			= '" + RFC_Maestro + "' AND " +
											"	RFCEmisor   = '" + RFC_Emisor  + "' AND " +
											"	folio		= '" + folio 	  + "' AND " +
											"	serie 		= '" + serie      + "' AND " +
											" 	PAC			= '" + PAC		  + "' AND " +
											"	tipo		= " + tipo;
		
		HashMap <String, ArrayList<String> >r = c.Consulta(sentencia);
		
		if ( r.isEmpty() ) {
			resultado = false;
		} else {
			// Solo se espera un valor de regreso,
			
			ArrayList<String> valores = r.get("1");
			
			if ( valores.isEmpty() ) {
				resultado = false;
			} else {
				String v = valores.get(0);
				if ( v.equals("0") ){
					resultado = false;
				} else {
					resultado = true;
				}
			}
			
		}
		
		
		return resultado;
	}
	
	
}


