package com.ptb.sll;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamSource;

import org.apache.axis.utils.ByteArrayOutputStream;

public class UriResolver implements URIResolver {

	String 		folder 	= "XSL";
	String		archivo = "";
	String		ruta	= "";
	
	public Source resolve(String href, String base) throws TransformerException {

		if ( href == null ){
			return null;
		}
		
		validaFolder();
		
		archivo 	= href.substring(href.lastIndexOf("/") + 1 );
		ruta		= folder + File.separator + archivo;
		
		File pCompleto = new File ( ruta );
		
		boolean descarga = true;
		
		descarga = !(pCompleto.exists());
		
		boolean cargaSource = true;
		
		if ( descarga ){
			cargaSource = descargaArchivo( href );
		}
		
		StreamSource resultado = null;
		
		if ( cargaSource ){
			InputStream in = this.getClass().getClassLoader().getResourceAsStream( ruta );
			resultado = new StreamSource(in);
		} 
		
		
		return resultado;
	}
	
	private void validaFolder(){
		
		File fol = new File( folder );
		
		if ( !fol.isDirectory() ) {
			fol.mkdir();
		} else {
			// El folder ya existe, procede a validar si se encuentra un archivo de texto con la fecha 
			// del dia de hoy. Si el archivo existe, y la fecha no es la del día de hoy.
			// Borra el contenido del folder en caso contrario no hace nada.
			
			
			
			
		}
		
		
		// verifica la fecha de la ultima descarga.
		
		
		
		//TODO: VALIDA FECHA DE LA ULTIMA DESCARGA
		
	}
	
	private boolean descargaArchivo (String ur){
		
		boolean resultado = false;
		
		try {
			URL 					liga 	= new URL (ur );
			InputStream 			in 		= liga.openStream();
			
			ByteArrayOutputStream 	out 	= new ByteArrayOutputStream();
			byte[]					buf		= new byte[1024];
			
			int						n    	= 0;
			
			while ( -1!= (n = in.read(buf)) ) {
				out.write(buf, 0, n);
				out.flush();
			}
			
			out.close();
			in.close();
			
			byte[] dat = out.toByteArray();
			
			FileOutputStream fos = new FileOutputStream(ruta);
			
			fos.write(dat);
			fos.flush();
			fos.close();
			resultado = true;
			
		} catch (MalformedURLException e) {
			resultado = false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			resultado = false;
		}
		
		
		return resultado;
	}

}
