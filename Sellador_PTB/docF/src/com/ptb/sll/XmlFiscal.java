package com.ptb.sll;

import java.io.File;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlFiscal implements Error, id
{
	private String version 	= "0.0.0.0";
	private String ID		= "108";
	
	private boolean 	estado;
	private String		codigoDeError;
	private String		descripcionDeError;
	private String		mensajeError;
	
	private Map<String, Object> DatosEnXml = new HashMap<String, Object>();
	
	private ArrayList<String> 	log 	= new ArrayList<String>();

	private File				xml 	= null;
	
	enum errores 
	{
		SIN_ERROR(0),
		NO_EXISTE_EL_ARCHIVO(1),
		NO_SE_PUDO_CARGAR_EL_XML(2)
		;
		
		int e;
		private errores( int er ){
			e = er;
		}
		
		public String getCodError()
		{
			String resultado = "";
			switch (e) {
			case 0:
				resultado = "000";
				break;
			case 1:
				resultado = "001";
				break;
			case 2: 
				resultado = "002";
				break;
			default:
				break;
			}
			return resultado;
		}
		
		public String getDescripcionDeError ( String file ) {
			String resultado = "";
			switch (e) {
			case 0:
				resultado = "";
				break;
			case 1:
				resultado = "El archivo " + file + " no existe";
			case 2:
				resultado = "No se pudo cargar el archivo " + file;
			default:
				break;
			}
			
			return resultado;
		}
	}
	public String getVersion() {
		return version;
	}

	public String getID() {
		return ID;
	}

	public boolean getEstado() {
		return estado;
	}

	public String getCodError() {
		return codigoDeError;
	}

	public String getDesError() {
		return descripcionDeError;
	}
	
	public String getMensaje () {
		return mensajeError;
	}
	
	public ArrayList<String> getLog(){
		return log;
	}
	
	public XmlFiscal( String rutaAlArchivo )
	{
		setEstado(true, errores.SIN_ERROR.getCodError(), errores.SIN_ERROR.getDescripcionDeError(""), "", "Iniciando");
		
		xml = new File ( rutaAlArchivo );
		
		if ( !xml.exists() ){
			setEstado(false, errores.NO_EXISTE_EL_ARCHIVO.getCodError(), errores.NO_EXISTE_EL_ARCHIVO.getDescripcionDeError( rutaAlArchivo ), "El archivo no existe", "Se intento cargar el archivo " + rutaAlArchivo + " pero no existe");
		} else {
			setEstado(true, errores.SIN_ERROR.getCodError(), errores.SIN_ERROR.getDescripcionDeError(""), "", "Se cargo el archivo " + rutaAlArchivo + " ");
		}
			
	}
	
	public boolean loadXml() {
		boolean resultado = true;

		if ( !estado ){
			mensajeError = "No se puede continuar";
			return false;
		}
		
		setEstado(true, errores.SIN_ERROR.getCodError(), errores.SIN_ERROR.getDescripcionDeError(""), "", "Iniciando carga de archivo xml");
		try {
			DocumentBuilderFactory 	xmlFactory 	= DocumentBuilderFactory.newInstance();
			DocumentBuilder 		dBuilder	= xmlFactory.newDocumentBuilder();
			Document				xSellado	= dBuilder.parse( xml );
			
			xSellado.getDocumentElement().normalize();
			// Recupera el root
			Element r = xSellado.getDocumentElement();
			
			//Comienza a buscar 
			
			recorre( r );
			
			
			setEstado(true, errores.SIN_ERROR.getCodError(), errores.SIN_ERROR.getDescripcionDeError(""), "", "Finalizando carga de archivo xml");
		} catch ( Exception e ){
			setEstado(false, errores.NO_SE_PUDO_CARGAR_EL_XML.getCodError(), errores.NO_SE_PUDO_CARGAR_EL_XML.getDescripcionDeError(xml.getAbsolutePath()),e.toString(), "Finalizando carga de archivo xml");
		}
		
		
		
		
		return resultado;
	}
	
	
	private void setEstado( boolean est, String codError, String desError, String mensaje, String logMovimiento)
	{
		estado 				= est;
		codigoDeError 		= codError;
		descripcionDeError	= desError;
		mensajeError		= mensaje;
		if ( logMovimiento.trim().length() > 0 ){
			log.add(logMovimiento);
		}
	}
	
	private Map<String, Object> recorre( Element N )
	{
		Map<String, Object> resultado = new HashMap<String, Object>();
		
		boolean tieneHijos		= ( N.getChildNodes().getLength() > 0 ) ? true : false; 		
		boolean tieneAtributos  = ( N.getAttributes().getLength() > 0) ? true : false;
		
		String nodeName 		= N.getTagName();
		
		// Recupera todos los pares de atributos.
		
		
		if ( tieneAtributos ){
			Map<String, Object> att = new HashMap<String, Object>();
			for ( int i = 0 ; i < N.getAttributes().getLength() ; i ++ ){
				//String llave = "";
				//String valor = "";
			}
		}
		
		
		return resultado;
	}
	
}
