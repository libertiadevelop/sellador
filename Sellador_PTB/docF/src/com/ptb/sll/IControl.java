package com.ptb.sll;

import java.util.HashMap;

public interface IControl 
{
	boolean mandaInformacion ( String xmlDeEntrada, String xmlTimbrado, String error, int seTimbro, String Pac, String tipoDeDocumentoFiscal, String logSellado, String logDocumento);
	boolean existeRFCMaestro ( String RFCMaestro );
	int		timbresDisponibles ( String RFC );
	HashMap<String, HashMap<String, String>> OrdenDeTimbrado();
	String 	getFecha();
}
