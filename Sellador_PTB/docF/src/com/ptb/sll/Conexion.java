package com.ptb.sll;

import java.util.ArrayList;
import java.util.HashMap;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import com.ptb.sll.PTB.tipoValidacion;


public class Conexion implements Error, id, con  
{
	
	String 		id 			= "103";
	String 		version 	= "0.0.0.0";
	Connection 	con			= null;
	
	boolean		estado;
	Errores		err;
	String		info;
	
	ArrayList<String> log = new ArrayList<String>();
	
	String 		server;
	String 		puerto;
	String		baseDeDatos;
	String		usuario;
	String 		password;
	
	
	public static enum Errores 
	{
		
		SIN_ERRORES(103000),
		DATOS_INCOMPLETOS(103001),
		ERROR_AL_CONECTAR(103002),
		CONEXION_VACIA(103003),
		REGISTROS_VACIOS(103004),
		AL_REALIZAR_CONSULTA(103005),
		AL_INSERTAR_REGISTROS(103006)
		;
		
		private int numError = 0;
		
		private Errores( int ne )
		{
			numError = ne;
		}
		
		public String getDesError ()
		{
			String resultado = "";
			
			switch ( numError )	{
				case 103000:
					resultado = "";
					break;
				case 103001:
					resultado = "Faltan datos, para relizar la conexion.";
					break;
				case 103002:
					resultado = "Error al intentar conectar con la base de datos.";
					break;
				case 103003:
					resultado = "Error la conexion esta vacia";
					break;
				case 103004:
					resultado = "No existen registros";
					break;
				case 103005:
					resultado = "Error generico al consultar";
					break;
				case 103006:
					resultado = "No se pudieron insertar registros";
					break;
			}
			return resultado;
		}
		
		public int getCodError()
		{
			return numError;
		}
		
	}
	
	static enum Tipo 
	{
		CADENA(1), ENTERO(2), DOBLE(3), BLOB(4);
		int t;
		private Tipo ( int tipo )
		{
			t = tipo;
		}
		
		public String getTipo()
		{
			String resultado = "";
			switch (t) {
			case 1:
				resultado = "cadena";
				break;
			case 2:
				resultado = "entero";
				break;
			case 3:
				resultado = "doble";
				break;
			case 4: 
				resultado = "blob";
				break;

			}
			return resultado;
		}
		
	}
	public static enum rdbms
	{
		MYSQL(1);
		
		int db;
		private rdbms( int baseDeDatos )
		{
			db = baseDeDatos;
		}
		public String getConectionString()
		{
			String resultado = "";
			
			switch (db) {
			case 1:
				resultado = "jdbc:mysql://HOST/DATABASE";
				break;
			}
			
			return resultado;
		}
		
	}
	
	public String getVersion() {
		
		return version;
	}

	public String getID() {
		 
		return id;
	}

	public boolean getEstado() {
		
		return estado;
	}

	public String getCodError() {
		return String.valueOf( err.getCodError() );
	}

	public String getDesError() {
		return err.getDesError();
	}

	@Override
	public HashMap<String, ArrayList<String>> Consulta(String Sentencia) {
		
		if ( !estado ) {
			setEstado(false, Errores.CONEXION_VACIA, "No se conecto");
			return null;
		}
		
		HashMap<String, ArrayList<String>> resultadoConsultaVariasColumnas = new HashMap<String, ArrayList<String>>();
		
		ResultSet 			rs = null;
		PreparedStatement	ps = null;
		
		try {
			
			//region REALIZA LA CONSULTA
			ps = con.prepareStatement( Sentencia );
			rs = ps.executeQuery();
			//endregion
			
			//region VALIDA QUE HAYA REGISTROS
			
			if (!rs.isBeforeFirst() ) {    
				 setEstado(false, Errores.REGISTROS_VACIOS, "No se recuperaron registros para: " + Sentencia );
				 return null;
				} 
		
			//endregion
			
			//region RECUPERA LA INFORMACION
			ResultSetMetaData  metadata =  rs.getMetaData();
			
			int numColumnas = metadata.getColumnCount();
			int numRenglon	= 0;
			
			while ( ( rs.next() )  ) {
				numRenglon ++;
				ArrayList<String> columnas = new ArrayList<String>();
				
				for( int i = 1; i <= numColumnas; i++ ){
					columnas.add( rs.getString( i ));
				}
				
				resultadoConsultaVariasColumnas.put( String.valueOf( numRenglon ), columnas );
			}
			 
			rs.close();
			
			setEstado(true, Errores.SIN_ERRORES, "Consulta terminada");
			
			return resultadoConsultaVariasColumnas;
			
			//endregion
			
		} catch (SQLException e) {
			setEstado(false, Errores.AL_REALIZAR_CONSULTA, e.toString() );
		} catch (Exception e) {
			setEstado(false,  Errores.AL_REALIZAR_CONSULTA, e.toString());
		} 
		
		
		return resultadoConsultaVariasColumnas;
		
	}

	@Override
	public int InsertaRegistros(String sentencia, ArrayList<String> campos,
			HashMap<Integer, com.ptb.sll.con.Tipo> TipoDeColumna) {
		
		int registrosInsertados = 0;
		
		if ( !estado ) {
			return 0;
		}
		
		PreparedStatement ps = null;
		
		try {
			
			ps = con.prepareStatement( sentencia );
			
			// Recupera los valores pasados, ( valores a guardar en la base de datos )
			
			HashMap<Integer, String> datos = convierteDeArrayListAMap(campos);			
			
			// Procede a generar la cadena para insertar los datos
			
			for ( int col : TipoDeColumna.keySet() ){
			
				com.ptb.sll.con.Tipo t = TipoDeColumna.get(col) ;
				
				switch (t) {
				case CADENA:
					ps.setString(col,datos.get(col));
					break;
				case DOBLE:
					ps.setDouble(col, Double.parseDouble(datos.get(col)));
					break;
				case ENTERO:
					ps.setInt(col,  Integer.parseInt(datos.get(col)));
					break;
				case BLOB:
					ps.setBlob(col, new FileInputStream(datos.get(col)));				
					break;
				}
			}
			
			registrosInsertados = ps.executeUpdate();
			
			setEstado(true, Errores.SIN_ERRORES, "");
		} catch ( Exception e ){
			setEstado(false, Errores.AL_INSERTAR_REGISTROS , e.toString() );
		}
		
		
		return registrosInsertados;
	}

	@Override
	public int UpdateRegistro(String sentencia, ArrayList<String> campos,
			HashMap<Integer, com.ptb.sll.con.Tipo> TipoDeColumna) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean CloseConexion() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setBatch(Integer numRegistros) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getBatch() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	public Conexion ( String Server, String Puerto, String BaseDeDatos, String Usuario, String Password) 
	{
		
		String mensaje = "";
		
		// Valida que toda la informacion sea, correcta antes de intentar conectar la base de datos
		
		setEstado( true, Errores.SIN_ERRORES, "Validando parametros de conexion");
		
		if ( Server.length() == 0 ) {
			mensaje = "La direccion del server esta vacia.";
			setEstado(false, Errores.DATOS_INCOMPLETOS, mensaje);
		}
		
		if ( Puerto.length() == 0 ){
			mensaje = "La direccion del puerto esta vacia";
			setEstado( false, Errores.DATOS_INCOMPLETOS, mensaje);
		}
		
		if( BaseDeDatos.length() == 0 ) {
			mensaje = "El nombre de la base de datos esta vacio";
			setEstado( false, Errores.DATOS_INCOMPLETOS, mensaje);
		}
		
		if ( Usuario.trim().length() == 0 ) {
			mensaje = "El usuario de la base de datos esta vacio";
			setEstado( false, Errores.DATOS_INCOMPLETOS, mensaje);
		}
		
		if ( Password.trim().length() == 0 ){
			mensaje = "El password de la conexion esta vacio";
			setEstado( false, Errores.DATOS_INCOMPLETOS, mensaje);
		}
		
		if ( estado ){
			setEstado( estado, Errores.SIN_ERRORES, "Terminando validacion de conexiones");
			server 		= Server;
			puerto		= Puerto;
			baseDeDatos	= BaseDeDatos;
			usuario		= Usuario;
			password	= Password;
		} else {
			setEstado (estado, Errores.DATOS_INCOMPLETOS, "Termiando validacion de conexiones");
		}
		
	}		
	
	private void setEstado ( boolean edo, Errores er, String mensaje )
	{
		estado 	= edo;
		err		= er;
		info 	= mensaje;
		log.add(mensaje);
	}

	@Override
	public boolean OpenConexion( con.rdbms mysql) {
		if ( !estado ){
			setEstado( false, Errores.DATOS_INCOMPLETOS, "No se puede realizar la conexion");
		}
		
		
		String CadenaDeConexion = mysql.getConectionString();
		
		String sp 	= server + ":" + puerto;
		
		CadenaDeConexion = CadenaDeConexion.replace("HOST", sp);
		CadenaDeConexion = CadenaDeConexion.replace("DATABASE", baseDeDatos);
		
		setEstado(true, Errores.SIN_ERRORES, "Intentado abrir la base de datos");
		
		try {
		    con = DriverManager.getConnection(CadenaDeConexion +"?" + "user=" + usuario + "&password=" + password);
		    setEstado(true, Errores.SIN_ERRORES, "Base de datos abierta");
		} catch (SQLException ex) {
		   setEstado(false, Errores.ERROR_AL_CONECTAR, ex.getMessage());
		}
		
		return estado;
	}

	HashMap<Integer, String> convierteDeArrayListAMap( ArrayList<String> datos ){
		HashMap<Integer, String> resultado = new HashMap<Integer, String>();
		
		int campo = 0;
		
		for ( String v : datos ){
			campo++;
			
			if ( v.isEmpty() ) {
				v = "0";
			}
			
			resultado.put(campo, v);
		}
		
		
		return resultado;
	}

	
	
	
	
}
