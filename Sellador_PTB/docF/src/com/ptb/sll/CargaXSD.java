package com.ptb.sll;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.ptb.xsd.Documento;

import com.ptb.sll.VersionSat;


public class CargaXSD 
{
	private static String 	error;
	private static boolean estado;
	
	public String getError()
	{
		return error;
	}
	public boolean getEstado()
	{
		return estado;
	}
	
	public static Documento getDatosFrom( String pathToXml ) 
	{
		estado = true;
		try {
			
			JAXBContext jaxbContext = JAXBContext.newInstance("com.ptb.xsd");
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			
			InputStream is = new FileInputStream( new File(pathToXml));
			
			Documento d = (Documento) unmarshaller.unmarshal(is);

			return d;
			
		} catch (JAXBException e) {
			estado = false;
			error = e.toString();
		} catch (FileNotFoundException e) {
			estado	= false;
			error	= e.toString();
		}
		
		return null;
	}
	
	public static Object getDatosFrom ( String pathToXml, VersionSat vs )
	{
		estado = true;
		
		String namespace = "";
		
		Object resultado = null;
		
		switch (vs) {
		case VERSION_2:
			namespace = "com.ptb.xsd.sat.v2";
			break;
		case VERSION_3_2:
			namespace = "com.ptb.xsd.sat.v32";
			break;
		default:
			break;
		}
		try {
			JAXBContext 	jaxbContext 	= JAXBContext.newInstance(namespace);
			Unmarshaller	unmarshaller 	= jaxbContext.createUnmarshaller();
			
			InputStream 	is				= new FileInputStream( new File(pathToXml));
			
			resultado = unmarshaller.unmarshal(is);
		
			return resultado;
		} catch ( JAXBException je ) {
			estado 	= false;
			error	= je.toString();
		} catch (FileNotFoundException fe) {
			estado	= false;
			error	= fe.toString();
		}
		
		return resultado;
		
	}
	
}
