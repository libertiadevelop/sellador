package com.ptb.sll;

import java.util.HashMap;

import com.ptb.sll.DocumentoFiscalFuente.validaciones;

public interface docFisFue {

	void SellaDocumento(String pathCert, String pathKey, String password, String pathXslt);

	String getXmlOriginal();
	String getXmlSellado();
	HashMap<String, Boolean> getResultadoDeLasValidaciones();
	void	valida( validaciones v );
	
}
