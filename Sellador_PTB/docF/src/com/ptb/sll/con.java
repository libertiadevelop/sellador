package com.ptb.sll;

import java.util.ArrayList;
import java.util.HashMap;

public interface con 
{
	static enum Tipo 
	{
		CADENA(1), ENTERO(2), DOBLE(3), BLOB(4);
		int t;
		private Tipo ( int tipo )
		{
			t = tipo;
		}
		
		public String getTipo()
		{
			String resultado = "";
			switch (t) {
			case 1:
				resultado = "cadena";
				break;
			case 2:
				resultado = "entero";
				break;
			case 3:
				resultado = "doble";
				break;
			case 4: 
				resultado = "blob";
				break;

			}
			return resultado;
		}
		
	}
	static enum rdbms
	{
		MYSQL(1);
		
		int db;
		private rdbms( int baseDeDatos )
		{
			db = baseDeDatos;
		}
		public String getConectionString()
		{
			String resultado = "";
			
			switch (db) {
			case 1:
				resultado = "jdbc:mysql://HOST/DATABASE";
				break;
			}
			
			return resultado;
		}
		
	}
	HashMap<String, ArrayList<String>> Consulta ( String Sentencia );
	int InsertaRegistros( String sentencia, ArrayList<String> campos, HashMap<Integer, Tipo> TipoDeColumna ); 
	int  UpdateRegistro(String sentencia, ArrayList<String> campos, HashMap<Integer, Tipo> TipoDeColumna );
	boolean CloseConexion(  ); 
	boolean getEstado( );
	void setBatch ( Integer numRegistros );
	int getBatch ( );
	boolean OpenConexion( con.rdbms mysql);

}
