package com.ptb.sll;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

public class Utilidades {
	
	public static enum tipoDeDocumento{
		FACTURA(1),
		DEVOLUCION(2),
		NOTA_DE_CREDITO(3),
		NOTA_DE_CARGO(4)
		;
		
		int td;
		private tipoDeDocumento( int t ){
			td = t;
		}
		
		public String getTipo(){
			String resultado = "";
			switch (td) {
			case 1:
				resultado = "FACTURA";
				break;
			case 2:
				resultado = "DEVOLUCION";
				break;
			case 3:
				resultado = "NOTA DE CREDITO";
				break;
			case 4:
				resultado = "NOTA DE CARGO";
				break;
			}
			
			return resultado;
		}
		
	}
	
	public static enum procesoFiscal {
		GENERACION(0),
		CANCELACION(1)
		;
		int pf;
		private procesoFiscal( int p ){
			pf = p;
		}
		public String getProceso(){
			String resultado = "";
			switch (pf) {
			case 0:
				resultado = "Generacion";
				break;
			case 1:
				resultado = "Cancelacion";
				break;
			}
			return resultado;
		}
	}
	
	public static enum logLev{
		AVISO, CRITICO,REGISTRO;
	}

	public static enum formatosDefecha{
		bitacoraTimbres
		;
	}
	
	public static String getFecha()
	{
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		return timeStamp;
		
	}
	
	public static String getFecha( formatosDefecha f ){
		String resultado = "";
		switch (f) {
		case bitacoraTimbres:
			resultado = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
			break;
		}
		return resultado;
	}
	
	public static boolean isNumeric( String cadena )
	{
		return cadena.matches("((-|\\+)?[0-9]+(\\.[0-9]+)?)+");
	}

	public static File GetArchivo ( String path ){
		File resultado = null;
		
		if ( ExisteElArchivo( path ) ){
			resultado = new File ( path );
		}
		
		return resultado;
	}
	
	public static boolean ExisteElArchivo ( String Path ){
		boolean resultado = false;
		
		if ( !(Path.trim().length() == 0) ){
			File archivo = new File( Path );
			
			resultado = archivo.exists();	
		}
		
		return resultado;
	}
	
	public static boolean borraArchivo ( String path ){
		boolean resultado = false;
		
		if ( path.trim().isEmpty() ) {
			resultado = false;
		} else {
			File x = null;
			try {
				x = new File( path );
				resultado = x.delete();
			} catch ( Exception e ){
				resultado = false;
			}
		}
		
		return resultado;
	}
	
	public static String getPcName () {
		String nombre = "";
		
		try {
			
			InetAddress addr;
			
			addr 	= InetAddress.getLocalHost();
			nombre 	= addr.getHostName();
			
		} catch ( UnknownHostException un ) {
			nombre = "Desconocido";
		}
			
		return nombre;
	}
}
