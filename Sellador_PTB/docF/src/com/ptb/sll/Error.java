package com.ptb.sll;

public interface Error 
{
	boolean getEstado();
	String 	getCodError();
	String 	getDesError();
	
}
