package com.ptb.sll;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArchivoIni implements Error, Ai, id {

	private String version 	= "0.0.0.0";
	private String id 		= "100";
	
	private		String	pathToFile	= "";
	private		File 	archivoIni 	= null;

	private		boolean estado 				= false;
	private		String	codigoDeError		= "";
	private		String	DescripcionDeError 	= "";

	private 	Map<String, String> datos 	= new HashMap<String, String>();
	
	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public String getID() {
		
		return id;
	}

	@Override
	public String getValor(String llave) {
		getDatosFromFile();
		
		return datos.get(llave);
	}

	@Override
	public void setValor(String llave, String valor) {
		
		// valida que la llave no exista en el archivo
		String cadenaAGuardar = llave.trim() + "=" + valor.trim();
		
		if ( laCadenaExiste(cadenaAGuardar) ){
			estado				= false;
			codigoDeError 		= "111";
			DescripcionDeError 	= "La cadena ya existe";
			return;
		}
		
		try{
			PrintWriter p = new PrintWriter( new BufferedWriter( new FileWriter( archivoIni, true )) );
			String aGuardar = cadenaAGuardar.replaceAll("\n", "\\n");
			p.println( aGuardar );
			p.close();
			estado = true;
		} catch ( Exception sve ){
			estado 				= false;
			codigoDeError		= "102";
			DescripcionDeError	= sve.toString();
		}

	}

	@Override
	public List<String> getLlaves() {
		getDatosFromFile();
		
		List<String> l = new ArrayList<String>( datos.keySet() );
		
		return l;
	}

	@Override
	public Map<String, String> getPares() {
		getDatosFromFile();
		return datos;
	}

	@Override
	public boolean getEstado() {
		return estado;
	}

	@Override
	public String getCodError() {
		return codigoDeError;
	}

	@Override
	public String getDesError() {
		return DescripcionDeError;
	}
	
	public ArchivoIni( String path )
	{
		pathToFile = path;
		if ( ExisteUnoAnterior() ){
			EliminaPrevio( );
		}
		
		archivoIni = new File( pathToFile );
	}
	
	private boolean ExisteUnoAnterior()
	{
		boolean resultado = false;
		
		File fa = new File( pathToFile );
		
		if ( fa.exists() ){
			resultado = true;
		} else {
			resultado = false;
		}
		
		return resultado;
	}
	
	private void EliminaPrevio( )
	{
		File fa = new File ( pathToFile );
		fa.delete();
	}
	
	private boolean laCadenaExiste( String cadena ){
		boolean resultado = false;
		if ( archivoIni.exists() ){
			
			BufferedReader br = null;
			
			try {
			
				List<String> resultadoNumeroDeLineas = new ArrayList<String>();
				
				 br = new BufferedReader( new FileReader( archivoIni ) );
				
				String s;
				
				while ( (s = br.readLine()) != null  ){
					if ( s.trim().length() > 0 ) {
						resultadoNumeroDeLineas.add(s);
					}
				}
				br.close();
				
				if ( resultadoNumeroDeLineas.contains(cadena) ) {
					resultado = true;
				} else {
					resultado = false;
				}
				
			} catch ( Exception lcee ){
				resultado = false;
			} finally {
				if ( br != null ) {
					try {
						br.close();
					} catch (IOException e) { 
						e.printStackTrace();
					}
				}
			}
			
		}
		
		return resultado;
	}
	
	private void getDatosFromFile()
	{
		if ( archivoIni.exists() ){
			
			datos.clear();
			
			BufferedReader br = null;
			
			try {
				
				 br = new BufferedReader( new FileReader( archivoIni ) );
				
				String s;
				
				while ( (s = br.readLine()) != null  ){
					if ( s.trim().length() > 0 ) {
						String llave = s.substring(0, s.indexOf("=") );
						String valor = s.substring(s.indexOf("=") + 1 );
						
						datos.put(llave, valor);
			
					}
				}
				br.close();
				
				estado = true;
				
			} catch ( Exception lcee ){
				estado = false;
			} finally {
				if ( br != null ) {
					try {
						br.close();
					} catch (IOException e) { 
						e.printStackTrace();
					}
				}
			}
			
		}
	}

}
