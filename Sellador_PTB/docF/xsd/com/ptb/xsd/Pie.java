//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.08.31 at 12:08:00 PM CDT 
//


package com.ptb.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}margenes"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "margenes"
})
@XmlRootElement(name = "Pie")
public class Pie {

    @XmlElement(required = true)
    protected Margenes margenes;

    /**
     * Gets the value of the margenes property.
     * 
     * @return
     *     possible object is
     *     {@link Margenes }
     *     
     */
    public Margenes getMargenes() {
        return margenes;
    }

    /**
     * Sets the value of the margenes property.
     * 
     * @param value
     *     allowed object is
     *     {@link Margenes }
     *     
     */
    public void setMargenes(Margenes value) {
        this.margenes = value;
    }

}
