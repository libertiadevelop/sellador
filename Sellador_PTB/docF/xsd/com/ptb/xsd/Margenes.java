//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.08.31 at 12:19:25 PM CDT 
//


package com.ptb.xsd;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}izquierdo"/>
 *         &lt;element ref="{}derecho"/>
 *         &lt;element ref="{}superior"/>
 *         &lt;element ref="{}inferior"/>
 *       &lt;/sequence>
 *       &lt;attribute name="mostrar" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "izquierdo",
    "derecho",
    "superior",
    "inferior"
})
@XmlRootElement(name = "margenes")
public class Margenes {

    @XmlElement(required = true)
    protected BigInteger izquierdo;
    @XmlElement(required = true)
    protected BigInteger derecho;
    @XmlElement(required = true)
    protected BigInteger superior;
    @XmlElement(required = true)
    protected BigInteger inferior;
    @XmlAttribute(name = "mostrar", required = true)
    protected BigInteger mostrar;

    /**
     * Gets the value of the izquierdo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getIzquierdo() {
        return izquierdo;
    }

    /**
     * Sets the value of the izquierdo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setIzquierdo(BigInteger value) {
        this.izquierdo = value;
    }

    /**
     * Gets the value of the derecho property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDerecho() {
        return derecho;
    }

    /**
     * Sets the value of the derecho property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDerecho(BigInteger value) {
        this.derecho = value;
    }

    /**
     * Gets the value of the superior property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSuperior() {
        return superior;
    }

    /**
     * Sets the value of the superior property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSuperior(BigInteger value) {
        this.superior = value;
    }

    /**
     * Gets the value of the inferior property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getInferior() {
        return inferior;
    }

    /**
     * Sets the value of the inferior property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setInferior(BigInteger value) {
        this.inferior = value;
    }

    /**
     * Gets the value of the mostrar property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMostrar() {
        return mostrar;
    }

    /**
     * Sets the value of the mostrar property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMostrar(BigInteger value) {
        this.mostrar = value;
    }

}
