package com.bmx.aud;

import java.util.Timer;
import java.util.TimerTask;

import com.bmx.log.Bitacora;

public class Auditor
{

	/**
	 * @param args
	 */
	
	/* FUNCIONAMIENTO DE ESTE PROGRAMA
	 * 
	 * 1.- RECIVE UN PARAMETRO QUE ES EL ARCHIVO DONDE SE ENCUENTRA LA INFORMACION DE CONFIGURACION. 
	 * 	   Y OTRO DONDE SE PUEDE GUARDAR EL LOG.
	 * 2.- ALGUNOS PARAMETROS, LOS LEE EN LINEA, CADA VEZ QUE SE INVOCA.
	 * 3.- LANZA N CANTIDAD DE TIMERS, LOS CUALES SE DESCRIBEN A CONTINUACION:
	 *	
	 *		A - TIMER PARA VER SI EXISTE ALGUN PROCESO EN COLA, Y LO SUBE. 		
	 *		B - TIMER PARA VALIDAR QUE LA INFORMACION SEA CORRECTA:
	 *				ERP 	VS LOCAL
	 *				LOCAL 	VS REMOTO
	 *		C - TIMER PARA VALIDAR QUE LA INFORMACION EN Y Y/O Z, SE ENCUENTRE EN EL SITIO WEB. 
	 * 		D - VALIDA QUE SE ESTE CANCELANDO ADECUADAMENTE.
	 */
	
	public static Bitacora b = new Bitacora ( );
	
	public static void main ( String[] args ) {
		
		/* El auditor corre de la siguiente forma:
		 * 
		 * 1.- Un auditor que checa diario, cada 5 minutos que los documentos generados en el erp FACTURAS. 
		 *     se encuentren en la base local de mysql y en la base del sitio web. Este auditor envia correos
		 *     electronicos para los usuarios.
		 * 
		 * 
		 * 
		 * 
		 */
		
		// La primera vez que se lanza el proceso se mandan todos consecutivos.
		
		b.Info ( "Iniciando aplicación" );
		
		boolean	primeraVez = true;
		
		Timer 	ERPvsLocalVsRemoto 	= new Timer();		// Diario cada 	30 minutos
//		Timer 	ERPVsLocalMensual	= new Timer();		// Diario cada 	24 horas
//		Timer 	ERPVsLocalCompleto	= new Timer();		// Semanal cada 144 horas 
//		Timer 	FileSystemVsLocal	= new Timer();		// Diario en las noches
//		Timer 	LocaVsRemoto		= new Timer();		// Diario cada 60 minutos
		
		String Clase 	= Auditor.class.getName ( );
		String Metodo 	= new Object(){}.getClass().getEnclosingMethod().getName();

		// Corre cada 5 Minutos
		ERPvsLocalVsRemoto.scheduleAtFixedRate ( new TimerTask( )
		//region TIMER TASK
		{
			public void run ( ) {
				
			}
		//endregion
		}, 1000 * 60 * 5 , 1000 * 60 * 5 );
		
		
		
	}

}
