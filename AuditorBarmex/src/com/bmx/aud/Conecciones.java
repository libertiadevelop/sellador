package com.bmx.aud;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

public class Conecciones
{		
	public static ArrayList<String> error = new ArrayList<String>();
	
	
	public static Connection getERPCon()
	{
		Connection c = null;
		
		String  driver 		= Utilidades.RecuperaRecurso( "odbc_driver" );
		String 	Url			= Utilidades.RecuperaRecurso( "odbc_url" );
		
		// Recupera información de recursos
		try {
			Class.forName( driver );
			c = DriverManager.getConnection( Url ,"system", "manager" );
		} catch ( Exception e ){
			error.clear();
			error.add( "No se pudo generar la coneccion al erp" );
			error.add( e.toString() );
		}

		return c;
	}
	
	public static Connection getMySQLLocalFacturas()
	{
		Connection c = null;
		
		// Recupera información de recursos
		String driver 	= Utilidades.RecuperaRecurso( "bdlf_driver" );
		String Url		= Utilidades.RecuperaRecurso( "bdlf_url" );
		String host		= Utilidades.RecuperaRecurso( "bdlf_host" );
		String puerto	= Utilidades.RecuperaRecurso( "bdlf_puerto" );
		String usuario	= Utilidades.RecuperaRecurso( "bdlf_user" );
		String password	= Utilidades.RecuperaRecurso( "bdlf_password" );
		String base		= Utilidades.RecuperaRecurso( "bdlf_base" );
		
		String URL = Url + host + ":" + puerto + "/" + base;
		
		try{
			Class.forName( driver );
			
			c = DriverManager.getConnection( URL, usuario, password );
			
		} catch ( Exception e ) {
			error.clear();
			error.add( "No se pudo generar la coneccion al erp" );
			error.add( e.toString() );
		}
		
		return c;
	}
	
	public static Connection getMySQLLocalBarpro()
	{
		Connection c = null;
		
		// Recupera información de recursos
		String driver 	= Utilidades.RecuperaRecurso( "bdlb_driver" );
		String Url		= Utilidades.RecuperaRecurso( "bdlb_url" );
		String host		= Utilidades.RecuperaRecurso( "bdlb_host" );
		String puerto	= Utilidades.RecuperaRecurso( "bdlb_puerto" );
		String usuario	= Utilidades.RecuperaRecurso( "bdlb_user" );
		String password	= Utilidades.RecuperaRecurso( "bdlb_password" );
		String base		= Utilidades.RecuperaRecurso( "bdlb_base" );
		
		String URL = Url + host + ":" + puerto + "/" + base;
		
		try{
			Class.forName( driver );
			
			c = DriverManager.getConnection( URL, usuario, password );
			
		} catch ( Exception e ) {
			error.clear();
			error.add( "No se pudo generar la coneccion al erp" );
			error.add( e.toString() );
		}
		
		return c;
	}
	
	public static Connection getMySQLRemoto()
	{
		Connection c = null;
		
		// Recupera información de recursos
		
		String driver 	= Utilidades.RecuperaRecurso( "bdr_driver" );
		String Url		= Utilidades.RecuperaRecurso( "bdr_url" );
		String host		= Utilidades.RecuperaRecurso( "bdr_host" );
		String puerto	= Utilidades.RecuperaRecurso( "bdr_puerto" );
		String usuario	= Utilidades.RecuperaRecurso( "bdr_user" );
		String password	= Utilidades.RecuperaRecurso( "bdr_password" );
		String base		= Utilidades.RecuperaRecurso( "bdr_base" );
		
		String URL = Url + host + ":" + puerto + "/" + base;
		
		try{
			Class.forName( driver );
			
			c = DriverManager.getConnection( URL, usuario, password );
			
		} catch ( Exception e ) {
			error.clear();
			error.add( "No se pudo generar la coneccion al erp" );
			error.add( e.toString() );
		}
		
		
		return c;
	}
	
	public static void Close( Connection c )
	{
		if ( c != null ){
			try {
				c.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
