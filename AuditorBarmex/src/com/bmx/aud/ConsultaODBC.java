package com.bmx.aud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import com.mysql.jdbc.MySQLConnection;


public class ConsultaODBC 
{
	//region VARIABLES PRIVADAS
	boolean 		resultado;
	boolean			continuar;
	
	Connection		con;
	MySQLConnection mCom;
	
	ArrayList<String> 	log		=  new ArrayList<String>();
	String				error;
	//endregion
	
	//region PROPIEDADES
	public boolean getResultado() 
	{
		return resultado;
	}
	
	public String getError()
	{
		return error;
	}
	
	public void setMySQLConn( MySQLConnection c )
	{
		if ( c == null ){
			continuar = false;
		} else {
			mCom = c;
			continuar = true;
		}
		
		resultado = continuar;
	}
	//endregion
	
	//region CONSTRUCTOR
	public ConsultaODBC( Connection c )
	{
		if ( c == null ){
			continuar = false;
		} else {
			con = c;
			continuar = true;
		}
		
		resultado = continuar;
	}
	public ConsultaODBC( MySQLConnection c )
	{
		if ( c == null ){
			continuar = false;
		} else {
			mCom = c;
			continuar = true;
		}
		
		resultado = continuar;
	}
	//endregion
	
	//region METODOS PUBLICOS
	public HashMap<String, ArrayList<String>> ConsultaVariasColumnasODBC( String sentencia ){
		
		if ( !continuar ) {
			error 		= "La conexion esta vacia";
			resultado	= false;
			return null;
		}
		
		HashMap<String, ArrayList<String>> resultadoConsultaVariasColumnas = new HashMap<String, ArrayList<String>>();
		
		ResultSet 			rs = null;
		PreparedStatement	ps = null;
		
		try {
			
			//region REALIZA LA CONSULTA
			ps = con.prepareStatement( sentencia );
			rs = ps.executeQuery();
			//endregion
			
			//region VALIDA QUE HAYA REGISTROS
		
			//endregion
			
			//region RECUPERA LA INFORMACION
			ResultSetMetaData  metadata =  rs.getMetaData();

			int numColumnas = metadata.getColumnCount();
			int numRenglon	= 0;
			
			while ( ( rs.next() )  ) {
				numRenglon ++;
				ArrayList<String> columnas = new ArrayList<String>();
				
				for( int i = 1; i <= numColumnas; i++ ){
					columnas.add( rs.getString( i ));
				}
				
				resultadoConsultaVariasColumnas.put( String.valueOf( numRenglon ), columnas );
			}
			 
			rs.close();
			
			resultado = true;
			
			return resultadoConsultaVariasColumnas;
			
			//endregion
			
		} catch (SQLException e) {
			error = e.toString();
			resultado = false;
		} catch (Exception e) {
			error = e.toString();
			resultado = false;
		} finally {
			
			if ( con != null ){
			
				try {
					con.close();
				} catch ( Exception e ) {
					error = e.toString();
				}
			}
		}
		
		if ( !resultado ) {
			resultadoConsultaVariasColumnas = null;
		}
		
		return resultadoConsultaVariasColumnas;
			
	}
	
	public HashMap<String, ArrayList<String>> ConsultaVariasColumnasMySQL( String sentencia ){
		
		if ( !continuar ) {
			error 		= "La conexion esta vacia";
			resultado	= false;
			return null;
		}
		
		HashMap<String, ArrayList<String>> resultadoConsultaVariasColumnas = new HashMap<String, ArrayList<String>>();
		
		ResultSet 			rs = null;
		PreparedStatement	ps = null;
		
		try {
			
			//region REALIZA LA CONSULTA
			ps = con.prepareStatement( sentencia );
			rs = ps.executeQuery();
			//endregion
			
			//region VALIDA QUE HAYA REGISTROS
			
			
			
			//endregion
			
			//region RECUPERA LA INFORMACION
			ResultSetMetaData  metadata =  rs.getMetaData();

			int numColumnas = metadata.getColumnCount();
			int numRenglon	= 0;
			
			while ( ( rs.next() )  ) {
				numRenglon ++;
				ArrayList<String> columnas = new ArrayList<String>();
				
				for( int i = 1; i <= numColumnas; i++ ){
					columnas.add( rs.getString( i ));
				}
				
				resultadoConsultaVariasColumnas.put( String.valueOf( numRenglon ), columnas );
			}
			 
			rs.close();
			
			resultado = true;
			
			return resultadoConsultaVariasColumnas;
			
			//endregion
			
		} catch (SQLException e) {
			error = e.toString();
			resultado = false;
		} catch (Exception e) {
			error = e.toString();
			resultado = false;
		} finally {
			if ( mCom != null ) {
				try {
					mCom.close();
				} catch ( Exception e ){
					error = e.toString();
				}
			}
		}
		
		if ( !resultado ) {
			resultadoConsultaVariasColumnas = null;
		}
		
		return resultadoConsultaVariasColumnas;
			
	}
	
	public boolean ActualizaRegistroMySQL ( String sentencia ){
		boolean ResultadoActualizaRegistro = false;
		
		if ( !continuar ) {
			error 		= "La conexion esta vacia";
			resultado	= false;
			return false;
		}
		
		PreparedStatement	ps = null;
		
		
		
		try {
			
			//region REALIZA LA actualizacion
			ps = con.prepareStatement( sentencia );
			int Registros = ps.executeUpdate( );
			//endregion
				
			if ( Registros > 0 ) {
				resultado = true;
			} else {
				resultado = false;
			}
	
			ResultadoActualizaRegistro = resultado;
			
			return ResultadoActualizaRegistro;
			
		} catch (SQLException e) {
			error = e.toString();
			resultado = false;
		} catch (Exception e) {
			error = e.toString();
			resultado = false;
		} finally {
			if ( mCom != null ) {
				try {
					mCom.close();
				} catch ( Exception e ){
					error = e.toString();
				}
			}
		}
		
		if ( !resultado ) {
			ResultadoActualizaRegistro = false;
		}
		
		return ResultadoActualizaRegistro;
	}
	
	public boolean InsertaRegistroMySQL ( String sentencia )
	{
		boolean resultadoInsertaRegistrosMySQL = true;
		
		if ( !continuar ){
			error 		= "La conexion esta vacia";
			resultado	= false;
			return false;
		}
		
		PreparedStatement	ps = null;
		
		try {
			
			//region REALIZA LA actualizacion
			ps = con.prepareStatement( sentencia );
			int Registros = ps.executeUpdate( );
			//endregion
				
			if ( Registros > 0 ) {
				resultado = true;
			} else {
				resultado = false;
			}
	
			resultadoInsertaRegistrosMySQL = resultado;
			
			return resultadoInsertaRegistrosMySQL;
			
		} catch (SQLException e) {
			error = e.toString();
			resultado = false;
		} catch (Exception e) {
			error = e.toString();
			resultado = false;
		} finally {
			if ( mCom != null ) {
				try {
					mCom.close();
				} catch ( Exception e ){
					error = e.toString();
				}
			}
		}
		
		return resultado;
	}
	
	//endregion
}
