package com.bmx.aud;

import java.io.File;
import java.util.ArrayList;

public class archivosErp
{
	private ArrayList <String> 	error 		= new ArrayList <String> ( );
	private boolean				resultado;
	
	
	public ArrayList <String> getError ()
	{
		return error;
	}
	public boolean getResultado()
	{
		return resultado;
	}
	
	
	public File[] getFiles ( String ruta )
	{
		File[] resultadoGetFiles = null;
		
		if ( ruta.length ( ) != 0 ){
			
			File carpeta = new File ( ruta );
			
			if ( carpeta.exists ( ) && carpeta.isDirectory ( ) ){
				
				resultadoGetFiles = carpeta.listFiles ( );
				resultado = true;
				
			} else {
				
				error.add ( "No existe la ruta o no es directorio" );
				resultado = false;
				
			}
						
		} else {
			error.add ( "La ruta esta vac�a" );
			resultado = false;
		}
		
		return resultadoGetFiles;
	}
	
}
