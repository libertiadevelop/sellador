package com.bmx.log;

import java.io.File;
import org.apache.log4j.*;

public class Bitacora
{
	final Logger log = Logger.getRootLogger ( );
	public Bitacora() 
	{
		// Crea el folder en caso de no existir.
		
		boolean validaExistenciaDeFolder = false;
		
		File f = new File( "log" );

		validaExistenciaDeFolder = ( f.exists ( ) & f.isDirectory ( ) );
		
		if ( !validaExistenciaDeFolder ){
			// Crea el directorio.
			f.mkdir ( );
		}
	}

	public void Warning( String Clase, String Metodo, String linea, String mensaje )
	{
		log.warn ( "Clase: " + Clase + "(Metodo: " + Metodo + ":" + linea + ") " + mensaje );
	}
	public void Error ( String Clase, String Metodo,  String linea, String mensaje )
	{
		log.error (  "Clase: " + Clase + "(Metodo: " + Metodo + ":" + linea + ") " + mensaje );
	}
	public void Fatal ( String Clase, String Metodo,  String linea, String mensaje )
	{
		log.fatal (  "Clase: " + Clase + "(Metodo: " + Metodo + ":" + linea + ") " + mensaje );
	}
	public void Info ( String mensaje )
	{
		log.info ( mensaje );
	}
	public void Debug ( String Clase, String Metodo,  String linea, String mensaje )
	{
		log.debug (  "Clase: " + Clase + "(Metodo: " + Metodo + ":" + linea + ") " + mensaje );
	}
}
