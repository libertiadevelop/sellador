package com.bmx.log;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

public class BitacoraTest
{
	final String directorio = "log";
	final String logArchivo	= "log.t";
	@Test
	public void testConstructor ( ) 
	{
		// Si existe el directorio lo elimina antes de empezar las pruebas.
		EliminaDirectorio ( directorio );
		
		File f = new File( directorio );
		
		boolean validaExistenciaFolder = (f.exists ( ) & f.isDirectory ( ) );
		
		assertFalse ( "No debe existir el directorio", validaExistenciaFolder );
		
		@SuppressWarnings ( "unused" )
		Bitacora b = new Bitacora ( );
		
		validaExistenciaFolder = (f.exists ( ) & f.isDirectory ( ) );
		
		assertTrue ( "Se debio crear el directorio", validaExistenciaFolder );
		
		EliminaDirectorio ( directorio );
		
	}
	@Test
	public void testWarning ( )
	{
		
		String nombreClase 	= this.getClass ( ).getName ( );
		Throwable t = new Throwable ( );
		
		String nombreMetodo	= t.getStackTrace ( )[0].getMethodName ( );
		String lineaDeCodigo = ""; 
		EliminaDirectorio ( directorio );
		
		Bitacora b = new Bitacora ( );
		lineaDeCodigo = String.valueOf ( t.getStackTrace ( )[0].getLineNumber ( ) );
		b.Warning ( nombreClase, nombreMetodo, lineaDeCodigo, "Mensaje de prueba" );
		
		
		File f = new File ( directorio + "/" + logArchivo );
		
		assertTrue ( "El archivo se debio de crear" , f.exists ( ) );
		
		EliminaDirectorio ( directorio );
	}
	@Test
	public void testMensajes ( )
	{
		String nombreClase 	= this.getClass ( ).getName ( );
		Throwable t = new Throwable ( );

		String nombreMetodo	= t.getStackTrace ( )[0].getMethodName ( );
		String lineaDeCodigo = "";
		EliminaDirectorio ( directorio );
		
		Bitacora b = new Bitacora ( );
		lineaDeCodigo = String.valueOf ( t.getStackTrace ( )[0].getLineNumber ( ) );
		b.Warning 	( nombreClase, nombreMetodo, lineaDeCodigo, "Mensaje de prueba" );
		lineaDeCodigo = String.valueOf ( t.getStackTrace ( )[0].getLineNumber ( ) );
		b.Error 	( nombreClase, nombreMetodo, lineaDeCodigo, "Mensaje de prueba" );
		lineaDeCodigo = String.valueOf ( t.getStackTrace ( )[0].getLineNumber ( ) );
		b.Fatal 	( nombreClase, nombreMetodo, lineaDeCodigo, "Mensaje de prueba" );
		b.Info 		( "Mensaje de prueba" );
		lineaDeCodigo = String.valueOf ( t.getStackTrace ( )[0].getLineNumber ( ) );
		b.Debug 	( nombreClase, nombreMetodo, lineaDeCodigo, "Mensaje de prueba" );
		
	}
	
	private void EliminaDirectorio( String directorio )
	{
		File f = new File( directorio );
		
		if ( f.exists ( ) ){
			File[] hijos = f.listFiles ( );
			
			if ( hijos.length > 0 ){
				for ( File hijo : hijos ) {
					hijo.delete ( );
				}
				f.delete ( );
			} else {
				f.delete ( );
			}
			
		}
	}
}
